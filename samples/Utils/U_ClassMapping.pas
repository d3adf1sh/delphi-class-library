//=============================================================================
// U_ClassMapping
// Class Library Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ClassMapping;

interface

uses
  Classes,
  U_Value,
  U_OPFClassMapping,
  U_DTO;

type
  // TRLSamplesClassMappingPopulator
  TRLSamplesClassMappingPopulator = class(TRLOPFClassMappingPopulator)
  protected
    procedure RegisterPerfil(const AClassMapping: TRLOPFClassMapping); virtual;
    procedure RegisterRelPerfilUsuario(const AClassMapping: TRLOPFClassMapping); virtual;
    procedure RegisterUsuario(const AClassMapping: TRLOPFClassMapping); virtual;
  public
    procedure Populate(const AClassMapping: TRLOPFClassMapping); override;
  end;

implementation

{ TRLSamplesClassMappingPopulator }

procedure TRLSamplesClassMappingPopulator.RegisterPerfil(
  const AClassMapping: TRLOPFClassMapping);
begin
  // dbo.Perfil
  AClassMapping.RegisterClass(TRLPerfilDTO, 'dbo', 'Perfil');
  AClassMapping.RegisterAttribute(TRLPerfilDTO, 'ID', 'ID', vtInteger, True, True);
  AClassMapping.RegisterAttribute(TRLPerfilDTO, 'Nome', 'Nome', vtString, False, False);
end;

procedure TRLSamplesClassMappingPopulator.RegisterRelPerfilUsuario(
  const AClassMapping: TRLOPFClassMapping);
begin
  // dbo.RelPerfilUsuario
  AClassMapping.RegisterClass(TRLRelPerfilUsuarioDTO, 'dbo', 'RelPerfilUsuario');
  AClassMapping.RegisterAttribute(TRLRelPerfilUsuarioDTO, 'ID', 'ID', vtInteger, True, True);
  AClassMapping.RegisterAttribute(TRLRelPerfilUsuarioDTO, 'ID_Perfil', 'ID_Perfil', vtInteger, False, False);
  AClassMapping.RegisterAttribute(TRLRelPerfilUsuarioDTO, 'ID_Usuario', 'ID_Usuario', vtInteger, False, False);
end;

procedure TRLSamplesClassMappingPopulator.RegisterUsuario(
  const AClassMapping: TRLOPFClassMapping);
begin
  // dbo.Usuario
  AClassMapping.RegisterClass(TRLUsuarioDTO, 'dbo', 'Usuario');
  AClassMapping.RegisterAttribute(TRLUsuarioDTO, 'ID', 'ID', vtInteger, True, True);
  AClassMapping.RegisterAttribute(TRLUsuarioDTO, 'Nome', 'Nome', vtString, False, False);
  AClassMapping.RegisterAttribute(TRLUsuarioDTO, 'Apelido', 'Apelido', vtString, False, False);
  AClassMapping.RegisterAttribute(TRLUsuarioDTO, 'Senha', 'Senha', vtString, False, False);
  AClassMapping.RegisterAttribute(TRLUsuarioDTO, 'Inativo', 'Inativo', vtBoolean, False, False);
end;

procedure TRLSamplesClassMappingPopulator.Populate(
  const AClassMapping: TRLOPFClassMapping);
begin
  RegisterPerfil(AClassMapping);
  RegisterRelPerfilUsuario(AClassMapping);
  RegisterUsuario(AClassMapping);
end;

initialization
  RegisterClass(TRLSamplesClassMappingPopulator);

end.