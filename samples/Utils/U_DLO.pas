//=============================================================================
// U_DLO
// Class Library Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DLO;

interface

uses
  SysUtils,
  U_Connections,
  U_OPFCommand,
  U_OPFDPO,
  U_DPO;

type
  // TRLPerfilDLO
  TRLPerfilDLO = class(TRLOPFCommand)
  private
    FPerfilDPO: TRLPerfilDPO;
  protected
    procedure ProviderChanged; override;
    procedure DatabaseChanged; override;
    procedure ClassMappingChanged; override;
    function CreatePerfilDPO: TRLPerfilDPO; virtual;
    property PerfilDPO: TRLPerfilDPO read FPerfilDPO;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  // TRLRelPerfilUsuarioDLO
  TRLRelPerfilUsuarioDLO = class(TRLOPFCommand)
  private
    FRelPerfilUsuarioDPO: TRLRelPerfilUsuarioDPO;
  protected
    procedure ProviderChanged; override;
    procedure DatabaseChanged; override;
    procedure ClassMappingChanged; override;
    function CreateRelPerfilUsuarioDPO: TRLRelPerfilUsuarioDPO; virtual;
    property RelPerfilUsuarioDPO: TRLRelPerfilUsuarioDPO read FRelPerfilUsuarioDPO;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  // TRLUsuarioDLO
  TRLUsuarioDLO = class(TRLOPFCommand)
  private
    FUsuarioDPO: TRLUsuarioDPO;
  protected
    procedure ProviderChanged; override;
    procedure DatabaseChanged; override;
    procedure ClassMappingChanged; override;
    function CreateUsuarioDPO: TRLUsuarioDPO; virtual;
    property UsuarioDPO: TRLUsuarioDPO read FUsuarioDPO;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

{ TRLPerfilDLO }

constructor TRLPerfilDLO.Create;
var
  lConnection: TRLConnection;
begin
  inherited;
  FPerfilDPO := CreatePerfilDPO;
  lConnection := TRLConnections.Instance.FindByName('Samples');
  Provider := lConnection.Provider;
  Database := lConnection.Database;
  ClassMapping := lConnection.ClassMapping;
end;

destructor TRLPerfilDLO.Destroy;
begin
  FreeAndNil(FPerfilDPO);
  inherited;
end;

procedure TRLPerfilDLO.ProviderChanged;
begin
  FPerfilDPO.Provider := Provider;
end;

procedure TRLPerfilDLO.DatabaseChanged;
begin
  FPerfilDPO.Database := Database;
end;

procedure TRLPerfilDLO.ClassMappingChanged;
begin
  FPerfilDPO.ClassMapping := ClassMapping;
end;

function TRLPerfilDLO.CreatePerfilDPO: TRLPerfilDPO;
begin
  Result := TRLPerfilDPO.Create;
end;

{ TRLRelPerfilUsuarioDLO }

constructor TRLRelPerfilUsuarioDLO.Create;
var
  lConnection: TRLConnection;
begin
  inherited;
  FRelPerfilUsuarioDPO := CreateRelPerfilUsuarioDPO;
  lConnection := TRLConnections.Instance.FindByName('Samples');
  Provider := lConnection.Provider;
  Database := lConnection.Database;
  ClassMapping := lConnection.ClassMapping;
end;

destructor TRLRelPerfilUsuarioDLO.Destroy;
begin
  FreeAndNil(FRelPerfilUsuarioDPO);
  inherited;
end;

procedure TRLRelPerfilUsuarioDLO.ProviderChanged;
begin
  FRelPerfilUsuarioDPO.Provider := Provider;
end;

procedure TRLRelPerfilUsuarioDLO.DatabaseChanged;
begin
  FRelPerfilUsuarioDPO.Database := Database;
end;

procedure TRLRelPerfilUsuarioDLO.ClassMappingChanged;
begin
  FRelPerfilUsuarioDPO.ClassMapping := ClassMapping;
end;

function TRLRelPerfilUsuarioDLO.CreateRelPerfilUsuarioDPO: TRLRelPerfilUsuarioDPO;
begin
  Result := TRLRelPerfilUsuarioDPO.Create;
end;

{ TRLUsuarioDLO }

constructor TRLUsuarioDLO.Create;
var
  lConnection: TRLConnection;
begin
  inherited;
  FUsuarioDPO := CreateUsuarioDPO;
  lConnection := TRLConnections.Instance.FindByName('Samples');
  Provider := lConnection.Provider;
  Database := lConnection.Database;
  ClassMapping := lConnection.ClassMapping;
end;

destructor TRLUsuarioDLO.Destroy;
begin
  FreeAndNil(FUsuarioDPO);
  inherited;
end;

procedure TRLUsuarioDLO.ProviderChanged;
begin
  FUsuarioDPO.Provider := Provider;
end;

procedure TRLUsuarioDLO.DatabaseChanged;
begin
  FUsuarioDPO.Database := Database;
end;

procedure TRLUsuarioDLO.ClassMappingChanged;
begin
  FUsuarioDPO.ClassMapping := ClassMapping;
end;

function TRLUsuarioDLO.CreateUsuarioDPO: TRLUsuarioDPO;
begin
  Result := TRLUsuarioDPO.Create;
end;

end.