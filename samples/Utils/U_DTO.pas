//=============================================================================
// U_DTO
// Class Library Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DTO;

interface

uses
  SysUtils,
  Classes,
  Controls,
  U_Collection,
  U_KeyedCollection,
  U_Value;

type
  // TRLPerfilDTO
  TRLPerfilDTO = class(TRLKeyedCollectionItem)
  private
    FID: TRLInteger;
    FNome: TRLString;
    function GetID: TRLInteger;
    function GetNome: TRLString;
    procedure KeyValueChanged(Sender: TObject);
  protected
    function MakeKey: string; override;
    function CreateID: TRLInteger; virtual;
    function CreateNome: TRLString; virtual;
  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property ID: TRLInteger read GetID;
    property Nome: TRLString read GetNome;
  end;

  // TRLListOfPerfilDTO
  TRLListOfPerfilDTO = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLPerfilDTO;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function MakeKey(const AID: Integer): string; virtual;
  public
    constructor CreateCollection(const AParent: TObject); override;
    function Add: TRLPerfilDTO;
    function Insert(const AIndex: Integer): TRLPerfilDTO;
    function Find(const AID: Integer): TRLPerfilDTO;
    function Contains(const AID: Integer): Boolean; reintroduce; virtual;
    property Items[const AIndex: Integer]: TRLPerfilDTO read GetItem; default;
  end;

  // TRLRelPerfilUsuarioDTO
  TRLRelPerfilUsuarioDTO = class(TRLKeyedCollectionItem)
  private
    FID: TRLInteger;
    FID_Perfil: TRLInteger;
    FID_Usuario: TRLInteger;
    function GetID: TRLInteger;
    function GetID_Perfil: TRLInteger;
    function GetID_Usuario: TRLInteger;
    procedure KeyValueChanged(Sender: TObject);
    procedure AlternativeKeyValueChanged(Sender: TObject);
  protected
    function MakeKey: string; override;
    function MakeAlternativeKey: string; override;
    function CreateID: TRLInteger; virtual;
    function CreateID_Perfil: TRLInteger; virtual;
    function CreateID_Usuario: TRLInteger; virtual;
  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property ID: TRLInteger read GetID;
    property ID_Perfil: TRLInteger read GetID_Perfil;
    property ID_Usuario: TRLInteger read GetID_Usuario;
  end;

  // TRLListOfRelPerfilUsuarioDTO
  TRLListOfRelPerfilUsuarioDTO = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLRelPerfilUsuarioDTO;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function MakeKey(const AID: Integer): string; virtual;
    function MakeAlternativeKey(const AID_Perfil: Integer; const AID_Usuario: Integer): string; virtual;
  public
    constructor CreateCollection(const AParent: TObject); override;
    function Add: TRLRelPerfilUsuarioDTO;
    function Insert(const AIndex: Integer): TRLRelPerfilUsuarioDTO;
    function Find(const AID: Integer): TRLRelPerfilUsuarioDTO;
    function Contains(const AID: Integer): Boolean; reintroduce; virtual;
    function FindRelPerfilUsuario(const AID_Perfil: Integer; const AID_Usuario: Integer): TRLRelPerfilUsuarioDTO;
    function ContainsRelPerfilUsuario(const AID_Perfil: Integer; const AID_Usuario: Integer): Boolean; virtual;
    property Items[const AIndex: Integer]: TRLRelPerfilUsuarioDTO read GetItem; default;
  end;

  // TRLUsuarioDTO
  TRLUsuarioDTO = class(TRLKeyedCollectionItem)
  private
    FID: TRLInteger;
    FNome: TRLString;
    FApelido: TRLString;
    FSenha: TRLString;
    FInativo: TRLBoolean;
    function GetID: TRLInteger;
    function GetNome: TRLString;
    function GetApelido: TRLString;
    function GetSenha: TRLString;
    function GetInativo: TRLBoolean;
    procedure KeyValueChanged(Sender: TObject);
    procedure AlternativeKeyValueChanged(Sender: TObject);
  protected
    function MakeKey: string; override;
    function MakeAlternativeKey: string; override;
    function CreateID: TRLInteger; virtual;
    function CreateNome: TRLString; virtual;
    function CreateApelido: TRLString; virtual;
    function CreateSenha: TRLString; virtual;
    function CreateInativo: TRLBoolean; virtual;
  public
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property ID: TRLInteger read GetID;
    property Nome: TRLString read GetNome;
    property Apelido: TRLString read GetApelido;
    property Senha: TRLString read GetSenha;
    property Inativo: TRLBoolean read GetInativo;
  end;

  // TRLListOfUsuarioDTO
  TRLListOfUsuarioDTO = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLUsuarioDTO;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function MakeKey(const AID: Integer): string; virtual;
    function MakeAlternativeKey(const AApelido: string): string; virtual;
  public
    constructor CreateCollection(const AParent: TObject); override;
    function Add: TRLUsuarioDTO;
    function Insert(const AIndex: Integer): TRLUsuarioDTO;
    function Find(const AID: Integer): TRLUsuarioDTO;
    function Contains(const AID: Integer): Boolean; reintroduce; virtual;
    function FindApelido(const AApelido: string): TRLUsuarioDTO;
    function ContainsApelido(const AApelido: string): Boolean; virtual;
    property Items[const AIndex: Integer]: TRLUsuarioDTO read GetItem; default;
  end;

var
  _HSPerfil: Integer = 0;
  _HSRelPerfilUsuario: Integer = 0;
  _HSUsuario: Integer = 0;

implementation

{ TRLPerfilDTO }

destructor TRLPerfilDTO.Destroy;
begin
  if Assigned(FNome) then
    FreeAndNil(FNome);
  if Assigned(FID) then
    FreeAndNil(FID);
  inherited;
end;

function TRLPerfilDTO.GetID: TRLInteger;
begin
  if not Assigned(FID) then
    FID := CreateID;
  Result := FID;
end;

function TRLPerfilDTO.GetNome: TRLString;
begin
  if not Assigned(FNome) then
    FNome := CreateNome;
  Result := FNome;
end;

procedure TRLPerfilDTO.KeyValueChanged(Sender: TObject);
begin
  UpdateKey;
end;

function TRLPerfilDTO.MakeKey: string;
begin
  Result := ID.AsString;
end;

function TRLPerfilDTO.CreateID: TRLInteger;
begin
  Result := TRLInteger.Create;
  Result.OnChange := KeyValueChanged;
end;

function TRLPerfilDTO.CreateNome: TRLString;
begin
  Result := TRLString.Create;
end;

procedure TRLPerfilDTO.Assign(Source: TPersistent);
var
  lPerfil: TRLPerfilDTO;
begin
  if Source is TRLPerfilDTO then
  begin
    lPerfil := TRLPerfilDTO(Source);
    ID.Assign(lPerfil.ID);
    Nome.Assign(lPerfil.Nome);
  end;
end;

{ TRLListOfPerfilDTO }

constructor TRLListOfPerfilDTO.CreateCollection(const AParent: TObject);
begin
  inherited;
  if _HSPerfil > 0 then
    HashSize := _HSPerfil;
end;

function TRLListOfPerfilDTO.GetItem(const AIndex: Integer): TRLPerfilDTO;
begin
  Result := TRLPerfilDTO(inherited Items[AIndex]);
end;

function TRLListOfPerfilDTO.GetItemClass: TCollectionItemClass;
begin
  Result := TRLPerfilDTO;
end;

function TRLListOfPerfilDTO.MakeKey(const AID: Integer): string;
begin
  Result := IntToStr(AID);
end;

function TRLListOfPerfilDTO.Add: TRLPerfilDTO;
begin
  Result := TRLPerfilDTO(inherited Add);
end;

function TRLListOfPerfilDTO.Insert(const AIndex: Integer): TRLPerfilDTO;
begin
  Result := TRLPerfilDTO(inherited Insert(AIndex));
end;

function TRLListOfPerfilDTO.Find(const AID: Integer): TRLPerfilDTO;
begin
  Result := TRLPerfilDTO(FindHash(MakeKey(AID)));
end;

function TRLListOfPerfilDTO.Contains(const AID: Integer): Boolean;
begin
  Result := Assigned(Find(AID));
end;

{ TRLRelPerfilUsuarioDTO }

destructor TRLRelPerfilUsuarioDTO.Destroy;
begin
  if Assigned(FID_Usuario) then
    FreeAndNil(FID_Usuario);
  if Assigned(FID_Perfil) then
    FreeAndNil(FID_Perfil);
  if Assigned(FID) then
    FreeAndNil(FID);
  inherited;
end;

function TRLRelPerfilUsuarioDTO.GetID: TRLInteger;
begin
  if not Assigned(FID) then
    FID := CreateID;
  Result := FID;
end;

function TRLRelPerfilUsuarioDTO.GetID_Perfil: TRLInteger;
begin
  if not Assigned(FID_Perfil) then
    FID_Perfil := CreateID_Perfil;
  Result := FID_Perfil;
end;

function TRLRelPerfilUsuarioDTO.GetID_Usuario: TRLInteger;
begin
  if not Assigned(FID_Usuario) then
    FID_Usuario := CreateID_Usuario;
  Result := FID_Usuario;
end;

procedure TRLRelPerfilUsuarioDTO.KeyValueChanged(Sender: TObject);
begin
  UpdateKey;
end;

function TRLRelPerfilUsuarioDTO.MakeKey: string;
begin
  Result := ID.AsString;
end;

procedure TRLRelPerfilUsuarioDTO.AlternativeKeyValueChanged(Sender: TObject);
begin
  UpdateAlternativeKey;
end;

function TRLRelPerfilUsuarioDTO.MakeAlternativeKey: string;
begin
  Result := ID_Perfil.AsString + ';' + ID_Usuario.AsString;
end;

function TRLRelPerfilUsuarioDTO.CreateID: TRLInteger;
begin
  Result := TRLInteger.Create;
  Result.OnChange := KeyValueChanged;
end;

function TRLRelPerfilUsuarioDTO.CreateID_Perfil: TRLInteger;
begin
  Result := TRLInteger.Create;
  Result.OnChange := AlternativeKeyValueChanged;
end;

function TRLRelPerfilUsuarioDTO.CreateID_Usuario: TRLInteger;
begin
  Result := TRLInteger.Create;
  Result.OnChange := AlternativeKeyValueChanged;
end;

procedure TRLRelPerfilUsuarioDTO.Assign(Source: TPersistent);
var
  lRelPerfilUsuario: TRLRelPerfilUsuarioDTO;
begin
  if Source is TRLRelPerfilUsuarioDTO then
  begin
    lRelPerfilUsuario := TRLRelPerfilUsuarioDTO(Source);
    ID.Assign(lRelPerfilUsuario.ID);
    ID_Perfil.Assign(lRelPerfilUsuario.ID_Perfil);
    ID_Usuario.Assign(lRelPerfilUsuario.ID_Usuario);
  end;
end;

{ TRLListOfRelPerfilUsuarioDTO }

constructor TRLListOfRelPerfilUsuarioDTO.CreateCollection(
  const AParent: TObject);
begin
  inherited;
  if _HSRelPerfilUsuario > 0 then
    HashSize := _HSRelPerfilUsuario;
end;

function TRLListOfRelPerfilUsuarioDTO.GetItem(
  const AIndex: Integer): TRLRelPerfilUsuarioDTO;
begin
  Result := TRLRelPerfilUsuarioDTO(inherited Items[AIndex]);
end;

function TRLListOfRelPerfilUsuarioDTO.GetItemClass: TCollectionItemClass;
begin
  Result := TRLRelPerfilUsuarioDTO;
end;

function TRLListOfRelPerfilUsuarioDTO.MakeKey(const AID: Integer): string;
begin
  Result := IntToStr(AID);
end;

function TRLListOfRelPerfilUsuarioDTO.MakeAlternativeKey(
  const AID_Perfil: Integer; const AID_Usuario: Integer): string;
begin
  Result := IntToStr(AID_Perfil) + ';' + IntToStr(AID_Usuario);
end;

function TRLListOfRelPerfilUsuarioDTO.Add: TRLRelPerfilUsuarioDTO;
begin
  Result := TRLRelPerfilUsuarioDTO(inherited Add);
end;

function TRLListOfRelPerfilUsuarioDTO.Insert(
  const AIndex: Integer): TRLRelPerfilUsuarioDTO;
begin
  Result := TRLRelPerfilUsuarioDTO(inherited Insert(AIndex));
end;

function TRLListOfRelPerfilUsuarioDTO.Find(
  const AID: Integer): TRLRelPerfilUsuarioDTO;
begin
  Result := TRLRelPerfilUsuarioDTO(FindHash(MakeKey(AID)));
end;

function TRLListOfRelPerfilUsuarioDTO.Contains(const AID: Integer): Boolean;
begin
  Result := Assigned(Find(AID));
end;

function TRLListOfRelPerfilUsuarioDTO.FindRelPerfilUsuario(
  const AID_Perfil: Integer; 
  const AID_Usuario: Integer): TRLRelPerfilUsuarioDTO;
begin
  Result := TRLRelPerfilUsuarioDTO(FindAlternativeHash(
    MakeAlternativeKey(AID_Perfil, AID_Usuario)));
end;

function TRLListOfRelPerfilUsuarioDTO.ContainsRelPerfilUsuario(
  const AID_Perfil: Integer; const AID_Usuario: Integer): Boolean;
begin
  Result := Assigned(FindRelPerfilUsuario(AID_Perfil, AID_Usuario));
end;

{ TRLUsuarioDTO }

destructor TRLUsuarioDTO.Destroy;
begin
  if Assigned(FInativo) then
    FreeAndNil(FInativo);
  if Assigned(FSenha) then
    FreeAndNil(FSenha);
  if Assigned(FApelido) then
    FreeAndNil(FApelido);
  if Assigned(FNome) then
    FreeAndNil(FNome);
  if Assigned(FID) then
    FreeAndNil(FID);
  inherited;
end;

function TRLUsuarioDTO.GetID: TRLInteger;
begin
  if not Assigned(FID) then
    FID := CreateID;
  Result := FID;
end;

function TRLUsuarioDTO.GetNome: TRLString;
begin
  if not Assigned(FNome) then
    FNome := CreateNome;
  Result := FNome;
end;

function TRLUsuarioDTO.GetApelido: TRLString;
begin
  if not Assigned(FApelido) then
    FApelido := CreateApelido;
  Result := FApelido;
end;

function TRLUsuarioDTO.GetSenha: TRLString;
begin
  if not Assigned(FSenha) then
    FSenha := CreateSenha;
  Result := FSenha;
end;

function TRLUsuarioDTO.GetInativo: TRLBoolean;
begin
  if not Assigned(FInativo) then
    FInativo := CreateInativo;
  Result := FInativo;
end;

procedure TRLUsuarioDTO.KeyValueChanged(Sender: TObject);
begin
  UpdateKey;
end;

function TRLUsuarioDTO.MakeKey: string;
begin
  Result := ID.AsString;
end;

procedure TRLUsuarioDTO.AlternativeKeyValueChanged(Sender: TObject);
begin
  UpdateAlternativeKey;
end;

function TRLUsuarioDTO.MakeAlternativeKey: string;
begin
  Result := Apelido.AsString;
end;

function TRLUsuarioDTO.CreateID: TRLInteger;
begin
  Result := TRLInteger.Create;
  Result.OnChange := KeyValueChanged;
end;

function TRLUsuarioDTO.CreateNome: TRLString;
begin
  Result := TRLString.Create;
end;

function TRLUsuarioDTO.CreateApelido: TRLString;
begin
  Result := TRLString.Create;
  Result.OnChange := AlternativeKeyValueChanged;
end;

function TRLUsuarioDTO.CreateSenha: TRLString;
begin
  Result := TRLString.Create;
end;

function TRLUsuarioDTO.CreateInativo: TRLBoolean;
begin
  Result := TRLBoolean.Create;
end;

procedure TRLUsuarioDTO.Assign(Source: TPersistent);
var
  lUsuario: TRLUsuarioDTO;
begin
  if Source is TRLUsuarioDTO then
  begin
    lUsuario := TRLUsuarioDTO(Source);
    ID.Assign(lUsuario.ID);
    Nome.Assign(lUsuario.Nome);
    Apelido.Assign(lUsuario.Apelido);
    Senha.Assign(lUsuario.Senha);
    Inativo.Assign(lUsuario.Inativo);
  end;
end;

{ TRLListOfUsuarioDTO }

constructor TRLListOfUsuarioDTO.CreateCollection(const AParent: TObject);
begin
  inherited;
  if _HSUsuario > 0 then
    HashSize := _HSUsuario;
end;

function TRLListOfUsuarioDTO.GetItem(const AIndex: Integer): TRLUsuarioDTO;
begin
  Result := TRLUsuarioDTO(inherited Items[AIndex]);
end;

function TRLListOfUsuarioDTO.GetItemClass: TCollectionItemClass;
begin
  Result := TRLUsuarioDTO;
end;

function TRLListOfUsuarioDTO.MakeKey(const AID: Integer): string;
begin
  Result := IntToStr(AID);
end;

function TRLListOfUsuarioDTO.MakeAlternativeKey(const AApelido: string): string;
begin
  Result := AApelido;
end;

function TRLListOfUsuarioDTO.Add: TRLUsuarioDTO;
begin
  Result := TRLUsuarioDTO(inherited Add);
end;

function TRLListOfUsuarioDTO.Insert(const AIndex: Integer): TRLUsuarioDTO;
begin
  Result := TRLUsuarioDTO(inherited Insert(AIndex));
end;

function TRLListOfUsuarioDTO.Find(const AID: Integer): TRLUsuarioDTO;
begin
  Result := TRLUsuarioDTO(FindHash(MakeKey(AID)));
end;

function TRLListOfUsuarioDTO.Contains(const AID: Integer): Boolean;
begin
  Result := Assigned(Find(AID));
end;

function TRLListOfUsuarioDTO.FindApelido(const AApelido: string): TRLUsuarioDTO;
begin
  Result := TRLUsuarioDTO(FindAlternativeHash(MakeAlternativeKey(AApelido)));
end;

function TRLListOfUsuarioDTO.ContainsApelido(const AApelido: string): Boolean;
begin
  Result := Assigned(FindApelido(AApelido));
end;

end.