//=============================================================================
// U_Messages
// SDK Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Messages;

{ TODO -oRafael : Registrar ERLInvalidConnectionName. }

interface

uses
  Classes,
  U_Exception,
  U_Message;

type
  // Exceptions
  ERLInvalidConnectionName = class(ERLException);

  // TRLGetServerDate
  TRLGetServerDate = class(TRLMessage)
  private
    FConnectionName: string;
    FServerDate: TDateTime;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property ConnectionName: string read FConnectionName write FConnectionName;
    property ServerDate: TDateTime read FServerDate write FServerDate;
  end;

implementation

{ TRLGetServerDate }

procedure TRLGetServerDate.Assign(Source: TPersistent);
var
  lGetServerDate: TRLGetServerDate;
begin
  inherited;
  if Source is TRLGetServerDate then
  begin
    lGetServerDate := TRLGetServerDate(Source);
    ConnectionName := lGetServerDate.ConnectionName;
    ServerDate := lGetServerDate.ServerDate;
  end;
end;

initialization
  RegisterClass({ERLInvalidConnectionName,} TRLGetServerDate);

end.
