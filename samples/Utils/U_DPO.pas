//=============================================================================
// U_DPO
// Class Library Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DPO;

interface

uses
  SysUtils,
  Classes,
  Controls,
  U_Collection,
  U_Value,
  U_Comparison,
  U_Connections,
  U_OPFConditions,
  U_OPFDPO,
  U_DTO;

type
  // TRLPerfilDPO
  TRLPerfilDPO = class(TRLOPFDPO)
  protected
    function GetItemClass: TRLCollectionItemClass; override;
    function GetCollectionClass: TRLCollectionClass; override;
  public
    constructor CreateDPO; virtual;
    procedure Insert(const APerfil: TRLPerfilDTO);
    function Update(const APerfil: TRLPerfilDTO): Boolean;
    function Save(const APerfil: TRLPerfilDTO): Boolean;
    function Load(const AConditions: TRLOPFConditions): TRLListOfPerfilDTO;
    procedure Delete(const AConditions: TRLOPFConditions); virtual;
    function LoadByKey(const AID: Integer): TRLPerfilDTO;
    procedure DeleteByKey(const AID: Integer); virtual;
    function LoadAll: TRLListOfPerfilDTO; virtual;
    procedure DeleteAll; virtual;
  end;

  // TRLRelPerfilUsuarioDPO
  TRLRelPerfilUsuarioDPO = class(TRLOPFDPO)
  protected
    function GetItemClass: TRLCollectionItemClass; override;
    function GetCollectionClass: TRLCollectionClass; override;
  public
    constructor CreateDPO; virtual;
    procedure Insert(const ARelPerfilUsuario: TRLRelPerfilUsuarioDTO);
    function Update(const ARelPerfilUsuario: TRLRelPerfilUsuarioDTO): Boolean;
    function Save(const ARelPerfilUsuario: TRLRelPerfilUsuarioDTO): Boolean;
    function Load(const AConditions: TRLOPFConditions): TRLListOfRelPerfilUsuarioDTO;
    procedure Delete(const AConditions: TRLOPFConditions); virtual;
    function LoadByKey(const AID: Integer): TRLRelPerfilUsuarioDTO;
    procedure DeleteByKey(const AID: Integer); virtual;
    function LoadByRelPerfilUsuario(const AID_Perfil: Integer; const AID_Usuario: Integer): TRLRelPerfilUsuarioDTO;
    procedure DeleteByRelPerfilUsuario(const AID_Perfil: Integer; const AID_Usuario: Integer); virtual;
    function GetIDByRelPerfilUsuario(const AID_Perfil: Integer; const AID_Usuario: Integer): Integer; virtual;
    function LoadByPerfil(const AID_Perfil: Integer): TRLListOfRelPerfilUsuarioDTO;
    procedure DeleteByPerfil(const AID_Perfil: Integer); virtual;
    function LoadByUsuario(const AID_Usuario: Integer): TRLListOfRelPerfilUsuarioDTO;
    procedure DeleteByUsuario(const AID_Usuario: Integer); virtual;
    function LoadAll: TRLListOfRelPerfilUsuarioDTO; virtual;
    procedure DeleteAll; virtual;
  end;

  // TRLUsuarioDPO
  TRLUsuarioDPO = class(TRLOPFDPO)
  protected
    function GetItemClass: TRLCollectionItemClass; override;
    function GetCollectionClass: TRLCollectionClass; override;
  public
    constructor CreateDPO; virtual;
    procedure Insert(const AUsuario: TRLUsuarioDTO);
    function Update(const AUsuario: TRLUsuarioDTO): Boolean;
    function Save(const AUsuario: TRLUsuarioDTO): Boolean;
    function Load(const AConditions: TRLOPFConditions): TRLListOfUsuarioDTO;
    procedure Delete(const AConditions: TRLOPFConditions); virtual;
    function LoadByKey(const AID: Integer): TRLUsuarioDTO;
    procedure DeleteByKey(const AID: Integer); virtual;
    function LoadByApelido(const AApelido: string): TRLUsuarioDTO;
    procedure DeleteByApelido(const AApelido: string); virtual;
    function GetIDByApelido(const AApelido: string): Integer; virtual;
    function LoadAll: TRLListOfUsuarioDTO; virtual;
    procedure DeleteAll; virtual;
  end;

implementation

{ TRLPerfilDPO }

constructor TRLPerfilDPO.CreateDPO;
var
  lConnection: TRLConnection;
begin
  inherited Create;
  lConnection := TRLConnections.Instance.FindByName('Samples');
  Provider := lConnection.Provider;
  ClassMapping := lConnection.ClassMapping;
end;

function TRLPerfilDPO.GetItemClass: TRLCollectionItemClass;
begin
  Result := TRLPerfilDTO;
end;

function TRLPerfilDPO.GetCollectionClass: TRLCollectionClass;
begin
  Result := TRLListOfPerfilDTO;
end;

procedure TRLPerfilDPO.Insert(const APerfil: TRLPerfilDTO);
begin
  InsertRecord(APerfil);
end;

function TRLPerfilDPO.Update(const APerfil: TRLPerfilDTO): Boolean;
begin
  Result := UpdateRecord(APerfil);
end;

function TRLPerfilDPO.Save(const APerfil: TRLPerfilDTO): Boolean;
begin
  Result := Assigned(APerfil);
  if Result then
  begin
    if APerfil.ID.Data = 0 then
      Insert(APerfil)
    else
      Result := Update(APerfil);
  end;
end;

function TRLPerfilDPO.Load(
  const AConditions: TRLOPFConditions): TRLListOfPerfilDTO;
begin
  Result := TRLListOfPerfilDTO(LoadRecordsByConditions(AConditions));
end;

procedure TRLPerfilDPO.Delete(const AConditions: TRLOPFConditions);
begin
  DeleteRecordsByConditions(AConditions);
end;

function TRLPerfilDPO.LoadByKey(const AID: Integer): TRLPerfilDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID;
    Result := TRLPerfilDTO(LoadRecordByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLPerfilDPO.DeleteByKey(const AID: Integer);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLPerfilDPO.LoadAll: TRLListOfPerfilDTO;
begin
  Result := TRLListOfPerfilDTO(LoadRecordsByConditions(nil));
end;

procedure TRLPerfilDPO.DeleteAll;
begin
  DeleteRecordsByConditions(nil);
end;

{ TRLRelPerfilUsuarioDPO }

constructor TRLRelPerfilUsuarioDPO.CreateDPO;
var
  lConnection: TRLConnection;
begin
  inherited Create;
  lConnection := TRLConnections.Instance.FindByName('Samples');
  Provider := lConnection.Provider;
  ClassMapping := lConnection.ClassMapping;
end;

function TRLRelPerfilUsuarioDPO.GetItemClass: TRLCollectionItemClass;
begin
  Result := TRLRelPerfilUsuarioDTO;
end;

function TRLRelPerfilUsuarioDPO.GetCollectionClass: TRLCollectionClass;
begin
  Result := TRLListOfRelPerfilUsuarioDTO;
end;

procedure TRLRelPerfilUsuarioDPO.Insert(
  const ARelPerfilUsuario: TRLRelPerfilUsuarioDTO);
begin
  InsertRecord(ARelPerfilUsuario);
end;

function TRLRelPerfilUsuarioDPO.Update(
  const ARelPerfilUsuario: TRLRelPerfilUsuarioDTO): Boolean;
begin
  Result := UpdateRecord(ARelPerfilUsuario);
end;

function TRLRelPerfilUsuarioDPO.Save(
  const ARelPerfilUsuario: TRLRelPerfilUsuarioDTO): Boolean;
begin
  Result := Assigned(ARelPerfilUsuario);
  if Result then
  begin
    if ARelPerfilUsuario.ID.Data = 0 then
      Insert(ARelPerfilUsuario)
    else
      Result := Update(ARelPerfilUsuario);
  end;
end;

function TRLRelPerfilUsuarioDPO.Load(
  const AConditions: TRLOPFConditions): TRLListOfRelPerfilUsuarioDTO;
begin
  Result := TRLListOfRelPerfilUsuarioDTO(LoadRecordsByConditions(AConditions));
end;

procedure TRLRelPerfilUsuarioDPO.Delete(const AConditions: TRLOPFConditions);
begin
  DeleteRecordsByConditions(AConditions);
end;

function TRLRelPerfilUsuarioDPO.LoadByKey(
  const AID: Integer): TRLRelPerfilUsuarioDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID;
    Result := TRLRelPerfilUsuarioDTO(LoadRecordByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLRelPerfilUsuarioDPO.DeleteByKey(const AID: Integer);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLRelPerfilUsuarioDPO.LoadByRelPerfilUsuario(
  const AID_Perfil: Integer; 
  const AID_Usuario: Integer): TRLRelPerfilUsuarioDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Perfil';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Perfil;
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Usuario';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Usuario;
    Result := TRLRelPerfilUsuarioDTO(LoadRecordByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLRelPerfilUsuarioDPO.DeleteByRelPerfilUsuario(
  const AID_Perfil: Integer; const AID_Usuario: Integer);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Perfil';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Perfil;
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Usuario';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Usuario;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLRelPerfilUsuarioDPO.GetIDByRelPerfilUsuario(
  const AID_Perfil: Integer; const AID_Usuario: Integer): Integer;
var
  lRelPerfilUsuario: TRLRelPerfilUsuarioDTO;
begin
  Result := 0;
  Names.Text := 'ID';
  lRelPerfilUsuario := LoadByRelPerfilUsuario(AID_Perfil, AID_Usuario);
  if Assigned(lRelPerfilUsuario) then
    try
      Result := lRelPerfilUsuario.ID.Data;
    finally
      FreeAndNil(lRelPerfilUsuario);
    end;
end;

function TRLRelPerfilUsuarioDPO.LoadByPerfil(
  const AID_Perfil: Integer): TRLListOfRelPerfilUsuarioDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Perfil';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Perfil;
    Result := TRLListOfRelPerfilUsuarioDTO(LoadRecordsByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLRelPerfilUsuarioDPO.DeleteByPerfil(const AID_Perfil: Integer);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Perfil';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Perfil;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLRelPerfilUsuarioDPO.LoadByUsuario(
  const AID_Usuario: Integer): TRLListOfRelPerfilUsuarioDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Usuario';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Usuario;
    Result := TRLListOfRelPerfilUsuarioDTO(LoadRecordsByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLRelPerfilUsuarioDPO.DeleteByUsuario(const AID_Usuario: Integer);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID_Usuario';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID_Usuario;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLRelPerfilUsuarioDPO.LoadAll: TRLListOfRelPerfilUsuarioDTO;
begin
  Result := TRLListOfRelPerfilUsuarioDTO(LoadRecordsByConditions(nil));
end;

procedure TRLRelPerfilUsuarioDPO.DeleteAll;
begin
  DeleteRecordsByConditions(nil);
end;

{ TRLUsuarioDPO }

constructor TRLUsuarioDPO.CreateDPO;
var
  lConnection: TRLConnection;
begin
  inherited Create;
  lConnection := TRLConnections.Instance.FindByName('Samples');
  Provider := lConnection.Provider;
  ClassMapping := lConnection.ClassMapping;
end;

function TRLUsuarioDPO.GetItemClass: TRLCollectionItemClass;
begin
  Result := TRLUsuarioDTO;
end;

function TRLUsuarioDPO.GetCollectionClass: TRLCollectionClass;
begin
  Result := TRLListOfUsuarioDTO;
end;

procedure TRLUsuarioDPO.Insert(const AUsuario: TRLUsuarioDTO);
begin
  InsertRecord(AUsuario);
end;

function TRLUsuarioDPO.Update(const AUsuario: TRLUsuarioDTO): Boolean;
begin
  Result := UpdateRecord(AUsuario);
end;

function TRLUsuarioDPO.Save(const AUsuario: TRLUsuarioDTO): Boolean;
begin
  Result := Assigned(AUsuario);
  if Result then
  begin
    if AUsuario.ID.Data = 0 then
      Insert(AUsuario)
    else
      Result := Update(AUsuario);
  end;
end;

function TRLUsuarioDPO.Load(
  const AConditions: TRLOPFConditions): TRLListOfUsuarioDTO;
begin
  Result := TRLListOfUsuarioDTO(LoadRecordsByConditions(AConditions));
end;

procedure TRLUsuarioDPO.Delete(const AConditions: TRLOPFConditions);
begin
  DeleteRecordsByConditions(AConditions);
end;

function TRLUsuarioDPO.LoadByKey(const AID: Integer): TRLUsuarioDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID;
    Result := TRLUsuarioDTO(LoadRecordByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLUsuarioDPO.DeleteByKey(const AID: Integer);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'ID';
    lCondition.Comparison.ValueType := vtInteger;
    lCondition.Comparison.Kind := ckEqual;
    TRLInteger(lCondition.Comparison.AddValue).Data := AID;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLUsuarioDPO.LoadByApelido(const AApelido: string): TRLUsuarioDTO;
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'Apelido';
    lCondition.Comparison.ValueType := vtString;
    lCondition.Comparison.Kind := ckEqual;
    TRLString(lCondition.Comparison.AddValue).Data := AApelido;
    Result := TRLUsuarioDTO(LoadRecordByConditions(lConditions));
  finally
    FreeAndNil(lConditions);
  end;
end;

procedure TRLUsuarioDPO.DeleteByApelido(const AApelido: string);
var
  lConditions: TRLOPFConditions;
  lCondition: TRLOPFCondition;
begin
  lConditions := TRLOPFConditions.Create;
  try
    lCondition := lConditions.Add;
    lCondition.AttributeName := 'Apelido';
    lCondition.Comparison.ValueType := vtString;
    lCondition.Comparison.Kind := ckEqual;
    TRLString(lCondition.Comparison.AddValue).Data := AApelido;
    DeleteRecordsByConditions(lConditions);
  finally
    FreeAndNil(lConditions);
  end;
end;

function TRLUsuarioDPO.GetIDByApelido(const AApelido: string): Integer;
var
  lUsuario: TRLUsuarioDTO;
begin
  Result := 0;
  Names.Text := 'ID';
  lUsuario := LoadByApelido(AApelido);
  if Assigned(lUsuario) then
    try
      Result := lUsuario.ID.Data;
    finally
      FreeAndNil(lUsuario);
    end;
end;

function TRLUsuarioDPO.LoadAll: TRLListOfUsuarioDTO;
begin
  Result := TRLListOfUsuarioDTO(LoadRecordsByConditions(nil));
end;

procedure TRLUsuarioDPO.DeleteAll;
begin
  DeleteRecordsByConditions(nil);
end;

end.