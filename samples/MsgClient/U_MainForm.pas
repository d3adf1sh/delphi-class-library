//=============================================================================
// U_MainForm
// SDK Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MainForm;

interface

uses
  System.SysUtils,
  System.Classes,
  FMX.Types,
  FMX.Controls,
  FMX.Controls.Presentation,
  FMX.Forms,
  FMX.Edit,
  FMX.StdCtrls;

type
  // TMainForm
  TMainForm = class(TForm)
    edtComponent: TEdit;
    edtHost: TEdit;
    edtPort: TEdit;
    btnGetServerDate: TButton;
    lblServerDate: TLabel;
    lblTime: TLabel;
    procedure btnGetServerDateClick(Sender: TObject);
  end;

var
  MainForm: TMainForm;

implementation

uses
  System.DateUtils,
  FMX.Dialogs,
  U_Sender,
  U_IDSender,
  U_Messages;

{$R *.fmx}

{ TMainForm }

procedure TMainForm.btnGetServerDateClick(Sender: TObject);
var
  lSender: TRLSender;
  lMessage: TRLGetServerDate;
  dtTime: TDateTime;
begin
  dtTime := Now;
  lSender := TRLSender.FindSender('ID');
  try
    lSender.Settings['Host'] := edtHost.Text;
    lSender.Settings['Port'] := edtPort.Text;
    lSender.Settings['ReadTimeout'] := '15000';
    lSender.Settings['ConnectionTimeout'] := '15000';
    lSender.Open;
    lMessage := TRLGetServerDate.Create;
    try
      lMessage.ConnectionName := 'Samples';
      try
        lSender.Send(lMessage);
        lblServerDate.Text := FormatDateTime('dd/mm/yyyy hh:nn:ss',
          lMessage.ServerDate);
      except
        on E: Exception do
          if E is ERLInvalidConnectionName then
            ShowMessage('Invalid connection.')
          else
            ShowMessage('Erro.' + sLineBreak + E.Message);
      end;
    finally
      FreeAndNil(lMessage);
    end;
  finally
    FreeAndNil(lSender);
  end;

  lblTime.Text := IntToStr(MilliSecondsBetween(Now, dtTime)) + ' ms';
end;

end.
