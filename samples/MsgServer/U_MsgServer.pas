//=============================================================================
// U_MsgServer
// SDK Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MsgServer;

interface

{$R Resources.res}

uses
  SysUtils,
  Classes,
  U_Settings,
  U_Message,
  U_Receiver,
  U_Service,
  U_DataApplication,
  U_Messages;

type
  // Methods
  TRLMessageMethod = procedure(Message: TRLMessage) of object;

  // TRLMsgServer
  TRLMsgServer = class(TRLDataApplication)
  private
    FOnMessage: TRLMessageEvent;
    FReceiver: TRLReceiver;
    procedure MessageReceived(Sender: TObject; Message: TRLMessage);
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure OpenReceiver;
    procedure CloseReceiver;
    function ReceiverOpened: Boolean;
    property OnMessage: TRLMessageEvent read FOnMessage write FOnMessage;
    class function Instance: TRLMsgServer;
  published
    procedure GetServerDate(Message: TRLGetServerDate);
  end;

  // TRLMsgServerSettings
  TRLMsgServerSettings = class(TRLSettings)
  private
    FReceiverName: string;
  published
    property ReceiverName: string read FReceiverName write FReceiverName;
  end;

  // TRLMsgServerService
  TRLMsgServerService = class(TRLService)
  protected
    procedure DoStart; override;
    function DoStop: Boolean; override;
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer); override;
  end;

implementation

uses
  U_AL,
  U_RTTIContext,
  U_Connections,
  U_FormManager,
  U_Constants;

{ TRLMsgServer }

constructor TRLMsgServer.Create;
begin
  inherited;
  ModulesFileName := FileLocation + 'Modules.xml';
  ConnectionsFileName := FileLocation + 'Connections.xml';
  TRLFormManager.Instance.UseSystemFont := True;
end;

destructor TRLMsgServer.Destroy;
begin
  if Assigned(FReceiver) then
    FreeAndNil(FReceiver);
  inherited;
end;

procedure TRLMsgServer.MessageReceived(Sender: TObject;
  Message: TRLMessage);
var
  lMethod: TMethod;
  sName: string;
begin
  try
    sName := Message.ClassName;
    Delete(sName, 1, 3);
    lMethod.Code := MethodAddress(sName);
    if Assigned(lMethod.Code) then
    begin
      Message.Received := True;
      lMethod.Data := Self;
      TRLMessageMethod(lMethod)(Message);
    end;
  finally
    if Assigned(FOnMessage) then
      FOnMessage(Self, Message);
  end;
end;

procedure TRLMsgServer.OpenReceiver;
var
  lSettings: TRLMsgServerSettings;
  sSettingsFileName: string;
begin
  if not ReceiverOpened then
  begin
    lSettings := TRLMsgServerSettings.Create;
    try
      sSettingsFileName := ChangeFileExt(FileName, '.xml');
      if FileExists(sSettingsFileName) then
      begin
        TRLRTTIContext.Instance.FromFile(lSettings, sSettingsFileName, sfXML);
        FReceiver := TRLReceiver.FindReceiver(lSettings.ReceiverName);
        FReceiver.Settings.Assign(lSettings);
      end
      else
        FReceiver := TRLReceiver.FindReceiver('ID');
      FReceiver.OnMessage := MessageReceived;
    finally
      FreeAndNil(lSettings);
    end;

    FReceiver.Open;
  end;
end;

procedure TRLMsgServer.CloseReceiver;
begin
  if ReceiverOpened then
    FReceiver.Close;
end;

function TRLMsgServer.ReceiverOpened: Boolean;
begin
  Result := Assigned(FReceiver) and FReceiver.IsOpen;
end;

class function TRLMsgServer.Instance: TRLMsgServer;
begin
  Result := TRLMsgServer(inherited Instance);
end;

procedure TRLMsgServer.GetServerDate(Message: TRLGetServerDate);
var
  lConnection: TRLConnection;
begin
  lConnection := TRLConnections.Instance.Find(Message.ConnectionName);
  if not Assigned(lConnection) then
    raise ERLInvalidConnectionName.CreateFmt('Connection "%s" is invalid.',
      [Message.ConnectionName]);
  lConnection.BeginUse;
  try
    Message.ServerDate := lConnection.Database.GetDateAndTime;
  finally
    lConnection.EndUse;
  end;
end;

{ TRLMsgServerService }

constructor TRLMsgServerService.CreateNew(AOwner: TComponent;
  Dummy: Integer);
begin
  inherited;
  Name := 'MsgServer';
  DisplayName := TRLMsgServer.Instance.Title;
end;

procedure TRLMsgServerService.DoStart;
begin
  TRLMsgServer.Instance.OpenReceiver;
  inherited;
end;

function TRLMsgServerService.DoStop: Boolean;
begin
  Result := inherited DoStop;
  if Result then
    TRLMsgServer.Instance.CloseReceiver;
end;

end.
