//=============================================================================
// U_MainForm
// SDK Samples
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MainForm;

interface

uses
  SysUtils,
  Classes,
  Forms,
  ImgList,
  Controls,
  cxClasses,
  cxControls,
  cxCustomData,
  cxGraphics,
  cxInplaceContainer,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxStyles,
  cxTextEdit,
  cxTL,
  cxTLdxBarBuiltInMenu,
  dxBar,
  dxSkinOffice2010Silver,
  dxSkinsCore,
  dxSkinsdxBarPainter,
  dxSkinsForm,
  U_Message,
  U_FormType;

type
  // TRLMainForm
  TRLMainForm = class(TRLFormType)
    bmToolbar: TdxBarManager;
    brToolbar: TdxBar;
    bbStart: TdxBarButton;
    bbStop: TdxBarButton;
    ilImages: TcxImageList;
    scSkin: TdxSkinController;
    tlMessages: TcxTreeList;
    tlcDate: TcxTreeListColumn;
    tlcMessage: TcxTreeListColumn;
    bbClear: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bbStartClick(Sender: TObject);
    procedure bbStopClick(Sender: TObject);
    procedure bbClearClick(Sender: TObject);
    procedure tlMessagesDblClick(Sender: TObject);
    procedure tlMessagesDeletion(Sender: TcxCustomTreeList; ANode: TcxTreeListNode);
  private
    procedure UpdateButtons;
    procedure MessageReceived(Sender: TObject; Message: TRLMessage);
  end;

implementation

uses
  Math,
  U_Value,
  U_MsgServer,
  U_RTTIContext,
  U_Dialog;

const
  RLImReceived = 2;
  RLImRLNonReceived = 3;

{$R *.dfm}

{ TRLMainForm }

procedure TRLMainForm.FormCreate(Sender: TObject);
begin
  Caption := Application.Title;
end;

procedure TRLMainForm.FormDestroy(Sender: TObject);
begin
  TRLMsgServer.Instance.CloseReceiver;
end;

procedure TRLMainForm.bbStartClick(Sender: TObject);
begin
  TRLMsgServer.Instance.OnMessage := MessageReceived;
  TRLMsgServer.Instance.OpenReceiver;
  UpdateButtons;
end;

procedure TRLMainForm.bbStopClick(Sender: TObject);
begin
  TRLMsgServer.Instance.CloseReceiver;
  TRLMsgServer.Instance.OnMessage := nil;
  UpdateButtons;
end;

procedure TRLMainForm.bbClearClick(Sender: TObject);
begin
  tlMessages.Clear;
end;

procedure TRLMainForm.tlMessagesDblClick(Sender: TObject);
begin
  if (tlMessages.SelectionCount > 0) and
    Assigned(tlMessages.Selections[0].Data) then
    TRLDialog.Instance.Show(TRLString(tlMessages.Selections[0].Data).AsString);
end;

procedure TRLMainForm.tlMessagesDeletion(Sender: TcxCustomTreeList;
  ANode: TcxTreeListNode);
begin
  if Assigned(ANode.Data) then
    TObject(ANode.Data).Free;
end;

procedure TRLMainForm.UpdateButtons;
begin
  bbStart.Enabled := not TRLMsgServer.Instance.ReceiverOpened;
  bbStop.Enabled := not bbStart.Enabled;
end;

procedure TRLMainForm.MessageReceived(Sender: TObject;
  Message: TRLMessage);
var
  lItem: TcxTreeListNode;
begin
  tlMessages.BeginUpdate;
  try
    lItem := tlMessages.AddFirst;
    lItem.Values[0] := FormatDateTime('dd/mm/yyyy hh:nn:ss', Now);
    lItem.Values[1] := Message.ClassName;
    lItem.Data := TRLString.CreateString(TRLRTTIContext.Instance.ToXML(Message, True));
    lItem.ImageIndex := IfThen(Message.Received, RLImReceived, RLImRLNonReceived);
  finally
    tlMessages.EndUpdate;
  end;
end;

end.
