program MsgServer;

uses
  MidasLib,
  Forms,
  U_MainForm in 'U_MainForm.pas' {RLMainForm},
  U_MsgServer in 'U_MsgServer.pas';

{$R *.res}

begin
  Application.Title := 'Message Server';
  TRLMsgServer.RunApplication(TRLMainForm, TRLMsgServerService);
end.
