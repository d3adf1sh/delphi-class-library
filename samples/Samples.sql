/*==============================================================*/
/* Table: Perfil                                                */
/*==============================================================*/
create table Perfil (
   ID                   int                  identity,
   Nome                 varchar(50)          not null,
   constraint PK_GrupoUsuarios primary key (ID)
)
go

/*==============================================================*/
/* Index: IDX_Perfil_Descricao                                  */
/*==============================================================*/
create index IDX_Perfil_Descricao on Perfil (
Nome ASC
)
go

/*==============================================================*/
/* Table: RelPerfilUsuario                                      */
/*==============================================================*/
create table RelPerfilUsuario (
   ID                   int                  identity,
   ID_Perfil            int                  not null,
   ID_Usuario           int                  not null,
   constraint PK_RelPerfilUsuario primary key (ID),
   constraint UK_RelPerfilUsuario unique (ID_Perfil, ID_Usuario)
)
go

/*==============================================================*/
/* Table: Usuario                                               */
/*==============================================================*/
create table Usuario (
   ID                   int                  identity,
   Nome                 varchar(50)          not null,
   Apelido              varchar(30)          not null,
   Senha                varchar(40)          not null,
   Inativo              bit                  null,
   constraint PK_Usuario primary key (ID),
   constraint UK_Usuario_Apelido unique (Apelido)
)
go

insert into Usuario (Nome
                   , Apelido
                   , Senha
                   , Inativo)
             values ('Rafael C. Luiz'
                   , 'Rafael'
                   , ''
                   , 0);

/*==============================================================*/
/* Index: IDX_Usuario_Nome                                      */
/*==============================================================*/
create index IDX_Usuario_Nome on Usuario (
Nome ASC
)
go

alter table RelPerfilUsuario
   add constraint FK_PerfilUsuario_Perfil foreign key (ID_Perfil)
      references Perfil (ID)
go

alter table RelPerfilUsuario
   add constraint FK_PerfilUsuario_Usuario foreign key (ID_Usuario)
      references Usuario (ID)
go

