if not exist Utils mkdir Utils

..\build\bin\CodeGenerator ^
/Type DTO ^
/Language Pascal ^
/FileName Utils\U_DTO.pas ^
/IdentifierMapping Identifiers.xml ^
/ConnectionName Samples ^
/ApplicationName "Class Library Samples" ^
/CompanyName "Copyright(C) 2015 Rafael Luiz"

..\build\bin\CodeGenerator ^
/Type ClassMapping ^
/Language Pascal ^
/FileName Utils\U_ClassMapping.pas ^
/IdentifierMapping Identifiers.xml ^
/ConnectionName Samples ^
/ApplicationName "Class Library Samples" ^
/CompanyName "Copyright(C) 2015 Rafael Luiz" ^
/DTOReference U_DTO

..\build\bin\CodeGenerator ^
/Type DPO ^
/Language Pascal ^
/FileName Utils\U_DPO.pas ^
/IdentifierMapping Identifiers.xml ^
/ConnectionName Samples ^
/ApplicationName "Class Library Samples" ^
/CompanyName "Copyright(C) 2015 Rafael Luiz" ^
/DTOReference U_DTO

..\build\bin\CodeGenerator ^
/Type DLO ^
/Language Pascal ^
/FileName Utils\U_DLO.pas ^
/IdentifierMapping Identifiers.xml ^
/ConnectionName Samples ^
/ApplicationName "Class Library Samples" ^
/CompanyName "Copyright(C) 2015 Rafael Luiz" ^
/DPOReference U_DPO

pause