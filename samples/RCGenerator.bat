..\build\bin\RCGenerator ^
/FileName PNG16.rc ^
/Type RCData ^
/Path ..\Resources\Icons\16 ^
/Prefix RES_PNG_

brcc32.exe PNG16.rc PNG16.res -32
move PNG16.res ..\Include
del PNG16.rc

..\build\bin\RCGenerator ^
/FileName PNG24.rc ^
/Type RCData ^
/Path ..\Resources\Icons\24 ^
/Prefix RES_PNG_

brcc32.exe PNG24.rc PNG24.res -32
move PNG24.res ..\Include
del PNG24.rc

..\build\bin\RCGenerator ^
/FileName PNG32.rc ^
/Type RCData ^
/Path ..\Resources\Icons\32 ^
/Prefix RES_PNG_

brcc32.exe PNG32.rc PNG32.res -32
move PNG32.res ..\Include
del PNG32.rc

..\build\bin\RCGenerator ^
/FileName PNG48.rc ^
/Type RCData ^
/Path ..\Resources\Icons\48 ^
/Prefix RES_PNG_

brcc32.exe PNG48.rc PNG48.res -32
move PNG48.res ..\Include
del PNG48.rc

pause