# Delphi Class Library

Biblioteca de classes para suprir recursos que o Delphi não traz (ou trazia) de forma nativa como tabelas hash, listas indexadas, serialização de objetos, um protocolo de mensagens via socket, MVC, um OPF, entre outros.

Requer DevExpress 16.2.5+, DOA 4.1.3+ e SDAC 8.0.1+.

## Execução

Abrir `All.groupproj` e compilar todos os projetos. Os binários serão gerados na pasta `build\bin`.

Copiar os XMLs e as DLLs da pasta `resources` para `build\bin`.

Criar um banco de dados SQLServer e executar o script `samples\Samples.sql`.

Executar `build\bin\CodeGenerator -EditConnections` e configurar o banco de dados recém criado na conexão **Samples**.

Executar os BATs da pasta `samples`.