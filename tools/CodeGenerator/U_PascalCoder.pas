//=============================================================================
// U_PascalCoder
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_PascalCoder;

interface

uses
  U_Coder;

type
  // TRLPascalCoder
  TRLPascalCoder = class(TRLCoder)
  protected
    function CanBreakLine(const AChar: Char): Boolean; override;
    function GetTypePrefix: string; override;
    function GetAttributePrefix: string; override;
    function GetLocalAttributePrefix: string; override;
    function GetParameterPrefix: string; override;
    function GetBlockBegin: string; override;
    function GetBlockEnd: string; override;
    procedure WriteBlockEnd; override;
    function GetRecordPrefix: string; virtual;
  public
    function Write(const AText: string): TRLPascalCoder; reintroduce; virtual;
    function WriteSpace: TRLPascalCoder; reintroduce; virtual;
    function WriteLineBreak: TRLPascalCoder; reintroduce; virtual;
    function WriteBlankLine: TRLPascalCoder; reintroduce; virtual;
    function WriteDot: TRLPascalCoder; reintroduce; virtual;
    function WriteComma: TRLPascalCoder; reintroduce; virtual;
    function WriteColon: TRLPascalCoder; reintroduce; virtual;
    function WriteSemicolon: TRLPascalCoder; reintroduce; virtual;
    function WriteQuote: TRLPascalCoder; reintroduce; virtual;
    function WriteDoubleQuote: TRLPascalCoder; reintroduce; virtual;
    function WriteQuotedText(const AText: string): TRLPascalCoder; reintroduce; virtual;
    function WriteEqual: TRLPascalCoder; reintroduce; virtual;
    function WriteGreater: TRLPascalCoder; reintroduce; virtual;
    function WriteGreaterEqual: TRLPascalCoder; reintroduce; virtual;
    function WriteLess: TRLPascalCoder; reintroduce; virtual;
    function WriteLessEqual: TRLPascalCoder; reintroduce; virtual;
    function WritePlus: TRLPascalCoder; reintroduce; virtual;
    function WriteMinus: TRLPascalCoder; reintroduce; virtual;
    function WriteAsterisk: TRLPascalCoder; reintroduce; virtual;
    function WritePercent: TRLPascalCoder; reintroduce; virtual;
    function WriteSlash: TRLPascalCoder; reintroduce; virtual;
    function WriteBackSlash: TRLPascalCoder; reintroduce; virtual;
    function WriteVerticalSlash: TRLPascalCoder; reintroduce; virtual;
    function WriteDoubleSlash: TRLPascalCoder; reintroduce; virtual;
    function WriteDoubleBackSlash: TRLPascalCoder; reintroduce; virtual;
    function WriteDoubleVerticalSlash: TRLPascalCoder; reintroduce; virtual;
    function WriteUnderscore: TRLPascalCoder; reintroduce; virtual;
    function WriteCaret: TRLPascalCoder; reintroduce; virtual;
    function WriteAt: TRLPascalCoder; reintroduce; virtual;
    function WriteAmpersand: TRLPascalCoder; reintroduce; virtual;
    function WriteLeftParenthesis: TRLPascalCoder; reintroduce; virtual;
    function WriteRightParenthesis: TRLPascalCoder; reintroduce; virtual;
    function WriteLeftBracket: TRLPascalCoder; reintroduce; virtual;
    function WriteRightBracket: TRLPascalCoder; reintroduce; virtual;
    function WriteLeftBrace: TRLPascalCoder; reintroduce; virtual;
    function WriteRightBrace: TRLPascalCoder; reintroduce; virtual;
    function WritePrefix: TRLPascalCoder; reintroduce; virtual;
    function WriteTypePrefix: TRLPascalCoder; reintroduce; virtual;
    function WriteAttributePrefix: TRLPascalCoder; reintroduce; virtual;
    function WriteLocalAttributePrefix: TRLPascalCoder; reintroduce; virtual;
    function WriteParameterPrefix: TRLPascalCoder; reintroduce; virtual;
    function WriteTypeIdentifier(const AIdentifier: string): TRLPascalCoder; reintroduce; virtual;
    function WriteQuotedTypeIdentifier(const AIdentifier: string): TRLPascalCoder; reintroduce; virtual;
    function WriteAttributeIdentifier(const AIdentifier: string): TRLPascalCoder; reintroduce; virtual;
    function WriteLocalAttributeIdentifier(const AIdentifier: string): TRLPascalCoder; reintroduce; virtual;
    function WriteParameterIdentifier(const AIdentifier: string): TRLPascalCoder; reintroduce; virtual;
    function WriteHeaderLine: TRLPascalCoder; virtual;
    function WriteHeaderInformation(const AText: string): TRLPascalCoder; virtual;
    function WriteHeader(const ATitle, ADescription, ACompanyName: string): TRLPascalCoder; virtual;
    function WriteUnit: TRLPascalCoder; virtual;
    function WriteInterface: TRLPascalCoder; virtual;
    function WriteImplementation: TRLPascalCoder; virtual;
    function WriteInitialization: TRLPascalCoder; virtual;
    function WriteFinalization: TRLPascalCoder; virtual;
    function WriteUses: TRLPascalCoder; virtual;
    function WriteType: TRLPascalCoder; virtual;
    function WriteTypeHeader(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteTypeCodeHeader(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteSet: TRLPascalCoder; virtual;
    function WriteOf: TRLPascalCoder; virtual;
    function WriteSetOf: TRLPascalCoder; virtual;
    function WriteRecord: TRLPascalCoder; virtual;
    function WriteRecordPrefix: TRLPascalCoder; virtual;
    function WriteRecordIdentifier(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteQuotedRecordIdentifier(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteRecordHeader(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteRecordCodeHeader(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteClass: TRLPascalCoder; virtual;
    function WritePrivate: TRLPascalCoder; virtual;
    function WriteProtected: TRLPascalCoder; virtual;
    function WritePublic: TRLPascalCoder; virtual;
    function WritePublished: TRLPascalCoder; virtual;
    function WriteProperty: TRLPascalCoder; virtual;
    function WriteRead: TRLPascalCoder; virtual;
    function WriteWrite: TRLPascalCoder; virtual;
    function WriteDefault: TRLPascalCoder; virtual;
    function WriteFunction: TRLPascalCoder; virtual;
    function WriteProcedure: TRLPascalCoder; virtual;
    function WriteClassFunction: TRLPascalCoder; virtual;
    function WriteClassProcedure: TRLPascalCoder; virtual;
    function WriteConstructor: TRLPascalCoder; virtual;
    function WriteDestructor: TRLPascalCoder; virtual;
    function WriteCreate: TRLPascalCoder; virtual;
    function WriteDestroy: TRLPascalCoder; virtual;
    function WriteVirtual: TRLPascalCoder; virtual;
    function WriteDynamic: TRLPascalCoder; virtual;
    function WriteAbstract: TRLPascalCoder; virtual;
    function WriteOverride: TRLPascalCoder; virtual;
    function WriteReintroduce: TRLPascalCoder; virtual;
    function WriteOperator: TRLPascalCoder; virtual;
    function WriteClassOperator: TRLPascalCoder; virtual;
    function WriteResourcestring: TRLPascalCoder; virtual;
    function WriteConst: TRLPascalCoder; virtual;
    function WriteVar: TRLPascalCoder; virtual;
    function WriteOut: TRLPascalCoder; virtual;
    function WriteChar: TRLPascalCoder; virtual;
    function WriteString: TRLPascalCoder; virtual;
    function WriteInteger: TRLPascalCoder; virtual;
    function WriteInt64: TRLPascalCoder; virtual;
    function WriteDouble: TRLPascalCoder; virtual;
    function WriteCurrency: TRLPascalCoder; virtual;
    function WriteTDate: TRLPascalCoder; virtual;
    function WriteTTime: TRLPascalCoder; virtual;
    function WriteTDateTime: TRLPascalCoder; virtual;
    function WriteBoolean: TRLPascalCoder; virtual;
    function WriteFalse: TRLPascalCoder; virtual;
    function WriteTrue: TRLPascalCoder; virtual;
    function WriteBegin: TRLPascalCoder; virtual;
    function WriteEnd: TRLPascalCoder; virtual;
    function WriteInherited: TRLPascalCoder; virtual;
    function WriteAssignment: TRLPascalCoder; virtual;
    function WriteResult: TRLPascalCoder; virtual;
    function WriteFreeAndNil: TRLPascalCoder; virtual;
    function WriteResultDestruction: TRLPascalCoder; virtual;
    function WriteAttributeDestruction(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteLocalAttributeDestruction(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteAssigned: TRLPascalCoder; virtual;
    function WriteResultChecking: TRLPascalCoder; virtual;
    function WriteAttributeChecking(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteLocalAttributeChecking(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteParameterChecking(const AIdentifier: string): TRLPascalCoder; virtual;
    function WriteIf: TRLPascalCoder; virtual;
    function WriteThen: TRLPascalCoder; virtual;
    function WriteElse: TRLPascalCoder; virtual;
    function WriteAnd: TRLPascalCoder; virtual;
    function WriteOr: TRLPascalCoder; virtual;
    function WriteNot: TRLPascalCoder; virtual;
    function WriteIn: TRLPascalCoder; virtual;
    function WriteIs: TRLPascalCoder; virtual;
    function WriteOn: TRLPascalCoder; virtual;
    function WriteNil: TRLPascalCoder; virtual;
    function WriteCase: TRLPascalCoder; virtual;
    function WriteWhile: TRLPascalCoder; virtual;
    function WriteDo: TRLPascalCoder; virtual;
    function WriteFor: TRLPascalCoder; virtual;
    function WriteTo: TRLPascalCoder; virtual;
    function WriteDownto: TRLPascalCoder; virtual;
    function WriteRepeat: TRLPascalCoder; virtual;
    function WriteUntil: TRLPascalCoder; virtual;
    function WriteMod: TRLPascalCoder; virtual;
    function WriteDiv: TRLPascalCoder; virtual;
    function WriteTry: TRLPascalCoder; virtual;
    function WriteFinally: TRLPascalCoder; virtual;
    function WriteExcept: TRLPascalCoder; virtual;
    function WriteException: TRLPascalCoder; virtual;
    function WriteRaise: TRLPascalCoder; virtual;
    function WriteProgram: TRLPascalCoder; virtual;
    function WritePackage: TRLPascalCoder; virtual;
    function WriteRequires: TRLPascalCoder; virtual;
    function WriteContains: TRLPascalCoder; virtual;
    function WriteLibrary: TRLPascalCoder; virtual;
    function WriteExports: TRLPascalCoder; virtual;
  end;

implementation

uses
  U_System;

{ TRLPascalCoder }

function TRLPascalCoder.CanBreakLine(const AChar: Char): Boolean;
begin
  Result := U_System.CharInSet(AChar, [',', ';', '+']);
end;

function TRLPascalCoder.GetTypePrefix: string;
begin
  Result := 'T';
end;

function TRLPascalCoder.GetAttributePrefix: string;
begin
  Result := 'F';
end;

function TRLPascalCoder.GetLocalAttributePrefix: string;
begin
  Result := 'l';
end;

function TRLPascalCoder.GetParameterPrefix: string;
begin
  Result := 'A';
end;

function TRLPascalCoder.GetBlockBegin: string;
begin
  Result := 'begin';
end;

function TRLPascalCoder.GetBlockEnd: string;
begin
  Result := 'end';
end;

procedure TRLPascalCoder.WriteBlockEnd;
begin
  inherited;
  WriteSemicolon;
end;

function TRLPascalCoder.GetRecordPrefix: string;
begin
  Result := '_';
end;

function TRLPascalCoder.Write(const AText: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited Write(AText));
end;

function TRLPascalCoder.WriteSpace: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteSpace);
end;

function TRLPascalCoder.WriteLineBreak: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLineBreak);
end;

function TRLPascalCoder.WriteBlankLine: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteBlankLine);
end;

function TRLPascalCoder.WriteDot: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteDot);
end;

function TRLPascalCoder.WriteComma: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteComma);
end;

function TRLPascalCoder.WriteColon: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteColon);
end;

function TRLPascalCoder.WriteSemicolon: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteSemicolon);
end;

function TRLPascalCoder.WriteQuote: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteQuote);
end;

function TRLPascalCoder.WriteDoubleQuote: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteDoubleQuote);
end;

function TRLPascalCoder.WriteQuotedText(
  const AText: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteQuotedText(AText));
end;

function TRLPascalCoder.WriteEqual: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteEqual);
end;

function TRLPascalCoder.WriteGreater: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteGreater);
end;

function TRLPascalCoder.WriteGreaterEqual: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteGreaterEqual);
end;

function TRLPascalCoder.WriteLess: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLess);
end;

function TRLPascalCoder.WriteLessEqual: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLessEqual);
end;

function TRLPascalCoder.WritePlus: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WritePlus);
end;

function TRLPascalCoder.WriteMinus: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteMinus);
end;

function TRLPascalCoder.WriteAsterisk: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteAsterisk);
end;

function TRLPascalCoder.WritePercent: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WritePercent);
end;

function TRLPascalCoder.WriteSlash: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteSlash);
end;

function TRLPascalCoder.WriteBackSlash: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteBackSlash);
end;

function TRLPascalCoder.WriteVerticalSlash: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteVerticalSlash);
end;

function TRLPascalCoder.WriteDoubleSlash: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteDoubleSlash);
end;

function TRLPascalCoder.WriteDoubleBackSlash: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteDoubleBackSlash);
end;

function TRLPascalCoder.WriteDoubleVerticalSlash: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteDoubleVerticalSlash);
end;

function TRLPascalCoder.WriteUnderscore: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteUnderscore);
end;

function TRLPascalCoder.WriteCaret: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteCaret);
end;

function TRLPascalCoder.WriteAt: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteAt);
end;

function TRLPascalCoder.WriteAmpersand: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteAmpersand);
end;

function TRLPascalCoder.WriteLeftParenthesis: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLeftParenthesis);
end;

function TRLPascalCoder.WriteRightParenthesis: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteRightParenthesis);
end;

function TRLPascalCoder.WriteLeftBracket: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLeftBracket);
end;

function TRLPascalCoder.WriteRightBracket: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteRightBracket);
end;

function TRLPascalCoder.WriteLeftBrace: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLeftBrace);
end;

function TRLPascalCoder.WriteRightBrace: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteRightBrace);
end;

function TRLPascalCoder.WritePrefix: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WritePrefix);
end;

function TRLPascalCoder.WriteTypePrefix: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteTypePrefix);
end;

function TRLPascalCoder.WriteAttributePrefix: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteAttributePrefix);
end;

function TRLPascalCoder.WriteLocalAttributePrefix: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteLocalAttributePrefix);
end;

function TRLPascalCoder.WriteParameterPrefix: TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteParameterPrefix);
end;

function TRLPascalCoder.WriteTypeIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteTypeIdentifier(AIdentifier));
end;

function TRLPascalCoder.WriteQuotedTypeIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteQuotedTypeIdentifier(AIdentifier));
end;

function TRLPascalCoder.WriteAttributeIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteAttributeIdentifier(AIdentifier));
end;

function TRLPascalCoder.WriteLocalAttributeIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(
    inherited WriteLocalAttributeIdentifier(AIdentifier));
end;

function TRLPascalCoder.WriteParameterIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  Result := TRLPascalCoder(inherited WriteParameterIdentifier(AIdentifier));
end;

function TRLPascalCoder.WriteHeaderLine: TRLPascalCoder;
var
  I: Integer;
begin
  WriteDoubleSlash;
  for I := 1 to RightMargin - 3 do
    WriteEqual;
  Result := Self;
end;

function TRLPascalCoder.WriteHeaderInformation(
  const AText: string): TRLPascalCoder;
begin
  WriteDoubleSlash;
  WriteSpace;
  Write(AText);
  Result := Self;
end;

function TRLPascalCoder.WriteHeader(const ATitle, ADescription,
  ACompanyName: string): TRLPascalCoder;
begin
  WriteHeaderLine;
  WriteLineBreak;
  WriteHeaderInformation(ATitle).WriteLineBreak;
  if ADescription <> '' then
    WriteHeaderInformation(ADescription).WriteLineBreak;
  if ACompanyName <> '' then
    WriteHeaderInformation(ACompanyName).WriteLineBreak;
  WriteHeaderLine;
  Result := Self;
end;

function TRLPascalCoder.WriteUnit: TRLPascalCoder;
begin
  Write('unit');
  Result := Self;
end;

function TRLPascalCoder.WriteInterface: TRLPascalCoder;
begin
  Write('interface');
  Result := Self;
end;

function TRLPascalCoder.WriteImplementation: TRLPascalCoder;
begin
  Write('implementation');
  Result := Self;
end;

function TRLPascalCoder.WriteInitialization: TRLPascalCoder;
begin
  Write('initialization');
  Result := Self;
end;

function TRLPascalCoder.WriteFinalization: TRLPascalCoder;
begin
  Write('finalization');
  Result := Self;
end;

function TRLPascalCoder.WriteUses: TRLPascalCoder;
begin
  Write('uses');
  Result := Self;
end;

function TRLPascalCoder.WriteType: TRLPascalCoder;
begin
  Write('type');
  Result := Self;
end;

function TRLPascalCoder.WriteTypeHeader(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteDoubleSlash;
  WriteSpace;
  WriteTypeIdentifier(AIdentifier);
  Result := Self;
end;

function TRLPascalCoder.WriteTypeCodeHeader(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteLeftBrace.WriteSpace;
  try
    WriteTypeIdentifier(AIdentifier);
  finally
    WriteSpace.WriteRightBrace;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteSet: TRLPascalCoder;
begin
  Write('set');
  Result := Self;
end;

function TRLPascalCoder.WriteOf: TRLPascalCoder;
begin
  Write('of');
  Result := Self;
end;

function TRLPascalCoder.WriteSetOf: TRLPascalCoder;
begin
  WriteSet;
  WriteSpace;
  WriteOf;
  Result := Self;
end;

function TRLPascalCoder.WriteRecord: TRLPascalCoder;
begin
  Write('record');
  Result := Self;
end;

function TRLPascalCoder.WriteRecordPrefix: TRLPascalCoder;
begin
  Write(GetRecordPrefix);
  Result := Self;
end;

function TRLPascalCoder.WriteRecordIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteRecordPrefix;
  WritePrefix;
  Write(AIdentifier);
  Result := Self;
end;

function TRLPascalCoder.WriteQuotedRecordIdentifier(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteQuote;
  try
    WriteRecordIdentifier(AIdentifier);
  finally
    WriteQuote;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteRecordHeader(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteDoubleSlash;
  WriteSpace;
  WriteRecordIdentifier(AIdentifier);
  Result := Self;
end;

function TRLPascalCoder.WriteRecordCodeHeader(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteLeftBrace.WriteSpace;
  try
    WriteRecordIdentifier(AIdentifier);
  finally
    WriteSpace.WriteRightBrace;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteClass: TRLPascalCoder;
begin
  Write('class');
  Result := Self;
end;

function TRLPascalCoder.WritePrivate: TRLPascalCoder;
begin
  Write('private');
  Result := Self;
end;

function TRLPascalCoder.WriteProtected: TRLPascalCoder;
begin
  Write('protected');
  Result := Self;
end;

function TRLPascalCoder.WritePublic: TRLPascalCoder;
begin
  Write('public');
  Result := Self;
end;

function TRLPascalCoder.WritePublished: TRLPascalCoder;
begin
  Write('published');
  Result := Self;
end;

function TRLPascalCoder.WriteProperty: TRLPascalCoder;
begin
  Write('property');
  Result := Self;
end;

function TRLPascalCoder.WriteRead: TRLPascalCoder;
begin
  Write('read');
  Result := Self;
end;

function TRLPascalCoder.WriteWrite: TRLPascalCoder;
begin
  Write('write');
  Result := Self;
end;

function TRLPascalCoder.WriteDefault: TRLPascalCoder;
begin
  Write('default');
  Result := Self;
end;

function TRLPascalCoder.WriteFunction: TRLPascalCoder;
begin
  Write('function');
  Result := Self;
end;

function TRLPascalCoder.WriteProcedure: TRLPascalCoder;
begin
  Write('procedure');
  Result := Self;
end;

function TRLPascalCoder.WriteClassFunction: TRLPascalCoder;
begin
  WriteClass;
  WriteFunction;
  Result := Self;
end;

function TRLPascalCoder.WriteClassProcedure: TRLPascalCoder;
begin
  WriteClass;
  Writefunction;
  Result := Self;
end;

function TRLPascalCoder.WriteConstructor: TRLPascalCoder;
begin
  Write('constructor');
  Result := Self;
end;

function TRLPascalCoder.WriteDestructor: TRLPascalCoder;
begin
  Write('destructor');
  Result := Self;
end;

function TRLPascalCoder.WriteCreate: TRLPascalCoder;
begin
  Write('Create');
  Result := Self;
end;

function TRLPascalCoder.WriteDestroy: TRLPascalCoder;
begin
  Write('Destroy');
  Result := Self;
end;

function TRLPascalCoder.WriteVirtual: TRLPascalCoder;
begin
  Write('virtual');
  Result := Self;
end;

function TRLPascalCoder.WriteDynamic: TRLPascalCoder;
begin
  Write('dynamic');
  Result := Self;
end;

function TRLPascalCoder.WriteAbstract: TRLPascalCoder;
begin
  Write('abstract');
  Result := Self;
end;

function TRLPascalCoder.WriteOverride: TRLPascalCoder;
begin
  Write('override');
  Result := Self;
end;

function TRLPascalCoder.WriteReintroduce: TRLPascalCoder;
begin
  Write('reintroduce');
  Result := Self;
end;

function TRLPascalCoder.WriteOperator: TRLPascalCoder;
begin
  Write('operator');
  Result := Self;
end;

function TRLPascalCoder.WriteClassOperator: TRLPascalCoder;
begin
  WriteClass;
  WriteSpace;
  WriteOperator;
  Result := Self;
end;

function TRLPascalCoder.WriteResourcestring: TRLPascalCoder;
begin
  Write('resourcestring');
  Result := Self;
end;

function TRLPascalCoder.WriteConst: TRLPascalCoder;
begin
  Write('const');
  Result := Self;
end;

function TRLPascalCoder.WriteVar: TRLPascalCoder;
begin
  Write('var');
  Result := Self;
end;

function TRLPascalCoder.WriteOut: TRLPascalCoder;
begin
  Write('out');
  Result := Self;
end;

function TRLPascalCoder.WriteChar: TRLPascalCoder;
begin
  Write('Char');
  Result := Self;
end;

function TRLPascalCoder.WriteString: TRLPascalCoder;
begin
  Write('string');
  Result := Self;
end;

function TRLPascalCoder.WriteInteger: TRLPascalCoder;
begin
  Write('Integer');
  Result := Self;
end;

function TRLPascalCoder.WriteInt64: TRLPascalCoder;
begin
  Write('Int64');
  Result := Self;
end;

function TRLPascalCoder.WriteDouble: TRLPascalCoder;
begin
  Write('Double');
  Result := Self;
end;

function TRLPascalCoder.WriteCurrency: TRLPascalCoder;
begin
  Write('Currency');
  Result := Self;
end;

function TRLPascalCoder.WriteTDate: TRLPascalCoder;
begin
  Write('TDate');
  Result := Self;
end;

function TRLPascalCoder.WriteTTime: TRLPascalCoder;
begin
  Write('TTime');
  Result := Self;
end;

function TRLPascalCoder.WriteTDateTime: TRLPascalCoder;
begin
  Write('TDateTime');
  Result := Self;
end;

function TRLPascalCoder.WriteBoolean: TRLPascalCoder;
begin
  Write('Boolean');
  Result := Self;
end;

function TRLPascalCoder.WriteFalse: TRLPascalCoder;
begin
  Write('False');
  Result := Self;
end;

function TRLPascalCoder.WriteTrue: TRLPascalCoder;
begin
  Write('True');
  Result := Self;
end;

function TRLPascalCoder.WriteBegin: TRLPascalCoder;
begin
  Write(GetBlockBegin);
  Result := Self;
end;

function TRLPascalCoder.WriteEnd: TRLPascalCoder;
begin
  Write(GetBlockEnd);
  Result := Self;
end;

function TRLPascalCoder.WriteInherited: TRLPascalCoder;
begin
  Write('inherited');
  Result := Self;
end;

function TRLPascalCoder.WriteAssignment: TRLPascalCoder;
begin
  Write(':=');
  Result := Self;
end;

function TRLPascalCoder.WriteResult: TRLPascalCoder;
begin
  Write('Result');
  Result := Self;
end;

function TRLPascalCoder.WriteFreeAndNil: TRLPascalCoder;
begin
  Write('FreeAndNil');
  Result := Self;
end;

function TRLPascalCoder.WriteResultDestruction: TRLPascalCoder;
begin
  WriteFreeAndNil;
  WriteLeftParenthesis;
  try
    WriteResult;
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteAttributeDestruction(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteFreeAndNil;
  WriteLeftParenthesis;
  try
    WriteAttributeIdentifier(AIdentifier);
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteLocalAttributeDestruction(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteFreeAndNil;
  WriteLeftParenthesis;
  try
    WriteLocalAttributeIdentifier(AIdentifier);
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteAssigned: TRLPascalCoder;
begin
  Write('Assigned');
  Result := Self;
end;

function TRLPascalCoder.WriteResultChecking: TRLPascalCoder;
begin
  WriteAssigned;
  WriteLeftParenthesis;
  try
    WriteResult;
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteAttributeChecking(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteAssigned;
  WriteLeftParenthesis;
  try
    WriteAttributeIdentifier(AIdentifier);
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteLocalAttributeChecking(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteAssigned;
  WriteLeftParenthesis;
  try
    WriteLocalAttributeIdentifier(AIdentifier);
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteParameterChecking(
  const AIdentifier: string): TRLPascalCoder;
begin
  WriteAssigned;
  WriteLeftParenthesis;
  try
    WriteParameterIdentifier(AIdentifier);
  finally
    WriteRightParenthesis;
  end;

  Result := Self;
end;

function TRLPascalCoder.WriteIf: TRLPascalCoder;
begin
  Write('if');
  Result := Self;
end;

function TRLPascalCoder.WriteThen: TRLPascalCoder;
begin
  Write('then');
  Result := Self;
end;

function TRLPascalCoder.WriteElse: TRLPascalCoder;
begin
  Write('else');
  Result := Self;
end;

function TRLPascalCoder.WriteAnd: TRLPascalCoder;
begin
  Write('and');
  Result := Self;
end;

function TRLPascalCoder.WriteOr: TRLPascalCoder;
begin
  Write('or');
  Result := Self;
end;

function TRLPascalCoder.WriteNot: TRLPascalCoder;
begin
  Write('not');
  Result := Self;
end;

function TRLPascalCoder.WriteIn: TRLPascalCoder;
begin
  Write('in');
  Result := Self;
end;

function TRLPascalCoder.WriteIs: TRLPascalCoder;
begin
  Write('is');
  Result := Self;
end;

function TRLPascalCoder.WriteOn: TRLPascalCoder;
begin
  Write('on');
  Result := Self;
end;

function TRLPascalCoder.WriteNil: TRLPascalCoder;
begin
  Write('nil');
  Result := Self;
end;

function TRLPascalCoder.WriteCase: TRLPascalCoder;
begin
  Write('case');
  Result := Self;
end;

function TRLPascalCoder.WriteWhile: TRLPascalCoder;
begin
  Write('while');
  Result := Self;
end;

function TRLPascalCoder.WriteDo: TRLPascalCoder;
begin
  Write('do');
  Result := Self;
end;

function TRLPascalCoder.WriteFor: TRLPascalCoder;
begin
  Write('for');
  Result := Self;
end;

function TRLPascalCoder.WriteTo: TRLPascalCoder;
begin
  Write('to');
  Result := Self;
end;

function TRLPascalCoder.WriteDownto: TRLPascalCoder;
begin
  Write('downto');
  Result := Self;
end;

function TRLPascalCoder.WriteRepeat: TRLPascalCoder;
begin
  Write('repeat');
  Result := Self;
end;

function TRLPascalCoder.WriteUntil: TRLPascalCoder;
begin
  Write('until');
  Result := Self;
end;

function TRLPascalCoder.WriteMod: TRLPascalCoder;
begin
  Write('mod');
  Result := Self;
end;

function TRLPascalCoder.WriteDiv: TRLPascalCoder;
begin
  Write('div');
  Result := Self;
end;

function TRLPascalCoder.WriteTry: TRLPascalCoder;
begin
  Write('try');
  Result := Self;
end;

function TRLPascalCoder.WriteFinally: TRLPascalCoder;
begin
  Write('finally');
  Result := Self;
end;

function TRLPascalCoder.WriteExcept: TRLPascalCoder;
begin
  Write('except');
  Result := Self;
end;

function TRLPascalCoder.WriteException: TRLPascalCoder;
begin
  Write('Exception');
  Result := Self;
end;

function TRLPascalCoder.WriteRaise: TRLPascalCoder;
begin
  Write('raise');
  Result := Self;
end;

function TRLPascalCoder.WriteProgram: TRLPascalCoder;
begin
  Write('program');
  Result := Self;
end;

function TRLPascalCoder.WritePackage: TRLPascalCoder;
begin
  Write('package');
  Result := Self;
end;

function TRLPascalCoder.WriteRequires: TRLPascalCoder;
begin
  Write('requires');
  Result := Self;
end;

function TRLPascalCoder.WriteContains: TRLPascalCoder;
begin
  Write('contains');
  Result := Self;
end;

function TRLPascalCoder.WriteLibrary: TRLPascalCoder;
begin
  Write('library');
  Result := Self;
end;

function TRLPascalCoder.WriteExports: TRLPascalCoder;
begin
  Write('exports');
  Result := Self;
end;

end.
