//=============================================================================
// U_ClassMappingCG
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ClassMappingCG;

interface

uses
  U_ClassMappingPreparer,
  U_PascalClassMappingGenerator;

implementation

end.
