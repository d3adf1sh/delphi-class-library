//=============================================================================
// U_DLOGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DLOGenerator;

interface

uses
  U_Generator,                                
  U_DLOPreparer;

type
  // TRLDLOGenerator
  TRLDLOGenerator = class(TRLGenerator)
  private
    function GetPreparer: TRLDLOPreparer;
    procedure SetPreparer(const AValue: TRLDLOPreparer);
  public
    property Preparer: TRLDLOPreparer read GetPreparer write SetPreparer;
  end;

implementation

{ TRLDLOGenerator }

function TRLDLOGenerator.GetPreparer: TRLDLOPreparer;
begin
  Result := TRLDLOPreparer(inherited Preparer);
end;

procedure TRLDLOGenerator.SetPreparer(const AValue: TRLDLOPreparer);
begin
  inherited Preparer := AValue;
end;

end.
