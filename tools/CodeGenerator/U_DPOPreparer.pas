//=============================================================================
// U_DPOPreparer
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DPOPreparer;

interface

uses
  Classes,
  U_DBPreparer;

type
  // TRLDPOPreparer
  TRLDPOPreparer = class(TRLDBPreparer)
  private
    FDTOReference: string;
  published
    property DTOReference: string read FDTOReference write FDTOReference;
  end;

implementation

initialization
  RegisterClass(TRLDPOPreparer);

end.
