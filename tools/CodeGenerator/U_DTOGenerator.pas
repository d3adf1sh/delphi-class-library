//=============================================================================
// U_DTOGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DTOGenerator;

interface

uses
  U_Generator,
  U_DTOPreparer;

type
  // TRLDTOGenerator
  TRLDTOGenerator = class(TRLGenerator)
  private
    function GetPreparer: TRLDTOPreparer;
    procedure SetPreparer(const AValue: TRLDTOPreparer);
  public
    property Preparer: TRLDTOPreparer read GetPreparer write SetPreparer;
  end;

implementation

{ TRLDTOGenerator }

function TRLDTOGenerator.GetPreparer: TRLDTOPreparer;
begin
  Result := TRLDTOPreparer(inherited Preparer);
end;

procedure TRLDTOGenerator.SetPreparer(const AValue: TRLDTOPreparer);
begin
  inherited Preparer := AValue;
end;

end.
