//=============================================================================
// U_PascalDTOGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_PascalDTOGenerator;

interface

uses
  SysUtils,
  Classes,
  U_Collection,
  U_KeyedCollection,
  U_Value,
  U_Relations,
  U_Fields,
  U_Keys,
  U_PascalCoder,
  U_DTOGenerator;

const
  CDTO = 'DTO';
  CListOf = 'ListOf';
  CGet = 'Get';
  CMake = 'Make';
  CFind = 'Find';
  CContains = 'Contains';
  CAlternative = 'Alternative';
  CKey = 'Key';
  CValue = 'Value';
  CChanged = 'Changed';
  CMakeKey = CMake + CKey;
  CAlternativeKey = CAlternative + CKey;
  CMakeAlternativeKey = CMake + CAlternativeKey;
  CValueChanged = CValue + CChanged;
  CKeyValueChanged = CKey + CValueChanged;
  CAlternativeKeyValueChanged = CAlternativeKey + CValueChanged;
  CHS = 'HS';
  CVarPrefix = '_';
  CHSVarPrefix = CVarPrefix + CHS;
  CKeySeparator = ';';

type
  // TRLPascalDTOGenerator
  TRLPascalDTOGenerator = class(TRLDTOGenerator)
  private
//intf
    procedure WriteClass(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDestructor(const ACoder: TRLPascalCoder);
    procedure WriteAttributeGetter(const ACoder: TRLPascalCoder; const AField: TRLField);
    procedure WriteKeyValueEvent(const ACoder: TRLPascalCoder; const AKey: TRLKey);
    procedure WriteKeyMaker(const ACoder: TRLPascalCoder; const AKey: TRLKey);
    procedure WriteAttributeCreator(const ACoder: TRLPascalCoder; const AField: TRLField);
    procedure WriteAssignment(const ACoder: TRLPascalCoder);
    procedure WriteCollectionClass(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteCollectionConstructor(const ACoder: TRLPascalCoder);
    procedure WriteItemGetter(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteItemClassGetter(const ACoder: TRLPascalCoder);
    procedure WriteCollectionKeyMaker(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteAddition(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteInsertion(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteFinder(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteChecker(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
//impl
    procedure WriteClassCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDestructorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteAttributeGetterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AField: TRLField);
    procedure WriteKeyValueEventCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteKeyMakerCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteAttributeCreatorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AField: TRLField);
    procedure WriteAssignmentCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteCollectionClassCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteCollectionConstructorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteItemGetterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteItemClassGetterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteCollectionKeyMakerCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteAdditionCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteInsertionCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteFinderCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteCheckerCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
  protected
    procedure Generate(const AStream: TStream); override;
  end;

var
  RLValueTypeToString: array[TRLValueType] of string = ('', 'IntToStr',
    'IntToStr', 'FloatToStr', 'CurrToString', 'DateToStr', 'TimeToStr',
    'DateToStr', 'BoolToStr', '');
    
implementation

uses
  StrUtils,
  U_NTUtils,
  U_CGUtils;

procedure TRLPascalDTOGenerator.WriteClass(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
var
  I: Integer;  
begin
  ACoder.BeginIdentation;
  try
    ACoder.WriteTypeHeader(ARelation.Identifier + CDTO).WriteLineBreak;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSpace;
    ACoder.WriteEqual;
    ACoder.WriteSpace;
    ACoder.WriteClass;
    ACoder.WriteLeftParenthesis;
    try
      if ARelation.Keys.Count > 0 then
        ACoder.Write(TRLKeyedCollectionItem.ClassName)
      else
        ACoder.Write(TRLCollectionItem.ClassName);
    finally
      ACoder.WriteRightParenthesis;
    end;

    ACoder.WriteLineBreak;
    ACoder.WritePrivate.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      for I := 0 to Pred(ARelation.Fields.Count) do
      begin
        ACoder.WriteAttributeIdentifier(ARelation.Fields[I].Identifier);
        ACoder.WriteColon;
        ACoder.WriteSpace;
        ACoder.Write(
          RLValueClass[ARelation.Fields[I].ValueType].ClassName).WriteSemicolon;
        ACoder.WriteLineBreak;
      end;

      for I := 0 to Pred(ARelation.Fields.Count) do
        WriteAttributeGetter(ACoder, ARelation.Fields[I]);
      if Assigned(ARelation.Keys.PrimaryKey) then
        WriteKeyValueEvent(ACoder, ARelation.Keys.PrimaryKey);
      if Assigned(ARelation.Keys.UniqueKey) then
        WriteKeyValueEvent(ACoder, ARelation.Keys.UniqueKey);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteProtected.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      if Assigned(ARelation.Keys.PrimaryKey) then
        WriteKeyMaker(ACoder, ARelation.Keys.PrimaryKey);
      if Assigned(ARelation.Keys.UniqueKey) then
        WriteKeyMaker(ACoder, ARelation.Keys.UniqueKey);
      for I := 0 to Pred(ARelation.Fields.Count) do
        WriteAttributeCreator(ACoder, ARelation.Fields[I]);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WritePublic.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteDestructor(ACoder);
      WriteAssignment(ACoder);
    finally
      ACoder.EndIdendation;
    end;
 
    ACoder.WritePublished.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      for I := 0 to Pred(ARelation.Fields.Count) do
      begin
        ACoder.WriteProperty.WriteSpace;
        ACoder.Write(ARelation.Fields[I].Identifier);
        ACoder.WriteColon;
        ACoder.WriteSpace;
        ACoder.Write(
          RLValueClass[ARelation.Fields[I].ValueType].ClassName).WriteSpace;
        ACoder.WriteRead.WriteSpace;
        ACoder.Write(CGet + ARelation.Fields[I].Identifier).WriteSemicolon;
        ACoder.WriteLineBreak;
      end;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.WriteBlankLine;
end;

procedure TRLPascalDTOGenerator.WriteDestructor(const ACoder: TRLPascalCoder);
begin
  ACoder.WriteDestructor.WriteSpace;
  ACoder.WriteDestroy.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteAttributeGetter(
  const ACoder: TRLPascalCoder; const AField: TRLField);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(CGet + AField.Identifier);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write(RLValueClass[AField.ValueType].ClassName).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteKeyValueEvent(const ACoder: TRLPascalCoder;
  const AKey: TRLKey);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write(
    IfThen(AKey.KeyType = ktPrimary, CKeyValueChanged,
      CAlternativeKeyValueChanged));
  ACoder.WriteLeftParenthesis;
  try
    ACoder.Write('Sender');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TObject.ClassName);
  finally
    ACoder.WriteRightParenthesis;
    ACoder.WriteSemicolon;
  end;

  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteKeyMaker(const ACoder: TRLPascalCoder;
  const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(IfThen(AKey.KeyType = ktPrimary, CMakeKey, CMakeAlternativeKey));
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteString.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteAttributeCreator(
  const ACoder: TRLPascalCoder; const AField: TRLField);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteCreate.Write(AField.Identifier);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write(RLValueClass[AField.ValueType].ClassName).WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteAssignment(const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('Assign');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.Write('Source');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TPersistent.ClassName)
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteCollectionClass(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.BeginIdentation;
  try
    ACoder.WriteTypeHeader(CListOf + ARelation.Identifier +
      CDTO).WriteLineBreak;
    ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier +
      CDTO).WriteSpace;
    ACoder.WriteEqual;
    ACoder.WriteSpace;
    ACoder.WriteClass;
    ACoder.WriteLeftParenthesis;
    try
      if ARelation.Keys.Count > 0 then
        ACoder.Write(TRLKeyedCollection.ClassName)
      else
        ACoder.Write(TRLCollection.ClassName);
    finally
      ACoder.WriteRightParenthesis;
    end;

    ACoder.WriteLineBreak;
    ACoder.WritePrivate.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteItemGetter(ACoder, ARelation);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteProtected.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteItemClassGetter(ACoder);
      if Assigned(ARelation.Keys.PrimaryKey) then
        WriteCollectionKeyMaker(ACoder, ARelation, ARelation.Keys.PrimaryKey);
      if Assigned(ARelation.Keys.UniqueKey) then
        WriteCollectionKeyMaker(ACoder, ARelation, ARelation.Keys.UniqueKey);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WritePublic.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      if ARelation.Keys.Count > 0 then
        WriteCollectionConstructor(ACoder);
      WriteAddition(ACoder, ARelation);
      WriteInsertion(ACoder, ARelation);
      if Assigned(ARelation.Keys.PrimaryKey) then
      begin
        WriteFinder(ACoder, ARelation, ARelation.Keys.PrimaryKey);
        WriteChecker(ACoder, ARelation, ARelation.Keys.PrimaryKey);
      end;

      if Assigned(ARelation.Keys.UniqueKey) then
      begin
        WriteFinder(ACoder, ARelation, ARelation.Keys.UniqueKey);
        WriteChecker(ACoder, ARelation, ARelation.Keys.UniqueKey);
      end;

      ACoder.WriteProperty.WriteSpace;
      ACoder.Write('Items');
      ACoder.WriteLeftBracket;
      try
        ACoder.WriteConst.WriteSpace;
        ACoder.WriteParameterIdentifier('Index');
        ACoder.WriteColon;
        ACoder.WriteSpace;
        ACoder.WriteInteger;
      finally
        ACoder.WriteRightBracket;
      end;

      ACoder.WriteColon;
      ACoder.WriteSpace;
      ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSpace;
      ACoder.WriteRead.WriteSpace;
      ACoder.Write('GetItem').WriteSemicolon;
      ACoder.WriteSpace;
      ACoder.WriteDefault.WriteSemicolon;
      ACoder.WriteLineBreak;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.WriteBlankLine;
end;

procedure TRLPascalDTOGenerator.WriteCollectionConstructor(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteConstructor.WriteSpace;
  ACoder.Write('CreateCollection');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier('Parent');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TObject.ClassName);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteItemGetter(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('GetItem');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier('Index');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteInteger;
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteItemClassGetter(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('GetItemClass');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write('TCollectionItemClass').WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteCollectionKeyMaker(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(IfThen(AKey.KeyType = ktPrimary, CMakeKey, CMakeAlternativeKey));
  ACoder.WriteLeftParenthesis;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteString.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteAddition(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('Add');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteInsertion(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('Insert');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier('Index');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteInteger;
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteFinder(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation; const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(
    IfThen(AKey.KeyType = ktPrimary, CFind, CFind + AKey.TargetIdentifier));
  ACoder.WriteLeftParenthesis;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteChecker(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation; const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(
    IfThen(AKey.KeyType = ktPrimary, CContains,
      CContains + AKey.TargetIdentifier));
  ACoder.WriteLeftParenthesis;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteBoolean.WriteSemicolon;
  ACoder.WriteSpace;
  if AKey.KeyType = ktPrimary then
  begin
    ACoder.WriteReintroduce.WriteSemicolon;
    ACoder.WriteSpace;
  end;
  
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDTOGenerator.WriteClassCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
var
  I: Integer;
begin
  ACoder.WriteTypeCodeHeader(ARelation.Identifier + CDTO).WriteBlankLine;
  WriteDestructorCode(ACoder, ARelation);
  for I := 0 to Pred(ARelation.Fields.Count) do
    WriteAttributeGetterCode(ACoder, ARelation, ARelation.Fields[I]);
  if Assigned(ARelation.Keys.PrimaryKey) then
  begin
    WriteKeyValueEventCode(ACoder, ARelation, ARelation.Keys.PrimaryKey);
    WriteKeyMakerCode(ACoder, ARelation, ARelation.Keys.PrimaryKey);
  end;

  if Assigned(ARelation.Keys.UniqueKey) then
  begin
    WriteKeyValueEventCode(ACoder, ARelation, ARelation.Keys.UniqueKey);
    WriteKeyMakerCode(ACoder, ARelation, ARelation.Keys.UniqueKey);
  end;

  for I := 0 to Pred(ARelation.Fields.Count) do
    WriteAttributeCreatorCode(ACoder, ARelation, ARelation.Fields[I]);
  WriteAssignmentCode(ACoder, ARelation);
end;

procedure TRLPascalDTOGenerator.WriteDestructorCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
var
  I: Integer;
begin
  ACoder.WriteDestructor.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteDot;
  ACoder.WriteDestroy.WriteSemicolon;
  ACoder.BeginBlock;
  try
    for I := Pred(ARelation.Fields.Count) downto 0 do
    begin
      ACoder.WriteIf.WriteSpace;
      ACoder.WriteAttributeChecking(ARelation.Fields[I].Identifier).WriteSpace;
      ACoder.WriteThen.WriteLineBreak;
      ACoder.BeginIdentation;
      try
        ACoder.WriteAttributeDestruction(
          ARelation.Fields[I].Identifier).WriteSemicolon;
      finally
        ACoder.EndIdendation;
      end;

      ACoder.WriteLineBreak;
    end;

    ACoder.WriteInherited.WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteAttributeGetterCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AField: TRLField);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write(CGet + AField.Identifier);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write(RLValueClass[AField.ValueType].ClassName).WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteIf.WriteSpace;
    ACoder.WriteNot.WriteSpace;
    ACoder.WriteAttributeChecking(AField.Identifier).WriteSpace;
    ACoder.WriteThen.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      ACoder.WriteAttributeIdentifier(AField.Identifier).WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.WriteCreate.Write(AField.Identifier).WriteSemicolon;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteLineBreak;
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteAttributeIdentifier(AField.Identifier).WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteKeyValueEventCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AKey: TRLKey);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write(
    IfThen(AKey.KeyType = ktPrimary, CKeyValueChanged,
      CAlternativeKeyValueChanged));
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.Write('Sender');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TObject.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.Write(
      IfThen(AKey.KeyType = ktPrimary, 'UpdateKey',
        'UpdateAlternativeKey')).WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteKeyMakerCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation; const AKey: TRLKey);
var
  lField: TRLField;
  bInCache: Boolean;
  I: Integer;
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write(IfThen(AKey.KeyType = ktPrimary, CMakeKey, CMakeAlternativeKey));
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteString.WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.BeginAutoLineBreak;
    try
      for I := 0 to Pred(AKey.Fields.Count) do
      begin
        lField := ARelation.Fields.Find(AKey.Fields[I].Name);
        if Assigned(lField) then
        begin
          ACoder.Write(lField.Identifier).WriteDot;
          ACoder.Write('AsString');
          if I < Pred(AKey.Fields.Count) then
          begin
            ACoder.WriteSpace;
            ACoder.WritePlus;
            bInCache := ACoder.InCache;
            if bInCache then
              ACoder.EndCache;
            ACoder.WriteSpace;
            if bInCache then
              ACoder.BeginCache;
            ACoder.WriteQuotedText(CKeySeparator).WriteSpace;
            ACoder.WritePlus;
            bInCache := ACoder.InCache;
            if bInCache then
              ACoder.EndCache;
            ACoder.WriteSpace;
            if bInCache then
              ACoder.BeginCache;
          end;
        end;
      end;

      ACoder.WriteSemicolon;
    finally
      ACoder.EndAutoLineBreak;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteAttributeCreatorCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AField: TRLField);
var
  lKey: TRLKey;  
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteDot;
  ACoder.WriteCreate.Write(AField.Identifier);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write(RLValueClass[AField.ValueType].ClassName).WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(RLValueClass[AField.ValueType].ClassName).WriteDot;
    ACoder.WriteCreate.WriteSemicolon;
    lKey := nil;
    if Assigned(ARelation.Keys.PrimaryKey) and
      ARelation.Keys.PrimaryKey.Fields.Contains(AField.Name) then
      lKey := ARelation.Keys.PrimaryKey
    else
      if Assigned(ARelation.Keys.UniqueKey) and
        ARelation.Keys.UniqueKey.Fields.Contains(AField.Name) then
        lKey := ARelation.Keys.UniqueKey;
    if Assigned(lKey) then
    begin
      ACoder.WriteLineBreak;
      ACoder.WriteResult.WriteDot;
      ACoder.Write('OnChange').WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.Write(
        IfThen(lKey.KeyType = ktPrimary, CKeyValueChanged,
          CAlternativeKeyValueChanged)).WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteAssignmentCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
var
  I: Integer;
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write('Assign');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.Write('Source');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TPersistent.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.WriteLineBreak;
  ACoder.WriteVar.WriteLineBreak;
  ACoder.BeginIdentation;
  try
    ACoder.WriteLocalAttributeIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteIf.WriteSpace;
    ACoder.Write('Source').WriteSpace;
    ACoder.WriteIs.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSpace;
    ACoder.WriteThen;
    ACoder.InsertLineAfterEndBlock := False;
    ACoder.BeginBlock;
    try
      ACoder.WriteLocalAttributeIdentifier(ARelation.Identifier).WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
      ACoder.WriteLeftParenthesis;
      try
        ACoder.Write('Source');
      finally
        ACoder.WriteRightParenthesis;
        ACoder.WriteSemicolon;
      end;

      for I := 0 to Pred(ARelation.Fields.Count) do
      begin
        ACoder.WriteLineBreak;
        ACoder.Write(ARelation.Fields[I].Identifier).WriteDot;
        ACoder.Write('Assign');
        ACoder.WriteLeftParenthesis;
        try
          ACoder.WriteLocalAttributeIdentifier(ARelation.Identifier).WriteDot;
          ACoder.Write(ARelation.Fields[I].Identifier);
        finally
          ACoder.WriteRightParenthesis;
          ACoder.WriteSemicolon;
        end;
      end;
    finally
      ACoder.EndBlock;
      ACoder.InsertLineAfterEndBlock := True;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteCollectionClassCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteLeftBrace.WriteSpace;
  try
    ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO);
  finally
    ACoder.WriteSpace.WriteRightBrace;
  end;

  ACoder.WriteBlankLine;
  if ARelation.Keys.Count > 0 then
    WriteCollectionConstructorCode(ACoder, ARelation);
  WriteItemGetterCode(ACoder, ARelation);
  WriteItemClassGetterCode(ACoder, ARelation);
  if Assigned(ARelation.Keys.PrimaryKey) then
    WriteCollectionKeyMakerCode(ACoder, ARelation, ARelation.Keys.PrimaryKey);
  if Assigned(ARelation.Keys.UniqueKey) then
    WriteCollectionKeyMakerCode(ACoder, ARelation, ARelation.Keys.UniqueKey);
  WriteAdditionCode(ACoder, ARelation);
  WriteInsertionCode(ACoder, ARelation);
  if Assigned(ARelation.Keys.PrimaryKey) then
  begin
    WriteFinderCode(ACoder, ARelation, ARelation.Keys.PrimaryKey);
    WriteCheckerCode(ACoder, ARelation, ARelation.Keys.PrimaryKey);
  end;

  if Assigned(ARelation.Keys.UniqueKey) then
  begin
    WriteFinderCode(ACoder, ARelation, ARelation.Keys.UniqueKey);
    WriteCheckerCode(ACoder, ARelation, ARelation.Keys.UniqueKey);
  end;
end;

procedure TRLPascalDTOGenerator.WriteCollectionConstructorCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteConstructor.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write('CreateCollection');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier('Parent');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TObject.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteInherited.WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteIf.WriteSpace;
    ACoder.Write(CHSVarPrefix + ARelation.Identifier).WriteSpace;
    ACoder.WriteGreater.WriteSpace;
    ACoder.Write(#48).WriteSpace;
    ACoder.WriteThen.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      ACoder.Write('HashSize').WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.Write(CHSVarPrefix + ARelation.Identifier).WriteSemicolon;
    finally
      ACoder.EndIdendation;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteItemGetterCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write('GetItem');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier('Index');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteInteger;
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteInherited.WriteSpace;
      ACoder.Write('Items');
      ACoder.WriteLeftBracket;
      try
        ACoder.WriteParameterIdentifier('Index');
      finally
        ACoder.WriteRightBracket;
      end;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteItemClassGetterCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write('GetItemClass');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write('TCollectionItemClass').WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteCollectionKeyMakerCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AKey: TRLKey);
var
  lField: TRLField;
  bInCache: Boolean;
  I: Integer;
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write(IfThen(AKey.KeyType = ktPrimary, CMakeKey, CMakeAlternativeKey));
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteString.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.BeginAutoLineBreak;
    try
      for I := 0 to Pred(AKey.Fields.Count) do
      begin
        lField := ARelation.Fields.Find(AKey.Fields[I].Name);
        if Assigned(lField) then
        begin
          if RLValueTypeToString[lField.ValueType] <> '' then
          begin
            ACoder.Write(RLValueTypeToString[lField.ValueType]);
            ACoder.WriteLeftParenthesis;
          end;

          try
            ACoder.WriteParameterIdentifier(lField.Identifier);
          finally
            if RLValueTypeToString[lField.ValueType] <> '' then
              ACoder.WriteRightParenthesis;
          end;

          if I < Pred(AKey.Fields.Count) then
          begin
            ACoder.WriteSpace;
            ACoder.WritePlus;
            bInCache := ACoder.InCache;
            if bInCache then
              ACoder.EndCache;
            ACoder.WriteSpace;
            if bInCache then
              ACoder.BeginCache;
            ACoder.WriteQuotedText(CKeySeparator).WriteSpace;
            ACoder.WritePlus;
            bInCache := ACoder.InCache;
            if bInCache then
              ACoder.EndCache;
            ACoder.WriteSpace;
            if bInCache then
              ACoder.BeginCache;
          end;
        end;
      end;

      ACoder.WriteSemicolon;
    finally
      ACoder.EndAutoLineBreak;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteAdditionCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write('Add');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteInherited.WriteSpace;
      ACoder.Write('Add');
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteInsertionCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write('Insert');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier('Index');
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteInteger;
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteInherited.WriteSpace;
      ACoder.Write('Insert');
      ACoder.WriteLeftParenthesis;
      try
        ACoder.WriteParameterIdentifier('Index');
      finally
        ACoder.WriteRightParenthesis;
      end;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteFinderCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation; const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write(
    IfThen(AKey.KeyType = ktPrimary, CFind, CFind + AKey.TargetIdentifier));
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.Write(
        IfThen(AKey.KeyType = ktPrimary, 'FindHash', 'FindAlternativeHash'));
      ACoder.WriteLeftParenthesis; //(
      ACoder.BeginAutoLineBreak;
      try
        ACoder.Write(
          IfThen(AKey.KeyType = ktPrimary, CMakeKey, CMakeAlternativeKey));
        ACoder.WriteLeftParenthesis;
        try
          TRLCGUtils.WritePascalParameterNames(ACoder, ARelation, AKey.Fields);
        finally
          ACoder.WriteRightParenthesis;
        end;

        ACoder.WriteRightParenthesis; //)
      finally
        ACoder.EndAutoLineBreak;
      end;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.WriteCheckerCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation; const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO).WriteDot;
  ACoder.Write(
    IfThen(AKey.KeyType = ktPrimary, CContains,
      CContains + AKey.TargetIdentifier));
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteBoolean.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteAssigned;
    ACoder.WriteLeftParenthesis; //(
    ACoder.BeginAutoLineBreak;
    try
      ACoder.Write(
        IfThen(AKey.KeyType = ktPrimary, CFind, CFind + AKey.TargetIdentifier));
      ACoder.WriteLeftParenthesis;
      try
        TRLCGUtils.WritePascalParameterNames(ACoder, ARelation, AKey.Fields);
      finally
        ACoder.WriteRightParenthesis;
      end;

      ACoder.WriteRightParenthesis; //)
      ACoder.WriteSemicolon;
    finally
      ACoder.EndAutoLineBreak;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDTOGenerator.Generate(const AStream: TStream);
var
  lCoder: TRLPascalCoder;
  S: Integer;
  R: Integer;
begin
  lCoder := TRLPascalCoder.Create;
  try
    lCoder.Stream := AStream;
    lCoder.WriteHeader(Preparer.Title, Preparer.ApplicationName,
      Preparer.CompanyName);
    lCoder.WriteBlankLine;
    lCoder.WriteUnit.WriteSpace;
    lCoder.Write(
      TRLNTUtils.GetNameOfFileWithoutExtension(FileName)).WriteSemicolon;
    lCoder.WriteBlankLine;
    lCoder.WriteInterface;
    lCoder.WriteBlankLine;
    lCoder.WriteUses.WriteLineBreak;
    lCoder.BeginIdentation;
    try
      lCoder.Write('SysUtils').WriteComma.WriteLineBreak;
      lCoder.Write('Classes').WriteComma.WriteLineBreak;
      lCoder.Write('Controls').WriteComma.WriteLineBreak;
      lCoder.Write('U_Collection').WriteComma.WriteLineBreak;
      lCoder.Write('U_KeyedCollection').WriteComma.WriteLineBreak;
      lCoder.Write('U_Value').WriteSemicolon;
    finally
      lCoder.EndIdendation;
    end;

    lCoder.WriteBlankLine;
    lCoder.WriteType.WriteLineBreak;
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
      begin
        WriteClass(lCoder, Preparer.Schemas[S].Relations[R]);
        WriteCollectionClass(lCoder, Preparer.Schemas[S].Relations[R]);
      end;

    lCoder.WriteVar;
    lCoder.BeginIdentation;
    try
      for S := 0 to Pred(Preparer.Schemas.Count) do
        for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
        begin
          lCoder.WriteLineBreak;
          lCoder.Write(
            CHSVarPrefix + Preparer.Schemas[S].Relations[R].Identifier);
          lCoder.WriteColon;
          lCoder.WriteSpace;
          lCoder.WriteInteger.WriteSpace;
          lCoder.WriteEqual;
          lCoder.WriteSpace;
          lCoder.Write(#48).WriteSemicolon;
        end;
    finally
      lCoder.EndIdendation;
    end;

    lCoder.WriteBlankLine;
    lCoder.WriteImplementation;
    lCoder.WriteBlankLine;
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
      begin
        WriteClassCode(lCoder, Preparer.Schemas[S].Relations[R]);
        WriteCollectionClassCode(lCoder, Preparer.Schemas[S].Relations[R]);
      end;

    lCoder.WriteEnd.WriteDot;
  finally
    FreeAndNil(lCoder);
  end;
end;

initialization
  RegisterClass(TRLPascalDTOGenerator);

end.
