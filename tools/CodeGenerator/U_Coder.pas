//=============================================================================
// U_Coder
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Coder;

interface

uses
  SysUtils,
  Classes,
  U_BaseType;

type
  // TRLCoder
  TRLCoder = class(TRLBaseType)
  private
    FStream: TStream;
    FBlockIdentation: Integer;
    FRightMargin: Integer;
    FPrefix: string;
    FBreakLineBeforeBeginBlock: Boolean;
    FInsertLineAfterEndBlock: Boolean;
    FCacheCount: Integer;
    FCache: string;
    FBlockCount: Integer;
    FIdentationCount: Integer;
    FAutoLineBreak: Boolean;
    FBreakedLine: Boolean;
    FLineSize: Integer;
    FLastChar: Char;
    FFreeStream: Boolean;
    function GetStream: TStream;
    procedure SetStream(const AValue: TStream);
  protected
    function CreateStream: TStream; virtual;
    function CanBreakLine(const AChar: Char): Boolean; virtual; abstract;
    function GetTypePrefix: string; virtual; abstract;
    function GetAttributePrefix: string; virtual; abstract;
    function GetLocalAttributePrefix: string; virtual; abstract;
    function GetParameterPrefix: string; virtual; abstract;
    function GetBlockBegin: string; virtual; abstract;
    function GetBlockEnd: string; virtual; abstract;
    procedure WriteBlockBegin; virtual;
    procedure WriteBlockEnd; virtual;
    property CacheCount: Integer read FCacheCount;
    property Cache: string read FCache;
    property BlockCount: Integer read FBlockCount;
    property IdentationCount: Integer read FIdentationCount;
    property AutoLineBreak: Boolean read FAutoLineBreak;
    property BreakedLine: Boolean read FBreakedLine;
    property LineSize: Integer read FLineSize;
    property LastChar: Char read FLastChar;
  public
    constructor Create; override;
    destructor Destroy; override;
    function Write(const AText: string): TRLCoder; virtual;
    function WriteSpace: TRLCoder; virtual;
    function WriteLineBreak: TRLCoder; virtual;
    function WriteBlankLine: TRLCoder; virtual;
    function WriteDot: TRLCoder; virtual;
    function WriteComma: TRLCoder; virtual;
    function WriteColon: TRLCoder; virtual;
    function WriteSemicolon: TRLCoder; virtual;
    function WriteQuote: TRLCoder; virtual;
    function WriteDoubleQuote: TRLCoder; virtual;
    function WriteQuotedText(const AText: string): TRLCoder; virtual;
    function WriteEqual: TRLCoder; virtual;
    function WriteGreater: TRLCoder; virtual;
    function WriteGreaterEqual: TRLCoder; virtual;
    function WriteLess: TRLCoder; virtual;
    function WriteLessEqual: TRLCoder; virtual;
    function WritePlus: TRLCoder; virtual;
    function WriteMinus: TRLCoder; virtual;
    function WriteAsterisk: TRLCoder; virtual;
    function WritePercent: TRLCoder; virtual;
    function WriteSlash: TRLCoder; virtual;
    function WriteBackSlash: TRLCoder; virtual;
    function WriteVerticalSlash: TRLCoder; virtual;
    function WriteDoubleSlash: TRLCoder; virtual;
    function WriteDoubleBackSlash: TRLCoder; virtual;
    function WriteDoubleVerticalSlash: TRLCoder; virtual;
    function WriteUnderscore: TRLCoder; virtual;
    function WriteCaret: TRLCoder; virtual;
    function WriteAt: TRLCoder; virtual;
    function WriteAmpersand: TRLCoder; virtual;
    function WriteLeftParenthesis: TRLCoder; virtual;
    function WriteRightParenthesis: TRLCoder; virtual;
    function WriteLeftBracket: TRLCoder; virtual;
    function WriteRightBracket: TRLCoder; virtual;
    function WriteLeftBrace: TRLCoder; virtual;
    function WriteRightBrace: TRLCoder; virtual;
    function WritePrefix: TRLCoder; virtual;
    function WriteTypePrefix: TRLCoder; virtual;
    function WriteAttributePrefix: TRLCoder; virtual;
    function WriteLocalAttributePrefix: TRLCoder; virtual;
    function WriteParameterPrefix: TRLCoder; virtual;
    function WriteTypeIdentifier(const AIdentifier: string): TRLCoder; virtual;
    function WriteQuotedTypeIdentifier(const AIdentifier: string): TRLCoder; virtual;
    function WriteAttributeIdentifier(const AIdentifier: string): TRLCoder; virtual;
    function WriteLocalAttributeIdentifier(const AIdentifier: string): TRLCoder; virtual;
    function WriteParameterIdentifier(const AIdentifier: string): TRLCoder; virtual;
    procedure BeginCache; virtual;
    procedure EndCache; virtual;
    function InCache: Boolean; virtual;
    procedure BeginBlock; overload; virtual;
    procedure BeginBlock(const ABreakLineBefore: Boolean); overload; virtual;
    procedure EndBlock; overload; virtual;
    procedure EndBlock(const AInsertLineAfter: Boolean); overload; virtual;
    function InBlock: Boolean; virtual;
    procedure BeginIdentation; virtual;
    procedure EndIdendation; virtual;
    function InIndendation: Boolean; virtual;
    procedure BeginAutoLineBreak; virtual;
    procedure EndAutoLineBreak; virtual;
    function IsEmpty: Boolean; virtual;
    procedure Clear; virtual;
    property Stream: TStream read GetStream write SetStream;
    property BlockIdentation: Integer read FBlockIdentation write FBlockIdentation;
    property RightMargin: Integer read FRightMargin write FRightMargin;
    property Prefix: string read FPrefix write FPrefix;
    property BreakLineBeforeBeginBlock: Boolean read FBreakLineBeforeBeginBlock write FBreakLineBeforeBeginBlock;
    property InsertLineAfterEndBlock: Boolean read FInsertLineAfterEndBlock write FInsertLineAfterEndBlock;
  end;

implementation

uses
  U_System;

{ TRLCoder }

constructor TRLCoder.Create;
begin
  inherited;
  FBlockIdentation := 2;
  FRightMargin := 80;
  FPrefix := 'RL';
  FBreakLineBeforeBeginBlock := True;
  FInsertLineAfterEndBlock := True;
  Clear;
end;

destructor TRLCoder.Destroy;
begin
  if Assigned(FStream) and FFreeStream then
    FreeAndNil(FStream);
  inherited;
end;

function TRLCoder.GetStream: TStream;
begin
  if not Assigned(FStream) then
  begin
    FStream := CreateStream;
    FFreeStream := True;
  end;

  Result := FStream;
end;

procedure TRLCoder.SetStream(const AValue: TStream);
begin
  if AValue <> FStream then
  begin
    if Assigned(FStream) and FFreeStream then
    begin
      FreeAndNil(FStream);
      FFreeStream := False;
    end;

    FStream := AValue;
  end;
end;

function TRLCoder.CreateStream: TStream;
begin
  Result := TMemoryStream.Create;
end;

procedure TRLCoder.WriteBlockBegin;
begin
  Write(GetBlockBegin);
end;

procedure TRLCoder.WriteBlockEnd;
begin
  Write(GetBlockEnd);
end;

function TRLCoder.Write(const AText: string): TRLCoder;
var
  sText: string;
  bFound: Boolean;
  I: Integer;
begin
  if FAutoLineBreak and (FCacheCount = 0) then
  begin
    if (FLineSize + Length(AText) > FRightMargin) and (Trim(AText) <> '') and
       ((FLastChar = #0) or CanBreakLine(FLastChar)) then
    begin
      WriteLineBreak;
      if not FBreakedLine then
      begin
        BeginIdentation;
        FBreakedLine := True;
      end;
    end;
  end;

  if FLineSize = 0 then
    sText := StringOfChar(' ', FIdentationCount * FBlockIdentation) + AText
  else
    sText := AText;
  if FCacheCount = 0 then
  begin
    U_System.WriteStream(Stream, sText);
    FLineSize := FLineSize + Length(sText);
  end
  else
    FCache := FCache + sText;
  if FAutoLineBreak and (FCacheCount = 0) then
  begin
    I := Length(sText);
    bFound := False;
    while (I > 0) and not bFound do
      if sText[I] <> ' ' then
      begin
        FLastChar := sText[I];
        bFound := True;
      end
      else
        Dec(I);
  end;

  Result := Self;
end;

function TRLCoder.WriteSpace: TRLCoder;
begin
  Write(' ');
  Result := Self;
end;

function TRLCoder.WriteLineBreak: TRLCoder;
begin
  Write(sLineBreak);
  FLineSize := 0;
  FLastChar := #0;
  Result := Self;
end;

function TRLCoder.WriteBlankLine: TRLCoder;
begin
  WriteLineBreak;
  WriteLineBreak;
  Result := Self;
end;

function TRLCoder.WriteDot: TRLCoder;
begin
  Write('.');
  Result := Self;
end;

function TRLCoder.WriteComma: TRLCoder;
begin
  Write(',');
  Result := Self;
end;

function TRLCoder.WriteColon: TRLCoder;
begin
  Write(':');
  Result := Self;
end;

function TRLCoder.WriteSemicolon: TRLCoder;
begin
  Write(';');
  Result := Self;
end;

function TRLCoder.WriteQuote: TRLCoder;
begin
  Write('''');
  Result := Self;
end;

function TRLCoder.WriteDoubleQuote: TRLCoder;
begin
  Write('"');
  Result := Self;
end;

function TRLCoder.WriteQuotedText(const AText: string): TRLCoder;
begin
  WriteQuote;
  try
    Write(AText);
  finally
    WriteQuote;
  end;

  Result := Self;
end;

function TRLCoder.WriteEqual: TRLCoder;
begin
  Write('=');
  Result := Self;
end;

function TRLCoder.WriteGreater: TRLCoder;
begin
  Write('>');
  Result := Self;
end;

function TRLCoder.WriteGreaterEqual: TRLCoder;
begin
  WriteGreater;
  WriteEqual;
  Result := Self;
end;

function TRLCoder.WriteLess: TRLCoder;
begin
  Write('<');
  Result := Self;
end;

function TRLCoder.WriteLessEqual: TRLCoder;
begin
  WriteLess;
  WriteEqual;
  Result := Self;
end;

function TRLCoder.WritePlus: TRLCoder;
begin
  Write('+');
  Result := Self;
end;

function TRLCoder.WriteMinus: TRLCoder;
begin
  Write('-');
  Result := Self;
end;

function TRLCoder.WriteAsterisk: TRLCoder;
begin
  Write('*');
  Result := Self;
end;

function TRLCoder.WritePercent: TRLCoder;
begin
  Write('%');
  Result := Self;
end;

function TRLCoder.WriteSlash: TRLCoder;
begin
  Write('/');
  Result := Self;
end;

function TRLCoder.WriteBackSlash: TRLCoder;
begin
  Write('\');
  Result := Self;
end;

function TRLCoder.WriteVerticalSlash: TRLCoder;
begin
  Write('|');
  Result := Self;
end;

function TRLCoder.WriteDoubleSlash: TRLCoder;
begin
  WriteSlash;
  WriteSlash;
  Result := Self;
end;

function TRLCoder.WriteDoubleBackSlash: TRLCoder;
begin
  WriteBackSlash;
  WriteBackSlash;
  Result := Self;
end;

function TRLCoder.WriteDoubleVerticalSlash: TRLCoder;
begin
  WriteVerticalSlash;
  WriteVerticalSlash;
  Result := Self;
end;

function TRLCoder.WriteUnderscore: TRLCoder;
begin
  Write('_');
  Result := Self;
end;

function TRLCoder.WriteCaret: TRLCoder;
begin
  Write('^');
  Result := Self;
end;

function TRLCoder.WriteAt: TRLCoder;
begin
  Write('@');
  Result := Self;
end;

function TRLCoder.WriteAmpersand: TRLCoder;
begin
  Write('&');
  Result := Self;
end;

function TRLCoder.WriteLeftParenthesis: TRLCoder;
begin
  Write('(');
  Result := Self;
end;

function TRLCoder.WriteRightParenthesis: TRLCoder;
begin
  Write(')');
  Result := Self;
end;

function TRLCoder.WriteLeftBracket: TRLCoder;
begin
  Write('[');
  Result := Self;
end;

function TRLCoder.WriteRightBracket: TRLCoder;
begin
  Write(']');
  Result := Self;
end;

function TRLCoder.WriteLeftBrace: TRLCoder;
begin
  Write('{');
  Result := Self;
end;

function TRLCoder.WriteRightBrace: TRLCoder;
begin
  Write('}');
  Result := Self;
end;

function TRLCoder.WritePrefix: TRLCoder;
begin
  Write(FPrefix);
  Result := Self;
end;

function TRLCoder.WriteTypePrefix: TRLCoder;
begin
  Write(GetTypePrefix);
  Result := Self;
end;

function TRLCoder.WriteAttributePrefix: TRLCoder;
begin
  Write(GetAttributePrefix);
  Result := Self;
end;

function TRLCoder.WriteLocalAttributePrefix: TRLCoder;
begin
  Write(GetLocalAttributePrefix);
  Result := Self;
end;

function TRLCoder.WriteParameterPrefix: TRLCoder;
begin
  Write(GetParameterPrefix);
  Result := Self;
end;

function TRLCoder.WriteTypeIdentifier(const AIdentifier: string): TRLCoder;
begin
  WriteTypePrefix;
  WritePrefix;
  Write(AIdentifier);
  Result := Self;
end;

function TRLCoder.WriteQuotedTypeIdentifier(
  const AIdentifier: string): TRLCoder;
begin
  WriteQuote;
  try
    WriteTypeIdentifier(AIdentifier);
  finally
    WriteQuote;
  end;

  Result := Self;
end;

function TRLCoder.WriteAttributeIdentifier(const AIdentifier: string): TRLCoder;
begin
  WriteAttributePrefix;
  Write(AIdentifier);
  Result := Self;
end;

function TRLCoder.WriteLocalAttributeIdentifier(
  const AIdentifier: string): TRLCoder;
begin
  WriteLocalAttributePrefix;
  Write(AIdentifier);
  Result := Self;
end;

function TRLCoder.WriteParameterIdentifier(const AIdentifier: string): TRLCoder;
begin
  WriteParameterPrefix;
  Write(AIdentifier);
  Result := Self;
end;

procedure TRLCoder.BeginCache;
begin
  Inc(FCacheCount);
end;

procedure TRLCoder.EndCache;
begin
  if FCacheCount > 0 then
  begin
    Dec(FCacheCount);
    if FCacheCount = 0 then
    begin
      Write(FCache);
      FCache := '';
    end;
  end;
end;

function TRLCoder.InCache: Boolean;
begin
  Result := FCacheCount > 0;
end;

procedure TRLCoder.BeginBlock;
begin
  BeginBlock(FBreakLineBeforeBeginBlock);
end;

procedure TRLCoder.BeginBlock(const ABreakLineBefore: Boolean);
begin
  if ABreakLineBefore then
    WriteLineBreak;
  WriteBlockBegin;
  WriteLineBreak;
  BeginIdentation;
  Inc(FBlockCount);
end;

procedure TRLCoder.EndBlock;
begin
  EndBlock(FInsertLineAfterEndBlock);
end;

procedure TRLCoder.EndBlock(const AInsertLineAfter: Boolean);
begin
  if FBlockCount > 0 then
  begin
    EndIdendation;
    WriteLineBreak;
    WriteBlockEnd;
    if AInsertLineAfter then
      WriteBlankLine;
    Dec(FBlockCount);
  end;
end;

function TRLCoder.InBlock: Boolean;
begin
  Result := FBlockCount > 0;
end;

procedure TRLCoder.BeginIdentation;
begin
  Inc(FIdentationCount);
end;

procedure TRLCoder.EndIdendation;
begin
  if FIdentationCount > 0 then
    Dec(FIdentationCount);
end;

function TRLCoder.InIndendation: Boolean;
begin
  Result := FIdentationCount > 0;
end;

procedure TRLCoder.BeginAutoLineBreak;
begin
  if not FAutoLineBreak then
  begin
    BeginCache;
    FAutoLineBreak := True;
  end;
end;

procedure TRLCoder.EndAutoLineBreak;
begin
  if FAutoLineBreak then
  begin
    EndCache;
    FAutoLineBreak := False;
    if FBreakedLine then
    begin
      EndIdendation;
      FBreakedLine := False;
    end;
  end;
end;

function TRLCoder.IsEmpty: Boolean;
begin
  Result := Stream.Size = 0;
end;

procedure TRLCoder.Clear;
begin
  Stream.Size := 0;
  FCacheCount := 0;
  FCache := '';
  FBlockCount := 0;
  FIdentationCount := 0;
  FAutoLineBreak := False;
  FBreakedLine := False;
  FLineSize := 0;
  FLastChar := #0;
end;

end.
