//=============================================================================
// U_CGUtils
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_CGUtils;

interface

uses
  U_BaseType,
  U_Value,
  U_NamedCollection,
  U_Fields,
  U_Relations,
  U_PascalCoder;

type
  // TRLCGUtils
  TRLCGUtils = class(TRLBaseType)
  public
    class procedure WritePascalParameters(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection);
    class procedure WritePascalParameterNames(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection);
  end;

var
  RLValueTypeName: array[TRLValueType] of string = ('string', 'Integer',
    'Int64', 'Double', 'Currency', 'TDate', 'TTime', 'TDateTime', 'Boolean',
    'TStream');

implementation

{ TRLCGUtils }

class procedure TRLCGUtils.WritePascalParameters(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation; const AAttributes: TRLNamedCollection);
var
  lField: TRLField;
  bInCache: Boolean;
  I: Integer;
begin            
  if Assigned(ACoder) and Assigned(ARelation) and Assigned(AAttributes) then
    for I := 0 to Pred(AAttributes.Count) do
    begin
      lField := ARelation.Fields.Find(AAttributes[I].Name);
      if Assigned(lField) then
      begin
        ACoder.WriteConst.WriteSpace;
        ACoder.WriteParameterIdentifier(lField.Identifier);
        ACoder.WriteColon;
        ACoder.WriteSpace;
        ACoder.Write(RLValueTypeName[lField.ValueType]);
        if I < Pred(AAttributes.Count) then
        begin
          ACoder.WriteSemicolon;
          bInCache := ACoder.InCache;
          if bInCache then
            ACoder.EndCache;
          ACoder.WriteSpace;
          if bInCache then
            ACoder.BeginCache;
        end;
      end;
    end;
end;

class procedure TRLCGUtils.WritePascalParameterNames(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AAttributes: TRLNamedCollection);
var
  lField: TRLField;
  bInCache: Boolean;
  I: Integer;
begin
  if Assigned(ACoder) and Assigned(AAttributes) then
    for I := 0 to Pred(AAttributes.Count) do
    begin
      lField := ARelation.Fields.Find(AAttributes[I].Name);
      if Assigned(lField) then
      begin
        ACoder.WriteParameterIdentifier(lField.Identifier);
        if I < Pred(AAttributes.Count) then
        begin
          ACoder.WriteComma;
          bInCache := ACoder.InCache;
          if bInCache then
            ACoder.EndCache;
          ACoder.WriteSpace;
          if bInCache then
            ACoder.BeginCache;
        end;
      end;
    end;
end;

end.
