//=============================================================================
// U_Preparer
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Preparer;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception;

type
  // References
  TRLPreparerClass = class of TRLPreparer;

  // Exceptions
  ERLCouldNotPrepareCode = class(ERLException);
  ERLCouldNotUnPrepareCode = class(ERLException);
  ERLPreparerIsNull = class(ERLException);
  ERLPreparerNotFound = class(ERLException);

  // TRLPreparer
  TRLPreparer = class(TRLBaseType)
  private
    FTitle: string;
    FApplicationName: string;
    FCompanyName: string;
    FPrepared: Boolean;
  protected
    procedure PrepareCode; virtual; abstract;
    procedure UnPrepareCode; virtual; abstract;
  public
    destructor Destroy; override;
    procedure Prepare; virtual;
    function Prepared: Boolean; virtual;
    procedure UnPrepare; virtual;
    class function GetPreparer(const AName: string): TRLPreparer;
    class function FindPreparer(const AName: string): TRLPreparer;
  published
    property Title: string read FTitle write FTitle;
    property ApplicationName: string read FApplicationName write FApplicationName;
    property CompanyName: string read FCompanyName write FCompanyName;
  end;

implementation

{ TRLPreparer }

destructor TRLPreparer.Destroy;
begin
  UnPrepare;
  inherited;
end;

procedure TRLPreparer.Prepare;
begin
  if not Prepared then
    try
      PrepareCode;
      FPrepared := True;
    except
      on E: Exception do
        raise ERLCouldNotPrepareCode.Create('Could not prepare code.' +
          sLineBreak + E.Message);
    end;
end;

function TRLPreparer.Prepared: Boolean;
begin
  Result := FPrepared;
end;

procedure TRLPreparer.UnPrepare;
begin
  if Prepared then
    try
      UnPrepareCode;
      FPrepared := False;
    except
      on E: Exception do
        raise ERLCouldNotUnPrepareCode.Create('Could not unprepare code.' +
          sLineBreak + E.Message);
    end;
end;

class function TRLPreparer.GetPreparer(const AName: string): TRLPreparer;
var
  lClass: TRLPreparerClass;
begin
  lClass := TRLPreparerClass(GetClass(Format('TRL%sPreparer', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLPreparer.FindPreparer(const AName: string): TRLPreparer;
begin
  Result := GetPreparer(AName);
  if not Assigned(Result) then
    raise ERLPreparerNotFound.CreateFmt('Preparer "%s" not found.', [AName]);
end;

end.
