//=============================================================================
// U_DTOPreparer
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DTOPreparer;

interface

uses
  Classes,
  U_DBPreparer;

type
  // TRLDTOPreparer
  TRLDTOPreparer = class(TRLDBPreparer)
  end;

implementation

initialization
  RegisterClass(TRLDTOPreparer);

end.
