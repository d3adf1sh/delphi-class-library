//=============================================================================
// U_ClassMappingGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ClassMappingGenerator;

interface

uses
  U_Generator,
  U_ClassMappingPreparer;

type
  // TRLClassMappingGenerator
  TRLClassMappingGenerator = class(TRLGenerator)
  private
    function GetPreparer: TRLClassMappingPreparer;
    procedure SetPreparer(const AValue: TRLClassMappingPreparer);
  public
    property Preparer: TRLClassMappingPreparer read GetPreparer write SetPreparer;
  end;

implementation

{ TRLClassMappingGenerator }

function TRLClassMappingGenerator.GetPreparer: TRLClassMappingPreparer;
begin
  Result := TRLClassMappingPreparer(inherited Preparer);
end;

procedure TRLClassMappingGenerator.SetPreparer(
  const AValue: TRLClassMappingPreparer);
begin
  inherited Preparer := AValue;
end;

end.
