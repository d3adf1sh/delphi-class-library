//=============================================================================
// U_PascalDPOGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_PascalDPOGenerator;

{ TODO -oRafael : Usar uma lista de atributos � parte em GetIdentifierBy. }

interface

uses
  SysUtils,
  Classes,
  U_NamedCollection,
  U_Value,
  U_Comparison,
  U_Connections,
  U_Relations,
  U_Fields,
  U_Keys,
  U_OPFConditions,
  U_OPFDPO,
  U_PascalCoder,
  U_DPOGenerator;

const
  CDTO = 'DTO';
  CDPO = 'DPO';
  CListOf = 'ListOf';
  CLoad = 'Load';
  CDelete = 'Delete';
  CGet = 'Get';
  CKey = 'Key';
  CID = 'ID';
  CBy = 'By';
  CAll = 'All';
  CLoadBy = CLoad + CBy;
  CLoadAll = CLoad + CAll;
  CDeleteBy = CDelete + CBy;
  CDeleteAll = CDelete + CAll;
  CGetID = CGet + CID;
  CGetIDBy = CGetID + CBy;
  CConnection = 'Connection';
  CConditions = 'Conditions';
  CCondition = 'Condition';

type
  // TRLPascalDPOGenerator
  TRLPascalDPOGenerator = class(TRLDPOGenerator)
  private
//intf
    procedure WriteClass(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteConstructor(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteItemClassGetter(const ACoder: TRLPascalCoder);
    procedure WriteCollectionClassGetter(const ACoder: TRLPascalCoder);
    procedure WriteInsertion(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteUpdater(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteSaver(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteLoaderByConditions(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteLoaderByAttributes(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection; const AIdentifier: string; const AReturnList: Boolean);
    procedure WriteLoaderAll(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDeletionByConditions(const ACoder: TRLPascalCoder);
    procedure WriteDeletionByAttributes(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection; const AIdentifier: string);
    procedure WriteDeletionAll(const ACoder: TRLPascalCoder);
    procedure WriteIdentifierGetter(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
//impl
    procedure WriteClassCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteConstructorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteItemClassGetterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteCollectionClassGetterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteInsertionCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteUpdaterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteSaverCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteLoaderByConditionsCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteLoaderByAttributesCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection; const AIdentifier: string; const AReturnList: Boolean);
    procedure WriteLoaderAllCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDeletionByConditionsCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDeletionByAttributesCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection; const AIdentifier: string);
    procedure WriteDeletionAllCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteIdentifierGetterCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AKey: TRLKey);
    procedure WriteConditionsCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AAttributes: TRLNamedCollection);
  protected
    procedure Generate(const AStream: TStream); override;
  end;

implementation

uses
  StrUtils,
  U_NTUtils,
  U_CGUtils;

procedure TRLPascalDPOGenerator.WriteClass(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
var
  sIdentifier: string;
  I: Integer;
begin
  ACoder.BeginIdentation;
  try
    ACoder.WriteTypeHeader(ARelation.Identifier + CDPO).WriteLineBreak;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteSpace;
    ACoder.WriteEqual;
    ACoder.WriteSpace;
    ACoder.WriteClass;
    ACoder.WriteLeftParenthesis;
    try
      ACoder.Write(TRLOPFDPO.ClassName);
    finally
      ACoder.WriteRightParenthesis;
    end;

    ACoder.WriteLineBreak;
    ACoder.WriteProtected.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteItemClassGetter(ACoder);
      WriteCollectionClassGetter(ACoder);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WritePublic.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteConstructor(ACoder, ARelation);
      WriteInsertion(ACoder, ARelation);
      WriteUpdater(ACoder, ARelation);
      if Assigned(ARelation.Fields.IDField) then
        WriteSaver(ACoder, ARelation);
      WriteLoaderByConditions(ACoder, ARelation);
      WriteDeletionByConditions(ACoder);
      for I := 0 to Pred(ARelation.Keys.Count) do
      begin
        if ARelation.Keys[I].KeyType = ktPrimary then
          sIdentifier := CKey
        else
          sIdentifier := ARelation.Keys[I].TargetIdentifier;
        WriteLoaderByAttributes(ACoder, ARelation, ARelation.Keys[I].Fields,
          sIdentifier, False);
        WriteDeletionByAttributes(ACoder, ARelation, ARelation.Keys[I].Fields,
          sIdentifier);
        if Assigned(ARelation.Fields.IDField) and
          (ARelation.Keys[I].KeyType = ktUnique) then
          WriteIdentifierGetter(ACoder, ARelation, ARelation.Keys[I]);
      end;

      for I := 0 to Pred(ARelation.References.Count) do
      begin
        WriteLoaderByAttributes(ACoder, ARelation,
          ARelation.References[I].Fields,
          ARelation.References[I].TargetIdentifier, True);
        WriteDeletionByAttributes(ACoder, ARelation,
          ARelation.References[I].Fields,
          ARelation.References[I].TargetIdentifier);
      end;

      WriteLoaderAll(ACoder, ARelation);
      WriteDeletionAll(ACoder);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.WriteBlankLine;
end;

procedure TRLPascalDPOGenerator.WriteConstructor(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteConstructor.WriteSpace;
  ACoder.WriteCreate.Write(CDPO).WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteItemClassGetter(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('GetItemClass');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write('TRLCollectionItemClass').WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteCollectionClassGetter(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('GetCollectionClass');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write('TRLCollectionClass').WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteInsertion(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('Insert');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
  finally
    ACoder.WriteRightParenthesis;
    ACoder.WriteSemicolon;
  end;

  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteUpdater(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('Update');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteBoolean.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteSaver(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('Save');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteBoolean.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteLoaderByConditions(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(CLoad);
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CConditions);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier +
    CDTO).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteLoaderByAttributes(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AAttributes: TRLNamedCollection; const AIdentifier: string;
  const AReturnList: Boolean);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(CLoadBy + AIdentifier);
  ACoder.WriteLeftParenthesis;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AAttributes);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(IfThen(AReturnList, CListOf) +
    ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteLoaderAll(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(CLoadAll);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(
    CListOf + ARelation.Identifier + CDTO).WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteDeletionByConditions(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write(CDelete);
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CConditions);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteDeletionByAttributes(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AAttributes: TRLNamedCollection; const AIdentifier: string);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write(CDeleteBy + AIdentifier);
  ACoder.WriteLeftParenthesis;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AAttributes);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteDeletionAll(const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write(CDeleteAll).WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteIdentifierGetter(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write(CGetIDBy + AKey.TargetIdentifier);
  ACoder.WriteLeftParenthesis;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteInteger.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDPOGenerator.WriteClassCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
var
  sIdentifier: string;
  I: Integer;
begin
  ACoder.WriteTypeCodeHeader(ARelation.Identifier + CDPO).WriteBlankLine;
  WriteConstructorCode(ACoder, ARelation);
  WriteItemClassGetterCode(ACoder, ARelation);
  WriteCollectionClassGetterCode(ACoder, ARelation);
  WriteInsertionCode(ACoder, ARelation);
  WriteUpdaterCode(ACoder, ARelation);
  if Assigned(ARelation.Fields.IDField) then
    WriteSaverCode(ACoder, ARelation);
  WriteLoaderByConditionsCode(ACoder, ARelation);
  WriteDeletionByConditionsCode(ACoder, ARelation);
  for I := 0 to Pred(ARelation.Keys.Count) do
  begin
    if ARelation.Keys[I].KeyType = ktPrimary then
      sIdentifier := CKey
    else
      sIdentifier := ARelation.Keys[I].TargetIdentifier;
    WriteLoaderByAttributesCode(ACoder, ARelation, ARelation.Keys[I].Fields,
      sIdentifier, False);
    WriteDeletionByAttributesCode(ACoder, ARelation,
      ARelation.Keys[I].Fields, sIdentifier);
    if Assigned(ARelation.Fields.IDField) and
      (ARelation.Keys[I].KeyType = ktUnique) then
      WriteIdentifierGetterCode(ACoder, ARelation, ARelation.Keys[I]);
  end;

  for I := 0 to Pred(ARelation.References.Count) do
  begin
    WriteLoaderByAttributesCode(ACoder, ARelation,
      ARelation.References[I].Fields,
      ARelation.References[I].TargetIdentifier, True);
    WriteDeletionByAttributesCode(ACoder, ARelation,
      ARelation.References[I].Fields, ARelation.References[I].TargetIdentifier);
  end;
  
  WriteLoaderAllCode(ACoder, ARelation);
  WriteDeletionAllCode(ACoder, ARelation);
end;

procedure TRLPascalDPOGenerator.WriteConstructorCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteConstructor.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.WriteCreate.Write(CDPO).WriteSemicolon;
  ACoder.WriteLineBreak;
  ACoder.WriteVar.WriteLineBreak;
  ACoder.BeginIdentation;
  try
    ACoder.WriteLocalAttributeIdentifier(CConnection);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLConnection.ClassName).WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteInherited.WriteSpace;
    ACoder.WriteCreate.WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(TRLConnections.ClassName).WriteDot;
    ACoder.Write('Instance').WriteDot;
    ACoder.Write('FindByName');
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteQuotedText(Preparer.Connection.Name);
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;

    ACoder.WriteLineBreak;
    ACoder.Write('Provider').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteDot;
    ACoder.Write('Provider').WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.Write('ClassMapping').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteDot;
    ACoder.Write('ClassMapping').WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteItemClassGetterCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write('GetItemClass');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write('TRLCollectionItemClass').WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteCollectionClassGetterCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write('GetCollectionClass');
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.Write('TRLCollectionClass').WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier +
      CDTO).WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteInsertionCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write('Insert');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.Write('InsertRecord');
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteParameterIdentifier(ARelation.Identifier);
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteUpdaterCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write('Update');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteBoolean.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write('UpdateRecord');
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteParameterIdentifier(ARelation.Identifier);
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteSaverCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write('Save');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteBoolean.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteParameterChecking(ARelation.Identifier).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteIf.WriteSpace;
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteThen; //if Result...
    ACoder.BeginBlock;
    try
      ACoder.WriteIf.WriteSpace; //if ... = 0
      ACoder.WriteParameterIdentifier(ARelation.Identifier).WriteDot;
      ACoder.Write(CID).WriteDot;
      ACoder.Write('Data').WriteSpace;
      ACoder.WriteEqual.WriteSpace;
      ACoder.Write(#48).WriteSpace;
      ACoder.WriteThen.WriteLineBreak;
      ACoder.BeginIdentation;
      try
        ACoder.Write('Insert');
        ACoder.WriteLeftParenthesis;
        try
          ACoder.WriteParameterIdentifier(ARelation.Identifier);
        finally
          ACoder.WriteRightParenthesis;
        end;
      finally
        ACoder.EndIdendation;
      end;

      ACoder.WriteLineBreak;
      ACoder.WriteElse.WriteLineBreak;
      ACoder.BeginIdentation;
      try
        ACoder.WriteResult.WriteSpace;
        ACoder.WriteAssignment.WriteSpace;
        ACoder.Write('Update');
        ACoder.WriteLeftParenthesis;
        try
          ACoder.WriteParameterIdentifier(ARelation.Identifier);
        finally
          ACoder.WriteRightParenthesis;
          ACoder.WriteSemicolon;
        end;
      finally
        ACoder.EndIdendation; //end if ... = 0
      end;
    finally
      ACoder.EndBlock(False); //end if Result
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteLoaderByConditionsCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CLoad);
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CConditions);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier +
      CDTO).WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.Write('LoadRecordsByConditions');
      ACoder.WriteLeftParenthesis;
      try
        ACoder.WriteParameterIdentifier(CConditions);
      finally
        ACoder.WriteRightParenthesis;
      end;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteLoaderByAttributesCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AAttributes: TRLNamedCollection; const AIdentifier: string;
  const AReturnList: Boolean);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CLoadBy + AIdentifier);
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AAttributes);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(IfThen(AReturnList, CListOf) +
      ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.WriteLineBreak;
  ACoder.WriteVar.WriteLineBreak;
  ACoder.BeginIdentation;
  try
    ACoder.WriteLocalAttributeIdentifier(CConditions);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteLocalAttributeIdentifier(CCondition);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFCondition.ClassName).WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteLocalAttributeIdentifier(CConditions).WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName).WriteDot;
    ACoder.WriteCreate.WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteTry.WriteLineBreak; //try
    ACoder.BeginIdentation;
    try
      WriteConditionsCode(ACoder, ARelation, AAttributes);
      ACoder.WriteResult.WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.WriteTypeIdentifier(IfThen(AReturnList, CListOf) +
        ARelation.Identifier + CDTO);
      ACoder.WriteLeftParenthesis;
      try
        ACoder.Write(
          IfThen(AReturnList, 'LoadRecordsByConditions',
            'LoadRecordByConditions'));
        ACoder.WriteLeftParenthesis;
        try
          ACoder.WriteLocalAttributeIdentifier(CConditions);
        finally
          ACoder.WriteRightParenthesis;
        end;
      finally
        ACoder.WriteRightParenthesis;
        ACoder.WriteSemicolon;
      end;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteLineBreak;
    ACoder.WriteFinally.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      ACoder.WriteLocalAttributeDestruction(CConditions).WriteSemicolon;
      ACoder.WriteLineBreak;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon; //end try
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteLoaderAllCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CLoadAll);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier +
    CDTO).WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(CListOf + ARelation.Identifier + CDTO);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.Write('LoadRecordsByConditions');
      ACoder.WriteLeftParenthesis;
      try
        ACoder.WriteNil;
      finally
        ACoder.WriteRightParenthesis;
      end;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteDeletionByConditionsCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CDelete);
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CConditions);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.Write('DeleteRecordsByConditions');
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteParameterIdentifier(CConditions);
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteDeletionByAttributesCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AAttributes: TRLNamedCollection; const AIdentifier: string);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CDeleteBy + AIdentifier);
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AAttributes);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.WriteLineBreak;
  ACoder.WriteVar.WriteLineBreak;
  ACoder.BeginIdentation;
  try
    ACoder.WriteLocalAttributeIdentifier(CConditions);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteLocalAttributeIdentifier(CCondition);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFCondition.ClassName).WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteLocalAttributeIdentifier(CConditions).WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(TRLOPFConditions.ClassName).WriteDot;
    ACoder.WriteCreate.WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteTry.WriteLineBreak; //try
    ACoder.BeginIdentation;
    try
      WriteConditionsCode(ACoder, ARelation, AAttributes);
      ACoder.Write('DeleteRecordsByConditions');
      ACoder.WriteLeftParenthesis;
      try
        ACoder.WriteLocalAttributeIdentifier(CConditions);
      finally
        ACoder.WriteRightParenthesis;
        ACoder.WriteSemicolon;
      end;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteLineBreak;
    ACoder.WriteFinally.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      ACoder.WriteLocalAttributeDestruction(CConditions).WriteSemicolon;
      ACoder.WriteLineBreak;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon; //end try
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteDeletionAllCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CDeleteAll).WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.Write('DeleteRecordsByConditions');
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteNil;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteIdentifierGetterCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AKey: TRLKey);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
  ACoder.Write(CGetIDBy + AKey.TargetIdentifier);
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    TRLCGUtils.WritePascalParameters(ACoder, ARelation, AKey.Fields);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteInteger.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.WriteLineBreak;
  ACoder.WriteVar.WriteLineBreak;
  ACoder.BeginIdentation;
  try
    ACoder.WriteLocalAttributeIdentifier(ARelation.Identifier);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO).WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(#48).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.Write('Names').WriteDot;
    ACoder.Write('Text').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteQuotedText(CID).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteLocalAttributeIdentifier(ARelation.Identifier).WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(CLoadBy + AKey.TargetIdentifier);
    ACoder.WriteLeftParenthesis;
    try
      ACoder.BeginAutoLineBreak;
      try
        TRLCGUtils.WritePascalParameterNames(ACoder, ARelation, AKey.Fields);
      finally
        ACoder.EndAutoLineBreak;
      end;
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;

    ACoder.WriteLineBreak;
    ACoder.WriteIf.WriteSpace;
    ACoder.WriteLocalAttributeChecking(ARelation.Identifier).WriteSpace;
    ACoder.WriteThen.WriteLineBreak; //if
    ACoder.BeginIdentation;
    try
      ACoder.WriteTry.WriteLineBreak; //try
      ACoder.BeginIdentation;
      try
        ACoder.WriteResult.WriteSpace;
        ACoder.WriteAssignment.WriteSpace;
        ACoder.WriteLocalAttributeIdentifier(ARelation.Identifier).WriteDot;
        ACoder.Write(CID).WriteDot;
        ACoder.Write('Data').WriteSemicolon;
      finally
        ACoder.EndIdendation;
      end;

      ACoder.WriteLineBreak;
      ACoder.WriteFinally.WriteLineBreak;
      ACoder.BeginIdentation;
      try
        ACoder.WriteLocalAttributeDestruction(
          ARelation.Identifier).WriteSemicolon;
        ACoder.WriteLineBreak;
      finally
        ACoder.EndIdendation;
      end;

      ACoder.WriteEnd.WriteSemicolon; //end try
    finally
      ACoder.EndIdendation; //end if
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDPOGenerator.WriteConditionsCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AAttributes: TRLNamedCollection);
var
  lField: TRLField;
  I: Integer;
begin
  for I := 0 to Pred(AAttributes.Count) do
  begin
    lField := ARelation.Fields.Find(AAttributes[I].Name);
    if Assigned(lField) then
    begin
      ACoder.WriteLocalAttributeIdentifier(CCondition).WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.WriteLocalAttributeIdentifier(CConditions).WriteDot;
      ACoder.Write('Add').WriteSemicolon;
      ACoder.WriteLineBreak;
      ACoder.WriteLocalAttributeIdentifier(CCondition).WriteDot;
      ACoder.Write('AttributeName').WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.WriteQuotedText(lField.Identifier).WriteSemicolon;
      ACoder.WriteLineBreak;
      ACoder.WriteLocalAttributeIdentifier(CCondition).WriteDot;
      ACoder.Write('Comparison').WriteDot;
      ACoder.Write('ValueType').WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.Write(ValueTypeToString(lField.ValueType)).WriteSemicolon;
      ACoder.WriteLineBreak;
      ACoder.WriteLocalAttributeIdentifier(CCondition).WriteDot;
      ACoder.Write('Comparison').WriteDot;
      ACoder.Write('Kind').WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.Write(ComparisonKindToString(ckEqual)).WriteSemicolon;
      ACoder.WriteLineBreak;
      ACoder.Write(RLValueClass[lField.ValueType].ClassName);
      ACoder.WriteLeftParenthesis;
      try
        ACoder.WriteLocalAttributeIdentifier(CCondition).WriteDot;
        ACoder.Write('Comparison').WriteDot;
        ACoder.Write('AddValue');
      finally
        ACoder.WriteRightParenthesis;
      end;

      ACoder.WriteDot;
      ACoder.Write('Data').WriteSpace;
      ACoder.WriteAssignment.WriteSpace;
      ACoder.WriteParameterIdentifier(lField.Identifier).WriteSemicolon;
      ACoder.WriteLineBreak;
    end;
  end;
end;

procedure TRLPascalDPOGenerator.Generate(const AStream: TStream);
var
  lCoder: TRLPascalCoder;
  S: Integer;
  R: Integer;
begin
  lCoder := TRLPascalCoder.Create;
  try
    lCoder.Stream := AStream;
    lCoder.WriteHeader(Preparer.Title, Preparer.ApplicationName,
      Preparer.CompanyName);
    lCoder.WriteBlankLine;
    lCoder.WriteUnit.WriteSpace;
    lCoder.Write(
      TRLNTUtils.GetNameOfFileWithoutExtension(FileName)).WriteSemicolon;
    lCoder.WriteBlankLine;
    lCoder.WriteInterface;
    lCoder.WriteBlankLine;
    lCoder.WriteUses.WriteLineBreak;
    lCoder.BeginIdentation;
    try
      lCoder.Write('SysUtils').WriteComma.WriteLineBreak;
      lCoder.Write('Classes').WriteComma.WriteLineBreak;
      lCoder.Write('Controls').WriteComma.WriteLineBreak;
      lCoder.Write('U_Collection').WriteComma.WriteLineBreak;
      lCoder.Write('U_Value').WriteComma.WriteLineBreak;
      lCoder.Write('U_Comparison').WriteComma.WriteLineBreak;
      lCoder.Write('U_Connections').WriteComma.WriteLineBreak;
      lCoder.Write('U_OPFConditions').WriteComma.WriteLineBreak;
      lCoder.Write('U_OPFDPO').WriteComma.WriteLineBreak;
      lCoder.Write(Preparer.DTOReference).WriteSemicolon;
    finally
      lCoder.EndIdendation;
    end;

    lCoder.WriteBlankLine;
    lCoder.WriteType.WriteLineBreak;
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
        WriteClass(lCoder, Preparer.Schemas[S].Relations[R]);
    lCoder.WriteImplementation;
    lCoder.WriteBlankLine;
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
        WriteClassCode(lCoder, Preparer.Schemas[S].Relations[R]);
    lCoder.WriteEnd.WriteDot;
  finally
    FreeAndNil(lCoder);
  end;
end;

initialization
  RegisterClass(TRLPascalDPOGenerator);

end.
