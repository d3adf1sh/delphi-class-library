//=============================================================================
// U_DBPreparer
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBPreparer;

interface

uses
  U_Schemas,
  U_Connections,
  U_Preparer;

type
  // TRLDBPreparer
  TRLDBPreparer = class(TRLPreparer)
  private
    FConnectionName: string;
    FKeepConnection: Boolean;
    FConnection: TRLConnection;
    FSchemas: TRLSchemas;
    procedure SetConnectionName(const AValue: string);
    function GetConnection: TRLConnection;
    function GetSchemas: TRLSchemas;
  protected
    procedure PrepareCode; override;
    procedure UnPrepareCode; override;
    procedure ConnectionChanged; virtual;
    procedure CheckConnection; virtual;
  public
    property Connection: TRLConnection read GetConnection write FConnection;
    property Schemas: TRLSchemas read GetSchemas write FSchemas;
    class function GetPreparer(const AName: string): TRLDBPreparer;
    class function FindPreparer(const AName: string): TRLDBPreparer;
  published
    property ConnectionName: string read FConnectionName write SetConnectionName;
    property KeepConnection: Boolean read FKeepConnection write FKeepConnection;
  end;

implementation

{ TRLDBPreparer }

procedure TRLDBPreparer.SetConnectionName(const AValue: string);
begin
  if AValue <> FConnectionName then
  begin
    FConnectionName := AValue;
    FConnection := nil;
    ConnectionChanged;
  end;
end;

function TRLDBPreparer.GetConnection: TRLConnection;
begin
  if not Assigned(FConnection) then
    FConnection := TRLConnections.Instance.Find(FConnectionName);
  Result := FConnection;
end;

function TRLDBPreparer.GetSchemas: TRLSchemas;
begin
  if Assigned(FSchemas) then
    Result := FSchemas
  else
    if Assigned(Connection) then
      Result := Connection.Schemas
    else
      Result := nil;
end;

procedure TRLDBPreparer.PrepareCode;
begin
  CheckConnection;
  Connection.Open;
end;

procedure TRLDBPreparer.UnPrepareCode;
begin
  if not FKeepConnection then
    Connection.Close;
end;

procedure TRLDBPreparer.ConnectionChanged;
begin
end;

procedure TRLDBPreparer.CheckConnection;
begin
  if not Assigned(Connection) then
    raise ERLConnectionIsInvalid.CreateFmt('Connection "%s" is invalid.',
      [FConnectionName]);
end;

class function TRLDBPreparer.GetPreparer(const AName: string): TRLDBPreparer;
begin
  Result := TRLDBPreparer(inherited GetPreparer(AName));
end;

class function TRLDBPreparer.FindPreparer(const AName: string): TRLDBPreparer;
begin
  Result := TRLDBPreparer(inherited FindPreparer(AName));
end;

end.
