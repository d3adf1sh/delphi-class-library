//=============================================================================
// U_DLOPreparer
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DLOPreparer;

interface

uses
  Classes,
  U_DBPreparer;

type
  // TRLDLOPreparer
  TRLDLOPreparer = class(TRLDBPreparer)
  private
    FNamespaceReference: string;
    FDPOReference: string;
  published
    property NamespaceReference: string read FNamespaceReference write FNamespaceReference;
    property DPOReference: string read FDPOReference write FDPOReference;
  end;

implementation

initialization
  RegisterClass(TRLDLOPreparer);

end.
