//=============================================================================
// U_Generator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Generator;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Preparer;

type
  // References
  TRLGeneratorClass = class of TRLGenerator;

  // Exceptions
  ERLCouldNotGenerateCode = class(ERLException);
  ERLGeneratorNotFound = class(ERLException);

  // TRLGenerator
  TRLGenerator = class(TRLBaseType)
  private
    FPreparer: TRLPreparer;
    FFileName: string;
  protected
    procedure Generate(const AStream: TStream); virtual; abstract;
    procedure CheckPreparer; virtual;
    property FileName: string read FFileName write FFileName;
  public
    procedure GenerateToFile(const AFileName: string); virtual;
    procedure GenerateToStream(const AStream: TStream); virtual;
    property Preparer: TRLPreparer read FPreparer write FPreparer;
    class function GetGenerator(const AName: string): TRLGenerator;
    class function FindGenerator(const AName: string): TRLGenerator;
  end;

implementation

{ TRLGenerator }

procedure TRLGenerator.CheckPreparer;
begin
  if not Assigned(FPreparer) then
    raise ERLPreparerIsNull.Create('Preparer is null.');
end;

procedure TRLGenerator.GenerateToFile(const AFileName: string);
var
  lStream: TFileStream;
begin
  lStream := TFileStream.Create(AFileName, fmCreate);
  try
    FFileName := AFileName;
    try
      GenerateToStream(lStream);
    finally
      FFileName := '';
    end;
  finally
    FreeAndNil(lStream);
  end;
end;

procedure TRLGenerator.GenerateToStream(const AStream: TStream);
begin
  CheckPreparer;
  if Assigned(AStream) then
  begin
    Preparer.Prepare;
    try
      try
        Generate(AStream);
      except
        on E: Exception do
          raise ERLCouldNotGenerateCode.Create('Could not generate code.' +
            sLineBreak + E.Message);
      end;
    finally
      Preparer.UnPrepare;
    end;
  end;
end;

class function TRLGenerator.GetGenerator(const AName: string): TRLGenerator;
var
  lClass: TRLGeneratorClass;
begin
  lClass := TRLGeneratorClass(GetClass(Format('TRL%sGenerator', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLGenerator.FindGenerator(const AName: string): TRLGenerator;
begin
  Result := GetGenerator(AName);
  if not Assigned(Result) then
    raise ERLGeneratorNotFound.CreateFmt('Generator "%s" not found.',
      [AName]);
end;

end.
