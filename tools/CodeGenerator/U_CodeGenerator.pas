//=============================================================================
// U_CodeGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_CodeGenerator;

interface

{$R Resources.res}

uses
  SysUtils,
  U_RTTIAttributes,
  U_IdentifierMapping,
  U_DataApplication,
  U_DBPreparer,
  U_Generator;

const
  CTypeParameter = 'Type';
  CLanguageParameter = 'Language';
  CFileNameParameter = 'FileName';
  CIdentifierMappingParameter = 'IdentifierMapping';

type
  // TRLCodeGenerator
  TRLCodeGenerator = class(TRLDataApplication)
  public
    constructor Create; override;
    function Run: Boolean; override;
  end;

implementation

uses
  U_AL,
  U_CG,
  U_NTUtils,
  U_RTTIContext;

{ TRLCodeGenerator }

constructor TRLCodeGenerator.Create;
begin
  inherited;
  ModulesFileName := FileLocation + 'Modules.xml';
  ConnectionsFileName := FileLocation + 'Connections.xml';
end;

function TRLCodeGenerator.Run: Boolean;
var
  lPreparer: TRLDBPreparer;
  lGenerator: TRLGenerator;
  lAttributes: TRLRTTIAttributes;
  lAttribute: TRLRTTIAttribute;
  sType: string;
  sFileName: string;
  sIdentifierMapping: string;
  I: Integer;
begin
  Result := inherited Run;
  if Result then
  begin
    WriteConsole(Title);
    sType := GetParameter(CTypeParameter);
    lPreparer := TRLDBPreparer.GetPreparer(sType);
    try
      if Assigned(lPreparer) then
      begin
        lGenerator := TRLGenerator.GetGenerator(
          GetParameter(CLanguageParameter) + sType);
        try
          if Assigned(lGenerator) then
          begin
            sFileName := GetParameter(CFileNameParameter);
            if TRLNTUtils.IsValidFile(sFileName) then
              try
                WriteConsole('Generating...');
                sIdentifierMapping := GetParameter(CIdentifierMappingParameter);
                if FileExists(sIdentifierMapping) then
                  TRLRTTIContext.Instance.FromFile(
                    TRLIdentifierMapping.Instance.Identifiers,
                      sIdentifierMapping, sfXML);
                lAttributes :=
                  TRLRTTIContext.Instance.Inspector.GetObjectAttributes(
                    lPreparer);
                for I := 0 to Pred(Parameters.Count) do
                begin
                  lAttribute := lAttributes.Find(Parameters.Keys[I]);
                  if Assigned(lAttribute) then
                    TRLRTTIContext.Instance.Inspector.WriteValue(lPreparer,
                      lAttribute, Parameters.ValueFromIndex[I], False);
                end;

                if lPreparer.Title = '' then
                  lPreparer.Title := TRLNTUtils.GetNameOfFileWithoutExtension(
                    sFileName);
                lGenerator.Preparer := lPreparer;
                lGenerator.GenerateToFile(sFileName);
                WriteConsole('Done.');
              except
                on E: Exception do
                  WriteConsole(Format('Error: [%s].', [E.Message]));
              end
            else
              WriteConsole('Invalid FileName.');
          end
          else
            WriteConsole('Invalid Language.');
        finally
          if Assigned(lGenerator) then
            FreeAndNil(lGenerator);
        end;
      end
      else
        WriteConsole('Invalid Type.');
    finally
      if Assigned(lPreparer) then
        FreeAndNil(lPreparer);
    end;
  end;
end;

end.
