//=============================================================================
// U_DPOGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DPOGenerator;

interface

uses
  U_Generator,
  U_DPOPreparer;

type
  // TRLDPOGenerator
  TRLDPOGenerator = class(TRLGenerator)
  private
    function GetPreparer: TRLDPOPreparer;
    procedure SetPreparer(const AValue: TRLDPOPreparer);
  public
    property Preparer: TRLDPOPreparer read GetPreparer write SetPreparer;
  end;

implementation

{ TRLDPOGenerator }

function TRLDPOGenerator.GetPreparer: TRLDPOPreparer;
begin
  Result := TRLDPOPreparer(inherited Preparer);
end;

procedure TRLDPOGenerator.SetPreparer(const AValue: TRLDPOPreparer);
begin
  inherited Preparer := AValue;
end;

end.
