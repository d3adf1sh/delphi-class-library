//=============================================================================
// U_PascalClassMappingGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_PascalClassMappingGenerator;

interface

uses
  SysUtils,
  Classes,
  U_Value,
  U_Schemas,
  U_Relations,
  U_Fields,
  U_Keys,
  U_OPFClassMapping,
  U_PascalCoder,
  U_ClassMappingGenerator;

const
  CDTO = 'DTO';
  CClassMapping = 'ClassMapping';
  CClassMappingPopulator = CClassMapping + 'Populator';

type
  // TRLPascalClassMappingGenerator
  TRLPascalClassMappingGenerator = class(TRLClassMappingGenerator)
  private
//intf
    procedure WriteClass(const ACoder: TRLPascalCoder);
    procedure WritePopulator(const ACoder: TRLPascalCoder);
    procedure WriteRegistration(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
//impl
    procedure WriteClassCode(const ACoder: TRLPascalCoder);
    procedure WritePopulatorCode(const ACoder: TRLPascalCoder);
    procedure WriteRegistrationCode(const ACoder: TRLPascalCoder; const ASchema: TRLSchema; const ARelation: TRLRelation);
    procedure WriteClassRegistrationCode(const ACoder: TRLPascalCoder; const ASchema: TRLSchema; const ARelation: TRLRelation);
    procedure WriteAttributeRegistrationCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation; const AField: TRLField);
  protected
    procedure Generate(const AStream: TStream); override;
  end;

implementation

uses
  StrUtils,
  U_NTUtils;

procedure TRLPascalClassMappingGenerator.WriteClass(
  const ACoder: TRLPascalCoder);
var
  S: Integer;
  R: Integer;
begin
  ACoder.BeginIdentation;
  try
    ACoder.WriteTypeHeader(Preparer.Connection.Name +
      CClassMappingPopulator).WriteLineBreak;
    ACoder.WriteTypeIdentifier(Preparer.Connection.Name +
      CClassMappingPopulator).WriteSpace;
    ACoder.WriteEqual;
    ACoder.WriteSpace;
    ACoder.WriteClass;
    ACoder.WriteLeftParenthesis;
    try
      ACoder.Write(TRLOPFClassMappingPopulator.ClassName);
    finally
      ACoder.WriteRightParenthesis;
    end;

    ACoder.WriteLineBreak;
    ACoder.WriteProtected.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      for S := 0 to Pred(Preparer.Schemas.Count) do
        for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
          WriteRegistration(ACoder, Preparer.Schemas[S].Relations[R]);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WritePublic.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WritePopulator(ACoder);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.WriteBlankLine;
end;

procedure TRLPascalClassMappingGenerator.WritePopulator(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('Populate');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CClassMapping);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFClassMapping.ClassName);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalClassMappingGenerator.WriteRegistration(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('Register' + ARelation.Identifier);
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CClassMapping);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFClassMapping.ClassName);
  finally
    ACoder.WriteRightParenthesis;
  end;

  ACoder.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalClassMappingGenerator.WriteClassCode(
  const ACoder: TRLPascalCoder);
var
  S: Integer;
  R: Integer;
begin
  ACoder.WriteTypeCodeHeader(Preparer.Connection.Name +
    CClassMappingPopulator).WriteBlankLine;
  for S := 0 to Pred(Preparer.Schemas.Count) do
    for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
      WriteRegistrationCode(ACoder, Preparer.Schemas[S],
        Preparer.Schemas[S].Relations[R]);
  WritePopulatorCode(ACoder);
end;

procedure TRLPascalClassMappingGenerator.WritePopulatorCode(
  const ACoder: TRLPascalCoder);
var
  S: Integer;
  R: Integer;
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(Preparer.Connection.Name +
    CClassMappingPopulator).WriteDot;
  ACoder.Write('Populate');
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CClassMapping);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFClassMapping.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
      begin
        if (S > 0) or (R > 0) then
          ACoder.WriteLineBreak;
        ACoder.Write('Register' + Preparer.Schemas[S].Relations[R].Identifier);
        ACoder.WriteLeftParenthesis;
        try
          ACoder.WriteParameterIdentifier(CClassMapping);
        finally
          ACoder.WriteRightParenthesis;
          ACoder.WriteSemicolon;
        end;
      end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalClassMappingGenerator.WriteRegistrationCode(
  const ACoder: TRLPascalCoder; const ASchema: TRLSchema;
  const ARelation: TRLRelation);
var
  I: Integer;
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(Preparer.Connection.Name +
    CClassMappingPopulator).WriteDot;
  ACoder.Write('Register' + ARelation.Identifier);
  ACoder.WriteLeftParenthesis; //(
  ACoder.BeginAutoLineBreak;
  try
    ACoder.WriteConst.WriteSpace;
    ACoder.WriteParameterIdentifier(CClassMapping);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLOPFClassMapping.ClassName);
    ACoder.WriteRightParenthesis; //)
    ACoder.WriteSemicolon;
  finally
    ACoder.EndAutoLineBreak;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteDoubleSlash;
    ACoder.WriteSpace;
    ACoder.Write(ARelation.FullName).WriteLineBreak;
    WriteClassRegistrationCode(ACoder, ASchema, ARelation);
    for I := 0 to Pred(ARelation.Fields.Count) do
    begin
      ACoder.WriteLineBreak;
      WriteAttributeRegistrationCode(ACoder, ARelation, ARelation.Fields[I]);
    end;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalClassMappingGenerator.WriteClassRegistrationCode(
  const ACoder: TRLPascalCoder; const ASchema: TRLSchema;
  const ARelation: TRLRelation);
begin
  ACoder.WriteParameterIdentifier(CClassMapping).WriteDot;
  ACoder.Write('RegisterClass');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteComma;
    ACoder.WriteSpace;
    ACoder.WriteQuotedText(ASchema.Name);
    ACoder.WriteComma;
    ACoder.WriteSpace;
    ACoder.WriteQuotedText(ARelation.Name);
  finally
    ACoder.WriteRightParenthesis;
    ACoder.WriteSemicolon;
  end;
end;

procedure TRLPascalClassMappingGenerator.WriteAttributeRegistrationCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation;
  const AField: TRLField);
begin
  ACoder.WriteParameterIdentifier(CClassMapping).WriteDot;
  ACoder.Write('RegisterAttribute');
  ACoder.WriteLeftParenthesis;
  try
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDTO);
    ACoder.WriteComma;
    ACoder.WriteSpace;
    ACoder.WriteQuotedText(AField.Identifier);
    ACoder.WriteComma;
    ACoder.WriteSpace;
    ACoder.WriteQuotedText(AField.Name);
    ACoder.WriteComma;
    ACoder.WriteSpace;
    ACoder.Write(ValueTypeToString(AField.ValueType));
    ACoder.WriteComma;
    ACoder.WriteSpace;
    if AField.AutoIncrement then
      ACoder.Write('True')
    else
      ACoder.Write('False');
    ACoder.WriteComma;
    ACoder.WriteSpace;
    if Assigned(ARelation.Keys.PrimaryKey) and
      ARelation.Keys.PrimaryKey.Fields.Contains(AField.Name) then
      ACoder.Write('True')
    else
      ACoder.Write('False');
  finally
    ACoder.WriteRightParenthesis;
    ACoder.WriteSemicolon;
  end;
end;
  
procedure TRLPascalClassMappingGenerator.Generate(const AStream: TStream);
var
  lCoder: TRLPascalCoder;
begin
  lCoder := TRLPascalCoder.Create;
  try
    lCoder.Stream := AStream;
    lCoder.WriteHeader(Preparer.Title, Preparer.ApplicationName,
      Preparer.CompanyName);
    lCoder.WriteBlankLine;
    lCoder.WriteUnit.WriteSpace;
    lCoder.Write(
      TRLNTUtils.GetNameOfFileWithoutExtension(FileName)).WriteSemicolon;
    lCoder.WriteBlankLine;
    lCoder.WriteInterface;
    lCoder.WriteBlankLine;
    lCoder.WriteUses.WriteLineBreak;
    lCoder.BeginIdentation;
    try
      lCoder.Write('Classes').WriteComma.WriteLineBreak;
      lCoder.Write('U_Value').WriteComma.WriteLineBreak;
      lCoder.Write('U_OPFClassMapping').WriteComma.WriteLineBreak;
      lCoder.Write(Preparer.DTOReference).WriteSemicolon;
    finally
      lCoder.EndIdendation;
    end;

    lCoder.WriteBlankLine;
    lCoder.WriteType.WriteLineBreak;
    WriteClass(lCoder);
    lCoder.WriteImplementation;
    lCoder.WriteBlankLine;
    WriteClassCode(lCoder);
    lCoder.WriteInitialization.WriteLineBreak;
    lCoder.BeginIdentation;
    try
      lCoder.Write('RegisterClass');
      lCoder.WriteLeftParenthesis;
      try
        lCoder.WriteTypeIdentifier(Preparer.Connection.Name +
          CClassMappingPopulator);
      finally
        lCoder.WriteRightParenthesis;
        lCoder.WriteSemicolon;
      end;
    finally
      lCoder.EndIdendation;
    end;

    lCoder.WriteBlankLine;
    lCoder.WriteEnd.WriteDot;
  finally
    FreeAndNil(lCoder);
  end;
end;

initialization
  RegisterClass(TRLPascalClassMappingGenerator);

end.
