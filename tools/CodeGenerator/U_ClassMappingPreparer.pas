//=============================================================================
// U_ClassMappingPreparer
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ClassMappingPreparer;

interface

uses
  Classes,
  U_DBPreparer;

type
  // TRLClassMappingPreparer
  TRLClassMappingPreparer = class(TRLDBPreparer)
  private
    FDTOReference: string;
  published
    property DTOReference: string read FDTOReference write FDTOReference;
  end;

implementation

initialization
  RegisterClass(TRLClassMappingPreparer);

end.
