program CodeGenerator;

{$APPTYPE CONSOLE}

uses
  MidasLib,
  Forms,
  U_CG in 'U_CG.pas',
  U_CGUtils in 'U_CGUtils.pas',
  U_ClassMappingCG in 'U_ClassMappingCG.pas',
  U_ClassMappingGenerator in 'U_ClassMappingGenerator.pas',
  U_ClassMappingPreparer in 'U_ClassMappingPreparer.pas',
  U_CodeGenerator in 'U_CodeGenerator.pas',
  U_Coder in 'U_Coder.pas',
  U_DBPreparer in 'U_DBPreparer.pas',
  U_DLOCG in 'U_DLOCG.pas',
  U_DLOGenerator in 'U_DLOGenerator.pas',
  U_DLOPreparer in 'U_DLOPreparer.pas',
  U_DPOCG in 'U_DPOCG.pas',
  U_DPOGenerator in 'U_DPOGenerator.pas',
  U_DPOPreparer in 'U_DPOPreparer.pas',
  U_DTOCG in 'U_DTOCG.pas',
  U_DTOGenerator in 'U_DTOGenerator.pas',
  U_DTOPreparer in 'U_DTOPreparer.pas',
  U_Generator in 'U_Generator.pas',
  U_PascalClassMappingGenerator in 'U_PascalClassMappingGenerator.pas',
  U_PascalCoder in 'U_PascalCoder.pas',
  U_PascalDLOGenerator in 'U_PascalDLOGenerator.pas',
  U_PascalDPOGenerator in 'U_PascalDPOGenerator.pas',
  U_PascalDTOGenerator in 'U_PascalDTOGenerator.pas',
  U_Preparer in 'U_Preparer.pas';

{$R *.res}

begin
  Application.Title := 'Code Generation Tool';
  TRLCodeGenerator.RunApplication(nil, nil);
end.
