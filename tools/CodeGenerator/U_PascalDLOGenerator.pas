//=============================================================================
// U_PascalDLOGenerator
// Code Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_PascalDLOGenerator;

interface

uses
  SysUtils,
  Classes,
  U_Connections,
  U_Relations,
  U_OPFCommand,
  U_OPFDPO,
  U_PascalCoder,
  U_DLOGenerator;

const
  CDLO = 'DLO';
  CDPO = 'DPO';
  CConnection = 'Connection';
  
type
  // TRLPascalDLOGenerator
  TRLPascalDLOGenerator = class(TRLDLOGenerator)
  private
//intf
    procedure WriteClass(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteConstructor(const ACoder: TRLPascalCoder);
    procedure WriteDestructor(const ACoder: TRLPascalCoder);
    procedure WriteProviderChanged(const ACoder: TRLPascalCoder);
    procedure WriteDatabaseChanged(const ACoder: TRLPascalCoder);
    procedure WriteClassMappingChanged(const ACoder: TRLPascalCoder);
    procedure WriteDPOCreator(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
//impl    
    procedure WriteClassCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteConstructorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDestructorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteProviderChangedCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDatabaseChangedCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteClassMappingChangedCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
    procedure WriteDPOCreatorCode(const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
  protected
    procedure Generate(const AStream: TStream); override;
  end;

implementation

uses
  U_NTUtils;

procedure TRLPascalDLOGenerator.WriteClass(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.BeginIdentation;
  try
    ACoder.WriteTypeHeader(ARelation.Identifier + CDLO).WriteLineBreak;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteSpace;
    ACoder.WriteEqual;
    ACoder.WriteSpace;
    ACoder.WriteClass;
    ACoder.WriteLeftParenthesis;
    try
      ACoder.Write(TRLOPFCommand.ClassName);
    finally
      ACoder.WriteRightParenthesis;
    end;

    ACoder.WriteLineBreak;
    ACoder.WritePrivate.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      ACoder.WriteAttributeIdentifier(ARelation.Identifier + CDPO);
      ACoder.WriteColon;
      ACoder.WriteSpace;
      ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteSemicolon;
      ACoder.WriteLineBreak;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteProtected.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteProviderChanged(ACoder);
      WriteDatabaseChanged(ACoder);
      WriteClassMappingChanged(ACoder);
      WriteDPOCreator(ACoder, ARelation);
      ACoder.WriteProperty.WriteSpace;
      ACoder.Write(ARelation.Identifier + CDPO);
      ACoder.WriteColon;
      ACoder.WriteSpace;
      ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteSpace;
      ACoder.WriteRead.WriteSpace;
      ACoder.WriteAttributeIdentifier(ARelation.Identifier +
        CDPO).WriteSemicolon;
      ACoder.WriteLineBreak;
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WritePublic.WriteLineBreak;
    ACoder.BeginIdentation;
    try
      WriteConstructor(ACoder);
      WriteDestructor(ACoder);
    finally
      ACoder.EndIdendation;
    end;

    ACoder.WriteEnd.WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.WriteBlankLine;
end;

procedure TRLPascalDLOGenerator.WriteConstructor(const ACoder: TRLPascalCoder);
begin
  ACoder.WriteConstructor.WriteSpace;
  ACoder.WriteCreate.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDLOGenerator.WriteDestructor(const ACoder: TRLPascalCoder);
begin
  ACoder.WriteDestructor.WriteSpace;
  ACoder.WriteDestroy.WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDLOGenerator.WriteProviderChanged(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('ProviderChanged').WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDLOGenerator.WriteDatabaseChanged(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('DatabaseChanged').WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDLOGenerator.WriteClassMappingChanged(
  const ACoder: TRLPascalCoder);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.Write('ClassMappingChanged').WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteOverride.WriteSemicolon;
  ACoder.WriteLineBreak;
end;


procedure TRLPascalDLOGenerator.WriteDPOCreator(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.Write('Create' + ARelation.Identifier + CDPO);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteSemicolon;
  ACoder.WriteSpace;
  ACoder.WriteVirtual.WriteSemicolon;
  ACoder.WriteLineBreak;
end;

procedure TRLPascalDLOGenerator.WriteClassCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteTypeCodeHeader(ARelation.Identifier + CDLO).WriteBlankLine;
  WriteConstructorCode(ACoder, ARelation);
  WriteDestructorCode(ACoder, ARelation);
  WriteProviderChangedCode(ACoder, ARelation);
  WriteDatabaseChangedCode(ACoder, ARelation);
  WriteClassMappingChangedCode(ACoder, ARelation);
  WriteDPOCreatorCode(ACoder, ARelation);
end;

procedure TRLPascalDLOGenerator.WriteConstructorCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteConstructor.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteDot;
  ACoder.WriteCreate.WriteSemicolon;
  ACoder.WriteLineBreak;
  ACoder.WriteVar.WriteLineBreak;
  ACoder.BeginIdentation;
  try
    ACoder.WriteLocalAttributeIdentifier(CConnection);
    ACoder.WriteColon;
    ACoder.WriteSpace;
    ACoder.Write(TRLConnection.ClassName).WriteSemicolon;
  finally
    ACoder.EndIdendation;
  end;

  ACoder.BeginBlock;
  try
    ACoder.WriteInherited.WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteAttributeIdentifier(ARelation.Identifier + CDPO).WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write('Create' + ARelation.Identifier + CDPO).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write(TRLConnections.ClassName).WriteDot;
    ACoder.Write('Instance').WriteDot;
    ACoder.Write('FindByName');
    ACoder.WriteLeftParenthesis;
    try
      ACoder.WriteQuotedText(Preparer.Connection.Name);
    finally
      ACoder.WriteRightParenthesis;
      ACoder.WriteSemicolon;
    end;

    ACoder.WriteLineBreak;
    ACoder.Write('Provider').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteDot;
    ACoder.Write('Provider').WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.Write('Database').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteDot;
    ACoder.Write('Database').WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.Write('ClassMapping').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteLocalAttributeIdentifier(CConnection).WriteDot;
    ACoder.Write('ClassMapping').WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDLOGenerator.WriteDestructorCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteDestructor.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteDot;
  ACoder.WriteDestroy.WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteAttributeDestruction(ARelation.Identifier +
      CDPO).WriteSemicolon;
    ACoder.WriteLineBreak;
    ACoder.WriteInherited.WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDLOGenerator.WriteProviderChangedCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteDot;
  ACoder.Write('ProviderChanged').WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteAttributeIdentifier(ARelation.Identifier + CDPO).WriteDot;
    ACoder.Write('Provider').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write('Provider').WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDLOGenerator.WriteDatabaseChangedCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteDot;
  ACoder.Write('DatabaseChanged').WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteAttributeIdentifier(ARelation.Identifier + CDPO).WriteDot;
    ACoder.Write('Database').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write('Database').WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDLOGenerator.WriteClassMappingChangedCode(
  const ACoder: TRLPascalCoder; const ARelation: TRLRelation);
begin
  ACoder.WriteProcedure.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteDot;
  ACoder.Write('ClassMappingChanged').WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteAttributeIdentifier(ARelation.Identifier + CDPO).WriteDot;
    ACoder.Write('ClassMapping').WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.Write('ClassMapping').WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDLOGenerator.WriteDPOCreatorCode(const ACoder: TRLPascalCoder;
  const ARelation: TRLRelation);
begin
  ACoder.WriteFunction.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDLO).WriteDot;
  ACoder.Write('Create' + ARelation.Identifier + CDPO);
  ACoder.WriteColon;
  ACoder.WriteSpace;
  ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteSemicolon;
  ACoder.BeginBlock;
  try
    ACoder.WriteResult.WriteSpace;
    ACoder.WriteAssignment.WriteSpace;
    ACoder.WriteTypeIdentifier(ARelation.Identifier + CDPO).WriteDot;
    ACoder.WriteCreate.WriteSemicolon;
  finally
    ACoder.EndBlock;
  end;
end;

procedure TRLPascalDLOGenerator.Generate(const AStream: TStream);
var
  lCoder: TRLPascalCoder;
  S: Integer;
  R: Integer;
begin
  lCoder := TRLPascalCoder.Create;
  try
    lCoder.Stream := AStream;
    lCoder.WriteHeader(Preparer.Title, Preparer.ApplicationName,
      Preparer.CompanyName);
    lCoder.WriteBlankLine;
    lCoder.WriteUnit.WriteSpace;
    lCoder.Write(
      TRLNTUtils.GetNameOfFileWithoutExtension(FileName)).WriteSemicolon;
    lCoder.WriteBlankLine;
    lCoder.WriteInterface;
    lCoder.WriteBlankLine;
    lCoder.WriteUses.WriteLineBreak;
    lCoder.BeginIdentation;
    try
      lCoder.Write('SysUtils').WriteComma.WriteLineBreak;
      lCoder.Write('U_Connections').WriteComma.WriteLineBreak;
      lCoder.Write('U_OPFCommand').WriteComma.WriteLineBreak;
      lCoder.Write('U_OPFDPO').WriteComma.WriteLineBreak;
      lCoder.Write(Preparer.DPOReference).WriteSemicolon;
    finally
      lCoder.EndIdendation;
    end;

    lCoder.WriteBlankLine;
    lCoder.WriteType.WriteLineBreak;
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
        WriteClass(lCoder, Preparer.Schemas[S].Relations[R]);
    lCoder.WriteImplementation;
    lCoder.WriteBlankLine;
    for S := 0 to Pred(Preparer.Schemas.Count) do
      for R := 0 to Pred(Preparer.Schemas[S].Relations.Count) do
        WriteClassCode(lCoder, Preparer.Schemas[S].Relations[R]);
    lCoder.WriteEnd.WriteDot;
  finally
    FreeAndNil(lCoder);
  end;
end;

initialization
  RegisterClass(TRLPascalDLOGenerator);

end.
