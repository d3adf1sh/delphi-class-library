//=============================================================================
// U_RCGenerator
// Resource Script Generation Tool
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_RCGenerator;

interface

uses
  SysUtils,
  Classes,
  U_Application;

const
  CFileNameParameter = 'FileName';
  CTypeParameter = 'Type';
  CPathParameter = 'Path';
  CPrefixParameter = 'Prefix';
  CSufixParameter = 'Sufix';
  CExtensionParameter = 'Extension';

type
  // TRLRCGenerator
  TRLRCGenerator = class(TRLApplication)
  public
    constructor Create; override;
    function Run: Boolean; override;
  end;

implementation

uses
  U_AL,
  U_System,
  U_NTUtils;

{ TRLRCGenerator }

constructor TRLRCGenerator.Create;
begin
  inherited;
  ModulesFileName := FileLocation + 'Modules.xml';
end;

function TRLRCGenerator.Run: Boolean;
var
  lFile: TFileStream;
  lSearch: TSearchRec;
  sFileName: string;
  sType: string;
  sPath: string;
  sPrefix: string;
  sSufix: string;
  sExtension: string;
  sLine: string;
begin
  Result := inherited Run;
  if Result then
  begin
    WriteConsole(Title);
    sFileName := GetParameter(CFileNameParameter);
    if TRLNTUtils.IsValidFile(sFileName) then
    begin
      sType := GetParameter(CTypeParameter);
      if SameText(sType, 'BITMAP') or SameText(sType, 'ICON') or
        SameText(sType, 'RCDATA') then
      begin
        sPath := IncludeTrailingPathDelimiter(GetParameter(CPathParameter));
        if DirectoryExists(sPath) then
        begin
          WriteConsole('Generating...');
          sPrefix := GetParameter(CPrefixParameter);
          sSufix := GetParameter(CSufixParameter);
          sExtension := GetParameter(CExtensionParameter);
          lFile := TFileStream.Create(sFileName, fmCreate);
          try
{$WARNINGS OFF W1002}
            if FindFirst(sPath + '*.*', faArchive, lSearch) = 0 then
{$WARNINGS ON}
              try
                repeat
                  if (sExtension = '') or
                    SameText(ExtractFileExt(lSearch.Name), sExtension) then
                  begin
                    sLine := Format('%s%s%s %s "%s"',
                      [sPrefix,
                        UpperCase(
                          TRLNTUtils.GetNameOfFileWithoutExtension(
                            lSearch.Name)),
                        sSufix, sType, sPath + lSearch.Name]);
                    U_System.WriteStream(lFile, sLine);
                    U_System.WriteStream(lFile, sLineBreak);
                  end;
                until FindNext(lSearch) <> 0;
              finally
                FindClose(lSearch);
              end;
          finally
            FreeAndNil(lFile);
          end;

          WriteConsole('Done.');
        end
        else
          WriteConsole('Invalid Path.');
      end
      else
        WriteConsole('Invalid Type.');
    end
    else
      WriteConsole('Invalid FileName.');
  end;
end;

end.

