program RCGenerator;

{$APPTYPE CONSOLE}

uses
  Forms,
  U_RCGenerator in 'U_RCGenerator.pas';

{$R *.res}

begin
  Application.Title := 'Resource Script Generation Tool';
  TRLRCGenerator.RunApplication(nil, nil);
end.
