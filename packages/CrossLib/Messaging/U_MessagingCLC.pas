//=============================================================================
// U_MessagingCLC
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MessagingCLC;

interface

var
  RLDefaultListenPort: Integer = 2110;
  RLDefaultInterval: Integer = 10;
  RLDefaultConnectionTimeout: Integer = 5000;
  RLDefaultReadTimeout: Integer = 10000;

implementation

end.
