//=============================================================================
// U_Sender
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Sender;

{ TODO -oRafael : Gerar exce��es com base no FaultKind. }

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Settings,
  U_Message;

type
  // References
  TRLSenderClass = class of TRLSender;

  // Exceptions
  ERLSenderNotFound = class(ERLException);
  ERLSenderIsNotActive = class(ERLException);
  ERLCouldNotSendMessage = class(ERLException);

  // TRLSender
  TRLSender = class(TRLBaseType)
  private
    FSettings: TRLSettings;
  protected
    function CreateSettings: TRLSettings; virtual;
    procedure CheckActive; virtual;
    procedure SendMessage(const AMessage: TRLMessage); reintroduce; virtual; abstract;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; virtual; abstract;
    procedure Close; virtual; abstract;
    function IsOpen: Boolean; virtual; abstract;
    procedure Send(const AMessage: TRLMessage); virtual;
    property Settings: TRLSettings read FSettings;
    class function GetSender(const AName: string): TRLSender;
    class function FindSender(const AName: string): TRLSender;
  end;

implementation

{ TRLSender }

constructor TRLSender.Create;
begin
  inherited;
  FSettings := CreateSettings;
end;

destructor TRLSender.Destroy;
begin
  Close;
  FreeAndNil(FSettings);
  inherited;
end;

function TRLSender.CreateSettings: TRLSettings;
begin
  Result := TRLSettings.Create;
end;

procedure TRLSender.CheckActive;
begin
  if not IsOpen then
    raise ERLSenderIsNotActive.Create('Sender is not active.');
end;

procedure TRLSender.Send(const AMessage: TRLMessage);
var
  lClass: ExceptClass;
begin
  CheckActive;
  if Assigned(AMessage) then
  begin
      AMessage.Received := False;
      AMessage.FaultName := '';
      AMessage.FaultMessage := '';
      try
        SendMessage(AMessage);
      except
        on E: Exception do
          raise ERLCouldNotSendMessage.Create('Could not send message.' +
            sLineBreak + E.Message);
      end;

      if AMessage.FaultName <> '' then
      begin
        lClass := ExceptClass(GetClass(AMessage.FaultName));
        if Assigned(lClass) then
          raise lClass.Create(AMessage.FaultName);
      end;
  end;
end;

class function TRLSender.GetSender(const AName: string): TRLSender;
var
  lClass: TRLSenderClass;
begin
  lClass := TRLSenderClass(GetClass(Format('TRL%sSender', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLSender.FindSender(const AName: string): TRLSender;
begin
  Result := GetSender(AName);
  if not Assigned(Result) then
    raise ERLSenderNotFound.CreateFmt('Sender "%s" not found.', [AName]);
end;

end.
