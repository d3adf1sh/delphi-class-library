//=============================================================================
// U_IDReceiver
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_IDReceiver;

{ TODO -oRafael : N�o aceitar conex�es enquanto estiver fechando (FClosing). }
{ TODO -oRafael : Socket Error 10093. }
{ TODO -oRafael : TIDSync deprecated. }

interface

uses
  SysUtils,
  Classes,
  IDContext,
  IDGlobal,
  IDSync,
  IDTCPServer,
  U_Message,
  U_Receiver;

type
  // Forward declarations
  TRLIDReceiver = class;

  // TRLIDReceiverSync
  {$WARNINGS OFF}
  TRLIDReceiverSync = class(TIDSync)
  private
    FReceiver: TRLIDReceiver;
    FMessage: TRLMessage;
  protected
    procedure DoSynchronize; override;
  public
    property Receiver: TRLIDReceiver read FReceiver write FReceiver;
    property Message: TRLMessage read FMessage write FMessage;
  end;
  {$WARNINGS ON}

  // TRLIDReceiver
  TRLIDReceiver = class(TRLReceiver)
  private
    FServer: TIDTCPServer;
    FInterval: Integer;
    FClosing: Boolean;
  protected
    function CreateServer: TIDTCPServer; virtual;
    procedure Execute(AContext: TIDContext); virtual;
    property Server: TIDTCPServer read FServer;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; override;
    procedure Close; override;
    function IsOpen: Boolean; override;
  end;

implementation

uses
  U_MessagingCLC,
  U_RTTIContext,
  U_IDUtils;

{ TRLIDReceiverSync }

procedure TRLIDReceiverSync.DoSynchronize;
begin
  FReceiver.ProcessMessage(FMessage);
end;

{ TRLIDReceiver }

constructor TRLIDReceiver.Create;
begin
  inherited;
  FServer := CreateServer;
end;

destructor TRLIDReceiver.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FServer);
end;

function TRLIDReceiver.CreateServer: TIDTCPServer;
begin
  Result := TIDTCPServer.Create(nil);
  Result.ReuseSocket := rsTrue;
  Result.OnExecute := Execute;
end;

procedure TRLIDReceiver.Execute(AContext: TIDContext);
var
  lStream: TMemoryStream;
  lMessage: TRLMessage;
  lSync: TRLIDReceiverSync;
begin
  Sleep(FInterval);
  if not AContext.Connection.IOHandler.InputBufferIsEmpty then
  begin
    lStream := TMemoryStream.Create;
    try
      while not AContext.Connection.IOHandler.InputBufferIsEmpty do
      begin
        TRLIDUtils.ReadStream(AContext.Connection.IOHandler, lStream);
        lMessage := TRLMessage(
          TRLRTTIContext.Instance.FromStream(nil, lStream, sfXML));
        if not Assigned(lMessage) then
          lMessage := TRLMessage.Create;
        try
          try
            {$WARNINGS OFF}
            lSync := TRLIDReceiverSync.Create;
            try
              lSync.Receiver := Self;
              lSync.Message := lMessage;
              lSync.Synchronize;
            finally
              FreeAndNil(lSync);
            end;
            {$WARNINGS ON}
          finally
            TRLRTTIContext.Instance.ToStream(lMessage, lStream, sfXML);
            TRLIDUtils.WriteStream(AContext.Connection.IOHandler, lStream);
          end;
        finally
          FreeAndNil(lMessage);
        end;
      end;
    finally
      FreeAndNil(lStream);
    end;
  end;
end;

procedure TRLIDReceiver.Open;
begin
  if not IsOpen then
  begin
    //ListenQueue: 15;
    //MaxConnections: 0;
    //TerminateWaitTime: 5000.
    FInterval := StrToIntDef(Settings['Interval'], RLDefaultInterval);
    FServer.DefaultPort := StrToIntDef(Settings['ListenPort'],
      RLDefaultListenPort);
    FServer.Active := True;
  end;
end;

procedure TRLIDReceiver.Close;
begin
  if IsOpen then
  begin
    FClosing := True;
    try
      FServer.Active := False;
    finally
      FClosing := False;
    end;
  end;
end;

function TRLIDReceiver.IsOpen: Boolean;
begin
  Result := FServer.Active;
end;

initialization
  RegisterClass(TRLIDReceiver);

end.
