//=============================================================================
// U_Message
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Message;

interface

uses
  Classes,
  U_BaseType;

type
  // Forward declarations
  TRLMessage = class;

  // Events
  TRLMessageEvent = procedure(Sender: TObject; Message: TRLMessage) of object;

  // TRLMsg
  TRLMessage = class(TRLBaseType)
  private
    FCode: Integer;
    FReceived: Boolean;
    FFaultName: string;
    FFaultMessage: string;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Code: Integer read FCode write FCode;
    property Received: Boolean read FReceived write FReceived;
    property FaultName: string read FFaultName write FFaultName;
    property FaultMessage: string read FFaultMessage write FFaultMessage;
  end;

implementation

{ TRLMessage }

procedure TRLMessage.Assign(Source: TPersistent);
var
  lMessage: TRLMessage;
begin
  inherited;
  if Source is TRLMessage then
  begin
    lMessage := TRLMessage(Source);
    Code := lMessage.Code;
    Received := lMessage.Received;
    FaultName := lMessage.FaultName;
    FaultMessage := lMessage.FaultMessage;
  end;
end;

initialization
  RegisterClass(TRLMessage);

end.
