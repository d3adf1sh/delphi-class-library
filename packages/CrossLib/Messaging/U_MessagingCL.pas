//=============================================================================
// U_MessagingCL
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MessagingCL;

interface

uses
  U_Message,
  U_IDSender,
  U_IDReceiver;
  
implementation

end.
