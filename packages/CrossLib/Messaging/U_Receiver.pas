//=============================================================================
// U_Receiver
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Receiver;

{ TODO -oRafael : N�o retornar os par�metros de entrada. }

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Settings,
  U_Message;

type
  // References
  TRLReceiverClass = class of TRLReceiver;

  // Exceptions
  ERLReceiverNotFound = class(ERLException);

  // TRLReceiver
  TRLReceiver = class(TRLBaseType)
  private
    FSettings: TRLSettings;
    FOnMessage: TRLMessageEvent;
  protected
    function CreateSettings: TRLSettings; virtual;
    procedure ProcessMessage(const AMessage: TRLMessage); virtual;
    procedure DoMessage(const AMessage: TRLMessage); virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; virtual; abstract;
    procedure Close; virtual; abstract;
    function IsOpen: Boolean; virtual; abstract;
    property Settings: TRLSettings read FSettings;
    property OnMessage: TRLMessageEvent read FOnMessage write FOnMessage;
    class function GetReceiver(const AName: string): TRLReceiver;
    class function FindReceiver(const AName: string): TRLReceiver;
  end;

implementation

{ TRLReceiver }

constructor TRLReceiver.Create;
begin
  inherited;
  FSettings := CreateSettings;
end;

destructor TRLReceiver.Destroy;
begin
  Close;
  FreeAndNil(FSettings);
  inherited;
end;

function TRLReceiver.CreateSettings: TRLSettings;
begin
  Result := TRLSettings.Create;
end;

procedure TRLReceiver.ProcessMessage(const AMessage: TRLMessage);
begin
  AMessage.Received := True;
  try
    DoMessage(AMessage);
  except
    on E: Exception do
    begin
      if E is ERLException then
        AMessage.FaultName := E.ClassName
      else
        AMessage.FaultName := ERLException.ClassName;
      AMessage.FaultMessage := E.Message;
    end;
  end;
end;

procedure TRLReceiver.DoMessage(const AMessage: TRLMessage);
begin
  if Assigned(FOnMessage) then
    FOnMessage(Self, AMessage);
end;

class function TRLReceiver.GetReceiver(const AName: string): TRLReceiver;
var
  lClass: TRLReceiverClass;
begin
  lClass := TRLReceiverClass(GetClass(Format('TRL%sReceiver', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLReceiver.FindReceiver(const AName: string): TRLReceiver;
begin
  Result := GetReceiver(AName);
  if not Assigned(Result) then
    raise ERLReceiverNotFound.CreateFmt('Receiver "%s" not found.', [AName]);
end;

end.
