//=============================================================================
// U_IDSender
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_IDSender;

interface

uses
  SysUtils,
  Classes,
  IDGlobal,
  IDTCPClient,
  U_Message,
  U_Sender;

type
  // TRLIDSender
  TRLIDSender = class(TRLSender)
  private
    FClient: TIDTCPClient;
  protected
    procedure SendMessage(const AMessage: TRLMessage); override;
    function CreateClient: TIDTCPClient; virtual;
    property Client: TIDTCPClient read FClient;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; override;
    procedure Close; override;
    function IsOpen: Boolean; override;
  end;

implementation

uses
  U_MessagingCLC,
  U_RTTIContext,
  U_IDUtils;

{ TRLIDSender }

constructor TRLIDSender.Create;
begin
  inherited;
  FClient := CreateClient;
end;

destructor TRLIDSender.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FClient);
end;

procedure TRLIDSender.SendMessage(const AMessage: TRLMessage);
var
  lStream: TMemoryStream;
begin
  if Assigned(AMessage) then
  begin
    lStream := TMemoryStream.Create;
    try
      TRLRTTIContext.Instance.ToStream(AMessage, lStream, sfXML);
      TRLIDUtils.WriteStream(FClient.IOHandler, lStream);
      TRLIDUtils.ReadStream(FClient.IOHandler, lStream);
      TRLRTTIContext.Instance.FromStream(AMessage, lStream, sfXML);
    finally
      FreeAndNil(lStream);
    end;
  end;
end;

function TRLIDSender.CreateClient: TIDTCPClient;
begin
  Result := TIDTCPClient.Create(nil);
  Result.ReuseSocket := rsTrue;
end;

procedure TRLIDSender.Open;
begin
  if not IsOpen then
  begin
    FClient.Host := Settings['Host'];
    FClient.Port := StrToIntDef(Settings['Port'], RLDefaultListenPort);
    FClient.ReadTimeout := StrToIntDef(Settings['ReadTimeout'], RLDefaultReadTimeout);
    FClient.ConnectTimeout := StrToIntDef(Settings['ConnectionTimeout'],
      RLDefaultConnectionTimeout);
    FClient.Connect;
  end;
end;

procedure TRLIDSender.Close;
begin
  if IsOpen then
  begin
    FClient.IOHandler.InputBuffer.Clear;
    FClient.Disconnect;
  end;
end;

function TRLIDSender.IsOpen: Boolean;
begin
  Result := FClient.Connected;
end;

initialization
  RegisterClass(TRLIDSender);

end.
