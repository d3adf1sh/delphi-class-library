//=============================================================================
// U_EmailNotification
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailNotification;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_StringList,
  U_Validator,
  U_EmailAccounts,
  U_EmailSender;

type
  // Exceptions
  ERLSenderNameIsEmpty = class(ERLException);

  // TRLEmailNotification
  TRLEmailNotification = class(TRLBaseType, IRLValidator)
  private
    FSenderName: string;
    FAccountName: string;
    FAccountDescription: string;
    FAccountEmail: string;
    FAddresses: TRLStringList;
    FCC: TRLStringList;
    FBCC: TRLStringList;
    FEnabled: Boolean;
    FAccount: TRLEmailAccount;
    procedure SetAccountName(const AValue: string);
    function GetAccount: TRLEmailAccount;
  protected
    function CreateAddresses: TRLStringList; virtual;
    function CreateCC: TRLStringList; virtual;
    function CreateBCC: TRLStringList; virtual;
    procedure AccountNameChanged; virtual;
    procedure CheckSenderName; virtual;
    procedure CheckAccount; virtual;
    procedure CheckAddresses; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Validate; virtual;
    property Account: TRLEmailAccount read GetAccount;
    function GetEmail: TRLEmailSender; overload; virtual;
    function GetEmail(const ASubject: string): TRLEmailSender; overload; virtual;
    function GetEmail(const ASubject: string; const ABody: TRLStringList): TRLEmailSender; overload; virtual;
    function GetEmail(const ASubject: string; const ABody, AAttachments: TRLStringList): TRLEmailSender; overload; virtual;
    function GetEmail(const ASubject: string; const ABody, AAttachments: TRLStringList; const AContentType: TRLEmailContentType): TRLEmailSender; overload; virtual;
    function SendEmail(const ASubject: string; const ABody, AAttachments: TRLStringList; const AContentType: TRLEmailContentType; const AForce: Boolean): Boolean; virtual;
  published
    property SenderName: string read FSenderName write FSenderName;
    property AccountName: string read FAccountName write SetAccountName;
    property AccountDescription: string read FAccountDescription write FAccountDescription;
    property AccountEmail: string read FAccountEmail write FAccountEmail;
    property Addresses: TRLStringList read FAddresses;
    property CC: TRLStringList read FCC;
    property BCC: TRLStringList read FBCC;
    property Enabled: Boolean read FEnabled write FEnabled;
  end;

implementation

{ TRLEmailNotification }

constructor TRLEmailNotification.Create;
begin
  inherited;
  FAddresses := CreateAddresses;
  FCC := CreateCC;
  FBCC := CreateBCC;
  FEnabled := True;
end;

destructor TRLEmailNotification.Destroy;
begin
  FreeAndNil(FBCC);
  FreeAndNil(FCC);
  FreeAndNil(FAddresses);
  inherited;
end;

procedure TRLEmailNotification.SetAccountName(const AValue: string);
begin
  if AValue <> FAccountName then
  begin
    FAccountName := AValue;
    FAccount := nil;
    AccountNameChanged;
  end;
end;

function TRLEmailNotification.GetAccount: TRLEmailAccount;
begin
  if not Assigned(FAccount) then
    FAccount := TRLEmailAccounts.Instance.Find(FAccountName);
  Result := FAccount;
end;

function TRLEmailNotification.CreateAddresses: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLEmailNotification.CreateCC: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLEmailNotification.CreateBCC: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

procedure TRLEmailNotification.AccountNameChanged;
begin
end;

procedure TRLEmailNotification.CheckSenderName;
begin
  if FSenderName = '' then
    raise ERLSenderNameIsEmpty.Create('SenderName is empty.');
end;

procedure TRLEmailNotification.CheckAccount;
begin
  if not Assigned(Account) then
    raise ERLEmailAccountIsInvalid.CreateFmt('Account "%s" is invalid.',
      [FAccountName]);
end;

procedure TRLEmailNotification.CheckAddresses;
begin
  if FAddresses.Count = 0 then
    raise ERLAddressesListIsEmpty.Create('Addresses list is empty.');
end;

procedure TRLEmailNotification.Assign(Source: TPersistent);
var
  lNotification: TRLEmailNotification;
begin
  inherited;
  if Source is TRLEmailNotification then
  begin
    lNotification := TRLEmailNotification(Source);
    SenderName := lNotification.SenderName;
    AccountName := lNotification.AccountName;
    AccountDescription := lNotification.AccountDescription;
    AccountEmail := lNotification.AccountEmail;
    Addresses.Assign(lNotification.Addresses);
    CC.Assign(lNotification.CC);
    BCC.Assign(lNotification.BCC);
    Enabled := lNotification.Enabled;
  end;
end;

procedure TRLEmailNotification.Validate;
begin
  CheckSenderName;
  CheckAccount;
  CheckAddresses;
end;

function TRLEmailNotification.GetEmail: TRLEmailSender;
begin
  Result := GetEmail('');
end;

function TRLEmailNotification.GetEmail(const ASubject: string): TRLEmailSender;
begin
  Result := GetEmail(ASubject, nil);
end;

function TRLEmailNotification.GetEmail(const ASubject: string;
  const ABody: TRLStringList): TRLEmailSender;
begin
  Result := GetEmail(ASubject, ABody, nil);
end;

function TRLEmailNotification.GetEmail(const ASubject: string; const ABody,
  AAttachments: TRLStringList): TRLEmailSender;
begin
  Result := GetEmail(ASubject, ABody, AAttachments, ectPlainText);
end;

function TRLEmailNotification.GetEmail(const ASubject: string; const ABody,
  AAttachments: TRLStringList;
  const AContentType: TRLEmailContentType): TRLEmailSender;
begin
  Validate;
  Result := TRLEmailSender.FindEmailSender(FSenderName);
  Result.Subject := ASubject;
  Result.Account.Assign(Account);
  if FAccountDescription <> '' then
    Result.Account.Description := FAccountDescription;
  if FAccountEmail <> '' then
    Result.Account.Email := FAccountEmail;
  Result.Addresses.Assign(FAddresses);
  Result.CC.Assign(FCC);
  Result.BCC.Assign(FBCC);
  if Assigned(ABody) then
    Result.Body.Assign(ABody);
  if Assigned(AAttachments) then
    Result.Attachments.Assign(AAttachments);
  Result.ContentType := AContentType;
end;

function TRLEmailNotification.SendEmail(const ASubject: string; const ABody,
  AAttachments: TRLStringList; const AContentType: TRLEmailContentType;
  const AForce: Boolean): Boolean;
var
  lEmail: TRLEmailSender;
begin
  if FEnabled or AForce then
  begin
    lEmail := GetEmail(ASubject, ABody, AAttachments, AContentType);
    try
      lEmail.Send;
      Result := True;
    finally
      FreeAndNil(lEmail);
    end;
  end
  else
    Result := False;
end;

end.
