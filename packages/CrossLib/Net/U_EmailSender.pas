//=============================================================================
// U_EmailSender
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailSender;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_StringList,
  U_EmailAccounts;

type
  // Enumerations
  TRLEmailContentType = (ectPlainText, ectHTML);

  // References
  TRLEmailSenderClass = class of TRLEmailSender;

  // Exceptions
  ERLEmailSenderNotFound = class(ERLException);
  ERLSubjectIsEmpty = class(ERLException);
  ERLAddressesListIsEmpty = class(ERLException);
  ERLAttachmentFileNotFound = class(ERLException);
  ERLErrorSendingEmail = class(ERLException);

  // TRLEmailSender
  TRLEmailSender = class(TRLBaseType)
  private
    FAccount: TRLEmailAccount;
    FSubject: string;
    FAddresses: TRLStringList;
    FCC: TRLStringList;
    FBCC: TRLStringList;
    FBody: TRLStringList;
    FAttachments: TRLStringList;
    FContentType: TRLEmailContentType;
  protected
    function CreateAccount: TRLEmailAccount; virtual;
    function CreateAddresses: TRLStringList; virtual;
    function CreateCC: TRLStringList; virtual;
    function CreateBCC: TRLStringList; virtual;
    function CreateBody: TRLStringList; virtual;
    function CreateAttachments: TRLStringList; virtual;
    procedure CheckAccount; virtual;
    procedure CheckSubject; virtual;
    procedure CheckAddresses; virtual;
    procedure CheckAttachments; virtual;
    procedure SendEmail; virtual; abstract;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Send; virtual;
    property Account: TRLEmailAccount read FAccount;
    property Subject: string read FSubject write FSubject;
    property Addresses: TRLStringList read FAddresses;
    property CC: TRLStringList read FCC;
    property BCC: TRLStringList read FBCC;
    property Body: TRLStringList read FBody;
    property Attachments: TRLStringList read FAttachments;
    property ContentType: TRLEmailContentType read FContentType write FContentType;
    class function GetEmailSender(const AName: string): TRLEmailSender;
    class function FindEmailSender(const AName: string): TRLEmailSender;
  end;

implementation

{ TRLEmailSender }

constructor TRLEmailSender.Create;
begin
  inherited;
  FAccount := CreateAccount;
  FAddresses := CreateAddresses;
  FCC := CreateCC;
  FBCC := CreateBCC;
  FBody := CreateBody;
  FAttachments := CreateAttachments;
end;

destructor TRLEmailSender.Destroy;
begin
  FreeAndNil(FAttachments);
  FreeAndNil(FBody);
  FreeAndNil(FBCC);
  FreeAndNil(FCC);
  FreeAndNil(FAddresses);
  FreeAndNil(FAccount);
  inherited;
end;

function TRLEmailSender.CreateAccount: TRLEmailAccount;
begin
  Result := TRLEmailAccount.Create(nil);
  Result.Name := 'Default';
end;

function TRLEmailSender.CreateAddresses: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLEmailSender.CreateCC: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLEmailSender.CreateBCC: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLEmailSender.CreateBody: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLEmailSender.CreateAttachments: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

procedure TRLEmailSender.CheckAccount;
begin
  FAccount.Validate;
end;

procedure TRLEmailSender.CheckSubject;
begin
  if FSubject = '' then
    raise ERLSubjectIsEmpty.Create('Subject is empty.');
end;

procedure TRLEmailSender.CheckAddresses;
begin
  if FAddresses.Count = 0 then
    raise ERLAddressesListIsEmpty.Create('Addresses list is empty.');
end;

procedure TRLEmailSender.CheckAttachments;
var
  I: Integer;
begin
  for I := 0 to Pred(FAttachments.Count) do
    if not FileExists(FAttachments[I]) then
      raise ERLAttachmentFileNotFound.CreateFmt(
        'Attachment file "%s" not found.', [FAttachments[I]]);
end;

procedure TRLEmailSender.Send;
begin
  CheckAccount;
  CheckSubject;
  CheckAddresses;
  CheckAttachments;
  try
    SendEmail;
  except
    on E: Exception do
      raise ERLErrorSendingEmail.Create('Could not send email.' + sLineBreak +
        E.Message);
  end;
end;

class function TRLEmailSender.GetEmailSender(
  const AName: string): TRLEmailSender;
var
  lClass: TRLEmailSenderClass;
begin
  lClass := TRLEmailSenderClass(GetClass(Format('TRL%sEmailSender', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLEmailSender.FindEmailSender(
  const AName: string): TRLEmailSender;
begin
  Result := GetEmailSender(AName);
  if not Assigned(Result) then
    raise ERLEmailSenderNotFound.CreateFmt('EmailSender "%s" not found.',
      [AName]);
end;

end.
