//=============================================================================
// U_EmailAccounts
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccounts;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_NamedCollection,
  U_Dictionary,
  U_AttributeSerializer;

const
  CPassword = 'Password';

type
  // Enumerations
  TRLTLS = (tlsNone, tlsImplicit, tlsExplicit);

  // Exceptions
  ERLEmailIsEmpty = class(ERLException);
  ERLHostIsEmpty = class(ERLException);
  ERLInvalidPort = class(ERLException);
  ERLEmailAccountNotFound = class(ERLException);
  ERLEmailAccountIsNull = class(ERLException); //External
  ERLEmailAccountIsInvalid = class(ERLException); //External

  // TRLEmailAccount
  TRLEmailAccount = class(TRLNamedCollectionItem, IRLAttributeSerializer)
  private
    FDescription: string;
    FEmail: string;
    FHost: string;
    FPort: Integer;
    FUserName: string;
    FPassword: string;
    FTLS: TRLTLS;
  protected
    function IsSerializableAttribute(const AName: string): Boolean; virtual;
    function ReadAttributeValue(const AName: string; out AValue: string): Boolean; virtual;
    function WriteAttributeValue(const AName, AValue: string): Boolean; virtual;
    procedure CheckEmail; virtual;
    procedure CheckHost; virtual;
    procedure CheckPort; virtual;
  public
    procedure Assign(Source: TPersistent); override;
    procedure Validate; override;
  published
    property Description: string read FDescription write FDescription;
    property Email: string read FEmail write FEmail;
    property Host: string read FHost write FHost;
    property Port: Integer read FPort write FPort;
    property UserName: string read FUserName write FUserName;
    property Password: string read FPassword write FPassword;
    property TLS: TRLTLS read FTLS write FTLS;
  end;

  // TRLEmailAccounts
  TRLEmailAccounts = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLEmailAccount;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TRLEmailAccount;
    function Insert(const AIndex: Integer): TRLEmailAccount;
    function Find(const AName: string): TRLEmailAccount;
    function FindByName(const AName: string): TRLEmailAccount;
    property Items[const AIndex: Integer]: TRLEmailAccount read GetItem; default;
    class function Instance: TRLEmailAccounts;
    class procedure DeleteInstance;
  end;

resourcestring
  SRLTLSNone = 'None';
  SRLTLSImplicit = 'Implicit';
  SRLTLSExplicit = 'Explicit';

var
  RLTLSCaption: array[TRLTLS] of string = (SRLTLSNone, SRLTLSImplicit,
    SRLTLSExplicit);

// Utils
function TLSToString(const AValue: TRLTLS): string;
function StringToTLS(const AValue: string): TRLTLS;
procedure PopulateTLSDictionary(const ADictionary: TRLDictionary);

implementation

uses
  TypInfo,
  U_System;

var
  RLInstance: TRLEmailAccounts = nil;

// Utils
function TLSToString(const AValue: TRLTLS): string;
begin
  Result := GetEnumName(TypeInfo(TRLTLS), Ord(AValue));
end;

function StringToTLS(const AValue: string): TRLTLS;
begin
  Result := TRLTLS(GetEnumValue(TypeInfo(TRLTLS), AValue));
end;

procedure PopulateTLSDictionary(const ADictionary: TRLDictionary);
var
  I: TRLTLS;
begin
  if Assigned(ADictionary) then
  begin
    ADictionary.Clear;
    for I := Low(TRLTLS) to High(TRLTLS) do
      ADictionary.Add(TLSToString(I), RLTLSCaption[I]);
  end;
end;

{ TRLEmailAccount }

function TRLEmailAccount.IsSerializableAttribute(const AName: string): Boolean;
begin
  Result := True;
end;

function TRLEmailAccount.ReadAttributeValue(const AName: string;
  out AValue: string): Boolean;
begin
  Result := SameText(AName, CPassword);
  if Result then
    AValue := U_System.EncryptString(Password);
end;

function TRLEmailAccount.WriteAttributeValue(const AName,
  AValue: string): Boolean;
begin
  Result := SameText(AName, CPassword);
  if Result then
    Password := U_System.DecryptString(AValue);
end;

procedure TRLEmailAccount.CheckEmail;
begin
  if FEmail = '' then
    raise ERLEmailIsEmpty.Create('Email is empty.');
end;

procedure TRLEmailAccount.CheckHost;
begin
  if FHost = '' then
    raise ERLHostIsEmpty.Create('Host is empty.');
end;

procedure TRLEmailAccount.CheckPort;
begin
  if FPort <= 0 then
    raise ERLInvalidPort.Create('Invalid port.');
end;

procedure TRLEmailAccount.Assign(Source: TPersistent);
var
  lAccount: TRLEmailAccount;
begin
  inherited;
  if Source is TRLEmailAccount then
  begin
    lAccount := TRLEmailAccount(Source);
    Description := lAccount.Description;
    Email := lAccount.Email;
    Host := lAccount.Host;
    Port := lAccount.Port;
    UserName := lAccount.UserName;
    Password := lAccount.Password;
    TLS := lAccount.TLS;
  end;
end;

procedure TRLEmailAccount.Validate;
begin
  inherited;
  CheckEmail;
  CheckHost;
  CheckPort;
end;

{ TRLEmailAccounts }

constructor TRLEmailAccounts.Create;
begin
  inherited;
  HashSize := 16;
end;

function TRLEmailAccounts.GetItem(const AIndex: Integer): TRLEmailAccount;
begin
  Result := TRLEmailAccount(inherited Items[AIndex]);
end;

function TRLEmailAccounts.GetItemClass: TCollectionItemClass;
begin
  Result := TRLEmailAccount;
end;

function TRLEmailAccounts.Add: TRLEmailAccount;
begin
  Result := TRLEmailAccount(inherited Add);
end;

function TRLEmailAccounts.Insert(const AIndex: Integer): TRLEmailAccount;
begin
  Result := TRLEmailAccount(inherited Insert(AIndex));
end;

function TRLEmailAccounts.Find(const AName: string): TRLEmailAccount;
begin
  Result := TRLEmailAccount(inherited Find(AName));
end;

function TRLEmailAccounts.FindByName(const AName: string): TRLEmailAccount;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLEmailAccountNotFound.CreateFmt('EmailAccount "%s" not found.',
      [AName]);
end;

class function TRLEmailAccounts.Instance: TRLEmailAccounts;
begin
  if not Assigned(RLInstance) then
    RLInstance := TRLEmailAccounts.Create;
  Result := RLInstance;
end;

class procedure TRLEmailAccounts.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLEmailAccounts.DeleteInstance;

end.
