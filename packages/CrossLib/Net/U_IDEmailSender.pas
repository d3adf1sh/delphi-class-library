//=============================================================================
// U_IDEmailSender
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_IDEmailSender;

{$I RL.INC}

interface

{ TODO -oRafael : Proxy. }

uses
  SysUtils,
  Classes,
  IDAttachmentFile,
  IDExplicitTLSClientServerBase,
  IDMessage,
  IDSMTP,
  IDSSLOpenSSL,
  IDText,
  U_EmailAccounts,
  U_EmailSender;

type
  // TRLIDEmailSender
  TRLIDEmailSender = class(TRLEmailSender)
  protected
    procedure SendEmail; override;
  end;

implementation

uses
  StrUtils,
  U_NetCLC;

{ TRLIDEmailSender }

procedure TRLIDEmailSender.SendEmail;
var
  sCharSet: string;
  sContentType: string;
  lMessage: TIDMessage;
  lText: TIDText;
  lSSL: TIDSSLIOHandlerSocketOpenSSL;
  lSMTP: TIDSMTP;
  I: Integer;
begin
  sCharSet := 'iso-8859-1';
  sContentType := IfThen(ContentType = ectHTML, 'text/html', 'text/plain');
  lMessage := TIDMessage.Create(nil);
  try
    lMessage.From.Name := IfThen(Account.Description <> '', Account.Description,
      Account.Email);
    lMessage.From.Address := Account.Email;
    lMessage.ReplyTo.EmailAddresses := lMessage.From.Address;
    lMessage.Subject := Subject;
    for I := 0 to Pred(Addresses.Count) do
      lMessage.Recipients.Add.Address := Addresses[I];
    for I := 0 to Pred(CC.Count) do
      lMessage.CCList.Add.Address := CC[I];
    for I := 0 to Pred(BCC.Count) do
      lMessage.BCCList.Add.Address := BCC[I];
    if Attachments.Count = 0 then
    begin
      lMessage.Body.Assign(Body);
      lMessage.ContentType := sContentType;
      lMessage.CharSet := sCharSet;
    end
    else
    begin
      lMessage.ContentType := 'multipart/alternative';
      lText := TIDText.Create(lMessage.MessageParts);
      lText.ContentType := sContentType;
      lText.CharSet := sCharSet;
      lText.Body.Assign(Body);
      for I := 0 to Pred(Attachments.Count) do
        TIDAttachmentFile.Create(lMessage.MessageParts, Attachments[I]);
    end;

    if Account.TLS <> tlsNone then
    begin
      lSSL := TIDSSLIOHandlerSocketOpenSSL.Create(nil);
      lSSL.SSLOptions.Method := sslvTLSv1;
    end
    else
      lSSL := nil;
    try
      lSMTP := TIDSMTP.Create(nil);
      try
        lSMTP.Host := Account.Host;
        lSMTP.Port := Account.Port;
        lSMTP.Username := Account.UserName;
        lSMTP.Password := Account.Password;
        lSMTP.IOHandler := lSSL;
{$IFDEF D15}
        lSMTP.ReadTimeout := RLDefaultSMTPTimeout;
{$ENDIF}
        case Account.TLS of
          tlsNone: lSMTP.UseTLS := utNoTLSSupport;
          tlsImplicit: lSMTP.UseTLS := utUseImplicitTLS;
          tlsExplicit: lSMTP.UseTLS := utUseExplicitTLS;
        end;

        lSMTP.Connect;
        lSMTP.Send(lMessage);
      finally
        FreeAndNil(lSMTP);
      end;
    finally
      if Assigned(lSSL) then
        FreeAndNil(lSSL);
    end;
  finally
    FreeAndNil(lMessage);
  end;
end;

initialization
  RegisterClass(TRLIDEmailSender);

end.
