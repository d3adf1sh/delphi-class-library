//=============================================================================
// U_IDUtils
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_IDUtils;

interface

uses
  Classes,
  IDIOHandler,
  U_BaseType;

type
  // TRLIDUtils
  TRLIDUtils = class(TRLBaseType)
  public
    class procedure WriteStream(const AIOHandler: TIDIOHandler; const AStream: TStream);
    class procedure ReadStream(const AIOHandler: TIDIOHandler; const AStream: TStream);
    { TODO -oRafael : Complementar. }
  end;

implementation

{ TRLIDUtils }

class procedure TRLIDUtils.WriteStream(const AIOHandler: TIDIOHandler;
  const AStream: TStream);
begin
  if Assigned(AIOHandler) and Assigned(AStream) then
  begin
    AIOHandler.WriteBufferOpen;
    try
      AIOHandler.Write(LongInt(AStream.Size)); //must be LongInt
      AIOHandler.Write(AStream);
      AIOHandler.WriteBufferFlush;
    finally
      AIOHandler.WriteBufferClose;
    end;
  end;
end;

class procedure TRLIDUtils.ReadStream(const AIOHandler: TIDIOHandler;
  const AStream: TStream);
begin
  if Assigned(AIOHandler) and Assigned(AStream) then
  begin
    AStream.Size := 0;
    AIOHandler.ReadStream(AStream, AIOHandler.ReadInt32);
  end;
end;

end.
