//=============================================================================
// U_RTTIDefaultInspector
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_RTTIDefaultInspector;

{$I RL.INC}

interface

uses
  SysUtils,
  TypInfo,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_RTTIAttributes,
  U_RTTIClasses,
  U_RTTIInspector;

type
  // TRLRTTIDefaultInspector
  TRLRTTIDefaultInspector = class(TRLRTTIInspector)
  protected
    procedure PopulateAttributes(const AClass: TRLRTTIClass); override;
    function GetValueType(const ATypeInfo: PTypeInfo): TRLRTTIAttributeValueType; virtual;
  public
    function ReadChar(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Char; override;
    function ReadString(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): string; override;
    function ReadInteger(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Integer; override;
    function ReadInt64(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Int64; override;
    function ReadFloat(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Double; override;
    function ReadCurrency(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Currency; override;
    function ReadDate(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TDate; override;
    function ReadTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TTime; override;
    function ReadDateTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TDateTime; override;
    function ReadBoolean(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Boolean; override;
    function ReadEnumeration(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): string; override;
    function ReadSet(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): string; override;
    function ReadObject(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TObject; override;
    function ReadVariant(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Variant; override;
    procedure WriteChar(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Char); override;
    procedure WriteString(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: string); override;
    procedure WriteInteger(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Integer); override;
    procedure WriteInt64(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Int64); override;
    procedure WriteFloat(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Double); override;
    procedure WriteCurrency(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Currency); override;
    procedure WriteDate(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TDate); override;
    procedure WriteTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TTime); override;
    procedure WriteDateTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TDateTime); override;
    procedure WriteBoolean(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Boolean); override;
    procedure WriteEnumeration(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: string); override;
    procedure WriteSet(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: string); override;
    procedure WriteObject(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TObject); override;
    procedure WriteVariant(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Variant); override;
  end;

implementation

uses
  Variants;

{ TRLRTTIDefaultInspector }

procedure TRLRTTIDefaultInspector.PopulateAttributes(
  const AClass: TRLRTTIClass);
var
  lAttribute: TRLRTTIAttribute;
  ptrPropList: PPropList;
  ptrTypeData: PTypeData;
  nPropCount: Integer;
  I: Integer;
begin
  if Assigned(AClass) then
  begin
    New(ptrPropList);
    try
      nPropCount := GetPropList(AClass.Reference.ClassInfo, tkProperties,
        ptrPropList, False);
      AClass.Attributes.BeginUpdate;
      try
        AClass.Attributes.Clear;
        for I := 0 to Pred(nPropCount) do
        begin
          lAttribute := AClass.Attributes.Add;
{$IFDEF NEXTGEN}
          lAttribute.Name := ptrPropList[I]^.NameFld.ToString;
{$ELSE}
  {$IFDEF D15UP}
          lAttribute.Name := string(ptrPropList[I]^.Name);
  {$ELSE}
          lAttribute.Name := ptrPropList[I]^.Name;
  {$ENDIF}
{$ENDIF}
          lAttribute.ValueType := GetValueType(ptrPropList[I]^.PropType^);
          if lAttribute.ValueType = avtObject then
          begin
            ptrTypeData := GetTypeData(ptrPropList[I]^.PropType^);
            lAttribute.Reference := ptrTypeData^.ClassType;
          end;

          lAttribute.IsReadable := Assigned(ptrPropList[I]^.GetProc);
          lAttribute.IsWritable := Assigned(ptrPropList[I]^.SetProc);
        end;
      finally
        AClass.Attributes.EndUpdate;
      end;
    finally
       Dispose(ptrPropList);
    end;
  end;
end;

function TRLRTTIDefaultInspector.GetValueType(
  const ATypeInfo: PTypeInfo): TRLRTTIAttributeValueType;
var
  sTypeName: string;
begin
  Result := avtUnknow;
  if Assigned(ATypeInfo) then
  begin
{$IFDEF NEXTGEN}
    sTypeName := ATypeInfo^.NameFld.ToString;
{$ELSE}
  {$IFDEF D15UP}
    sTypeName := string(ATypeInfo^.Name);
  {$ELSE}
    sTypeName := ATypeInfo^.Name;
  {$ENDIF}
{$ENDIF}
    case ATypeInfo^.Kind of
      tkChar, tkWChar: Result := avtChar;
      tkString, tkLString, tkWString: Result := avtString;
{$IFDEF D15UP}
      tkUString: Result := avtString;
{$ENDIF}
      tkInteger: Result := avtInteger;
      tkInt64: Result := avtInt64;
      tkFloat:
        begin
          if SameText('Currency', sTypeName) then
            Result := avtCurrency
          else if SameText('TDate', sTypeName) then
            Result := avtDate
          else if SameText('TTime', sTypeName) then
            Result := avtTime
          else if SameText('TDateTime', sTypeName) then
            Result := avtDateTime
          else
            Result := avtFloat;
        end;
      tkEnumeration:
        begin
          if SameText('Boolean', sTypeName) or
            SameText('Bytebool', sTypeName) or
            SameText('Wordbool', sTypeName) or
            SameText('Longbool', sTypeName) then
            Result := avtBoolean
          else
            Result := avtEnumeration;
        end;
      tkSet: Result := avtSet;
      tkClass: Result := avtObject;
      tkVariant: Result := avtVariant;
    end;
  end;
end;

function TRLRTTIDefaultInspector.ReadChar(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Char;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := Chr(GetOrdProp(AInstance, AAttribute.Name))
  else
    Result := #0;
end;

function TRLRTTIDefaultInspector.ReadString(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): string;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetStrProp(AInstance, AAttribute.Name)
  else
    Result := '';
end;

function TRLRTTIDefaultInspector.ReadInteger(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Integer;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetOrdProp(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadInt64(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Int64;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetInt64Prop(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadFloat(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Double;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetFloatProp(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadCurrency(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Currency;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetFloatProp(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadDate(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): TDate;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetFloatProp(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadTime(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): TTime;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetFloatProp(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadDateTime(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): TDateTime;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetFloatProp(AInstance, AAttribute.Name)
  else
    Result := 0;
end;

function TRLRTTIDefaultInspector.ReadBoolean(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Boolean;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := Boolean(GetOrdProp(AInstance, AAttribute.Name))
  else
    Result := False;
end;

function TRLRTTIDefaultInspector.ReadEnumeration(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): string;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetEnumProp(AInstance, AAttribute.Name)
  else
    Result := '';
end;

function TRLRTTIDefaultInspector.ReadSet(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): string;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetSetProp(AInstance, AAttribute.Name)
  else
    Result := '';
end;

function TRLRTTIDefaultInspector.ReadObject(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): TObject;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetObjectProp(AInstance, AAttribute.Name)
  else
    Result := nil;
end;

function TRLRTTIDefaultInspector.ReadVariant(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute): Variant;
begin
  if Assigned(AInstance) and Assigned(AAttribute) then
    Result := GetVariantProp(AInstance, AAttribute.Name)
  else
    Result := Null;
end;

procedure TRLRTTIDefaultInspector.WriteChar(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Char);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetOrdProp(AInstance, AAttribute.Name, Ord(AValue));
end;

procedure TRLRTTIDefaultInspector.WriteString(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: string);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetStrProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteInteger(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Integer);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetOrdProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteInt64(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Int64);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetInt64Prop(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteFloat(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Double);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetFloatProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteCurrency(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Currency);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetFloatProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteDate(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: TDate);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetFloatProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteTime(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: TTime);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetFloatProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteDateTime(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: TDateTime);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetFloatProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteBoolean(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Boolean);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetOrdProp(AInstance, AAttribute.Name, Integer(AValue));
end;

procedure TRLRTTIDefaultInspector.WriteEnumeration(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: string);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
  begin
    if AValue <> '' then
      SetEnumProp(AInstance, AAttribute.Name, AValue)
    else
      SetOrdProp(AInstance, AAttribute.Name, 0);
  end;
end;

procedure TRLRTTIDefaultInspector.WriteSet(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: string);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetSetProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteObject(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: TObject);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetObjectProp(AInstance, AAttribute.Name, AValue);
end;

procedure TRLRTTIDefaultInspector.WriteVariant(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Variant);
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable then
    SetVariantProp(AInstance, AAttribute.Name, AValue);
end;

end.
