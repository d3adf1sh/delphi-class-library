//=============================================================================
// U_ContainedObject
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ContainedObject;

interface

uses
  U_BaseType,
  U_Exception;

type
  // References
  TRLContainedObjectClass = class of TRLContainedObject;

  // Exceptions
  ERLParentIsNull = class(ERLException);

  // TRLContainedObject
  TRLContainedObject = class(TRLBaseType)
  private
    FParent: TObject;
    procedure SetParent(const AValue: TObject);
  protected
    procedure ParentChanged; virtual;
    procedure CheckParent; virtual;
  public
    constructor Create; override;
    constructor CreateContainedObject(const AParent: TObject); virtual;
    property Parent: TObject read FParent write SetParent;
  end;

implementation

{ TRLContainedObject }

constructor TRLContainedObject.Create;
begin
  CreateContainedObject(nil);
end;

constructor TRLContainedObject.CreateContainedObject(const AParent: TObject);
begin
  inherited Create;
  SetParent(AParent);
end;

procedure TRLContainedObject.ParentChanged;
begin
end;

procedure TRLContainedObject.CheckParent;
begin
  if not Assigned(FParent) then
    raise ERLParentIsNull.Create('Parent is null.');
end;

procedure TRLContainedObject.SetParent(const AValue: TObject);
begin
  if AValue <> FParent then
  begin
    FParent := AValue;
    ParentChanged;
  end;
end;

end.
