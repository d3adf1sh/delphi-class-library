//=============================================================================
// U_RTTIInspector
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_RTTIInspector;

{$I RL.INC}

interface

uses
  SysUtils,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_BaseType,
  U_RTTIAttributes,
  U_RTTIClasses;

type
  // TRLRTTIInspector
  TRLRTTIInspector = class(TRLBaseType)
  private
    FClasses: TRLRTTIClasses;
    FFormats: TFormatSettings;
  protected
    function CreateClasses: TRLRTTIClasses; virtual;
    procedure PopulateAttributes(const AClass: TRLRTTIClass); virtual; abstract;
  public
    constructor Create; override;
    destructor Destroy; override;
    function GetClass(const AClass: TClass): TRLRTTIClass; virtual;
    function GetAttributes(const AClass: TClass): TRLRTTIAttributes; virtual;
    function GetObjectClass(const AInstance: TObject): TRLRTTIClass; virtual;
    function GetObjectAttributes(const AInstance: TObject): TRLRTTIAttributes; virtual;
    function ReadValue(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AFormat: Boolean): Variant; virtual;
    function ReadChar(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Char; virtual; abstract;
    function ReadString(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): string; virtual; abstract;
    function ReadInteger(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Integer; virtual; abstract;
    function ReadInt64(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Int64; virtual; abstract;
    function ReadFloat(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Double; virtual; abstract;
    function ReadCurrency(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Currency; virtual; abstract;
    function ReadDate(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TDate; virtual; abstract;
    function ReadTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TTime; virtual; abstract;
    function ReadDateTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TDateTime; virtual; abstract;
    function ReadBoolean(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Boolean; virtual; abstract;
    function ReadEnumeration(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): string; virtual; abstract;
    function ReadSet(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): string; virtual; abstract;
    function ReadObject(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): TObject; virtual; abstract;
    function ReadVariant(const AInstance: TObject; const AAttribute: TRLRTTIAttribute): Variant; virtual; abstract;
    procedure WriteValue(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Variant; const AFormatted: Boolean); virtual;
    procedure WriteChar(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Char); virtual; abstract;
    procedure WriteString(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: string); virtual; abstract;
    procedure WriteInteger(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Integer); virtual; abstract;
    procedure WriteInt64(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Int64); virtual; abstract;
    procedure WriteFloat(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Double); virtual; abstract;
    procedure WriteCurrency(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Currency); virtual; abstract;
    procedure WriteDate(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TDate); virtual; abstract;
    procedure WriteTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TTime); virtual; abstract;
    procedure WriteDateTime(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TDateTime); virtual; abstract;
    procedure WriteBoolean(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Boolean); virtual; abstract;
    procedure WriteEnumeration(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: string); virtual; abstract;
    procedure WriteSet(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: string); virtual; abstract;
    procedure WriteObject(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: TObject); virtual; abstract;
    procedure WriteVariant(const AInstance: TObject; const AAttribute: TRLRTTIAttribute; const AValue: Variant); virtual; abstract;
    property Classes: TRLRTTIClasses read FClasses;
    property Formats: TFormatSettings read FFormats;
  end;

implementation

uses
  DateUtils,
  Variants,
  U_System;

{ TRLRTTIInspector }

constructor TRLRTTIInspector.Create;
begin
  inherited;
  FClasses := CreateClasses;
  FFormats.DecimalSeparator := '.';
  FFormats.ShortDateFormat := 'dd/mm/yyyy';
  FFormats.LongTimeFormat := 'hh:nn:ss.zzz';
  FFormats.DateSeparator := '/';
  FFormats.TimeSeparator := ':';
end;

destructor TRLRTTIInspector.Destroy;
begin
  FreeAndNil(FClasses);
  inherited;
end;

function TRLRTTIInspector.CreateClasses: TRLRTTIClasses;
begin
  Result := TRLRTTIClasses.Create;
end;

function TRLRTTIInspector.GetClass(const AClass: TClass): TRLRTTIClass;
begin
  if Assigned(AClass) then
  begin
    Result := FClasses.Find(AClass.ClassName);
    if not Assigned(Result) then
    begin
      Result := FClasses.Add;
      Result.Name := AClass.ClassName;
      Result.Reference := AClass;
      PopulateAttributes(Result);
    end;
  end
  else
    Result := nil;
end;

function TRLRTTIInspector.GetAttributes(
  const AClass: TClass): TRLRTTIAttributes;
begin
  if Assigned(AClass) then
    Result := GetClass(AClass).Attributes
  else
    Result := nil;
end;

function TRLRTTIInspector.GetObjectClass(
  const AInstance: TObject): TRLRTTIClass;
begin
  if Assigned(AInstance) then
    Result := GetClass(AInstance.ClassType)
  else
    Result := nil;
end;

function TRLRTTIInspector.GetObjectAttributes(
  const AInstance: TObject): TRLRTTIAttributes;
begin
  if Assigned(AInstance) then
    Result := GetAttributes(AInstance.ClassType)
  else
    Result := nil;
end;

function TRLRTTIInspector.ReadValue(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AFormat: Boolean): Variant;
var
  sChar: Char;
  dtDate: TDate;
  dtTime: TTime;
  dtDateTime: TDateTime;
begin
  Result := Null;
  if Assigned(AAttribute) and AAttribute.IsReadable then
  begin
    case AAttribute.ValueType of
      avtChar:
        begin
          sChar := ReadChar(AInstance, AAttribute);
          if sChar <> #0 then
            Result := sChar
          else
            Result := '';
        end;
      avtString: Result := ReadString(AInstance, AAttribute);
      avtInteger: Result := ReadInteger(AInstance, AAttribute);
      avtInt64: Result := ReadInt64(AInstance, AAttribute);
      avtFloat:
        if AFormat then
          Result := FloatToStr(ReadFloat(AInstance, AAttribute), FFormats)
        else
          Result := ReadFloat(AInstance, AAttribute);
      avtCurrency:
        if AFormat then
          Result := CurrToStr(ReadCurrency(AInstance, AAttribute), FFormats)
        else
          Result := ReadCurrency(AInstance, AAttribute);
      avtDate:
        begin
          dtDate := ReadDate(AInstance, AAttribute);
          if not SameDate(dtDate, 0) then
          begin
            if AFormat then
              Result := DateToStr(dtDate, FFormats)
            else
              Result := ReadDate(AInstance, AAttribute);
          end
          else
            Result := '';
        end;
      avtTime:
        begin
          dtTime := ReadTime(AInstance, AAttribute);
          if not SameTime(dtTime, 0) then
          begin
            if AFormat then
              Result := TimeToStr(dtTime, FFormats)
            else
              Result := ReadTime(AInstance, AAttribute);
          end
          else
            Result := '';
        end;
      avtDateTime:
        begin
          dtDateTime := ReadDateTime(AInstance, AAttribute);
          if not SameDateTime(dtDateTime, 0) then
          begin
            if AFormat then
              Result := DateTimeToStr(dtDateTime, FFormats)
            else
              Result := ReadDateTime(AInstance, AAttribute);
          end
          else
            Result := '';
        end;

      avtBoolean: Result := ReadBoolean(AInstance, AAttribute);
      avtEnumeration: Result := ReadEnumeration(AInstance, AAttribute);
      avtSet: Result := ReadSet(AInstance, AAttribute);
      avtObject: Result := Integer(ReadObject(AInstance, AAttribute));
      avtVariant: Result := ReadVariant(AInstance, AAttribute);
    end;
  end;
end;

procedure TRLRTTIInspector.WriteValue(const AInstance: TObject;
  const AAttribute: TRLRTTIAttribute; const AValue: Variant;
  const AFormatted: Boolean);
var
  sValue: string;  
begin
  if Assigned(AInstance) and Assigned(AAttribute) and AAttribute.IsWritable and
    not VarIsNull(AValue) then
  begin
    sValue := VarToStr(AValue);
    case AAttribute.ValueType of
      avtChar:
        begin
          if sValue = '' then
            sValue := #0;
          WriteChar(AInstance, AAttribute, sValue[LowCharIndex]);
        end;

      avtString: WriteString(AInstance, AAttribute, sValue);
      avtInteger: WriteInteger(AInstance, AAttribute, StrToIntDef(sValue, 0));
      avtInt64: WriteInt64(AInstance, AAttribute, StrToInt64Def(sValue, 0));
      avtFloat:
        if AFormatted then
          WriteFloat(AInstance, AAttribute,
            StrToFloatDef(sValue, 0, FFormats))
        else
          WriteFloat(AInstance, AAttribute, AValue);
      avtCurrency:
        if AFormatted then
          WriteCurrency(AInstance, AAttribute,
            StrToCurrDef(sValue, 0, FFormats))
        else
          WriteCurrency(AInstance, AAttribute, AValue);
      avtDate:
        begin
          if sValue <> '' then
          begin
            if AFormatted then
              WriteDate(AInstance, AAttribute,
                StrToDateDef(sValue, 0, FFormats))
            else
              WriteDate(AInstance, AAttribute, AValue);
          end
          else
            WriteDate(AInstance, AAttribute, 0);
        end;
      avtTime:
        begin
          if sValue <> '' then
          begin
            if AFormatted then
              WriteTime(AInstance, AAttribute,
                StrToTimeDef(sValue, 0, FFormats))
            else
              WriteTime(AInstance, AAttribute, AValue);
          end
          else
            WriteTime(AInstance, AAttribute, 0);
        end;
      avtDateTime:
        begin
          if sValue <> '' then
          begin
            if AFormatted then
              WriteDateTime(AInstance, AAttribute,
                StrToDateTimeDef(sValue, 0, FFormats))
            else
              WriteDateTime(AInstance, AAttribute, AValue);
          end
          else
            WriteDateTime(AInstance, AAttribute, 0);
        end;
      avtBoolean: WriteBoolean(AInstance, AAttribute,
        StrToBoolDef(sValue, False));
      avtEnumeration: WriteEnumeration(AInstance, AAttribute, sValue);
      avtSet: WriteSet(AInstance, AAttribute, sValue);
      avtObject: WriteObject(AInstance, AAttribute,
        TObject(StrToIntDef(sValue, 0)));
      avtVariant: WriteVariant(AInstance, AAttribute, AValue);
    end;
  end;
end;

end.
