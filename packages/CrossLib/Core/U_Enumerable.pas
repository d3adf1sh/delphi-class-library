//=============================================================================
// U_Enumerable
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Enumerable;

interface

uses
  U_Enumerator;

type
  // IRLEnumerable
  IRLEnumerable = interface
    ['{D2BEE45F-37ED-46A5-8059-E2F4E2BCCE07}']
    function GetEnumerator: IRLEnumerator;
  end;

implementation

end.
