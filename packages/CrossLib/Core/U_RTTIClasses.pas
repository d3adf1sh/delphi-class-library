//=============================================================================
// U_RTTIClasses
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_RTTIClasses;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_NamedCollection,
  U_RTTIAttributes;

type
  // Exceptions
  ERLRTTIClassNotFound = class(ERLException);

  // TRLRTTIClass
  TRLRTTIClass = class(TRLNamedCollectionItem)
  private
    FReference: TClass;
    FAttributes: TRLRTTIAttributes;
    function GetReference: TClass;
  protected
    procedure KeyChanged; override;
    function CreateAttributes: TRLRTTIAttributes; virtual;
  public
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    property Reference: TClass read GetReference write FReference;
    property Attributes: TRLRTTIAttributes read FAttributes;
  end;

  // TRLRTTIClasses
  TRLRTTIClasses = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLRTTIClass;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLRTTIClass;
    function Insert(const AIndex: Integer): TRLRTTIClass;
    function Find(const AName: string): TRLRTTIClass;
    function FindByName(const AName: string): TRLRTTIClass;
    property Items[const AIndex: Integer]: TRLRTTIClass read GetItem; default;
  end;

implementation

{ TRLRTTIClass }

constructor TRLRTTIClass.Create(ACollection: TCollection);
begin
  inherited;
  FAttributes := CreateAttributes;
end;

destructor TRLRTTIClass.Destroy;
begin
  FreeAndNil(FAttributes);
  inherited;
end;

function TRLRTTIClass.GetReference: TClass;
begin
  if not Assigned(FReference) then
    FReference := GetClass(Name);
  Result := FReference;
end;

procedure TRLRTTIClass.KeyChanged;
begin
  inherited;
  FReference := nil;
end;

function TRLRTTIClass.CreateAttributes: TRLRTTIAttributes;
begin
  Result := TRLRTTIAttributes.Create;
end;

{ TRLRTTIClasses }

constructor TRLRTTIClasses.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 4096;
end;

function TRLRTTIClasses.GetItem(const AIndex: Integer): TRLRTTIClass;
begin
  Result := TRLRTTIClass(inherited Items[AIndex]);
end;

function TRLRTTIClasses.GetItemClass: TCollectionItemClass;
begin
  Result := TRLRTTIClass;
end;

function TRLRTTIClasses.Add: TRLRTTIClass;
begin
  Result := TRLRTTIClass(inherited Add);
end;

function TRLRTTIClasses.Insert(const AIndex: Integer): TRLRTTIClass;
begin
  Result := TRLRTTIClass(inherited Insert(AIndex));
end;

function TRLRTTIClasses.Find(const AName: string): TRLRTTIClass;
begin
  Result := TRLRTTIClass(inherited Find(AName));
end;

function TRLRTTIClasses.FindByName(const AName: string): TRLRTTIClass;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLRTTIClassNotFound.CreateFmt('Class "%s" not found.', [AName]);
end;

end.
