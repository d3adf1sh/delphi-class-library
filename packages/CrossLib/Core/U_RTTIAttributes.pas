//=============================================================================
// U_RTTIAttributes
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_RTTIAttributes;

interface

uses
  Classes,
  U_Exception,
  U_NamedCollection;

type
  // Enumerations
  TRLRTTIAttributeValueType = (avtUnknow, avtChar, avtString, avtInteger,
    avtInt64, avtFloat, avtCurrency, avtDate, avtTime, avtDateTime, avtBoolean,
    avtEnumeration, avtSet, avtObject, avtVariant);

  // Exceptions
  ERLRTTIAttributeNotFound = class(ERLException);

  // TRLRTTIAttribute
  TRLRTTIAttribute = class(TRLNamedCollectionItem)
  private
    FValueType: TRLRTTIAttributeValueType;
    FReference: TClass;
    FIsReadable: Boolean;
    FIsWritable: Boolean;
  public
    property ValueType: TRLRTTIAttributeValueType read FValueType write FValueType;
    property Reference: TClass read FReference write FReference;
    property IsReadable: Boolean read FIsReadable write FIsReadable;
    property IsWritable: Boolean read FIsWritable write FIsWritable;
  end;

  // TRLRTTIAttributes
  TRLRTTIAttributes = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLRTTIAttribute;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TRLRTTIAttribute;
    function Insert(const AIndex: Integer): TRLRTTIAttribute;
    function Find(const AName: string): TRLRTTIAttribute;
    function FindByName(const AName: string): TRLRTTIAttribute;
    property Items[const AIndex: Integer]: TRLRTTIAttribute read GetItem; default;
  end;

implementation

{ TRLRTTIAttributes }

function TRLRTTIAttributes.GetItem(const AIndex: Integer): TRLRTTIAttribute;
begin
  Result := TRLRTTIAttribute(inherited Items[AIndex]);
end;

function TRLRTTIAttributes.GetItemClass: TCollectionItemClass;
begin
  Result := TRLRTTIAttribute;
end;

function TRLRTTIAttributes.Add: TRLRTTIAttribute;
begin
  Result := TRLRTTIAttribute(inherited Add);
end;

function TRLRTTIAttributes.Insert(const AIndex: Integer): TRLRTTIAttribute;
begin
  Result := TRLRTTIAttribute(inherited Insert(AIndex));
end;

function TRLRTTIAttributes.Find(const AName: string): TRLRTTIAttribute;
begin
  Result := TRLRTTIAttribute(inherited Find(AName));
end;

function TRLRTTIAttributes.FindByName(const AName: string): TRLRTTIAttribute;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLRTTIAttributeNotFound.CreateFmt('Attribute "%s" not found.',
      [AName]);
end;

end.
