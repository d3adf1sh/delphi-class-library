//=============================================================================
// U_Exception
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Exception;

interface

uses
  SysUtils;

type
  // References
  ERLExceptionClass = class of ERLException;

  // Events
  TRLExceptionEvent = procedure(Sender: TObject; Error: Exception) of object;

  // ERLException
  ERLException = class(Exception)
  public
    constructor Create(const AMessage: string); virtual;
  end;

implementation

{ ERLException }

constructor ERLException.Create(const AMessage: string);
begin
  inherited Create(AMessage);
end;

end.
