//=============================================================================
// U_AttributeSerializer
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_AttributeSerializer;

interface

type
  // IRLAttributeSerializer
  IRLAttributeSerializer = interface
    ['{A1537279-6E34-4138-A66C-C0BB3A0655EF}']
    function IsSerializableAttribute(const AName: string): Boolean;
    function ReadAttributeValue(const AName: string; out AValue: string): Boolean;
    function WriteAttributeValue(const AName, AValue: string): Boolean;
  end;

implementation

end.
