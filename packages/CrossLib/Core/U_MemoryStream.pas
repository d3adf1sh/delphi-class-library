//=============================================================================
// U_MemoryStream
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MemoryStream;

interface

uses
  Classes,
  XMLIntf,
  U_Serializer;

type
  // References
  TRLMemoryStreamClass = class of TRLMemoryStream;

{$M+}
  // TRLMemoryStream
  TRLMemoryStream = class(TMemoryStream, IRLSerializer)
  private
    FDestroyed: Boolean;
  protected
    property Destroyed: Boolean read FDestroyed;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
  end;
{$M-}

implementation

uses
  U_System;

{ TRLMemoryStream }

constructor TRLMemoryStream.Create;
begin
  inherited Create;
end;

destructor TRLMemoryStream.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

function TRLMemoryStream.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TRLMemoryStream._AddRef: Integer;
begin
  Result := -1;
end;

function TRLMemoryStream._Release: Integer;
begin
  Result := -1;
end;

procedure TRLMemoryStream.ToXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
    ANode.Text := U_System.GetBinaryStreamData(TStream(Self));
end;

procedure TRLMemoryStream.FromXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
    U_System.SetBinaryStreamData(TStream(Self), ANode.Text);
end;

end.

