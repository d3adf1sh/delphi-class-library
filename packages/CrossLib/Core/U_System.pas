//=============================================================================
// U_System
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_System;

{$I RL.INC}

interface

uses
  Types,
  SysUtils,
  Classes;

// Utils
{$IFDEF D15UP}
function CharEncoding: TEncoding;
{$ENDIF}
{$IFDEF NEXTGEN}
function CharInSet(const AChar: Char; const ACharSet: array of Char): Boolean;
{$ELSE}
function CharInSet(const AChar: Char; const ACharSet: TSysCharSet): Boolean;
{$ENDIF}
function UpChar(const AValue: Char): Char;
function LowChar(const AValue: Char): Char;
function LowCharIndex: Integer;
function HighCharIndex(const AValue: string): Integer;
function GetHashCode(const AValue: string): Cardinal;
function EncodeString(const AValue: string): string;
function DecodeString(const AValue: string): string;
function EncryptString(const AValue: string): string;
function DecryptString(const AValue: string): string;
function CompareString(const ALeft, ARight: string): TValueRelationship;
function ReadStream(const AStream: TStream; const ACount: Integer): string;
procedure WriteStream(const AStream: TStream; const AData: string);
function GetStreamData(const AStream: TStream): string;
procedure SetStreamData(const AStream: TStream; const AData: string);
function GetBinaryStreamData(const AStream: TStream): string;
procedure SetBinaryStreamData(const AStream: TStream; const AData: string);

implementation

{$IFDEF NEXTGEN}
uses
  Character;
{$ENDIF}

// Utils
{$IFDEF D15UP}
function CharEncoding: TEncoding;
begin
  {$IFDEF D20UP}
  Result := TEncoding.ANSI;
  {$ELSE}
  Result := TEncoding.Default;
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF NEXTGEN}
function CharInSet(const AChar: Char; const ACharSet: array of Char): Boolean;
{$ELSE}
function CharInSet(const AChar: Char; const ACharSet: TSysCharSet): Boolean;
{$ENDIF}
begin
{$IFDEF NEXTGEN}
  Result := AChar.IsInArray(ACharSet);
{$ELSE}
  {$IFDEF D15UP}
  Result := SysUtils.CharInSet(AChar, ACharSet);
  {$ELSE}
  Result := AChar in ACharSet;
  {$ENDIF}
{$ENDIF}
end;

function UpChar(const AValue: Char): Char;
begin
{$IFDEF NEXTGEN}
  Result := AValue.ToUpper;
{$ELSE}
  Result := AValue;
  if CharInSet(Result, ['a'..'z']) then
    Dec(Result, Ord('a') - Ord('A'));
{$ENDIF}
end;

function LowChar(const AValue: Char): Char;
begin
{$IFDEF NEXTGEN}
  Result := AValue.ToLower;
{$ELSE}
  Result := AValue;
  if CharInSet(Result, ['A'..'Z']) then
    Inc(Result, Ord('a') - Ord('A'));
{$ENDIF}
end;

function LowCharIndex: Integer;
begin
{$IFDEF NEXTGEN}
  Result := 0;
{$ELSE}
  Result := 1;
{$ENDIF}
end;

function HighCharIndex(const AValue: string): Integer;
begin
{$IFDEF NEXTGEN}
  Result := Pred(Length(AValue));
{$ELSE}
  Result := Length(AValue);
{$ENDIF}
end;

function GetHashCode(const AValue: string): Cardinal;
{$IFDEF D15UP}
var
  lBuffer: TBytes;
  I: Integer;
begin
  Result := 0;
  lBuffer := CharEncoding.GetBytes(AValue);
  for I := 0 to High(lBuffer) do
    Result := ((Result shl 2) or
      (Result shr (SizeOf(Result) * 8 - 2))) xor lBuffer[I];
end;
{$ELSE}
var
  I: Integer;
begin
  Result := 0;
  for I := LowCharIndex to HighCharIndex(AValue) do
    Result := ((Result shl 2) or
      (Result shr (SizeOf(Result) * 8 - 2))) xor Ord(AValue[I]);
end;
{$ENDIF}

function EncodeString(const AValue: string): string;
{$IFDEF D15UP}
var
  lBuffer: TBytes;
  I: Integer;
begin
  Result := '';
  lBuffer := CharEncoding.GetBytes(AValue);
  for I := 0 to High(lBuffer) do
    Result := Result + IntToHex(lBuffer[I], 2);
end;
{$ELSE}
var
  I: Integer;
begin
  Result := '';
  for I := LowCharIndex to HighCharIndex(AValue) do
    Result := Result + IntToHex(Ord(AValue[I]), 2);
end;
{$ENDIF}

function DecodeString(const AValue: string): string;
{$IFDEF D15UP}
var
  lBuffer: TBytes;
  I: Integer;
  J: Integer;
begin
  SetLength(lBuffer, Length(AValue) div 2);
  I := LowCharIndex;
  J := 0;
  while I <= HighCharIndex(AValue) do
  begin
    lBuffer[J] := StrToIntDef('$' + AValue[I] + AValue[Succ(I)], 0);
    Inc(J);
    Inc(I, 2);
  end;

  Result := CharEncoding.GetString(lBuffer);
end;
{$ELSE}
var
  I: Integer;
  J: Integer;
begin
  SetLength(Result, Length(AValue) div 2);
  I := LowCharIndex;
  J := LowCharIndex;
  while I <= HighCharIndex(AValue) do
  begin
    Result[J] := Char(StrToIntDef('$' + AValue[I] + AValue[Succ(I)], 0));
    Inc(I, 2);
    Inc(J);
  end;
end;
{$ENDIF}

function EncryptString(const AValue: string): string;
{$IFDEF D15UP}
var
  lBuffer: TBytes;
  I: Integer;
begin
  lBuffer := CharEncoding.GetBytes(AValue);
  for I := 0 to High(lBuffer) do
    lBuffer[I] := lBuffer[I] + 1;
  Result := EncodeString(CharEncoding.GetString(lBuffer));
end;
{$ELSE}
var
  sBuffer: string;
  I: Integer;
begin
  SetLength(sBuffer, Length(AValue));
  for I := LowCharIndex to HighCharIndex(AValue) do
    sBuffer[I] := Char(Ord(AValue[I]) + 1);
  Result := EncodeString(sBuffer);
end;
{$ENDIF}

function DecryptString(const AValue: string): string;
{$IFDEF D15UP}
var
  lBuffer: TBytes;
  I: Integer;
begin
  lBuffer := CharEncoding.GetBytes(DecodeString(AValue));
  for I := 0 to High(lBuffer) do
    lBuffer[I] := lBuffer[I] - 1;
  Result := CharEncoding.GetString(lBuffer);
end;
{$ELSE}
var
  sBuffer: string;
  I: Integer;
begin
  sBuffer := DecodeString(AValue);
  SetLength(Result, Length(sBuffer));
  for I := LowCharIndex to HighCharIndex(sBuffer) do
    Result[I] := Char(Ord(sBuffer[I]) - 1);
end;
{$ENDIF}

function CompareString(const ALeft, ARight: string): TValueRelationship;
var
  nComparison: Integer;
begin
  nComparison := CompareStr(ALeft, ARight);
  if nComparison > 0 then
    Result := GreaterThanValue
  else
    if nComparison < 0 then
      Result := LessThanValue
    else
      Result := EqualsValue;
end;

function ReadStream(const AStream: TStream; const ACount: Integer): string;
{$IFDEF D15UP}
var
  lBuffer: TBytes;
{$ENDIF}
begin
  if Assigned(AStream) and (ACount > 0) then
  begin
{$IFDEF D15UP}
    SetLength(lBuffer, ACount);
    AStream.ReadBuffer(lBuffer[0], ACount);
    Result := TEncoding.Default.GetString(lBuffer);
{$ELSE}
    SetLength(Result, ACount);
    AStream.Read(PChar(Result)^, ACount);
{$ENDIF}
  end
  else
    Result := '';
end;

procedure WriteStream(const AStream: TStream; const AData: string);
{$IFDEF D15UP}
var
  lBuffer: TBytes;
{$ENDIF}
begin
  if Assigned(AStream) then
{$IFDEF D15UP}
  begin
    lBuffer := TEncoding.Default.GetBytes(AData);
    AStream.WriteBuffer(lBuffer[0], Length(lBuffer));
  end;
{$ELSE}
    AStream.WriteBuffer(PChar(AData)^, Length(AData));
{$ENDIF}
end;

function GetStreamData(const AStream: TStream): string;
var
  nPosition: Int64;
begin
  if Assigned(AStream) then
  begin
    nPosition := AStream.Position;
    try
      AStream.Position := 0;
      Result := ReadStream(AStream, AStream.Size);
    finally
      AStream.Position := nPosition;
    end;
  end
  else
    Result := '';
end;

procedure SetStreamData(const AStream: TStream; const AData: string);
begin
  if Assigned(AStream) then
  begin
    AStream.Size := 0;
    WriteStream(AStream, AData);
    AStream.Position := 0;
  end;
end;

function GetBinaryStreamData(const AStream: TStream): string;
var
  lStream: TMemoryStream;
{$IFDEF NEXTGEN}
  lBuffer: TBytes;
{$ENDIF}
  nPosition: Int64;
begin
  if Assigned(AStream) and (AStream.Size > 0) then
  begin
    if AStream is TMemoryStream then
      lStream := TMemoryStream(AStream)
    else
      lStream := TMemoryStream.Create;
    try
      nPosition := AStream.Position;
      try
        AStream.Position := 0;
        if lStream <> AStream then
        begin
          lStream.CopyFrom(AStream, AStream.Size);
          lStream.Position := 0;
        end;

{$IFDEF NEXTGEN}
        SetLength(lBuffer, AStream.Size * 2);
        BinToHex(lStream.Memory, 0, lBuffer, 0, lStream.Size);
        Result := StringOf(lBuffer);
{$ELSE}
        SetLength(Result, lStream.Size * 2);
        BinToHex(Pointer(Integer(lStream.Memory)), PChar(Result), lStream.Size);
{$ENDIF}
      finally
        AStream.Position := nPosition;
      end;
    finally
      if lStream <> AStream then
        FreeAndNil(lStream);
    end;
  end
  else
    Result := '';
end;

procedure SetBinaryStreamData(const AStream: TStream; const AData: string);
var
  lStream: TMemoryStream;
{$IFDEF NEXTGEN}
  lBuffer: TBytes;
{$ENDIF}
begin
  if Assigned(AStream) then
  begin
    if AStream is TMemoryStream then
      lStream := TMemoryStream(AStream)
    else
      lStream := TMemoryStream.Create;
    try
      lStream.Size := Length(AData) div 2;
{$IFDEF NEXTGEN}
      SetLength(lBuffer, lStream.Size);
      HexToBin(BytesOf(AData), 0, lBuffer, 0, lStream.Size);
      lStream.Write(lBuffer[0], lStream.Size);
{$ELSE}
      HexToBin(PChar(AData), Pointer(Integer(lStream.Memory)), lStream.Size);
{$ENDIF}
      if lStream <> AStream then
      begin
        AStream.Size := 0;
        AStream.CopyFrom(lStream, lStream.Size);
      end;
    finally
      if lStream <> AStream then
        FreeAndNil(lStream);
    end;

    AStream.Position := 0;
  end;
end;

end.
