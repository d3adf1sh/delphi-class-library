//=============================================================================
// U_Component
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Component;

interface

uses
  Classes;

type
  // References
  TRLComponentClass = class of TRLComponent;

  // TRLComponent
  TRLComponent = class(TComponent)
  private
    FDestroyed: Boolean;
  protected
    property Destroyed: Boolean read FDestroyed;
  public
    destructor Destroy; override;
  end;

implementation

{ TRLComponent }

destructor TRLComponent.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

end.
