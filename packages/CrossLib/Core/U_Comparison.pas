//=============================================================================
// U_Comparison
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Comparison;

interface

uses
  SysUtils,
  Classes,
  XMLIntf,
  U_BaseType,
  U_Exception,
  U_Value,
  U_Validator,
  U_Serializer;

type
  // Enumerations
  TRLComparisonKind = (ckIsNull, ckEqual, ckGreater, ckGreaterOrEqual, ckLess,
    ckLessOrEqual, ckContains, ckBetween, ckIn);

  // Exceptions
  ERLInvalidValueCount = class(ERLException);

  // TRLComparison
  TRLComparison = class(TRLBaseType, IRLValidator, IRLSerializer)
  private
    FKind: TRLComparisonKind;
    FValueType: TRLValueType;
    FValueArray: TRLValueArray;
    FCaseSensitive: Boolean;
    procedure SetValueType(const AValue: TRLValueType);
    function GetValue(const AIndex: Integer): TRLValue;
    function GetValueCount: Integer;
  protected
    function CreateValueArray: TRLValueArray; virtual;
    procedure ValueTypeChanged; virtual;
    procedure CheckValues; virtual;
    function GetValuesIdentifier: string; virtual;
    property ValueArray: TRLValueArray read FValueArray;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Validate; virtual;
    function AddValue: TRLValue; virtual;
    procedure DeleteValue(const AIndex: Integer); virtual;
    procedure ClearValues; virtual;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
    property Values[const AIndex: Integer]: TRLValue read GetValue;
    property ValueCount: Integer read GetValueCount;
  published
    property Kind: TRLComparisonKind read FKind write FKind;
    property ValueType: TRLValueType read FValueType write SetValueType;
    property CaseSensitive: Boolean read FCaseSensitive write FCaseSensitive;
  end;

// Utils
function ComparisonKindToString(const AValue: TRLComparisonKind): string;
function StringToComparisonKind(const AValue: string): TRLComparisonKind;

implementation

uses
  TypInfo,
  U_RTTIContext;

// Utils
function ComparisonKindToString(const AValue: TRLComparisonKind): string;
begin
  Result := GetEnumName(TypeInfo(TRLComparisonKind), Ord(AValue));
end;

function StringToComparisonKind(const AValue: string): TRLComparisonKind;
begin
  Result := TRLComparisonKind(GetEnumValue(TypeInfo(TRLComparisonKind),
    AValue));
end;

{ TRLComparison }

constructor TRLComparison.Create;
begin
  inherited;
  FValueArray := CreateValueArray;
  FCaseSensitive := True;
end;

destructor TRLComparison.Destroy;
begin
  FreeAndNil(FValueArray);
  inherited;
end;

procedure TRLComparison.SetValueType(const AValue: TRLValueType);
begin
  if AValue <> FValueType then
  begin
    FValueType := AValue;
    ClearValues;
    ValueTypeChanged;
  end;
end;

function TRLComparison.GetValue(const AIndex: Integer): TRLValue;
begin
  Result := FValueArray[AIndex];
end;

function TRLComparison.GetValueCount: Integer;
begin
  Result := FValueArray.Count;
end;

function TRLComparison.CreateValueArray: TRLValueArray;
begin
  Result := TRLValueArray.Create;
  Result.FreeValues := True;
end;

procedure TRLComparison.ValueTypeChanged;
begin
end;

procedure TRLComparison.CheckValues;
begin
  if ((FKind = ckIsNull) and (ValueCount <> 0)) or
    ((FKind in [ckEqual..ckContains]) and (ValueCount <> 1)) or
    ((FKind = ckBetween) and (ValueCount <> 2)) or
    ((FKind = ckIn) and (ValueCount = 0)) then
    raise ERLInvalidValueCount.Create('Invalid value count.');
end;

function TRLComparison.GetValuesIdentifier: string;
begin
  Result := 'Values';
end;

procedure TRLComparison.Assign(Source: TPersistent);
var
  lComparison: TRLComparison;
begin
  if Source is TRLComparison then
  begin
    lComparison := TRLComparison(Source);
    Kind := lComparison.Kind;
    ValueType := lComparison.ValueType;
    ValueArray.Assign(lComparison.ValueArray);
    CaseSensitive := lComparison.CaseSensitive;
  end;
end;

procedure TRLComparison.Validate;
begin
  inherited;
  CheckValues;
end;

function TRLComparison.AddValue: TRLValue;
begin
  Result := RLValueClass[FValueType].Create;
  FValueArray.Add(Result);
end;

procedure TRLComparison.DeleteValue(const AIndex: Integer);
begin
  FValueArray.Delete(AIndex);
end;

procedure TRLComparison.ClearValues;
begin
  FValueArray.Clear;
end;

procedure TRLComparison.ToXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesToXML(Self, ANode);
    TRLRTTIContext.Instance.ObjectToXML(FValueArray,
      ANode.AddChild(GetValuesIdentifier));
  end;
end;

procedure TRLComparison.FromXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesFromXML(Self, ANode);
    TRLRTTIContext.Instance.ObjectFromXML(FValueArray,
      ANode.ChildNodes.FindNode(GetValuesIdentifier));
  end;
end;

end.
