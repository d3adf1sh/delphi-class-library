//=============================================================================
// U_Value
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Value;

{$I RL.INC}

interface

{ TODO -oRafael : Implementar TRLBinary/AsBinary. }

uses
  SysUtils,
  Classes,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  XMLIntf,
  U_BaseType,
  U_ArrayType,
  U_Exception,
  U_Serializer;

type
  // Enumerations
  TRLValueType = (vtString, vtInteger, vtInt64, vtFloat, vtCurrency, vtDate,
    vtTime, vtDateTime, vtBoolean, vtBinary);
  TRLValueRelationship = (vrLessThan, vrEquals, vrGreaterThan);

  // References
  TRLValueClass = class of TRLValue;

  // Exceptions
  ERLCouldNotReadValueAs = class(ERLException);
  ERLCouldNotWriteValueAs = class(ERLException);

  // IRLValueType
  IRLValueType = interface
    ['{DD791961-4961-4524-81CC-28A1E2E045C4}']
    function GetValueType: TRLValueType;
  end;

  // TRLValue
  TRLValue = class(TRLBaseType, IRLValueType, IRLSerializer)
  private
    FIsNull: Boolean;
    FModified: Boolean;
    FOnChange: TNotifyEvent;
    FOnModify: TNotifyEvent;
    FInitializing: Boolean;
  protected
    function CreateCouldNotReadValueAs(const AType: string): ERLCouldNotReadValueAs; virtual;
    function CreateCouldNotWriteValueAs(const AType: string): ERLCouldNotWriteValueAs; virtual;
    function GetValueType: TRLValueType; virtual; abstract;
    function GetAsString: string; virtual;
    function GetAsInteger: Integer; virtual;
    function GetAsInt64: Int64; virtual;
    function GetAsFloat: Double; virtual;
    function GetAsCurrency: Currency; virtual;
    function GetAsDate: TDate; virtual;
    function GetAsTime: TTime; virtual;
    function GetAsDateTime: TDateTime; virtual;
    function GetAsBoolean: Boolean; virtual;
    procedure SetAsString(const AValue: string); virtual;
    procedure SetAsInteger(const AValue: Integer); virtual;
    procedure SetAsInt64(const AValue: Int64); virtual;
    procedure SetAsFloat(const AValue: Double); virtual;
    procedure SetAsCurrency(const AValue: Currency); virtual;
    procedure SetAsDate(const AValue: TDate); virtual;
    procedure SetAsTime(const AValue: TTime); virtual;
    procedure SetAsDateTime(const AValue: TDateTime); virtual;
    procedure SetAsBoolean(const AValue: Boolean); virtual;
    function GetModifiedIdentifier: string; virtual;
    function GetIsNullIdentifier: string; virtual;
    procedure DoChange; virtual;
    procedure DoModify; virtual;
    property Initializing: Boolean read FInitializing write FInitializing;
  public
    constructor Create; override;
    procedure Assign(Source: TPersistent); override;
    procedure InitializeString(const AData: string); virtual;
    procedure InitializeInteger(const AData: Integer); virtual;
    procedure InitializeInt64(const AData: Int64); virtual;
    procedure InitializeFloat(const AData: Double); virtual;
    procedure InitializeCurrency(const AData: Currency); virtual;
    procedure InitializeDate(const AData: TDate); virtual;
    procedure InitializeTime(const AData: TTime); virtual;
    procedure InitializeDateTime(const AData: TDateTime); virtual;
    procedure InitializeBoolean(const AData: Boolean); virtual;
    procedure Clear; virtual;
    function Compare(const AValue: TRLValue): TRLValueRelationship; virtual; abstract;
{$IFDEF D15UP}
    function Equals(const AValue: TRLValue): Boolean; reintroduce; virtual;
{$ELSE}
    function Equals(const AValue: TRLValue): Boolean; virtual;
{$ENDIF}
    function GreaterThan(const AValue: TRLValue): Boolean; virtual;
    function GreaterOrEqualThan(const AValue: TRLValue): Boolean; virtual;
    function LessThan(const AValue: TRLValue): Boolean; virtual;
    function LessOrEqualThan(const AValue: TRLValue): Boolean; virtual;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
    property ValueType: TRLValueType read GetValueType;
    property AsString: string read GetAsString write SetAsString;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    property AsInt64: Int64 read GetAsInt64 write SetAsInt64;
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property AsCurrency: Currency read GetAsCurrency write SetAsCurrency;
    property AsDate: TDate read GetAsDate write SetAsDate;
    property AsTime: TTime read GetAsTime write SetAsTime;
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    property IsNull: Boolean read FIsNull;
    property Modified: Boolean read FModified;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnModify: TNotifyEvent read FOnModify write FOnModify;
  end;

  // TRLString
  TRLString = class(TRLValue)
  private
    FData: string;
    procedure SetData(const AData: string);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    function GetAsDate: TDate; override;
    function GetAsTime: TTime; override;
    function GetAsDateTime: TDateTime; override;
    function GetAsBoolean: Boolean; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
    procedure SetAsDate(const AValue: TDate); override;
    procedure SetAsTime(const AValue: TTime); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
    procedure SetAsBoolean(const AValue: Boolean); override;
  public
    constructor CreateString(const AData: string); overload; virtual;
    constructor CreateString(const AData: string; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
    function Contains(const AValue: TRLValue): Boolean; virtual;
    function Format(const AFormat: string): string; virtual;
  published
    property Data: string read FData write SetData;
  end;

  // TRLNumber
  TRLNumber = class(TRLValue)
  public
    function Format(const AFormat: string): string; virtual;
  end;

  // TRLInteger
  TRLInteger = class(TRLNumber)
  private
    FData: Integer;
    procedure SetData(const AData: Integer);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
  public
    constructor CreateInteger(const AData: Integer); overload; virtual;
    constructor CreateInteger(const AData: Integer; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: Integer read FData write SetData;
  end;

  // TRLInt64
  TRLInt64 = class(TRLNumber)
  private
    FData: Int64;
    procedure SetData(const AData: Int64);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
  public
    constructor CreateInt64(const AData: Int64); overload; virtual;
    constructor CreateInt64(const AData: Int64; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: Int64 read FData write SetData;
  end;

  // TRLFloat
  TRLFloat = class(TRLNumber)
  private
    FData: Double;
    procedure SetData(const AData: Double);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
  public
    constructor CreateFloat(const AData: Double); overload; virtual;
    constructor CreateFloat(const AData: Double; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: Double read FData write SetData;
  end;

  // TRLCurrency
  TRLCurrency = class(TRLNumber)
  private
    FData: Currency;
    procedure SetData(const AData: Currency);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
  public
    constructor CreateCurrency(const AData: Currency); overload; virtual;
    constructor CreateCurrency(const AData: Currency; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: Currency read FData write SetData;
  end;

  // TRLCalendar
  TRLCalendar = class(TRLValue)
  public
    function Format(const AFormat: string): string; virtual;
  end;

  // TRLDate
  TRLDate = class(TRLCalendar)
  private
    FData: TDate;
    procedure SetData(const AData: TDate);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsDate: TDate; override;
    function GetAsDateTime: TDateTime; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsDate(const AValue: TDate); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
  public
    constructor CreateDate(const AData: TDate); overload; virtual;
    constructor CreateDate(const AData: TDate; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: TDate read FData write SetData;
  end;

  // TRLTime
  TRLTime = class(TRLCalendar)
  private
    FData: TTime;
    procedure SetData(const AData: TTime);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsTime: TTime; override;
    function GetAsDateTime: TDateTime; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsTime(const AValue: TTime); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
  public
    constructor CreateTime(const AData: TTime); overload; virtual;
    constructor CreateTime(const AData: TTime; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: TTime read FData write SetData;
  end;

  // TRLDateTime
  TRLDateTime = class(TRLCalendar)
  private
    FData: TDateTime;
    procedure SetData(const AData: TDateTime);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsDate: TDate; override;
    function GetAsTime: TTime; override;
    function GetAsDateTime: TDateTime; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsDate(const AValue: TDate); override;
    procedure SetAsTime(const AValue: TTime); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
  public
    constructor CreateDateTime(const AData: TDateTime); overload; virtual;
    constructor CreateDateTime(const AData: TDateTime; const AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: TDateTime read FData write SetData;
  end;

  // TRLBoolean
  TRLBoolean = class(TRLValue)
  private
    FData: Boolean;
    procedure SetData(const AData: Boolean);
  protected
    function GetValueType: TRLValueType; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsBoolean: Boolean; override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsBoolean(const AValue: Boolean); override;
  public
    constructor CreateBoolean(const AData: Boolean); overload; virtual;
    constructor CreateBoolean(const AData, AInitialize: Boolean); overload; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear; override;
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  published
    property Data: Boolean read FData write SetData;
  end;

  // TRLBinary
  TRLBinary = class(TRLValue)
  protected
    function GetValueType: TRLValueType; override;
  public
    function Compare(const AValue: TRLValue): TRLValueRelationship; override;
  end;

  // TRLValueArray
  TRLValueArray = class(TRLArrayType)
  private
    function GetItem(const AIndex: Integer): TRLValue;
    procedure SetItem(const AIndex: Integer; const AValue: TRLValue);
  public
    procedure Add(const AValue: TRLValue);
    procedure Insert(const AIndex: Integer; const AValue: TRLValue);
    property Items[const AIndex: Integer]: TRLValue read GetItem write SetItem; default;
  end;

var
  RLValueClass: array[TRLValueType] of TRLValueClass = (TRLString, TRLInteger,
    TRLInt64, TRLFloat, TRLCurrency, TRLDate, TRLTime, TRLDateTime, TRLBoolean,
    TRLBinary);

// Utils
function ValueTypeToString(const AValue: TRLValueType): string;
function StringToValueType(const AValue: string): TRLValueType;

implementation

uses
  MaskUtils,
  DateUtils,
  Math,
  Variants,
  TypInfo,
  U_System,
  U_RTTIContext;

// Utils
function ValueTypeToString(const AValue: TRLValueType): string;
begin
  Result := GetEnumName(TypeInfo(TRLValueType), Ord(AValue));
end;

function StringToValueType(const AValue: string): TRLValueType;
begin
  Result := TRLValueType(GetEnumValue(TypeInfo(TRLValueType), AValue));
end;

{ TRLValue }

constructor TRLValue.Create;
begin
  inherited;
  FIsNull := True;
end;

function TRLValue.CreateCouldNotReadValueAs(
  const AType: string): ERLCouldNotReadValueAs;
begin
  Result := ERLCouldNotReadValueAs.CreateFmt('Could not read value as %s.',
    [AType]);
end;

function TRLValue.CreateCouldNotWriteValueAs(
  const AType: string): ERLCouldNotWriteValueAs;
begin
  Result := ERLCouldNotWriteValueAs.CreateFmt('Could not write value as %s.',
    [AType]);
end;

function TRLValue.GetAsString: string;
begin
  raise CreateCouldNotReadValueAs('String');
end;

function TRLValue.GetAsInteger: Integer;
begin
  raise CreateCouldNotReadValueAs('Integer');
end;

function TRLValue.GetAsInt64: Int64;
begin
  raise CreateCouldNotReadValueAs('Int64');
end;

function TRLValue.GetAsFloat: Double;
begin
  raise CreateCouldNotReadValueAs('Float');
end;

function TRLValue.GetAsCurrency: Currency;
begin
  raise CreateCouldNotReadValueAs('Currency');
end;

function TRLValue.GetAsDate: TDate;
begin
  raise CreateCouldNotReadValueAs('Date');
end;

function TRLValue.GetAsTime: TTime;
begin
  raise CreateCouldNotReadValueAs('Time');
end;

function TRLValue.GetAsDateTime: TDateTime;
begin
  raise CreateCouldNotReadValueAs('DateTime');
end;

function TRLValue.GetAsBoolean: Boolean;
begin
  raise CreateCouldNotReadValueAs('Boolean');
end;

procedure TRLValue.SetAsString(const AValue: string);
begin
  raise CreateCouldNotWriteValueAs('String');
end;

procedure TRLValue.SetAsInteger(const AValue: Integer);
begin
  raise CreateCouldNotWriteValueAs('Integer');
end;

procedure TRLValue.SetAsInt64(const AValue: Int64);
begin
  raise CreateCouldNotWriteValueAs('Int64');
end;

procedure TRLValue.SetAsFloat(const AValue: Double);
begin
  raise CreateCouldNotWriteValueAs('Float');
end;

procedure TRLValue.SetAsCurrency(const AValue: Currency);
begin
  raise CreateCouldNotWriteValueAs('Currency');
end;

procedure TRLValue.SetAsDate(const AValue: TDate);
begin
  raise CreateCouldNotWriteValueAs('Date');
end;

procedure TRLValue.SetAsTime(const AValue: TTime);
begin
  raise CreateCouldNotWriteValueAs('Time');
end;

procedure TRLValue.SetAsDateTime(const AValue: TDateTime);
begin
  raise CreateCouldNotWriteValueAs('DateTime');
end;

procedure TRLValue.SetAsBoolean(const AValue: Boolean);
begin
  raise CreateCouldNotWriteValueAs('Boolean');
end;

function TRLValue.GetModifiedIdentifier: string;
begin
  Result := 'Modified';
end;

function TRLValue.GetIsNullIdentifier: string;
begin
  Result := 'IsNull';
end;

procedure TRLValue.DoChange;
begin
  FIsNull := False;
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TRLValue.DoModify;
begin
  FModified := not FInitializing;
  if FModified and Assigned(FOnModify) then
    FOnModify(Self);
end;

procedure TRLValue.Assign(Source: TPersistent);
var
  lValue: TRLValue;
begin
  if Source is TRLValue then
  begin
    lValue := TRLValue(Source);
    FIsNull := lValue.FIsNull;
    FModified := lValue.FModified;
  end;
end;

procedure TRLValue.InitializeString(const AData: string);
begin
  FInitializing := True;
  try
    AsString := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeInteger(const AData: Integer);
begin
  FInitializing := True;
  try
    AsInteger := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeInt64(const AData: Int64);
begin
  FInitializing := True;
  try
    AsInt64 := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeFloat(const AData: Double);
begin
  FInitializing := True;
  try
    AsFloat := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeCurrency(const AData: Currency);
begin
  FInitializing := True;
  try
    AsCurrency := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeDate(const AData: TDate);
begin
  FInitializing := True;
  try
    AsDate := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeTime(const AData: TTime);
begin
  FInitializing := True;
  try
    AsTime := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeDateTime(const AData: TDateTime);
begin
  FInitializing := True;
  try
    AsDateTime := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.InitializeBoolean(const AData: Boolean);
begin
  FInitializing := True;
  try
    AsBoolean := AData;
  finally
    FInitializing := False;
  end;
end;

procedure TRLValue.Clear;
begin
  FIsNull := True;
end;

function TRLValue.Equals(const AValue: TRLValue): Boolean;
begin
  Result := Assigned(AValue) and (Compare(AValue) = vrEquals);
end;

function TRLValue.GreaterThan(const AValue: TRLValue): Boolean;
begin
  Result := Assigned(AValue) and (Compare(AValue) = vrGreaterThan);
end;

function TRLValue.GreaterOrEqualThan(const AValue: TRLValue): Boolean;
begin
  Result := Assigned(AValue) and (Compare(AValue) in [vrGreaterThan, vrEquals]);
end;

function TRLValue.LessThan(const AValue: TRLValue): Boolean;
begin
  Result := Assigned(AValue) and (Compare(AValue) = vrLessThan);
end;

function TRLValue.LessOrEqualThan(const AValue: TRLValue): Boolean;
begin
  Result := Assigned(AValue) and (Compare(AValue) in [vrLessThan, vrEquals]);
end;

procedure TRLValue.ToXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
  begin
    if FIsNull then
      ANode.Attributes[GetIsNullIdentifier] := BoolToStr(FIsNull, True);
    if FModified then
      ANode.Attributes[GetModifiedIdentifier] := BoolToStr(FModified, True);
    TRLRTTIContext.Instance.AttributesToXML(Self, ANode);
  end;
end;

procedure TRLValue.FromXML(const ANode: IXMLNode);
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesFromXML(Self, ANode);
    FModified := StrToBoolDef(
      VarToStr(ANode.AttributeNodes[GetModifiedIdentifier]), False);
    FIsNull := StrToBoolDef(
      VarToStr(ANode.AttributeNodes[GetIsNullIdentifier]), False);
  end;
end;

{ TRLString }

constructor TRLString.CreateString(const AData: string);
begin
  CreateString(AData, False);
end;

constructor TRLString.CreateString(const AData: string;
  const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeString(AData)
  else
    AsString := AData;
end;

procedure TRLString.SetData(const AData: string);
begin
  if (AData <> FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLString.GetValueType: TRLValueType;
begin
  Result := vtString;
end;

function TRLString.GetAsString: string;
begin
  Result := Data;
end;

function TRLString.GetAsInteger: Integer;
begin
  Result := StrToIntDef(AsString, 0);
end;

function TRLString.GetAsInt64: Int64;
begin
  Result := StrToInt64Def(AsString, 0);
end;

function TRLString.GetAsFloat: Double;
begin
  Result := StrToFloatDef(AsString, 0);
end;

function TRLString.GetAsCurrency: Currency;
begin
  Result := StrToCurrDef(AsString, 0);
end;

function TRLString.GetAsDate: TDate;
begin
  Result := StrToDateDef(AsString, 0);
end;

function TRLString.GetAsTime: TTime;
begin
  Result := StrToTimeDef(AsString, 0);
end;

function TRLString.GetAsDateTime: TDateTime;
begin
  Result := StrToDateTimeDef(AsString, 0);
end;

function TRLString.GetAsBoolean: Boolean;
begin
  Result := StrToBoolDef(AsString, False);
end;

procedure TRLString.SetAsString(const AValue: string);
begin
  Data := AValue;
end;

procedure TRLString.SetAsInteger(const AValue: Integer);
begin
  AsString := IntToStr(AValue);
end;

procedure TRLString.SetAsInt64(const AValue: Int64);
begin
  AsString := IntToStr(AValue);
end;

procedure TRLString.SetAsFloat(const AValue: Double);
begin
  AsString := FloatToStr(AValue);
end;

procedure TRLString.SetAsCurrency(const AValue: Currency);
begin
  AsString := CurrToStr(AValue);
end;

procedure TRLString.SetAsDate(const AValue: TDate);
begin
  AsString := DateToStr(AValue);
end;

procedure TRLString.SetAsTime(const AValue: TTime);
begin
  AsString := TimeToStr(AValue);
end;

procedure TRLString.SetAsDateTime(const AValue: TDateTime);
begin
  AsString := DateTimeToStr(AValue);
end;

procedure TRLString.SetAsBoolean(const AValue: Boolean);
begin
  AsString := BoolToStr(AValue);
end;

procedure TRLString.Assign(Source: TPersistent);
begin
  if Source is TRLString then
    AsString := TRLString(Source).AsString;
  inherited;
end;

procedure TRLString.Clear;
begin
  if not IsNull then
    AsString := '';
  inherited;
end;

function TRLString.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(U_System.CompareString(AsString, AValue.AsString))))
  else
    Result := vrLessThan;
end;

function TRLString.Contains(const AValue: TRLValue): Boolean;
begin
  Result := Assigned(AValue) and (Pos(AValue.AsString, AsString) > 0);
end;

function TRLString.Format(const AFormat: string): string;
begin
  Result := FormatMaskText(AFormat + ';0', AsString);
end;

{ TRLNumber }

function TRLNumber.Format(const AFormat: string): string;
begin
  Result := FormatFloat(AFormat, AsFloat);
end;

{ TRLInteger }

constructor TRLInteger.CreateInteger(const AData: Integer);
begin
  CreateInteger(AData, False);
end;

constructor TRLInteger.CreateInteger(const AData: Integer;
  const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeInteger(AData)
  else
    AsInteger := AData;
end;

procedure TRLInteger.SetData(const AData: Integer);
begin
  if (AData <> FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLInteger.GetValueType: TRLValueType;
begin
  Result := vtInteger;
end;

function TRLInteger.GetAsString: string;
begin
  Result := IntToStr(AsInteger);
end;

function TRLInteger.GetAsInteger: Integer;
begin
  Result := Data;
end;

function TRLInteger.GetAsInt64: Int64;
begin
  Result := AsInteger;
end;

function TRLInteger.GetAsFloat: Double;
begin
  Result := AsInteger;
end;

function TRLInteger.GetAsCurrency: Currency;
begin
  Result := AsInteger;
end;

procedure TRLInteger.SetAsString(const AValue: string);
begin
  AsInteger := StrToIntDef(AValue, 0);
end;

procedure TRLInteger.SetAsInteger(const AValue: Integer);
begin
  Data := AValue;
end;

procedure TRLInteger.SetAsInt64(const AValue: Int64);
begin
  AsInteger := AValue;
end;

procedure TRLInteger.SetAsFloat(const AValue: Double);
begin
  AsInteger := Trunc(AValue);
end;

procedure TRLInteger.SetAsCurrency(const AValue: Currency);
begin
  AsInteger := Trunc(AValue);
end;

procedure TRLInteger.Assign(Source: TPersistent);
begin
  if Source is TRLInteger then
    AsInteger := TRLInteger(Source).AsInteger;
  inherited;
end;

procedure TRLInteger.Clear;
begin
  if not IsNull then
    AsInteger := 0;
  inherited;
end;

function TRLInteger.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareValue(AsInteger, AValue.AsInteger))))
  else
    Result := vrLessThan;
end;

{ TRLInt64 }

constructor TRLInt64.CreateInt64(const AData: Int64);
begin
  CreateInt64(AData, False);
end;

constructor TRLInt64.CreateInt64(const AData: Int64;
  const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeInt64(AData)
  else
    AsInt64 := AData;
end;

procedure TRLInt64.SetData(const AData: Int64);
begin
  if (AData <> FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLInt64.GetValueType: TRLValueType;
begin
  Result := vtInt64;
end;

function TRLInt64.GetAsString: string;
begin
  Result := IntToStr(AsInt64);
end;

function TRLInt64.GetAsInteger: Integer;
begin
  Result := AsInt64;
end;

function TRLInt64.GetAsInt64: Int64;
begin
  Result := Data;
end;

function TRLInt64.GetAsFloat: Double;
begin
  Result := AsInt64;
end;

function TRLInt64.GetAsCurrency: Currency;
begin
  Result := AsInt64;
end;

procedure TRLInt64.SetAsString(const AValue: string);
begin
  AsInt64 := StrToInt64Def(AValue, 0);
end;

procedure TRLInt64.SetAsInteger(const AValue: Integer);
begin
  AsInt64 := AValue;
end;

procedure TRLInt64.SetAsInt64(const AValue: Int64);
begin
  Data := AValue;
end;

procedure TRLInt64.SetAsFloat(const AValue: Double);
begin
  AsInt64 := Trunc(AValue);
end;

procedure TRLInt64.SetAsCurrency(const AValue: Currency);
begin
  AsInt64 := Trunc(AValue);
end;

procedure TRLInt64.Assign(Source: TPersistent);
begin
  if Source is TRLInt64 then
    AsInt64 := TRLInt64(Source).AsInt64;
  inherited;
end;

procedure TRLInt64.Clear;
begin
  if not IsNull then
    AsInt64 := 0;
  inherited;
end;

function TRLInt64.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareValue(AsInt64, AValue.AsInt64))))
  else
    Result := vrLessThan;
end;

{ TRLFloat }

constructor TRLFloat.CreateFloat(const AData: Double);
begin
  CreateFloat(AData, False);
end;

constructor TRLFloat.CreateFloat(const AData: Double;
  const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeFloat(AData)
  else
    AsFloat := AData;
end;

procedure TRLFloat.SetData(const AData: Double);
begin
  if not SameValue(AData, FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLFloat.GetValueType: TRLValueType;
begin
  Result := vtFloat;
end;

function TRLFloat.GetAsString: string;
begin
  Result := FloatToStr(AsFloat);
end;

function TRLFloat.GetAsInteger: Integer;
begin
  Result := Trunc(AsFloat);
end;

function TRLFloat.GetAsInt64: Int64;
begin
  Result := Trunc(AsFloat);
end;

function TRLFloat.GetAsFloat: Double;
begin
  Result := Data;
end;

function TRLFloat.GetAsCurrency: Currency;
begin
  Result := AsFloat;
end;

procedure TRLFloat.SetAsString(const AValue: string);
begin
  AsFloat := StrToFloatDef(AValue, 0);
end;

procedure TRLFloat.SetAsInteger(const AValue: Integer);
begin
  AsFloat := AValue;
end;

procedure TRLFloat.SetAsInt64(const AValue: Int64);
begin
  AsFloat := AValue;
end;

procedure TRLFloat.SetAsFloat(const AValue: Double);
begin
  Data := AValue;
end;

procedure TRLFloat.SetAsCurrency(const AValue: Currency);
begin
  AsFloat := AValue;
end;

procedure TRLFloat.Assign(Source: TPersistent);
begin
  if Source is TRLFloat then
    AsFloat := TRLFloat(Source).AsFloat;
  inherited;
end;

procedure TRLFloat.Clear;
begin
  if not IsNull then
    AsFloat := 0;
  inherited;
end;

function TRLFloat.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareValue(AsFloat, AValue.AsFloat))))
  else
    Result := vrLessThan;
end;

{ TRLCurrency }

constructor TRLCurrency.CreateCurrency(const AData: Currency);
begin
  CreateCurrency(AData, False);
end;

constructor TRLCurrency.CreateCurrency(const AData: Currency;
  const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeCurrency(AData)
  else
    AsCurrency := AData;
end;

procedure TRLCurrency.SetData(const AData: Currency);
begin
  if (AData <> FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLCurrency.GetValueType: TRLValueType;
begin
  Result := vtCurrency;
end;

function TRLCurrency.GetAsString: string;
begin
  Result := CurrToStr(AsCurrency);
end;

function TRLCurrency.GetAsInteger: Integer;
begin
  Result := Trunc(AsCurrency);
end;

function TRLCurrency.GetAsInt64: Int64;
begin
  Result := Trunc(AsCurrency);
end;

function TRLCurrency.GetAsFloat: Double;
begin
  Result := AsCurrency;
end;

function TRLCurrency.GetAsCurrency: Currency;
begin
  Result := Data;
end;

procedure TRLCurrency.SetAsString(const AValue: string);
begin
  AsCurrency := StrToCurrDef(AValue, 0);
end;

procedure TRLCurrency.SetAsInteger(const AValue: Integer);
begin
  AsCurrency := AValue;
end;

procedure TRLCurrency.SetAsInt64(const AValue: Int64);
begin
  AsCurrency := AValue;
end;

procedure TRLCurrency.SetAsFloat(const AValue: Double);
begin
  AsCurrency := AValue;
end;

procedure TRLCurrency.SetAsCurrency(const AValue: Currency);
begin
  Data := AValue;
end;

procedure TRLCurrency.Assign(Source: TPersistent);
begin
  if Source is TRLCurrency then
    AsCurrency := TRLCurrency(Source).AsCurrency;
  inherited;
end;

procedure TRLCurrency.Clear;
begin
  if not IsNull then
    AsCurrency := 0;
  inherited;
end;

function TRLCurrency.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
  begin
    if AsCurrency > AValue.AsCurrency then
      Result := vrGreaterThan
    else
      if AsCurrency < AValue.AsCurrency then
        Result := vrLessThan
      else
        Result := vrEquals;
  end
  else
    Result := vrLessThan;
end;

{ TRLCalendar }

function TRLCalendar.Format(const AFormat: string): string;
begin
  Result := FormatDateTime(AFormat, AsDateTime);
end;

{ TRLDateTime }

constructor TRLDateTime.CreateDateTime(const AData: TDateTime);
begin
  CreateDateTime(AData, False);
end;

constructor TRLDateTime.CreateDateTime(const AData: TDateTime;
  const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeDateTime(AData)
  else
    AsDateTime := AData;
end;

procedure TRLDateTime.SetData(const AData: TDateTime);
begin
  if not SameDateTime(AData, FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLDateTime.GetValueType: TRLValueType;
begin
  Result := vtDateTime;
end;

function TRLDateTime.GetAsString: string;
begin
  Result := DateTimeToStr(AsDateTime);
end;

function TRLDateTime.GetAsDate: TDate;
begin
  Result := Trunc(AsDateTime);
end;

function TRLDateTime.GetAsTime: TTime;
begin
  Result := Frac(AsDateTime);
end;

function TRLDateTime.GetAsDateTime: TDateTime;
begin
  Result := Data;
end;

procedure TRLDateTime.SetAsString(const AValue: string);
begin
  AsDateTime := StrToDateTimeDef(AValue, 0);
end;

procedure TRLDateTime.SetAsDate(const AValue: TDate);
begin
  AsDateTime := AValue;
end;

procedure TRLDateTime.SetAsTime(const AValue: TTime);
begin
  AsDateTime := AValue;
end;

procedure TRLDateTime.SetAsDateTime(const AValue: TDateTime);
begin
  Data := AValue;
end;

procedure TRLDateTime.Assign(Source: TPersistent);
begin
  if Source is TRLDateTime then
    AsDateTime := TRLDateTime(Source).AsDateTime;
  inherited;
end;

procedure TRLDateTime.Clear;
begin
  if not IsNull then
    AsDateTime := 0;
  inherited;
end;

function TRLDateTime.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareDateTime(AsDateTime, AValue.AsDateTime))))
  else
    Result := vrLessThan;
end;

{ TRLDate }

constructor TRLDate.CreateDate(const AData: TDate);
begin
  CreateDate(AData, False);
end;

constructor TRLDate.CreateDate(const AData: TDate; const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeDate(AData)
  else
    AsDate := AData;
end;

procedure TRLDate.SetData(const AData: TDate);
begin
  if not SameDate(AData, FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLDate.GetValueType: TRLValueType;
begin
  Result := vtDate;
end;

function TRLDate.GetAsString: string;
begin
  Result := DateToStr(AsDate);
end;

function TRLDate.GetAsDate: TDate;
begin
  Result := Data;
end;

function TRLDate.GetAsDateTime: TDateTime;
begin
  Result := AsDate;
end;

procedure TRLDate.SetAsString(const AValue: string);
begin
  AsDate := StrToDateDef(AValue, 0);
end;

procedure TRLDate.SetAsDate(const AValue: TDate);
begin
  Data := AValue;
end;

procedure TRLDate.SetAsDateTime(const AValue: TDateTime);
begin
  AsDate := Trunc(AValue);
end;

procedure TRLDate.Assign(Source: TPersistent);
begin
  if Source is TRLDate then
    AsDate := TRLDate(Source).AsDate;
  inherited;
end;

procedure TRLDate.Clear;
begin
  if not IsNull then
    AsDate := 0;
  inherited;
end;

function TRLDate.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareDate(AsDate, AValue.AsDate))))
  else
    Result := vrLessThan;
end;

{ TRLTime }

constructor TRLTime.CreateTime(const AData: TTime);
begin
  CreateTime(AData, False);
end;

constructor TRLTime.CreateTime(const AData: TTime; const AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeTime(AData)
  else
    AsTime := AData;
end;

procedure TRLTime.SetData(const AData: TTime);
begin
  if not SameTime(AData, FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLTime.GetValueType: TRLValueType;
begin
  Result := vtTime;
end;

function TRLTime.GetAsString: string;
begin
  Result := TimeToStr(AsTime);
end;

function TRLTime.GetAsTime: TTime;
begin
  Result := Data;
end;

function TRLTime.GetAsDateTime: TDateTime;
begin
  Result := AsTime;
end;

procedure TRLTime.SetAsString(const AValue: string);
begin
  AsTime := StrToTimeDef(AValue, 0);
end;

procedure TRLTime.SetAsTime(const AValue: TTime);
begin
  Data := AValue;
end;

procedure TRLTime.SetAsDateTime(const AValue: TDateTime);
begin
  AsTime := Frac(AValue);
end;

procedure TRLTime.Assign(Source: TPersistent);
begin
  if Source is TRLTime then
    AsTime := TRLTime(Source).AsTime;
  inherited;
end;

procedure TRLTime.Clear;
begin
  if not IsNull then
    AsTime := 0;
  inherited;
end;

function TRLTime.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareTime(AsTime, AValue.AsTime))))
  else
    Result := vrLessThan;
end;

{ TRLBoolean }

constructor TRLBoolean.CreateBoolean(const AData: Boolean);
begin
  CreateBoolean(AData, False);
end;

constructor TRLBoolean.CreateBoolean(const AData, AInitialize: Boolean);
begin
  inherited Create;
  if AInitialize then
    InitializeBoolean(AData)
  else
    AsBoolean := AData;
end;

procedure TRLBoolean.SetData(const AData: Boolean);
begin
  if (AData <> FData) or IsNull then
  begin
    FData := AData;
    DoChange;
    DoModify;
  end;
end;

function TRLBoolean.GetValueType: TRLValueType;
begin
  Result := vtBoolean;
end;

function TRLBoolean.GetAsString: string;
begin
  Result := BoolToStr(AsBoolean, True);
end;

function TRLBoolean.GetAsInteger: Integer;
begin
  Result := Integer(AsBoolean);
end;

function TRLBoolean.GetAsBoolean: Boolean;
begin
  Result := Data;
end;

procedure TRLBoolean.SetAsInteger(const AValue: Integer);
begin
  AsBoolean := Boolean(AValue);
end;

procedure TRLBoolean.SetAsString(const AValue: string);
begin
  AsBoolean := StrToBoolDef(AValue, False);
end;

procedure TRLBoolean.SetAsBoolean(const AValue: Boolean);
begin
  Data := AValue;
end;

procedure TRLBoolean.Assign(Source: TPersistent);
begin
  if Source is TRLBoolean then
    AsBoolean := TRLBoolean(Source).AsBoolean;
  inherited;
end;

procedure TRLBoolean.Clear;
begin
  if not IsNull then
    AsBoolean := False;
  inherited;
end;

function TRLBoolean.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  if Assigned(AValue) then
    Result := TRLValueRelationship(
      Succ(Ord(CompareDate(AsInteger, AValue.AsInteger))))
  else
    Result := vrLessThan;
end;

{ TRLBinary }

function TRLBinary.GetValueType: TRLValueType;
begin
  Result := vtBinary;
end;

function TRLBinary.Compare(const AValue: TRLValue): TRLValueRelationship;
begin
  Result := vrLessThan;
end;

{ TRLValueArray }

function TRLValueArray.GetItem(const AIndex: Integer): TRLValue;
begin
  Result := TRLValue(inherited Items[AIndex]);
end;

procedure TRLValueArray.SetItem(const AIndex: Integer; const AValue: TRLValue);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLValueArray.Add(const AValue: TRLValue);
begin
  inherited Add(AValue);
end;

procedure TRLValueArray.Insert(const AIndex: Integer; const AValue: TRLValue);
begin
  inherited Insert(AIndex, AValue);
end;

initialization
  RegisterClasses([TRLString, TRLInteger, TRLInt64, TRLFloat, TRLCurrency,
    TRLDate, TRLTime, TRLDateTime, TRLBoolean, TRLBinary]);

end.
