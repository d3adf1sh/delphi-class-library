//=============================================================================
// U_Enumerator
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Enumerator;

interface

type
  // IRLEnumerator
  IRLEnumerator = interface
    ['{E5F08B05-AF32-481D-9D50-C28CEA9CC536}']
    function GetCurrent: TObject;
    function Next: Boolean;
    procedure Reset;
  end;

implementation

end.
