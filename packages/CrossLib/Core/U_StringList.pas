//=============================================================================
// U_StringList
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_StringList;

interface

uses
  Classes,
  XMLIntf,
  U_Serializer;

type
  // References
  TRLStringListClass = class of TRLStringList;

  // TRLStringList
  TRLStringList = class(TStringList, IRLSerializer)
  private
    FDestroyed: Boolean; 
  protected
    function GetItemIdentifier: string; virtual;
    function GetItemsIdentifier: string; virtual;
    function GetNameIdentifier: string; virtual;
    function GetValueIdentifier: string; virtual;
    function GetStringIdentifier: string; virtual;
    function ValueToXML(const AName, AValue: string): string; virtual;
    function ValueFromXML(const AName, AValue: string): string; virtual;
    property Destroyed: Boolean read FDestroyed;
  public                        
    constructor Create; virtual;
    destructor Destroy; override;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    procedure ToXML(const ANode: IXMLNode); virtual;
    procedure FromXML(const ANode: IXMLNode); virtual;
  end;

implementation

uses
  Variants,
  U_RTTIContext;

{ TRLStringList }

constructor TRLStringList.Create;
begin
  inherited Create;
end;

destructor TRLStringList.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

function TRLStringList.GetItemIdentifier: string;
begin
  Result := 'Item';
end;

function TRLStringList.GetItemsIdentifier: string;
begin
  Result := 'Items';
end;

function TRLStringList.GetNameIdentifier: string;
begin
  Result := 'Name';
end;

function TRLStringList.GetValueIdentifier: string;
begin
  Result := 'Value';
end;

function TRLStringList.GetStringIdentifier: string;
begin
  Result := 'String';
end;

function TRLStringList.ValueToXML(const AName, AValue: string): string;
begin
  Result := AValue;
end;

function TRLStringList.ValueFromXML(const AName, AValue: string): string;
begin
  Result := AValue;
end;

function TRLStringList.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TRLStringList._AddRef: Integer;
begin
  Result := -1;
end;

function TRLStringList._Release: Integer;
begin
  Result := -1;
end;

procedure TRLStringList.ToXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  lNode: IXMLNode;
  sName: string;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesToXML(Self, ANode);
    lNodes := ANode.AddChild(GetItemsIdentifier);
    for I := 0 to Pred(Count) do
    begin
      lNode := lNodes.AddChild(GetItemIdentifier);
      sName := Names[I];
      if sName <> '' then
      begin
        lNode.Attributes[GetNameIdentifier] := sName;
        lNode.Attributes[GetValueIdentifier] := ValueToXML(sName,
          ValueFromIndex[I]);
      end
      else
        lNode.Attributes[GetStringIdentifier] := ValueToXML('', Strings[I]);
    end;
  end;
end;

procedure TRLStringList.FromXML(const ANode: IXMLNode);
var
  lNodes: IXMLNode;
  sName: string;
  I: Integer;
begin
  if Assigned(ANode) then
  begin
    TRLRTTIContext.Instance.AttributesFromXML(Self, ANode);
    lNodes := ANode.ChildNodes.FindNode(GetItemsIdentifier);
    if Assigned(lNodes) then
    begin
      BeginUpdate;
      try
        Clear;
        for I := 0 to Pred(lNodes.ChildNodes.Count) do
        begin
          sName := VarToStr(lNodes.ChildNodes[I].Attributes[GetNameIdentifier]);
          if sName <> '' then
            Values[sName] := ValueFromXML(sName,
              VarToStr(lNodes.ChildNodes[I].Attributes[GetValueIdentifier]))
          else
            Add(
              ValueFromXML('',
                VarToStr(
                  lNodes.ChildNodes[I].Attributes[GetStringIdentifier])));
        end;
      finally
        EndUpdate;
      end;
    end;
  end;
end;

end.

