//=============================================================================
// U_Settings
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Settings;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Dictionary,
  U_AttributeSerializer,
  U_Validator;

type
  // References
  TRLSettingsClass = class of TRLSettings;

  // Exceptions
  ERLSettingsNotFound = class(ERLException);

  // TRLSettings
  TRLSettings = class(TRLDictionary, IRLAttributeSerializer, IRLValidator)
  protected
    function GetItemsIdentifier: string; override;
    function GetItemIdentifier: string; override;
    function GetKeyIdentifier: string; override;
    function ValueToXML(const AKey: string; const AValue: string): string; override;
    function ValueFromXML(const AKey: string; const AValue: string): string; override;
    function IsEncrypted(const AKey: string): Boolean; virtual;
    function IsSerializableAttribute(const AName: string): Boolean; virtual;
    function ReadAttributeValue(const AName: string; out AValue: string): Boolean; virtual;
    function WriteAttributeValue(const AName: string; const AValue: string): Boolean; virtual;
  public
    constructor Create; override;
    procedure Validate; virtual;
    class function GetSettings(const AName: string): TRLSettings;
    class function FindSettings(const AName: string): TRLSettings;
  end;

implementation

uses
  U_System;

{ TRLSettings }

constructor TRLSettings.Create;
begin
  inherited;
  HashSize := 32;
end;

function TRLSettings.GetItemsIdentifier: string;
begin
  Result := 'Parameters';
end;

function TRLSettings.GetItemIdentifier: string;
begin
  Result := 'Parameter';
end;

function TRLSettings.GetKeyIdentifier: string;
begin
  Result := 'Name';
end;

function TRLSettings.ValueToXML(const AKey, AValue: string): string;
begin
  if IsEncrypted(AKey) then
    Result := U_System.EncryptString(AValue)
  else
    Result := AValue;
end;

function TRLSettings.ValueFromXML(const AKey, AValue: string): string;
begin
  if IsEncrypted(AKey) then
    Result := U_System.DecryptString(AValue)
  else
    Result := AValue;
end;

function TRLSettings.IsEncrypted(const AKey: string): Boolean;
begin
  Result := SameText(AKey, 'Password');
end;

function TRLSettings.IsSerializableAttribute(const AName: string): Boolean;
begin
  Result := True;
end;

function TRLSettings.ReadAttributeValue(const AName: string;
  out AValue: string): Boolean;
begin
  Result := False;
end;

function TRLSettings.WriteAttributeValue(const AName, AValue: string): Boolean;
begin
  Result := False;
end;

procedure TRLSettings.Validate;
begin
end;

class function TRLSettings.GetSettings(const AName: string): TRLSettings;
var
  lClass: TRLSettingsClass;
begin
  lClass := TRLSettingsClass(GetClass(Format('TRL%sSettings', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLSettings.FindSettings(const AName: string): TRLSettings;
begin
  Result := GetSettings(AName);
  if not Assigned(Result) then
    raise ERLSettingsNotFound.CreateFmt('Settings "%s" not found.', [AName]);
end;

end.
