//=============================================================================
// U_Serializer
// Cross-plataform class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Serializer;

interface

uses
  XMLIntf;

type
  // IRLSerializer
  IRLSerializer = interface
    ['{A56E1973-5BF9-484F-82DC-D657A5AF9D84}']
    procedure ToXML(const ANode: IXMLNode);
    procedure FromXML(const ANode: IXMLNode);
  end;

implementation

end.
