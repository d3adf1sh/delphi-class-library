//=============================================================================
// U_AL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_AL;

interface

uses
  U_CL,
  U_CoreAL,
  U_DataAL,
  U_FormsAL,
  U_VCLAL,
  U_DevExpressAL,
  U_ApplicationAL;

implementation

end.
