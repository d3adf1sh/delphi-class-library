//=============================================================================
// U_OPFAttributes
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFAttributes;

interface

uses
  SysUtils,
  Classes,
  DB,
  U_Exception,
  U_StringList,
  U_Collection,
  U_NamedCollection,
  U_Value;

type
  // Exceptions
  ERLOPFFieldNameIsEmpty = class(ERLException);
  ERLOPFFieldNameAlreadyExists = class(ERLException);
  ERLOPFFieldNameNotFound = class(ERLException);
  ERLOPFAttributeNotFound = class(ERLException);
  ERLOPFAttributeIsInvalid = class(ERLException);
  ERLOPFInvalidKeyAttributesCount = class(ERLException);
  ERLOPFInvalidNormalAttributesCount = class(ERLException);
  ERLOPFInvalidAutoIncrementAttribute = class(ERLException);

  // TRLOPFAttribute
  TRLOPFAttribute = class(TRLNamedCollectionItem)
  private
    FValueType: TRLValueType;
    FAutoIncrement: Boolean;
    FIsKey: Boolean;
  protected
    function GetAlternativeKey: string; override;
    procedure SetAlternativeKey(const AValue: string); override;
    procedure AlternativeKeyAlreadyExists(const AValue: string); override;
    procedure CheckFieldName; virtual;
  public
    procedure Validate; override;
    property FieldName: string read GetAlternativeKey write SetAlternativeKey;
    property ValueType: TRLValueType read FValueType write FValueType;
    property AutoIncrement: Boolean read FAutoIncrement write FAutoIncrement;
    property IsKey: Boolean read FIsKey write FIsKey;
  end;

  // TRLOPFAttributeArray
  TRLOPFAttributeArray = class(TRLNamedCollectionItemArray)
  private
    function GetItem(const AIndex: Integer): TRLOPFAttribute;
    procedure SetItem(const AIndex: Integer; const AValue: TRLOPFAttribute);
  public
    procedure Add(const AValue: TRLOPFAttribute);
    procedure Insert(const AIndex: Integer; const AValue: TRLOPFAttribute);
    property Items[const AIndex: Integer]: TRLOPFAttribute read GetItem write SetItem; default;
  end;

  // TRLOPFAttributes
  TRLOPFAttributes = class(TRLNamedCollection)
  private
    FAutoIncrementAttribute: TRLOPFAttribute;
    function GetItem(const AIndex: Integer): TRLOPFAttribute;
  protected
    procedure Update(Item: TCollectionItem); override;
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
    procedure CheckKeyAttributes; virtual;
    procedure CheckNormalAttributes; virtual;
    procedure CheckAutoIncrementAttribute; virtual;
  public
    constructor CreateParented(const AParent: TObject); override;
    procedure Validate; override;
    function Add: TRLOPFAttribute;
    function Insert(const AIndex: Integer): TRLOPFAttribute;
    function Find(const AName: string): TRLOPFAttribute;
    function FindByName(const AName: string): TRLOPFAttribute;
    function FindFieldName(const AFieldName: string): TRLOPFAttribute;
    function FindByFieldName(const AFieldName: string): TRLOPFAttribute;
    function GetKeyAttributes: TRLOPFAttributeArray;
    function GetNormalAttributes: TRLOPFAttributeArray;
    function GetAutoIncrementAttribute: TRLOPFAttribute;
    function ToArray: TRLOPFAttributeArray; overload;
    function ToArray(const ANames: TRLStringList): TRLOPFAttributeArray; overload;
    property Items[const AIndex: Integer]: TRLOPFAttribute read GetItem; default;
  end;

implementation

uses
  U_OPFClasses;

{ TRLOPFAttribute }

function TRLOPFAttribute.GetAlternativeKey: string;
begin
  Result := inherited GetAlternativeKey;
end;

procedure TRLOPFAttribute.SetAlternativeKey(const AValue: string);
begin
  inherited;
end;

procedure TRLOPFAttribute.AlternativeKeyAlreadyExists(const AValue: string);
begin
  raise ERLOPFFieldNameAlreadyExists.CreateFmt('FieldName "%s" already exists.',
    [AValue]);
end;

procedure TRLOPFAttribute.CheckFieldName;
begin
  if FieldName = '' then
    raise ERLOPFFieldNameIsEmpty.Create('FieldName is empty.');
end;

procedure TRLOPFAttribute.Validate;
begin
  inherited;
  CheckFieldName;
end;

{ TRLOPFAttributeArray }

function TRLOPFAttributeArray.GetItem(const AIndex: Integer): TRLOPFAttribute;
begin
  Result := TRLOPFAttribute(inherited Items[AIndex]);
end;

procedure TRLOPFAttributeArray.SetItem(const AIndex: Integer;
  const AValue: TRLOPFAttribute);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLOPFAttributeArray.Add(const AValue: TRLOPFAttribute);
begin
  inherited Add(AValue);
end;

procedure TRLOPFAttributeArray.Insert(const AIndex: Integer;
  const AValue: TRLOPFAttribute);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLOPFAttributes }

constructor TRLOPFAttributes.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 128;
  AlternativeHash.CaseSensitive := False;
end;

function TRLOPFAttributes.GetItem(const AIndex: Integer): TRLOPFAttribute;
begin
  Result := TRLOPFAttribute(inherited Items[AIndex]);
end;

procedure TRLOPFAttributes.Update(Item: TCollectionItem);
begin
  inherited;
  FAutoIncrementAttribute := nil;
end;

function TRLOPFAttributes.GetItemClass: TCollectionItemClass;
begin
  Result := TRLOPFAttribute;
end;

function TRLOPFAttributes.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLOPFAttributeArray;
end;

procedure TRLOPFAttributes.CheckKeyAttributes;
var
  lAttributes: TRLOPFAttributeArray;
begin
  lAttributes := GetKeyAttributes;
  try
    if lAttributes.Count = 0 then
      raise ERLOPFInvalidKeyAttributesCount.Create(
        'Invalid key attributes count.');
  finally
    FreeAndNil(lAttributes);
  end;
end;

procedure TRLOPFAttributes.CheckNormalAttributes;
var
  lAttributes: TRLOPFAttributeArray;
begin
  lAttributes := GetNormalAttributes;
  try
    if lAttributes.Count = 0 then
      raise ERLOPFInvalidNormalAttributesCount.Create(
        'Invalid normal attributes count.');
  finally
    FreeAndNil(lAttributes);
  end;
end;

procedure TRLOPFAttributes.CheckAutoIncrementAttribute;
var
  lAttribute: TRLOPFAttribute;
begin
  lAttribute := GetAutoIncrementAttribute;
  if Assigned(lAttribute) and (lAttribute.ValueType <> vtInteger) then
    raise ERLOPFInvalidAutoIncrementAttribute.Create(
      'Invalid auto increment attribute.');
end;

procedure TRLOPFAttributes.Validate;
begin
  inherited;
  CheckKeyAttributes;
  CheckNormalAttributes;
  CheckAutoIncrementAttribute;
end;

function TRLOPFAttributes.Add: TRLOPFAttribute;
begin
  Result := TRLOPFAttribute(inherited Add);
end;

function TRLOPFAttributes.Insert(const AIndex: Integer): TRLOPFAttribute;
begin
  Result := TRLOPFAttribute(inherited Insert(AIndex));
end;

function TRLOPFAttributes.Find(const AName: string): TRLOPFAttribute;
begin
  Result := TRLOPFAttribute(inherited Find(AName));
end;

function TRLOPFAttributes.FindByName(const AName: string): TRLOPFAttribute;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLOPFAttributeNotFound.CreateFmt('Attribute "%s" not found.',
      [AName]);
end;

function TRLOPFAttributes.FindFieldName(
  const AFieldName: string): TRLOPFAttribute;
begin
  Result := TRLOPFAttribute(FindAlternativeHash(AFieldName));
end;

function TRLOPFAttributes.FindByFieldName(
  const AFieldName: string): TRLOPFAttribute;
begin
  Result := FindFieldName(AFieldName);
  if not Assigned(Result) then
    raise ERLOPFFieldNameNotFound.CreateFmt('FieldName "%s" not found.',
      [AFieldName]);
end;

function TRLOPFAttributes.GetKeyAttributes: TRLOPFAttributeArray;
var
  I: Integer;
begin
  Result := TRLOPFAttributeArray.Create;
  for I := 0 to Pred(Count) do
    if Items[I].IsKey then
      Result.Add(Items[I]);
end;

function TRLOPFAttributes.GetNormalAttributes: TRLOPFAttributeArray;
var
  I: Integer;
begin
  Result := TRLOPFAttributeArray.Create;
  for I := 0 to Pred(Count) do
    if not Items[I].IsKey then
      Result.Add(Items[I]);
end;

function TRLOPFAttributes.GetAutoIncrementAttribute: TRLOPFAttribute;
var
  I: Integer;
begin
  if not Assigned(FAutoIncrementAttribute) then
  begin
    I := 0;
    while (I <= Pred(Count)) and not Assigned(FAutoIncrementAttribute) do
      if Items[I].AutoIncrement then
        FAutoIncrementAttribute := Items[I]
      else
        Inc(I);
  end;

  Result := FAutoIncrementAttribute;
end;

function TRLOPFAttributes.ToArray: TRLOPFAttributeArray;
begin
  Result := TRLOPFAttributeArray(inherited ToArray);
end;

function TRLOPFAttributes.ToArray(
  const ANames: TRLStringList): TRLOPFAttributeArray;
begin
  Result := TRLOPFAttributeArray(inherited ToArray(ANames));
end;

end.
