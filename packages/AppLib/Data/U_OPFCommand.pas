//=============================================================================
// U_OPFCommand
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFCommand;

interface

uses
  SysUtils,
  U_Command,
  U_Database,
  U_OPFClassMapping;

type
  // TRLOPFCommand
  TRLOPFCommand = class(TRLCommand)
  private
    FDatabase: TRLDatabase;
    FClassMapping: TRLOPFClassMapping;
    FFreeDatabase: Boolean;
    function GetDatabase: TRLDatabase;
    procedure SetDatabase(const AValue: TRLDatabase);
    procedure SetClassMapping(const AValue: TRLOPFClassMapping);
  protected
    procedure ProviderChanged; override;
    procedure DatabaseChanged; virtual;
    procedure ClassMappingChanged; virtual;
    procedure CheckClassMapping; virtual;
  public
    destructor Destroy; override;
    property Database: TRLDatabase read GetDatabase write SetDatabase;
    property ClassMapping: TRLOPFClassMapping read FClassMapping write SetClassMapping;
  end;

implementation

{ TRLOPFCommand }

destructor TRLOPFCommand.Destroy;
begin
  if Assigned(FDatabase) and FFreeDatabase then
    FreeAndNil(FDatabase);
  inherited;
end;

function TRLOPFCommand.GetDatabase: TRLDatabase;
begin
  if not Assigned(FDatabase) then
  begin
    CheckProvider;
    FDatabase := TRLDatabase.FindDatabase(Provider);
    FFreeDatabase := True;
  end;

  Result := FDatabase;
end;

procedure TRLOPFCommand.SetDatabase(const AValue: TRLDatabase);
begin
  if AValue <> FDatabase then
  begin
    if Assigned(FDatabase) and FFreeDatabase then
    begin
      FreeAndNil(FDatabase);
      FFreeDatabase := False;
    end;
    
    FDatabase := AValue;
    DatabaseChanged;
  end;
end;

procedure TRLOPFCommand.SetClassMapping(const AValue: TRLOPFClassMapping);
begin
  if AValue <> FClassMapping then
  begin
    FClassMapping := AValue;
    ClassMappingChanged;
  end;
end;

procedure TRLOPFCommand.ProviderChanged;
begin
  inherited;
  if Assigned(FDatabase) then
    FreeAndNil(FDatabase);
end;

procedure TRLOPFCommand.DatabaseChanged;
begin
end;

procedure TRLOPFCommand.ClassMappingChanged;
begin
end;

procedure TRLOPFCommand.CheckClassMapping;
begin
  if not Assigned(FClassMapping) then
    raise ERLOPFClassMappingIsNull.Create('ClassMapping is null.');
end;

end.
