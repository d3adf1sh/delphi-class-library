//=============================================================================
// U_DBXParameters
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXParameters;

interface

uses
  Classes,
  DB,
  SqlTimSt,
  U_VCLParameters;

type
  // TRLDBXParameter
  TRLDBXParameter = class(TRLVCLParameter)
  protected
    function GetDataType: TFieldType; override;
    function GetAsDateTime: TDateTime; override;
    procedure SetDataType(const AValue: TFieldType); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
  end;

  // TRLDBXParameters
  TRLDBXParameters = class(TRLVCLParameters)
  protected
    function GetItemClass: TCollectionItemClass; override;
  end;

implementation

{ TRLDBXParameter }

function TRLDBXParameter.GetDataType: TFieldType;
begin
  if Assigned(Reference) then
  begin
    if Reference.DataType <> ftTimeStamp then
      Result := Reference.DataType
    else
      Result := ftDateTime;  
  end
  else
    Result := ftUnknown;
end;

function TRLDBXParameter.GetAsDateTime: TDateTime;
begin
  if not IsNull then
    Result := SQLTimeStampToDateTime(Reference.AsSQLTimeStamp)
  else
    Result := 0;
end;

procedure TRLDBXParameter.SetDataType(const AValue: TFieldType);
begin
  if Assigned(Reference) then
  begin
    if AValue <> ftDateTime then
      Reference.DataType := AValue
    else
      Reference.DataType := ftTimeStamp;
  end;
end;

procedure TRLDBXParameter.SetAsDateTime(const AValue: TDateTime);
begin
  if Assigned(Reference) then
    Reference.AsSQLTimeStamp := DateTimeToSQLTimeStamp(AValue);
end;

{ TRLDBXParameters }

function TRLDBXParameters.GetItemClass: TCollectionItemClass;
begin
  Result := TRLDBXParameter;
end;

end.
