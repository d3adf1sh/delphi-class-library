//=============================================================================
// U_KeyFields
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_KeyFields;

interface

uses
  Classes,
  U_Exception,
  U_NamedCollection;

type
  // Exceptions
  ERLKeyFieldNotFound = class(ERLException);

  // TRLKeyField
  TRLKeyField = class(TRLNamedCollectionItem)
  private
    FPosition: Integer;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Position: Integer read FPosition write FPosition;
  end;

  // TRLKeyFields
  TRLKeyFields = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLKeyField;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLKeyField;
    function Insert(const AIndex: Integer): TRLKeyField;
    function Find(const AName: string): TRLKeyField;
    function FindByName(const AName: string): TRLKeyField;
    property Items[const AIndex: Integer]: TRLKeyField read GetItem; default;
  end;

implementation

{ TRLKeyField }

procedure TRLKeyField.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TRLKeyField then
    Position := TRLKeyField(Source).Position;
end;

{ TRLKeyFields }

constructor TRLKeyFields.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLKeyFields.GetItem(const AIndex: Integer): TRLKeyField;
begin
  Result := TRLKeyField(inherited Items[AIndex]);
end;

function TRLKeyFields.GetItemClass: TCollectionItemClass;
begin
  Result := TRLKeyField;
end;

function TRLKeyFields.Add: TRLKeyField;
begin
  Result := TRLKeyField(inherited Add);
end;

function TRLKeyFields.Insert(const AIndex: Integer): TRLKeyField;
begin
  Result := TRLKeyField(inherited Insert(AIndex));
end;

function TRLKeyFields.Find(const AName: string): TRLKeyField;
begin
  Result := TRLKeyField(inherited Find(AName));
end;

function TRLKeyFields.FindByName(const AName: string): TRLKeyField;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLKeyFieldNotFound.CreateFmt('KeyField "%s" not found.', [AName]);
end;

end.
