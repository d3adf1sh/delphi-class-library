//=============================================================================
// U_SDACQuery
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACQuery;

interface

uses
  SysUtils,
  Classes,
  DB,
  MSAccess,
  U_SQL,
  U_Parameters,
  U_Query,
  U_SDACProvider,
  U_SDACSQL,
  U_SDACParameters;

type
  // TRLSDACQuery
  TRLSDACQuery = class(TRLQuery)
  private
    FQuery: TMSQuery;
  protected
    procedure ProviderChanged; override;
    function CreateSQL: TRLSQL; override;
    function CreateParameters: TRLParameters; override;
    function GetDataSet: TDataSet; override;
    function GetRecordCount: Integer; override;
    property Query: TMSQuery read FQuery;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
  end;

implementation

{ TRLSDACQuery }

constructor TRLSDACQuery.Create;
begin
  inherited;
  FQuery := TMSQuery.Create(nil);
  SQL.Reference := FQuery.SQL;
  Parameters.Reference := FQuery.Params;
  ProviderChanged;
end;

destructor TRLSDACQuery.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FQuery);
end;

procedure TRLSDACQuery.ProviderChanged;
begin
  if Assigned(FQuery) then
  begin
    if Provider is TRLSDACProvider then
      FQuery.Connection := TRLSDACProvider(Provider).Connection
    else
      FQuery.Connection := nil;
  end;
end;

function TRLSDACQuery.CreateSQL: TRLSQL;
begin
  Result := TRLSDACSQL.Create;
end;

function TRLSDACQuery.CreateParameters: TRLParameters;
begin
  Result := TRLSDACParameters.CreateParented(Self);
end;

function TRLSDACQuery.GetDataSet: TDataSet;
begin
  Result := FQuery;
end;

function TRLSDACQuery.GetRecordCount: Integer;
begin
  if not FQuery.Fetched then
    FQuery.FetchAll;
  Result := inherited GetRecordCount;
end;

procedure TRLSDACQuery.Execute;
begin
  CheckProvider;
  FQuery.ExecSQL;
end;

initialization
  RegisterClass(TRLSDACQuery);

end.
