//=============================================================================
// U_DataAL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DataAL;

interface

uses
  Classes,
  U_ProviderDefs,
  U_ADOProvider,
  U_ADOQuery,
  U_ADOSettings,
  U_DBXProvider,
  U_DBXDriver,
  U_DBXQuery,
  U_DBXSettings,
  U_SDACProvider,
  U_SDACQuery,
  U_SDACSettings,
  U_MSSQLDatabase,
  U_OracleDatabase;

var
  lDriver: TRLDBXDriver;

implementation

initialization
//ADO
  TRLProviderManager.Instance.RegisterProvider(RLADO, 'ADO Express', TRLADOProvider);
  TRLProviderManager.Instance.RegisterDriver(RLADO, 'SQLOLEDB.1', RLMSSQL);
  TRLProviderManager.Instance.RegisterDriver(RLADO, 'MSDAORA.1', RLOracle);
//DBX
  TRLProviderManager.Instance.RegisterProvider(RLDBX, 'DB Express', TRLDBXProvider);
  lDriver := TRLDBXDriver(TRLProviderManager.Instance.RegisterDriver(TRLDBXDriver, RLDBX, 'MSSQL', RLMSSQL));
  lDriver.DriverFunction := 'getSQLDriverMSSQL';
  lDriver.LibraryName := 'dbxmss30.dll';
  lDriver.VendorLibrary := 'oledb';
  lDriver.Settings.Add('HostName', '');
  lDriver.Settings.Add('DataBase', '');
  lDriver.Settings.Add('User_Name', '');
  lDriver.Settings.Add('Password', '');
  lDriver.Settings.Add('BlobSize', '-1');
  lDriver.Settings.Add('ErrorResourceFile', '');
  lDriver.Settings.Add('LocaleCode', '0000');
  lDriver.Settings.Add('MSSQL TransIsolation', 'ReadCommited');
  lDriver.Settings.Add('OS Authentication', 'False');
  lDriver.Settings.Add('Prepare SQL', 'False');
//SDAC
  TRLProviderManager.Instance.RegisterProvider(RLSDAC, 'SDAC', TRLSDACProvider);
  TRLProviderManager.Instance.RegisterDriver(RLSDAC, RLMSSQL, RLMSSQL);

end.
