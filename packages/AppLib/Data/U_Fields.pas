//=============================================================================
// U_Fields
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Fields;

interface

uses
  Classes,
  U_Exception,
  U_StringList,
  U_Collection,
  U_Value,
  U_Query,
  U_DBObjects;

type
  // TRLField
  TRLField = class(TRLDBObject, IRLValueType)
  private
    FPosition: Integer;
    FDataType: string;
    FValueType: TRLValueType;
    FLength: Integer;
    FPrecision: Integer;
    FScale: Integer;
    FAutoIncrement: Boolean;
    FRequired: Boolean;
    function GetValueType: TRLValueType;
    procedure SetValueType(const AValue: TRLValueType);
  protected
    procedure KeyChanged; override;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Position: Integer read FPosition write FPosition;
    property DataType: string read FDataType write FDataType;
    property ValueType: TRLValueType read GetValueType write SetValueType;
    property Length: Integer read FLength write FLength;
    property Precision: Integer read FPrecision write FPrecision;
    property Scale: Integer read FScale write FScale;
    property AutoIncrement: Boolean read FAutoIncrement write FAutoIncrement;
    property Required: Boolean read FRequired write FRequired;
  end;

  // TRLFieldArray
  TRLFieldArray = class(TRLDBObjectArray)
  private
    function GetItem(const AIndex: Integer): TRLField;
    procedure SetItem(const AIndex: Integer; const AValue: TRLField);
  public
    procedure Add(const AValue: TRLField);
    procedure Insert(const AIndex: Integer; const AValue: TRLField);
    property Items[const AIndex: Integer]: TRLField read GetItem write SetItem; default;
  end;

  // TRLFields
  TRLFields = class(TRLDBObjects)
  private
    FIDField: TRLField;
    function GetItem(const AIndex: Integer): TRLField;
    function GetIDField: TRLField;
  protected
    procedure Update(Item: TCollectionItem); override;
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
  public
    constructor Create; override;
    function Add: TRLField;
    function Insert(const AIndex: Integer): TRLField;
    function Find(const AName: string): TRLField;
    function FindByName(const AName: string): TRLField;
    function GetAutoIncrementFields: TRLFieldArray;
    function GetRequiredFields: TRLFieldArray;
    function ToArray: TRLFieldArray; overload;
    function ToArray(const ANames: TRLStringList): TRLFieldArray; overload;
    property Items[const AIndex: Integer]: TRLField read GetItem; default;
    property IDField: TRLField read GetIDField;
  end;

implementation

{ TRLField }

function TRLField.GetValueType: TRLValueType;
begin
  Result := FValueType;
end;

procedure TRLField.SetValueType(const AValue: TRLValueType);
begin
  if AValue <> FValueType then
  begin
    FValueType := AValue;
    Changed(False);
  end;
end;

procedure TRLField.KeyChanged;
begin
  inherited;
  Changed(False);
end;

procedure TRLField.Assign(Source: TPersistent);
var
  lField: TRLField;
begin
  inherited;
  if Source is TRLField then
  begin
    lField := TRLField(Source);
    Position := lField.Position;
    DataType := lField.DataType;
    ValueType := lField.ValueType;
    Length := lField.Length;
    Precision := lField.Precision;
    Scale := lField.Scale;
    AutoIncrement := lField.AutoIncrement;
    Required := lField.Required;
  end;
end;

{ TRLFieldArray }

function TRLFieldArray.GetItem(const AIndex: Integer): TRLField;
begin
  Result := TRLField(inherited Items[AIndex]);
end;

procedure TRLFieldArray.SetItem(const AIndex: Integer; const AValue: TRLField);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLFieldArray.Add(const AValue: TRLField);
begin
  inherited Add(AValue);
end;

procedure TRLFieldArray.Insert(const AIndex: Integer; const AValue: TRLField);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLFields }

constructor TRLFields.Create;
begin
  inherited;
  HashSize := 256;
end;

function TRLFields.GetItem(const AIndex: Integer): TRLField;
begin
  Result := TRLField(inherited Items[AIndex]);
end;

function TRLFields.GetIDField: TRLField;
var
  lIDField: TRLField;
begin
  if not Assigned(FIDField) then
  begin
    lIDField := Find('ID');
    if Assigned(lIDField) and (lIDField.ValueType = vtInteger) then
      FIDField := lIDField;
  end;

  Result := FIDField;
end;

procedure TRLFields.Update(Item: TCollectionItem);
begin
  inherited;
  FIDField := nil;
end;

function TRLFields.GetItemClass: TCollectionItemClass;
begin
  Result := TRLField;
end;

function TRLFields.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLFieldArray;
end;

function TRLFields.Add: TRLField;
begin
  Result := TRLField(inherited Add);
end;

function TRLFields.Insert(const AIndex: Integer): TRLField;
begin
  Result := TRLField(inherited Insert(AIndex));
end;

function TRLFields.Find(const AName: string): TRLField;
begin
  Result := TRLField(inherited Find(AName));
end;

function TRLFields.FindByName(const AName: string): TRLField;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLFieldNotFound.CreateFmt('Field "%s" not found.', [AName]); 
end;

function TRLFields.GetAutoIncrementFields: TRLFieldArray;
var
  I: Integer;
begin
  Result := TRLFieldArray.Create;
  for I := 0 to Pred(Count) do
   if Items[I].AutoIncrement then
     Result.Add(Items[I]);
end;

function TRLFields.GetRequiredFields: TRLFieldArray;
var
  I: Integer;
begin
  Result := TRLFieldArray.Create;
  for I := 0 to Pred(Count) do
   if Items[I].Required then
     Result.Add(Items[I]);
end;

function TRLFields.ToArray: TRLFieldArray;
begin
  Result := TRLFieldArray(inherited ToArray);
end;

function TRLFields.ToArray(const ANames: TRLStringList): TRLFieldArray;
begin
  Result := TRLFieldArray(inherited ToArray(ANames));
end;

end.
