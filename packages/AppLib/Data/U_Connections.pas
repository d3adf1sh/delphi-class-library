//=============================================================================
// U_Connections
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Connections;

{ TODO -oRafael : Se��o cr�tica no begin/end..use. }

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_NamedCollection,
  U_Settings,
  U_Provider,
  U_Query,
  U_Database,
  U_Schemas,
  U_OPFClassMapping;                       

type
  // Exceptions
  ERLProviderNameIsEmpty = class(ERLException);
  ERLConnectionNotFound = class(ERLException);
  ERLConnectionIsInvalid = class(ERLException); //External
  ERLCouldNotOpenConnection = class(ERLException);
  ERLCouldNotCloseConnection = class(ERLException);

  // TRLConnection
  TRLConnection = class(TRLNamedCollectionItem)
  private
    FDescription: string;
    FProviderName: string;
    FSettings: TRLSettings;
    FProvider: TRLProvider;
    FDatabase: TRLDatabase;
    FSchemas: TRLSchemas;
    FClassMapping: TRLOPFClassMapping;
    FUseClassMappinRLopulator: Boolean;
    FUseCount: Integer;
    procedure SetProviderName(const AValue: string);
    function GetSettings: TRLSettings;
    function GetProvider: TRLProvider;
    function GetDatabase: TRLDatabase;
    function GetSchemas: TRLSchemas;
    function GetClassMapping: TRLOPFClassMapping;
  protected
    function CreateClassMapping: TRLOPFClassMapping; virtual;
    procedure PopulateClassMapping(const AClassMapping: TRLOPFClassMapping); virtual;
    procedure ProviderNameChanged; virtual;
    procedure CheckProviderName; virtual;
    procedure CheckProvider; virtual;
    property UseCount: Integer read FUseCount;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Validate; override;
    procedure Open; virtual;
    procedure Close; virtual;
    function IsOpen: Boolean; virtual;
    function Test(out AError: string): Boolean; virtual;
    procedure BeginUse; virtual;
    procedure EndUse; virtual;
    function InUse: Boolean; virtual;
    procedure StartTransaction; virtual;
    function InTransaction: Boolean; virtual;
    procedure CommitTransaction; virtual;
    procedure RollbackTransaction; virtual;
    function NewQuery: TRLQuery; virtual;
    function NewDatabase: TRLDatabase; virtual;
    property Provider: TRLProvider read GetProvider;
    property Database: TRLDatabase read GetDatabase;
    property Schemas: TRLSchemas read GetSchemas;
    property ClassMapping: TRLOPFClassMapping read GetClassMapping;
    property UseClassMappinRLopulator: Boolean read FUseClassMappinRLopulator write FUseClassMappinRLopulator;
  published
    property Description: string read FDescription write FDescription;
    property ProviderName: string read FProviderName write SetProviderName;
    property Settings: TRLSettings read GetSettings;
  end;

  // TRLConnections
  TRLConnections = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLConnection;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetProvider(const AName: string): TRLProvider; virtual;
    function GetDatabase(const AName: string): TRLDatabase; virtual;
    function GetSchemas(const AName: string): TRLSchemas; virtual;
    function GetClassMapping(const AName: string): TRLOPFClassMapping; virtual;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLConnection;
    function Insert(const AIndex: Integer): TRLConnection;
    function Find(const AName: string): TRLConnection;
    function FindByName(const AName: string): TRLConnection;
    procedure Open(const AName: string); virtual;
    procedure Close(const AName: string); virtual;
    function IsOpen(const AName: string): Boolean; virtual;
    function Test(const AName: string; out AError: string): Boolean; virtual;
    procedure BeginUse(const AName: string); virtual;
    procedure EndUse(const AName: string); virtual;
    function InUse(const AName: string): Boolean; virtual;
    procedure StartTransaction(const AName: string); virtual;
    function InTransaction(const AName: string): Boolean; virtual;
    procedure CommitTransaction(const AName: string); virtual;
    procedure RollbackTransaction(const AName: string); virtual;
    function NewQuery(const AName: string): TRLQuery; virtual;
    function NewDatabase(const AName: string): TRLDatabase; virtual;
    procedure OpenAll; virtual;
    procedure CloseAll; virtual;
    function AllIsOpen: Boolean; virtual;
    property Items[const AIndex: Integer]: TRLConnection read GetItem; default;
    property Provider[const AName: string]: TRLProvider read GetProvider;
    property Database[const AName: string]: TRLDatabase read GetDatabase;
    property Schemas[const AName: string]: TRLSchemas read GetSchemas;
    property ClassMapping[const AName: string]: TRLOPFClassMapping read GetClassMapping;
    class function Instance: TRLConnections;
    class procedure DeleteInstance;
  end;

implementation

var
  RLInstance: TRLConnections;

{ TRLConnection }

constructor TRLConnection.Create(Collection: TCollection);
begin
  inherited;
  FUseClassMappinRLopulator := True;
end;

destructor TRLConnection.Destroy;
begin
  if Assigned(FSettings) then
    FreeAndNil(FSettings);
  if Assigned(FClassMapping) then
    FreeAndNil(FClassMapping);
  if Assigned(FSchemas) then
    FreeAndNil(FSchemas);
  if Assigned(FDatabase) then
    FreeAndNil(FDatabase);
  if Assigned(FProvider) then
    FreeAndNil(FProvider);
  inherited;
end;

procedure TRLConnection.SetProviderName(const AValue: string);
begin
  if AValue <> FProviderName then
  begin
    if Assigned(FProvider) and FProvider.IsOpen then
      raise ERLProviderIsActive.Create('Provider is active.');
    if Assigned(FSettings) then
      FreeAndNil(FSettings);
    if Assigned(FSchemas) then
      FreeAndNil(FSchemas);
    if Assigned(FDatabase) then
      FreeAndNil(FDatabase);
    if Assigned(FProvider) then
      FreeAndNil(FProvider);
    FProviderName := AValue;
    ProviderNameChanged;
  end;
end;

function TRLConnection.GetSettings: TRLSettings;
begin
  if not Assigned(FSettings) then
  begin
    CheckProviderName;
    FSettings := TRLSettings.FindSettings(FProviderName);
  end;

  Result := FSettings;
end;

function TRLConnection.GetProvider: TRLProvider;
begin
  if not Assigned(FProvider) then
  begin
    CheckProviderName;
    FProvider := TRLProvider.FindProvider(FProviderName);
  end;

  Result := FProvider;
end;

function TRLConnection.GetDatabase: TRLDatabase;
begin
  if not Assigned(FDatabase) then
  begin
    CheckProvider;
    FDatabase := NewDatabase;
  end;
  
  Result := FDatabase;
end;

function TRLConnection.GetSchemas: TRLSchemas;
begin
  if not Assigned(FSchemas) then
    FSchemas := Database.GetSchemas;
  Result := FSchemas;
end;

function TRLConnection.GetClassMapping: TRLOPFClassMapping;
begin
  if not Assigned(FClassMapping) then
    FClassMapping := CreateClassMapping;
  if FClassMapping.IsEmpty and FUseClassMappinRLopulator then
    PopulateClassMapping(FClassMapping);
  Result := FClassMapping;
end;

function TRLConnection.CreateClassMapping: TRLOPFClassMapping;
begin
  Result := TRLOPFClassMapping.Create;
end;

procedure TRLConnection.PopulateClassMapping(
  const AClassMapping: TRLOPFClassMapping);
var
  lPopulator: TRLOPFClassMappingPopulator;
begin
  if Assigned(AClassMapping) then
  begin
    CheckName;
    lPopulator := TRLOPFClassMappingPopulator.FindClassMappinRLopulator(Name);
    try
      lPopulator.Populate(AClassMapping);
    finally
      FreeAndNil(lPopulator);
    end;
  end;
end;

procedure TRLConnection.CheckProviderName;
begin
  if FProviderName = '' then
    raise ERLProviderNameIsEmpty.Create('ProviderName is empty.');
end;

procedure TRLConnection.CheckProvider;
begin
  if not Provider.IsOpen then
    raise ERLProviderIsNotActive.Create('Provider is not active.');
end;

procedure TRLConnection.ProviderNameChanged;
begin
end;

procedure TRLConnection.Assign(Source: TPersistent);
var
  lConnection: TRLConnection;
begin
  inherited;
  if Source is TRLConnection then
  begin
    lConnection := TRLConnection(Source);
    Description := lConnection.Description;
    ProviderName := lConnection.ProviderName;
    Settings.Assign(lConnection.Settings);
  end;
end;

procedure TRLConnection.Validate;
begin
  inherited;
  CheckProviderName;
end;

procedure TRLConnection.Open;
begin
  if not IsOpen then
  begin
    Provider.Settings.Assign(Settings);
    try
      Provider.Open;
    except
      on E: Exception do
        raise ERLCouldNotOpenConnection.Create('Could not open connection.' +
          sLineBreak + E.Message);
    end;
  end;
end;

procedure TRLConnection.Close;
begin
  if IsOpen then
    try
      Provider.Close;
    except
      on E: Exception do
        raise ERLCouldNotCloseConnection.Create('Could not close connection.' +
          sLineBreak + E.Message);
    end;
end;

function TRLConnection.IsOpen: Boolean;
begin
  Result := Provider.IsOpen;
end;

function TRLConnection.Test(out AError: string): Boolean;
var
  lConnection: TRLConnection;
begin
  try
    lConnection := TRLConnection.Create(nil);
    try
      lConnection.Assign(Self);
      lConnection.Open;
      lConnection.Close;
    finally
      FreeAndNil(lConnection);
    end;

    AError := '';
    Result := True;
  except
    on E: Exception do
    begin
      AError := E.Message;
      Result := False;
    end;
  end;
end;

procedure TRLConnection.BeginUse;
begin
  Open;
  Inc(FUseCount);
end;

procedure TRLConnection.EndUse;
begin
  if FUseCount > 0 then
  begin
    Dec(FUseCount);
    if FUseCount = 0 then
      Close;
  end;
end;

function TRLConnection.InUse: Boolean;
begin
  Result := FUseCount > 0;
end;

procedure TRLConnection.StartTransaction;
begin
  Provider.StartTransaction;
end;

function TRLConnection.InTransaction: Boolean;
begin
  Result := Provider.InTransaction;
end;

procedure TRLConnection.CommitTransaction;
begin
  Provider.CommitTransaction;
end;

procedure TRLConnection.RollbackTransaction;
begin
  Provider.RollbackTransaction;
end;

function TRLConnection.NewQuery: TRLQuery;
begin
  Result := TRLQuery.FindQuery(Provider);
end;

function TRLConnection.NewDatabase: TRLDatabase;
begin
  Result := TRLDatabase.FindDatabase(Provider);
end;

{ TRLConnections }

constructor TRLConnections.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 32;
end;

function TRLConnections.GetItem(const AIndex: Integer): TRLConnection;
begin
  Result := TRLConnection(inherited Items[AIndex]);
end;

function TRLConnections.GetItemClass: TCollectionItemClass;
begin
  Result := TRLConnection;
end;

function TRLConnections.GetProvider(const AName: string): TRLProvider;
begin
  Result := FindByName(AName).Provider;
end;

function TRLConnections.GetDatabase(const AName: string): TRLDatabase;
begin
  Result := FindByName(AName).Database;
end;

function TRLConnections.GetSchemas(const AName: string): TRLSchemas;
begin
  Result := FindByName(AName).Schemas;
end;

function TRLConnections.GetClassMapping(
  const AName: string): TRLOPFClassMapping;
begin
  Result := FindByName(AName).ClassMapping;
end;

function TRLConnections.Add: TRLConnection;
begin
  Result := TRLConnection(inherited Add);
end;

function TRLConnections.Insert(const AIndex: Integer): TRLConnection;
begin
  Result := TRLConnection(inherited Insert(AIndex));
end;

function TRLConnections.Find(const AName: string): TRLConnection;
begin
  Result := TRLConnection(inherited Find(AName));
end;

function TRLConnections.FindByName(const AName: string): TRLConnection;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLConnectionNotFound.CreateFmt('Connection "%s" not found.',
      [AName]);
end;

procedure TRLConnections.Open(const AName: string);
begin
  FindByName(AName).Open;
end;

procedure TRLConnections.Close(const AName: string);
begin
  FindByName(AName).Close;
end;

function TRLConnections.IsOpen(const AName: string): Boolean;
begin
  Result := FindByName(AName).IsOpen;
end;

function TRLConnections.Test(const AName: string; out AError: string): Boolean;
begin
  Result := FindByName(AName).Test(AError);
end;

function TRLConnections.InUse(const AName: string): Boolean;
begin
  Result := FindByName(AName).InUse;
end;

procedure TRLConnections.BeginUse(const AName: string);
begin
  FindByName(AName).BeginUse;
end;

procedure TRLConnections.EndUse(const AName: string);
begin
  FindByName(AName).EndUse;
end;

procedure TRLConnections.StartTransaction(const AName: string);
begin
  FindByName(AName).StartTransaction;
end;

function TRLConnections.InTransaction(const AName: string): Boolean;
begin
  Result := FindByName(AName).InTransaction;
end;

procedure TRLConnections.CommitTransaction(const AName: string);
begin
  FindByName(AName).CommitTransaction;
end;

procedure TRLConnections.RollbackTransaction(const AName: string);
begin
  FindByName(AName).RollbackTransaction;
end;

function TRLConnections.NewQuery(const AName: string): TRLQuery;
begin
  Result := FindByName(AName).NewQuery;
end;

function TRLConnections.NewDatabase(const AName: string): TRLDatabase;
begin
  Result := FindByName(AName).NewDatabase;
end;

procedure TRLConnections.OpenAll;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Open;
end;

procedure TRLConnections.CloseAll;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Close;
end;

function TRLConnections.AllIsOpen: Boolean;
var
  I: Integer;
begin
  Result := True;
  I := 0;
  while (I <= Pred(Count)) and Result do
    if not Items[I].IsOpen then
      Result := False
    else
      Inc(I);
end;

class function TRLConnections.Instance: TRLConnections;
begin
  if not Assigned(RLInstance) then
    RLInstance := TRLConnections.Create;
  Result := RLInstance;
end;

class procedure TRLConnections.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLConnections.DeleteInstance;

end.
