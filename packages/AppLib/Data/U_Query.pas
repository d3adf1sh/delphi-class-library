//=============================================================================
// U_Query
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Query;

{ TODO -oRafael : Habilitar Unidirectional. }

interface

uses
  SysUtils,
  DB,
  U_Exception,
  U_HashTable,
  U_Provider,
  U_Command,
  U_SQL,
  U_Parameters;

type
  // Exceptions
  ERLQueryIsNotActive = class(ERLException);
  ERLFieldNotFound = class(ERLException); //External
  ERLQueryNotFound = class(ERLException);

  // TRLQuery
  TRLQuery = class(TRLCommand)
  private
    FSQL: TRLSQL;
    FParameters: TRLParameters;
    FHashTable: TRLHashTable;
    FClearHashTable: Boolean;
    function GetFieldFromName(const AName: string): TField;
  protected
    function CreateSQL: TRLSQL; virtual; abstract;
    function CreateParameters: TRLParameters; virtual; abstract;
    function CreateHashTable: TRLHashTable; virtual;
    procedure CheckActive; virtual;
    function GetDataSet: TDataSet; virtual; abstract;
    function GetFields: TFields; virtual;
    function GetRecordCount: Integer; virtual;
    property HashTable: TRLHashTable read FHashTable;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; virtual;
    procedure Close; virtual;
    function IsOpen: Boolean; virtual;
    function IsEmpty: Boolean; virtual;
    procedure EnableControls; virtual;
    function ControlsDisabled: Boolean; virtual;
    procedure DisableControls; virtual;
    function Bof: Boolean; virtual;
    function Eof: Boolean; virtual;
    procedure First; virtual;
    procedure Prior; virtual;
    procedure Next; virtual;
    procedure Last; virtual;
    function FindParameter(const AName: string): TRLParameter; virtual;
    function ParameterByName(const AName: string): TRLParameter; virtual;
    procedure FillParameters(const AParameters: TRLParameterDefArray); virtual;
    function FindField(const AName: string): TField; virtual;
    function FieldByName(const AName: string): TField; virtual;
    procedure Execute; virtual; abstract;
    property SQL: TRLSQL read FSQL;
    property Parameters: TRLParameters read FParameters;
    property DataSet: TDataSet read GetDataSet;
    property Fields: TFields read GetFields;
    property FieldFromName[const AName: string]: TField read GetFieldFromName; default;
    property RecordCount: Integer read GetRecordCount;
    class function GetQuery(const AProvider: TRLProvider): TRLQuery;
    class function FindQuery(const AProvider: TRLProvider): TRLQuery;
  end;

implementation

{ TRLQuery }

constructor TRLQuery.Create;
begin
  inherited;
  FParameters := CreateParameters;
  FSQL := CreateSQL;
  FSQL.Parameters := FParameters;
  FHashTable := CreateHashTable;
end;

destructor TRLQuery.Destroy;
begin
  Close;
  FreeAndNil(FParameters);
  FreeAndNil(FSQL);
  FreeAndNil(FHashTable);
  inherited;
end;

function TRLQuery.GetFieldFromName(const AName: string): TField;
begin
  Result := FieldByName(AName);
end;

function TRLQuery.CreateHashTable: TRLHashTable;
begin
  Result := TRLHashTable.Create;
  Result.CaseSensitive := False;
end;

procedure TRLQuery.CheckActive;
begin
  if not IsOpen then
    raise ERLQueryIsNotActive.Create('Query is not active.');
end;

function TRLQuery.GetFields: TFields;
begin
  Result := DataSet.Fields;
end;

function TRLQuery.GetRecordCount: Integer;
begin
  CheckActive;
  Result := DataSet.RecordCount;
end;

procedure TRLQuery.Open;
var
  I: Integer;
begin
  CheckProvider;
  if not IsOpen then
  begin
    DataSet.Open;
    if FClearHashTable then
      FHashTable.Clear;
    for I := 0 to Pred(Fields.Count) do
      FHashTable.Add(Fields[I].FieldName, Fields[I]);
    FClearHashTable := True;
  end;
end;

procedure TRLQuery.Close;
begin
  if IsOpen then
  begin
    FHashTable.Clear;
    DataSet.Close;
  end;
end;

function TRLQuery.IsOpen: Boolean;
begin
  Result := DataSet.Active;
end;

function TRLQuery.IsEmpty: Boolean;
begin
  CheckActive;
  Result := DataSet.IsEmpty;
end;

procedure TRLQuery.EnableControls;
begin
  CheckActive;
  DataSet.EnableControls;
end;

function TRLQuery.ControlsDisabled: Boolean;
begin
  CheckActive;
  Result := DataSet.ControlsDisabled;
end;

procedure TRLQuery.DisableControls;
begin
  CheckActive;
  DataSet.DisableControls;
end;

function TRLQuery.Bof: Boolean;
begin
  CheckActive;
  Result := DataSet.Bof;
end;

function TRLQuery.Eof: Boolean;
begin
  CheckActive;
  Result := DataSet.Eof;
end;

procedure TRLQuery.First;
begin
  CheckActive;
  DataSet.First;
end;

procedure TRLQuery.Prior;
begin
  CheckActive;
  DataSet.Prior;
end;

procedure TRLQuery.Next;
begin
  CheckActive;
  DataSet.Next;
end;

procedure TRLQuery.Last;
begin
  CheckActive;
  DataSet.Last;
end;

function TRLQuery.FindParameter(const AName: string): TRLParameter;
begin
  Result := FParameters.Find(AName);
end;

function TRLQuery.ParameterByName(const AName: string): TRLParameter;
begin
  Result := FParameters.FindByName(AName);
end;

procedure TRLQuery.FillParameters(const AParameters: TRLParameterDefArray);
begin
  FParameters.Fill(AParameters);
end;

function TRLQuery.FindField(const AName: string): TField;
var
  lField: TObject;
begin
  CheckActive;
  if FHashTable.Find(AName, lField) then
    Result := TField(lField)
  else
    Result := nil;
end;

function TRLQuery.FieldByName(const AName: string): TField;
begin
  Result := FindField(AName);
  if not Assigned(Result) then
    raise ERLFieldNotFound.CreateFmt('Field "%s" not found.', [AName]);
end;

class function TRLQuery.GetQuery(const AProvider: TRLProvider): TRLQuery;
begin
  Result := TRLQuery(GetProviderCommand('Query', AProvider));
end;

class function TRLQuery.FindQuery(const AProvider: TRLProvider): TRLQuery;
begin
  Result := GetQuery(AProvider);
  if not Assigned(Result) then
    raise ERLQueryNotFound.Create('Query not found.');
end;

end.
