//=============================================================================
// U_IndexFields
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_IndexFields;

interface

uses
  Classes,
  U_Exception,
  U_NamedCollection;

type
  // Exceptions
  ERLIndexFieldNotFound = class(ERLException);

  // TRLIndexField
  TRLIndexField = class(TRLNamedCollectionItem)
  private
    FPosition: Integer;
    FAscending: Boolean;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Position: Integer read FPosition write FPosition;
    property Ascending: Boolean read FAscending write FAscending;
  end;

  // TRLIndexFields
  TRLIndexFields = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLIndexField;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLIndexField;
    function Insert(const AIndex: Integer): TRLIndexField;
    function Find(const AName: string): TRLIndexField;
    function FindByName(const AName: string): TRLIndexField;
    property Items[const AIndex: Integer]: TRLIndexField read GetItem; default;
  end;

implementation

{ TRLIndexField }

procedure TRLIndexField.Assign(Source: TPersistent);
var
  lField: TRLIndexField;
begin
  inherited;
  if Source is TRLIndexField then
  begin
    lField := TRLIndexField(Source);
    Position := lField.Position;
    Ascending := lField.Ascending;
  end;
end;

{ TRLIndexFields }

constructor TRLIndexFields.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLIndexFields.GetItem(const AIndex: Integer): TRLIndexField;
begin
  Result := TRLIndexField(inherited Items[AIndex]);
end;

function TRLIndexFields.GetItemClass: TCollectionItemClass;
begin
  Result := TRLIndexField;
end;

function TRLIndexFields.Add: TRLIndexField;
begin
  Result := TRLIndexField(inherited Add);
end;

function TRLIndexFields.Insert(const AIndex: Integer): TRLIndexField;
begin
  Result := TRLIndexField(inherited Insert(AIndex));
end;

function TRLIndexFields.Find(const AName: string): TRLIndexField;
begin
  Result := TRLIndexField(inherited Find(AName));
end;

function TRLIndexFields.FindByName(const AName: string): TRLIndexField;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLIndexFieldNotFound.CreateFmt('IndexField "%s" not found.',
      [AName]);
end;

end.
