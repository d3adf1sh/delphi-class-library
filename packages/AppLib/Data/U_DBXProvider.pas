//=============================================================================
// U_DBXProvider
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXProvider;

{$I RL.INC}

interface

uses
  SysUtils,
{$IFDEF D15UP}
  DBXCommon,
{$ELSE}
  DBXpress,
{$ENDIF}
  SqlExpr,
  U_Settings,
  U_Provider,
  U_DBXSettings;

type
  // TRLDBXProvider
  TRLDBXProvider = class(TRLProvider)
  private
    FConnection: TSQLConnection;
{$IFDEF D15UP}
    FTransaction: TDBXTransaction;
{$ELSE}
    FTransaction: TTransactionDesc;
{$ENDIF}
    function GetSettings: TRLDBXSettings;
  protected
    function CreateSettings: TRLSettings; override;
    function GetDriverName: string; override;
    function CreateConnection: TSQLConnection; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; override;
    procedure Close; override;
    function IsOpen: Boolean; override;
    procedure StartTransaction; override;
    function InTransaction: Boolean; override;
    procedure CommitTransaction; override;
    procedure RollbackTransaction; override;
    property Settings: TRLDBXSettings read GetSettings;
    property Connection: TSQLConnection read FConnection;
  end;

const
  RLDBX = 'DBX';  

implementation

{ TRLDBXProvider }

constructor TRLDBXProvider.Create;
begin
  inherited;
  FConnection := CreateConnection;
end;

destructor TRLDBXProvider.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FConnection);
end;

function TRLDBXProvider.GetSettings: TRLDBXSettings;
begin
  Result := TRLDBXSettings(inherited Settings);
end;

function TRLDBXProvider.CreateSettings: TRLSettings;
begin
  Result := TRLDBXSettings.Create;
end;

function TRLDBXProvider.GetDriverName: string;
begin
  Result := Settings.DriverName;
end;

function TRLDBXProvider.CreateConnection: TSQLConnection;
begin
  Result := TSQLConnection.Create(nil);
  Result.LoginPrompt := False;
end;

procedure TRLDBXProvider.Open;
var
  I: Integer;
begin
  if not IsOpen then
  begin
    FConnection.DriverName := Settings.DriverName;
    FConnection.GetDriverFunc := Settings.DriverFunction;
    FConnection.LibraryName := Settings.LibraryName;
    FConnection.VendorLib := Settings.VendorLibrary;
    FConnection.Params.Clear;
    for I := 0 to Pred(Settings.Count) do
      FConnection.Params.Add(
        Format('%s=%s', [Settings.Keys[I], Settings.ValueFromIndex[I]]));
    FConnection.Open;
  end;
end;

procedure TRLDBXProvider.Close;
begin
  if IsOpen then
  begin
    RollbackTransaction;
    FConnection.Close;
  end;
end;

function TRLDBXProvider.IsOpen: Boolean;
begin
  Result := FConnection.Connected;
end;

procedure TRLDBXProvider.StartTransaction;
begin
  CheckActive;
  if not InTransaction then
{$IFDEF D15UP}
    FTransaction := FConnection.BeginTransaction;
{$ELSE}
    FConnection.StartTransaction(FTransaction);
{$ENDIF}
end;

function TRLDBXProvider.InTransaction: Boolean;
begin
  Result := FConnection.InTransaction;
end;

procedure TRLDBXProvider.CommitTransaction;
begin
  if InTransaction then
{$IFDEF D15UP}
    FConnection.CommitFreeAndNil(FTransaction);
{$ELSE}
    FConnection.Commit(FTransaction);
{$ENDIF}
end;

procedure TRLDBXProvider.RollbackTransaction;
begin
  if InTransaction then
{$IFDEF D15UP}
    FConnection.RollbackFreeAndNil(FTransaction);
{$ELSE}
    FConnection.Rollback(FTransaction);
{$ENDIF}
end;

end.
