//=============================================================================
// U_OPFConditions
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFConditions;

interface

uses
  Classes,
  U_Value,
  U_Conditions,
  U_OPFAttributes;

type
  // Forward declarations
  TRLOPFConditions = class;

  // TRLOPFCondition
  TRLOPFCondition = class(TRLCondition)
  private
    FAttributeName: string;
    FAttribute: TRLOPFAttribute;
    FAttributes: TRLOPFAttributes;
    function GetConditions: TRLOPFConditions;
    procedure SetAttributeName(const AValue: string);
    procedure SetAttributes(const AValue: TRLOPFAttributes);
    function GetAttribute: TRLOPFAttribute;
  protected
    function CreateConditions: TRLConditions; override;
    function GetFieldName: string; override;
    function GetValueType: TRLValueType; override;
    procedure AttributeNameChanged; virtual;
    procedure AttributesChanged; virtual;
    procedure ResetAttribute; virtual;
    procedure CheckAttribute; virtual;
  public
    constructor Create(Collection: TCollection); override;
    procedure Validate; override;
    property Conditions: TRLOPFConditions read GetConditions;
    property AttributeName: string read FAttributeName write SetAttributeName;
    property Attributes: TRLOPFAttributes read FAttributes write SetAttributes;
    property Attribute: TRLOPFAttribute read GetAttribute;
  end;

  // TRLOPFConditions
  TRLOPFConditions = class(TRLConditions)
  private
    FAttributes: TRLOPFAttributes;
    function GetItem(const AIndex: Integer): TRLOPFCondition;
    procedure SetAttributes(const AValue: TRLOPFAttributes);
  protected
    function GetItemClass: TCollectionItemClass; override;
    procedure AttributesChanged;
  public
    function Add: TRLOPFCondition;
    function Insert(const AIndex: Integer): TRLOPFCondition;
    property Items[const AIndex: Integer]: TRLOPFCondition read GetItem; default;
    property Attributes: TRLOPFAttributes read FAttributes write SetAttributes;
  end;

implementation

{ TRLOPFCondition }

constructor TRLOPFCondition.Create(Collection: TCollection);
begin
  inherited;
  if Collection is TRLOPFConditions then
    SetAttributes(TRLOPFConditions(Collection).Attributes);
end;

function TRLOPFCondition.GetConditions: TRLOPFConditions;
begin
  Result := TRLOPFConditions(inherited Conditions);
end;

procedure TRLOPFCondition.SetAttributeName(const AValue: string);
begin
  if FAttributeName <> AValue then
  begin
    FAttributeName := AValue;
    ResetAttribute;
    AttributeNameChanged;
  end;
end;

procedure TRLOPFCondition.SetAttributes(const AValue: TRLOPFAttributes);
begin
  if FAttributes <> AValue then
  begin
    FAttributes := AValue;
    ResetAttribute;
    AttributesChanged;
  end;
end;

function TRLOPFCondition.GetAttribute: TRLOPFAttribute;
begin
  if not Assigned(FAttribute) and Assigned(FAttributes) then
    FAttribute := FAttributes.Find(FAttributeName);
  Result := FAttribute;
end;

function TRLOPFCondition.CreateConditions: TRLConditions;
begin
  Result := TRLOPFConditions.CreateParented(Self);
end;

function TRLOPFCondition.GetFieldName: string;
begin
  if Assigned(Attribute) then
    Result := Attribute.FieldName
  else
    Result := '';
end;

function TRLOPFCondition.GetValueType: TRLValueType;
begin
  if Assigned(Attribute) then
    Result := Attribute.ValueType
  else
    Result := vtString;
end;

procedure TRLOPFCondition.AttributeNameChanged;
begin
end;

procedure TRLOPFCondition.AttributesChanged;
begin
  Conditions.Attributes := FAttributes;
end;

procedure TRLOPFCondition.ResetAttribute;
begin
  FAttribute := nil;
end;

procedure TRLOPFCondition.CheckAttribute;
begin
  if not Assigned(Attribute) then
    raise ERLOPFAttributeIsInvalid.CreateFmt('Attribute "%s" is invalid.',
      [FAttributeName]);
end;

procedure TRLOPFCondition.Validate;
begin
  inherited;
  CheckAttribute;
end;

{ TRLOPFConditions }

procedure TRLOPFConditions.SetAttributes(const AValue: TRLOPFAttributes);
begin
  if FAttributes <> AValue then
  begin
    FAttributes := AValue;
    AttributesChanged;
  end;
end;

function TRLOPFConditions.GetItem(const AIndex: Integer): TRLOPFCondition;
begin
  Result := TRLOPFCondition(inherited Items[AIndex]);
end;

function TRLOPFConditions.GetItemClass: TCollectionItemClass;
begin
  Result := TRLOPFCondition;
end;

procedure TRLOPFConditions.AttributesChanged;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Attributes := FAttributes;
end;

function TRLOPFConditions.Add: TRLOPFCondition;
begin
  Result := TRLOPFCondition(inherited Add);
end;

function TRLOPFConditions.Insert(const AIndex: Integer): TRLOPFCondition;
begin
  Result := TRLOPFCondition(inherited Insert(AIndex));
end;

end.
