//=============================================================================
// U_MSSQLDatabase
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MSSQLDatabase;

interface

uses
  SysUtils,
  Classes,
  U_Value,
  U_Database,
  U_SQL,
  U_Fields;

type
  // TRLMSSQLDatabase
  TRLMSSQLDatabase = class(TRLDatabase)
  protected
    function GetValueType(const AField: TRLField): TRLValueType; override;
    procedure BuildDateAndTimeSQL(const ASQL: TRLSQL); override;
    procedure BuildSchemasSQL(const ASQL: TRLSQL); override;
    procedure BuildRelationsSQL(const ASQL: TRLSQL); override;
    procedure BuildFieldsSQL(const ASQL: TRLSQL); override;
    procedure BuildKeysSQL(const ASQL: TRLSQL); override;
    procedure BuildIndexesSQL(const ASQL: TRLSQL); override;
    procedure BuildReferencesSQL(const ASQL: TRLSQL); override;
  end;

const
  RLMSSQL = 'MSSQL';

implementation

uses
  StrUtils;

{ TRLMSSQLDatabase }

function TRLMSSQLDatabase.GetValueType(const AField: TRLField): TRLValueType;
var
  sDataType: string;
begin
  sDataType := UpperCase(AField.DataType);
  if (sDataType = 'CHAR') or
    (sDataType = 'NCHAR') or
    (sDataType = 'VARCHAR') or
    (sDataType = 'NVARCHAR') then
    Result := vtString
  else if (sDataType = 'TINYINT') or
    (sDataType = 'SMALLINT') or
    (sDataType = 'INT') then
    Result := vtInteger
  else if sDataType = 'BIGINT' then
    Result := vtInt64
  else if (sDataType = 'DECIMAL') or
    (sDataType = 'NUMERIC') or
    (sDataType = 'FLOAT') or
    (sDataType = 'REAL') then
    Result := vtFloat
  else if (sDataType = 'MONEY') or
    (sDataType = 'SMALLMONEY') then
    Result := vtCurrency
  else if sDataType = 'DATE' then
    Result := vtDate
  else if sDataType = 'TIME' then
    Result := vtTime
  else if (sDataType = 'DATETIME') or
    (sDataType = 'SMALLDATETIME') then
    Result := vtDateTime
  else if sDataType = 'BIT' then
    Result := vtBoolean
  else if (sDataType = 'TEXT') or
    (sDataType = 'NTEXT') or
    (sDataType = 'IMAGE') or
    (sDataType = 'BINARY') or
    (sDataType = 'VARBINARY') then
    Result := vtBinary
  else
    Result := inherited GetValueType(AField);
end;

procedure TRLMSSQLDatabase.BuildDateAndTimeSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('SELECT CURRENT_TIMESTAMP DATE_TIME');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLMSSQLDatabase.BuildSchemasSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('SELECT S.NAME SCHEMA_NAME');
    ASQL.Add('  FROM SYS.SCHEMAS S');
    ASQL.Add(' WHERE S.SCHEMA_ID = 1');
    ASQL.Add('    OR S.SCHEMA_ID BETWEEN 5 AND 16383');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLMSSQLDatabase.BuildRelationsSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT S.NAME SCHEMA_NAME');
    ASQL.Add('         , O.NAME RELATION_NAME');
    ASQL.Add('         , O.RELATION_TYPE');
    ASQL.Add('      FROM (   SELECT T.SCHEMA_ID');
    ASQL.Add('                    , T.OBJECT_ID');
    ASQL.Add('                    , T.NAME');
    ASQL.Add('                    , ''rtTable'' RELATION_TYPE');
    ASQL.Add('                 FROM SYS.TABLES T');
    ASQL.Add('                WHERE T.TYPE = ''U''');
    ASQL.Add('            UNION ALL');
    ASQL.Add('               SELECT U.SCHEMA_ID');
    ASQL.Add('                    , U.OBJECT_ID');
    ASQL.Add('                    , U.NAME');
    ASQL.Add('                    , ''rtView'' RELATION_TYPE');
    ASQL.Add('                 FROM SYS.VIEWS U');
    ASQL.Add('                WHERE U.TYPE = ''V'') O');
    ASQL.Add('INNER JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = O.SCHEMA_ID');
    ASQL.Add('  ORDER BY S.NAME');
    ASQL.Add('         , O.NAME');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLMSSQLDatabase.BuildFieldsSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT S.NAME SCHEMA_NAME');
    ASQL.Add('         , O.NAME RELATION_NAME');
    ASQL.Add('         , C.NAME FIELD_NAME');
    ASQL.Add('         , COLUMNPROPERTY(C.OBJECT_ID, C.NAME, ''ORDINAL'') - 1 POSITION');
    ASQL.Add('         , TYPE_NAME(C.SYSTEM_TYPE_ID) DATA_TYPE');
    ASQL.Add('         , CASE');
    ASQL.Add('             WHEN C.SYSTEM_TYPE_ID IN (175/*CHAR*/,');
    ASQL.Add('                                       239/*NCHAR*/,');
    ASQL.Add('                                       167/*VARCHAR*/,');
    ASQL.Add('                                       231/*NVARCHAR*/,');
    ASQL.Add('                                       173/*BINARY*/,');
    ASQL.Add('                                       165/*VARBINARY*/) THEN COLUMNPROPERTY(C.OBJECT_ID, C.NAME, ''CHARMAXLEN'')');
    ASQL.Add('             ELSE 0');
    ASQL.Add('           END LENGTH');
    ASQL.Add('         , CASE');
    ASQL.Add('             WHEN C.SYSTEM_TYPE_ID IN (106/*DECIMAL*/,');
    ASQL.Add('                                       108/*NUMERIC*/) THEN C.PRECISION');
    ASQL.Add('             ELSE 0');
    ASQL.Add('           END PRECISION');
    ASQL.Add('         , CASE');
    ASQL.Add('             WHEN C.SYSTEM_TYPE_ID IN (106/*DECIMAL*/,');
    ASQL.Add('                                       108/*NUMERIC*/) THEN ODBCSCALE(C.SYSTEM_TYPE_ID, C.SCALE)');
    ASQL.Add('             ELSE 0');
    ASQL.Add('           END SCALE');
    ASQL.Add('         , CASE C.IS_IDENTITY');
    ASQL.Add('             WHEN 0 THEN ''False''');
    ASQL.Add('             WHEN 1 THEN ''True''');
    ASQL.Add('           END AUTO_INCREMENT');
    ASQL.Add('         , CASE C.IS_NULLABLE');
    ASQL.Add('             WHEN 0 THEN ''True''');
    ASQL.Add('             WHEN 1 THEN ''False''');
    ASQL.Add('           END REQUIRED');
    ASQL.Add('      FROM (   SELECT T.SCHEMA_ID');
    ASQL.Add('                    , T.OBJECT_ID');
    ASQL.Add('                    , T.NAME');
    ASQL.Add('                    , ''False'' IS_VIEW');
    ASQL.Add('                 FROM SYS.TABLES T');
    ASQL.Add('                WHERE T.TYPE = ''U''');
    ASQL.Add('            UNION ALL');
    ASQL.Add('               SELECT U.SCHEMA_ID');
    ASQL.Add('                    , U.OBJECT_ID');
    ASQL.Add('                    , U.NAME');
    ASQL.Add('                    , ''True'' IS_VIEW');
    ASQL.Add('                 FROM SYS.VIEWS U');
    ASQL.Add('                WHERE U.TYPE = ''V'') O');
    ASQL.Add('INNER JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = O.SCHEMA_ID');
    ASQL.Add('INNER JOIN SYS.COLUMNS C ON C.OBJECT_ID = O.OBJECT_ID');
    ASQL.Add('     WHERE C.IS_ROWGUIDCOL = 0');
    ASQL.Add('       AND C.IS_COMPUTED = 0');
    ASQL.Add('  ORDER BY S.NAME');
    ASQL.Add('         , O.NAME');
    ASQL.Add('         , COLUMNPROPERTY(C.OBJECT_ID, C.NAME, ''ORDINAL'')');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLMSSQLDatabase.BuildKeysSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT S.NAME SCHEMA_NAME');
    ASQL.Add('         , O.NAME RELATION_NAME');
    ASQL.Add('         , I.NAME KEY_NAME');
    ASQL.Add('         , CASE');
    ASQL.Add('             WHEN I.IS_PRIMARY_KEY = 1 THEN ''ktPrimary''');
    ASQL.Add('             WHEN I.IS_UNIQUE_CONSTRAINT = 1 THEN ''ktUnique''');
    ASQL.Add('           END KEY_TYPE');
    ASQL.Add('         , C.NAME FIELD_NAME');
    ASQL.Add('         , IC.KEY_ORDINAL - 1 POSITION');
    ASQL.Add('         , CASE I.IS_DISABLED');
    ASQL.Add('             WHEN 0 THEN ''True''');
    ASQL.Add('             WHEN 1 THEN ''False''');
    ASQL.Add('           END ENABLED');
    ASQL.Add('      FROM SYS.INDEXES I');
    ASQL.Add('INNER JOIN SYS.OBJECTS O ON O.OBJECT_ID = I.OBJECT_ID');
    ASQL.Add('INNER JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = O.SCHEMA_ID');
    ASQL.Add('INNER JOIN SYS.INDEX_COLUMNS IC ON IC.OBJECT_ID = I.OBJECT_ID AND IC.INDEX_ID = I.INDEX_ID');
    ASQL.Add('INNER JOIN SYS.COLUMNS C ON C.OBJECT_ID = I.OBJECT_ID AND C.COLUMN_ID = IC.COLUMN_ID');
    ASQL.Add('     WHERE I.IS_PRIMARY_KEY = 1');
    ASQL.Add('        OR I.IS_UNIQUE_CONSTRAINT = 1');
    ASQL.Add('  ORDER BY S.NAME');
    ASQL.Add('         , O.NAME');
    ASQL.Add('         , I.NAME');
    ASQL.Add('         , IC.KEY_ORDINAL');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLMSSQLDatabase.BuildIndexesSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT S.NAME SCHEMA_NAME');
    ASQL.Add('         , O.NAME RELATION_NAME');
    ASQL.Add('         , I.NAME INDEX_NAME');
    ASQL.Add('         , CASE I.IS_UNIQUE');
    ASQL.Add('             WHEN 0 THEN ''itIndex''');
    ASQL.Add('             WHEN 1 THEN ''itUniqueIndex''');
    ASQL.Add('           END INDEX_TYPE');
    ASQL.Add('         , C.NAME FIELD_NAME');
    ASQL.Add('         , IC.KEY_ORDINAL - 1 POSITION');
    ASQL.Add('         , CASE IC.IS_DESCENDING_KEY');
    ASQL.Add('             WHEN 0 THEN ''True''');
    ASQL.Add('             WHEN 1 THEN ''False''');
    ASQL.Add('           END ASCENDING');
    ASQL.Add('         , CASE I.IS_DISABLED');
    ASQL.Add('             WHEN 0 THEN ''True''');
    ASQL.Add('             WHEN 1 THEN ''False''');
    ASQL.Add('           END ENABLED');
    ASQL.Add('      FROM SYS.INDEXES I');
    ASQL.Add('INNER JOIN SYS.OBJECTS O ON O.OBJECT_ID = I.OBJECT_ID');
    ASQL.Add('INNER JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = O.SCHEMA_ID');
    ASQL.Add('INNER JOIN SYS.INDEX_COLUMNS IC ON IC.OBJECT_ID = I.OBJECT_ID AND IC.INDEX_ID = I.INDEX_ID');
    ASQL.Add('INNER JOIN SYS.COLUMNS C ON C.OBJECT_ID = I.OBJECT_ID AND C.COLUMN_ID = IC.COLUMN_ID');
    ASQL.Add('     WHERE I.IS_PRIMARY_KEY = 0');
    ASQL.Add('       AND I.IS_UNIQUE_CONSTRAINT = 0');
    ASQL.Add('       AND I.TYPE_DESC <> ''XML''');
    ASQL.Add('       AND O.TYPE = ''U''');
    ASQL.Add('  ORDER BY S.NAME');
    ASQL.Add('         , O.NAME');
    ASQL.Add('         , I.NAME');
    ASQL.Add('         , IC.KEY_ORDINAL');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLMSSQLDatabase.BuildReferencesSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT S.NAME SCHEMA_NAME');
    ASQL.Add('         , O.NAME RELATION_NAME');
    ASQL.Add('         , FK.NAME REFERENCE_NAME');
    ASQL.Add('         , C.NAME FIELD_NAME');
    ASQL.Add('         , FKC.CONSTRAINT_COLUMN_ID - 1 POSITION');
    ASQL.Add('         , RS.NAME PARENT_SCHEMA_NAME');
    ASQL.Add('         , RO.NAME PARENT_RELATION_NAME');
    ASQL.Add('         , RC.NAME PARENT_FIELD_NAME');
    ASQL.Add('         , CASE FK.UPDATE_REFERENTIAL_ACTION_DESC');
    ASQL.Add('             WHEN ''NO_ACTION'' THEN ''rrNoAction''');
    ASQL.Add('             WHEN ''CASCADE'' THEN ''rrCascade''');
    ASQL.Add('             WHEN ''SET_DEFAULT'' THEN ''rrSetDefault''');
    ASQL.Add('             WHEN ''SET_NULL'' THEN ''rrSetNull''');
    ASQL.Add('           END UPDATE_RULE');
    ASQL.Add('         , CASE FK.DELETE_REFERENTIAL_ACTION_DESC');
    ASQL.Add('             WHEN ''NO_ACTION'' THEN ''rrNoAction''');
    ASQL.Add('             WHEN ''CASCADE'' THEN ''rrCascade''');
    ASQL.Add('             WHEN ''SET_DEFAULT'' THEN ''rrSetDefault''');
    ASQL.Add('             WHEN ''SET_NULL'' THEN ''rrSetNull''');
    ASQL.Add('           END DELETE_RULE');
    ASQL.Add('         , CASE FK.IS_DISABLED');
    ASQL.Add('             WHEN 0 THEN ''True''');
    ASQL.Add('             WHEN 1 THEN ''False''');
    ASQL.Add('           END ENABLED');
    ASQL.Add('      FROM SYS.FOREIGN_KEYS FK');
    ASQL.Add('INNER JOIN SYS.OBJECTS O ON O.OBJECT_ID = FK.PARENT_OBJECT_ID');
    ASQL.Add('INNER JOIN SYS.SCHEMAS S ON S.SCHEMA_ID = O.SCHEMA_ID');
    ASQL.Add('INNER JOIN SYS.OBJECTS RO ON RO.OBJECT_ID = FK.REFERENCED_OBJECT_ID');
    ASQL.Add('INNER JOIN SYS.SCHEMAS RS ON RS.SCHEMA_ID = RO.SCHEMA_ID');
    ASQL.Add('INNER JOIN SYS.FOREIGN_KEY_COLUMNS FKC ON FKC.CONSTRAINT_OBJECT_ID = FK.OBJECT_ID');
    ASQL.Add('INNER JOIN SYS.COLUMNS C ON C.OBJECT_ID = FKC.PARENT_OBJECT_ID AND C.COLUMN_ID = FKC.PARENT_COLUMN_ID');
    ASQL.Add('INNER JOIN SYS.COLUMNS RC ON RC.OBJECT_ID = FKC.REFERENCED_OBJECT_ID AND RC.COLUMN_ID = FKC.REFERENCED_COLUMN_ID');
    ASQL.Add('  ORDER BY S.NAME');
    ASQL.Add('         , O.NAME');
    ASQL.Add('         , FK.NAME');
    ASQL.Add('         , FKC.CONSTRAINT_COLUMN_ID');
  finally
    ASQL.EndUpdate;
  end;
end;

initialization
  RegisterClass(TRLMSSQLDatabase);

end.
