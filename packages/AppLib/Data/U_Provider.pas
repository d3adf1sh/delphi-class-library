//=============================================================================
// U_Provider
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Provider;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Settings;

type
  // References
  TRLProviderClass = class of TRLProvider;

  // Exceptions
  ERLProviderIsNotActive = class(ERLException);
  ERLProviderIsActive = class(ERLException);
  ERLProviderIsNull = class(ERLException); //External
  ERLProviderNotFound = class(ERLException);

  // TRLProvider
  TRLProvider = class(TRLBaseType)
  private
    FSettings: TRLSettings;
  protected
    function CreateSettings: TRLSettings; virtual; abstract;
    function GetDriverName: string; virtual; abstract;
    procedure CheckActive; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; virtual; abstract;
    procedure Close; virtual; abstract;
    function IsOpen: Boolean; virtual; abstract;
    procedure StartTransaction; virtual; abstract;
    function InTransaction: Boolean; virtual; abstract;
    procedure CommitTransaction; virtual; abstract;
    procedure RollbackTransaction; virtual; abstract;
    property Settings: TRLSettings read FSettings;
    property DriverName: string read GetDriverName;
    class function GetProvider(const AName: string): TRLProvider;
    class function FindProvider(const AName: string): TRLProvider;
  end;

implementation

{ TRLProvider }

constructor TRLProvider.Create;
begin
  inherited;
  FSettings := CreateSettings;
end;

destructor TRLProvider.Destroy;
begin
  Close;
  FreeAndNil(FSettings);
  inherited;
end;

procedure TRLProvider.CheckActive;
begin
  if not IsOpen then
    raise ERLProviderIsNotActive.Create('Provider is not active.');
end;

class function TRLProvider.GetProvider(const AName: string): TRLProvider;
var
  lClass: TRLProviderClass;
begin
  lClass := TRLProviderClass(GetClass(Format('TRL%sProvider', [AName])));
  if Assigned(lClass) then
    Result := lClass.Create
  else
    Result := nil;
end;

class function TRLProvider.FindProvider(const AName: string): TRLProvider;
begin
  Result := GetProvider(AName);
  if not Assigned(Result) then
    raise ERLProviderNotFound.CreateFmt('Provider "%s" not found.', [AName]);
end;

end.
