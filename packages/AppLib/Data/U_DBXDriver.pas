//=============================================================================
// U_DBXDriver
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXDriver;

interface

uses
  U_Drivers;

type
  // TRLDBXDriver
  TRLDBXDriver = class(TRLDriver)
  private
    FDriverFunction: string;
    FLibraryName: string;
    FVendorLibrary: string;
  public
    property DriverFunction: string read FDriverFunction write FDriverFunction;
    property LibraryName: string read FLibraryName write FLibraryName;
    property VendorLibrary: string read FVendorLibrary write FVendorLibrary;
  end;

implementation

end.
