//=============================================================================
// U_SDACProvider
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACProvider;

interface

uses
  SysUtils,
  MSAccess,
  U_Settings,
  U_Provider,
  U_SDACSettings,
  U_MSSQLDatabase;

type
  // TRLSDACProvider
  TRLSDACProvider = class(TRLProvider)
  private
    FConnection: TMSConnection;
    function GetSettings: TRLSDACSettings;
  protected
    function CreateSettings: TRLSettings; override;
    function GetDriverName: string; override;
    function CreateConnection: TMSConnection; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; override;
    procedure Close; override;
    function IsOpen: Boolean; override;
    procedure StartTransaction; override;
    function InTransaction: Boolean; override;
    procedure CommitTransaction; override;
    procedure RollbackTransaction; override;
    property Settings: TRLSDACSettings read GetSettings;
    property Connection: TMSConnection read FConnection;
  end;

const
  RLSDAC = 'SDAC';

implementation

{ TRLSDACProvider }

constructor TRLSDACProvider.Create;
begin
  inherited;
  FConnection := CreateConnection;
end;

destructor TRLSDACProvider.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FConnection);
end;

function TRLSDACProvider.GetSettings: TRLSDACSettings;
begin
  Result := TRLSDACSettings(inherited Settings);
end;

function TRLSDACProvider.CreateSettings: TRLSettings;
begin
  Result := TRLSDACSettings.Create;
end;

function TRLSDACProvider.GetDriverName: string;
begin
  Result := RLMSSQL;
end;

function TRLSDACProvider.CreateConnection: TMSConnection;
begin
  Result := TMSConnection.Create(nil);
  Result.LoginPrompt := False;
end;

procedure TRLSDACProvider.Open;
begin
  if not IsOpen then
  begin
    FConnection.Server := Settings.Server;
    FConnection.Database := Settings.Database;
    FConnection.Username := Settings.Username;
    FConnection.Password := Settings.Password;
    FConnection.Open;
  end;
end;

procedure TRLSDACProvider.Close;
begin
  if IsOpen then
  begin
    RollbackTransaction;
    FConnection.Close;
  end;
end;

function TRLSDACProvider.IsOpen: Boolean;
begin
  Result := FConnection.Connected;
end;

procedure TRLSDACProvider.StartTransaction;
begin
  CheckActive;
  if not InTransaction then
    FConnection.StartTransaction;
end;

function TRLSDACProvider.InTransaction: Boolean;
begin
  Result := FConnection.InTransaction;
end;

procedure TRLSDACProvider.CommitTransaction;
begin
  if InTransaction then
    FConnection.Commit;
end;

procedure TRLSDACProvider.RollbackTransaction;
begin
  if InTransaction then
    FConnection.Rollback;
end;

end.
