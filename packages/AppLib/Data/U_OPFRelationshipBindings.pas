//=============================================================================
// U_OPFRelationshipBindings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFRelationshipBindings;

interface

uses
  Classes,
  U_Exception,
  U_KeyedCollection;

type
  // Exceptions
  ERLOPFAttributeNameIsEmpty = class(ERLException);
  ERLOPFAttributeNameAlreadyExists = class(ERLException);
  ERLOPFAttributeNameNotFound = class(ERLException);  
  ERLOPFRelatedAttributeNameIsEmpty = class(ERLException);

  // TRLOPFRelationshipBinding
  TRLOPFRelationshipBinding = class(TRLKeyedCollectionItem)
  private
    FRelatedAttributeName: string;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
    procedure KeyAlreadyExists(const AValue: string); override;
    procedure CheckAttributeName; virtual;
    procedure CheckRelatedAttributeName; virtual;
  public
    procedure Validate; override;
    property AttributeName: string read GetKey write SetKey;
    property RelatedAttributeName: string read FRelatedAttributeName write FRelatedAttributeName;
  end;

  // TRLOPFRelationshipBindings
  TRLOPFRelationshipBindings = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLOPFRelationshipBinding;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLOPFRelationshipBinding;
    function Insert(const AIndex: Integer): TRLOPFRelationshipBinding;
    function Find(const AAttributeName: string): TRLOPFRelationshipBinding;
    function FindByAttributeName(const AAttributeName: string): TRLOPFRelationshipBinding;
    function Contains(const AAttributeName: string): Boolean; reintroduce; virtual;
    function Remove(const AAttributeName: string): Boolean; reintroduce; virtual;
    property Items[const AIndex: Integer]: TRLOPFRelationshipBinding read GetItem; default;
  end;

implementation

{ TRLOPFRelationshipBinding }

function TRLOPFRelationshipBinding.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TRLOPFRelationshipBinding.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TRLOPFRelationshipBinding.KeyAlreadyExists(const AValue: string);
begin
  raise ERLOPFAttributeNameAlreadyExists.CreateFmt(
    'AttributeName "%s" already exists.', [AValue]);
end;

procedure TRLOPFRelationshipBinding.CheckAttributeName;
begin
  if AttributeName = '' then
    raise ERLOPFAttributeNameIsEmpty.Create('AttributeName is empty.');
end;

procedure TRLOPFRelationshipBinding.CheckRelatedAttributeName;
begin
  if FRelatedAttributeName = '' then
    raise ERLOPFRelatedAttributeNameIsEmpty.Create(
      'RelatedAttributeName is empty.');
end;

procedure TRLOPFRelationshipBinding.Validate;
begin
  inherited;
  CheckAttributeName;
  CheckRelatedAttributeName;
end;

{ TRLOPFRelationshipBindings }

constructor TRLOPFRelationshipBindings.CreateParented(
  const AParent: TObject);
begin
  inherited;
  HashSize := 16;
  CaseSensitive := False;
end;

function TRLOPFRelationshipBindings.GetItem(
  const AIndex: Integer): TRLOPFRelationshipBinding;
begin
  Result := TRLOPFRelationshipBinding(inherited Items[AIndex]);
end;

function TRLOPFRelationshipBindings.GetItemClass: TCollectionItemClass;
begin
  Result := TRLOPFRelationshipBinding;
end;

function TRLOPFRelationshipBindings.Add: TRLOPFRelationshipBinding;
begin
  Result := TRLOPFRelationshipBinding(inherited Add);
end;

function TRLOPFRelationshipBindings.Insert(
  const AIndex: Integer): TRLOPFRelationshipBinding;
begin
  Result := TRLOPFRelationshipBinding(inherited Insert(AIndex));
end;

function TRLOPFRelationshipBindings.Find(
  const AAttributeName: string): TRLOPFRelationshipBinding;
begin
  Result := TRLOPFRelationshipBinding(inherited Find(AAttributeName));
end;

function TRLOPFRelationshipBindings.FindByAttributeName(
  const AAttributeName: string): TRLOPFRelationshipBinding;
begin
  Result := Find(AAttributeName);
  if not Assigned(Result) then
    raise ERLOPFAttributeNameNotFound.CreateFmt('AttributeName "%s" not found.',
      [AAttributeName]);
end;

function TRLOPFRelationshipBindings.Contains(
  const AAttributeName: string): Boolean;
begin
  Result := inherited Contains(AAttributeName);
end;

function TRLOPFRelationshipBindings.Remove(
  const AAttributeName: string): Boolean;
begin
  Result := inherited Remove(AAttributeName);
end;

end.
