//=============================================================================
// U_SQLConditions
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SQLConditions;

interface

uses
  Classes,
  U_Value,
  U_Conditions;

type
  // TRLSQLCondition
  TRLSQLCondition = class(TRLCondition)
  private
    FFieldName: string;
  protected
    function GetFieldName: string; override;
    function GetValueType: TRLValueType; override;
  public
    property FieldName: string read GetFieldName write FFieldName;
  end;

  // TRLSQLConditions
  TRLSQLConditions = class(TRLConditions)
  private
    function GetItem(const AIndex: Integer): TRLSQLCondition;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TRLSQLCondition;
    function Insert(const AIndex: Integer): TRLSQLCondition;
    property Items[const AIndex: Integer]: TRLSQLCondition read GetItem; default;
  end;

implementation

{ TRLSQLCondition }

function TRLSQLCondition.GetFieldName: string;
begin
  Result := FFieldName;
end;

function TRLSQLCondition.GetValueType: TRLValueType;
begin
  Result := Comparison.ValueType;
end;

{ TRLSQLConditions }

function TRLSQLConditions.GetItem(const AIndex: Integer): TRLSQLCondition;
begin
  Result := TRLSQLCondition(inherited Items[AIndex]);
end;

function TRLSQLConditions.GetItemClass: TCollectionItemClass;
begin
  Result := TRLSQLCondition;
end;

function TRLSQLConditions.Add: TRLSQLCondition;
begin
  Result := TRLSQLCondition(inherited Add);
end;

function TRLSQLConditions.Insert(const AIndex: Integer): TRLSQLCondition;
begin
  Result := TRLSQLCondition(inherited Insert(AIndex));
end;

end.
