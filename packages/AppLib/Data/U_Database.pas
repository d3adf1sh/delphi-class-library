//=============================================================================
// U_Database
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Database;

{$I RL.INC}

interface

uses
  SysUtils,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_Exception,
  U_StringList,
  U_StringStream,
  U_Value,
  U_Comparison,
  U_Provider,
  U_Command,
  U_SQL,
  U_Parameters,
  U_Query,
  U_Conditions,
  U_Schemas,
  U_Relations,
  U_Fields,
  U_Keys,
  U_KeyFields,
  U_Indexes,
  U_IndexFields,
  U_References,
  U_ReferenceFields;

type
  // Exceptions
  ERLCouldNotBuildSQLFor = class(ERLException);
  ERLCouldNotGetDateAndTime = class(ERLException);
  ERLCouldNotGetSchemas = class(ERLException);
  ERLCouldNotGetSequenceValue = class(ERLException);
  ERLDatabaseNotFound = class(ERLException);

  // TRLDatabase
  TRLDatabase = class(TRLCommand)
  protected
    function CreateSchemas: TRLSchemas; virtual;
    function CreateCouldNotBuildSQLFor(const AName: string): ERLCouldNotBuildSQLFor; virtual;
    function GetValueType(const AField: TRLField): TRLValueType; virtual;
    procedure BuildDateAndTimeSQL(const ASQL: TRLSQL); virtual;
    procedure BuildSchemasSQL(const ASQL: TRLSQL); virtual;
    procedure BuildRelationsSQL(const ASQL: TRLSQL); virtual;
    procedure BuildFieldsSQL(const ASQL: TRLSQL); virtual;
    procedure BuildKeysSQL(const ASQL: TRLSQL); virtual;
    procedure BuildIndexesSQL(const ASQL: TRLSQL); virtual;
    procedure BuildReferencesSQL(const ASQL: TRLSQL); virtual;
    procedure BuildSequenceValueSQL(const ASQL: TRLSQL; const ASequenceName: string); virtual;
    procedure BuildConditionsSQL(const ASQL: TRLStringStream; const AParameters: TRLParameterDefArray; const AConditions: TRLConditions); virtual;
    procedure BuildConditionSQL(const ASQL: TRLStringStream; const AParameters: TRLParameterDefArray; const ACondition: TRLCondition); virtual;
    procedure RetrieveSchemas(const ASchemas: TRLSchemas); virtual;
    procedure RetrieveRelations(const ASchemas: TRLSchemas); virtual;
    procedure RetrieveFields(const ASchemas: TRLSchemas); virtual;
    procedure RetrieveKeys(const ASchemas: TRLSchemas); virtual;
    procedure RetrieveIndexes(const ASchemas: TRLSchemas); virtual;
    procedure RetrieveReferences(const ASchemas: TRLSchemas); virtual;
    procedure FillSchema(const ASchema: TRLSchema; const AQuery: TRLQuery); virtual;
    procedure FillRelation(const ARelation: TRLRelation; const AQuery: TRLQuery); virtual;
    procedure FillField(const AField: TRLField; const AQuery: TRLQuery); virtual;
    procedure FillKey(const AKey: TRLKey; const AQuery: TRLQuery); virtual;
    procedure FillKeyField(const AField: TRLKeyField; const AQuery: TRLQuery); virtual;
    procedure FillIndex(const AIndex: TRLIndex; const AQuery: TRLQuery); virtual;
    procedure FillIndexField(const AField: TRLIndexField; const AQuery: TRLQuery); virtual;
    procedure FillReference(const AReference: TRLReference; const AQuery: TRLQuery); virtual;
    procedure FillReferenceField(const AField: TRLReferenceField; const AQuery: TRLQuery); virtual;
  public
    function GetDateAndTime: TDateTime; virtual;
    function GetDate: TDate; virtual;
    function GetTime: TTime; virtual;
    function GetSchemas: TRLSchemas; virtual;
    function GetSequenceValue(const ASequenceName: string): Integer; virtual;
    procedure BuildSQL(const AQuery: TRLQuery; const ASQL: TRLStringList; const AConditions: TRLConditions); virtual;
    procedure BuildSQLConditions(const ASQL: TRLSQL; const AConditions: TRLConditions; const AParameters: TRLParameterDefArray); virtual;
    class function GetDatabase(const AProvider: TRLProvider): TRLDatabase;
    class function FindDatabase(const AProvider: TRLProvider): TRLDatabase;
  end;

implementation

uses
  StrUtils;

{ TRLDatabase }

function TRLDatabase.CreateSchemas: TRLSchemas;
begin
  Result := TRLSchemas.Create;
end;

function TRLDatabase.CreateCouldNotBuildSQLFor(
  const AName: string): ERLCouldNotBuildSQLFor;
begin
  Result := ERLCouldNotBuildSQLFor.CreateFmt('Could not build SQL for "%s".',
    [AName]);
end;

function TRLDatabase.GetValueType(const AField: TRLField): TRLValueType;
begin
  Result := vtString;
end;

procedure TRLDatabase.BuildDateAndTimeSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('Date and time');
end;

procedure TRLDatabase.BuildSchemasSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('Schemas');
end;

procedure TRLDatabase.BuildRelationsSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('Relations');
end;

procedure TRLDatabase.BuildFieldsSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('Fields');
end;

procedure TRLDatabase.BuildKeysSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('Keys');
end;

procedure TRLDatabase.BuildIndexesSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('Indexes');
end;

procedure TRLDatabase.BuildReferencesSQL(const ASQL: TRLSQL);
begin
  raise CreateCouldNotBuildSQLFor('References');
end;

procedure TRLDatabase.BuildSequenceValueSQL(const ASQL: TRLSQL;
  const ASequenceName: string);
begin
  raise CreateCouldNotBuildSQLFor('Sequence value');
end;

procedure TRLDatabase.BuildConditionsSQL(const ASQL: TRLStringStream;
  const AParameters: TRLParameterDefArray; const AConditions: TRLConditions);
var
  I: Integer;
begin
  for I := 0 to Pred(AConditions.Count) do
    BuildConditionSQL(ASQL, AParameters, AConditions[I]);
end;

procedure TRLDatabase.BuildConditionSQL(const ASQL: TRLStringStream;
  const AParameters: TRLParameterDefArray; const ACondition: TRLCondition);
var
  lParameter: TRLParameterDef;
  sParameterName: string;
  I: Integer;
begin
  if (ACondition.Index > 0) or
    ((ACondition.Collection is TRLConditions) and
      Assigned(TRLConditions(ACondition.Collection).Parent)) then
    ASQL.WriteString(' ' + IfThen(ACondition.IsOr, 'OR', 'AND') + ' ');
  if ACondition.Conditions.Count > 0 then
    ASQL.WriteString('(');
  if ACondition.IsNot then
    ASQL.WriteString('NOT ');
  if ACondition.Comparison.CaseSensitive then
    ASQL.WriteString(ACondition.FieldName + ' ')
  else
    ASQL.WriteString('UPPER(' + ACondition.FieldName + ') ');
  if ACondition.Comparison.Kind = ckIsNull then
    ASQL.WriteString('IS NULL')
  else
  begin
    sParameterName := 'P' + IntToStr(Succ(AParameters.Count));
    case ACondition.Comparison.Kind of
      ckEqual: ASQL.WriteString('= :' + sParameterName);
      ckGreater: ASQL.WriteString('> :' + sParameterName);
      ckGreaterOrEqual: ASQL.WriteString('>= :' + sParameterName);
      ckLess: ASQL.WriteString('< :' + sParameterName);
      ckLessOrEqual: ASQL.WriteString('<= :' + sParameterName);
      ckContains: ASQL.WriteString('LIKE :' + sParameterName);
      ckBetween: ASQL.WriteString('BETWEEN :' + sParameterName + '_1 AND :' +
        sParameterName + '_2');
      ckIn:
        begin
          ASQL.WriteString('IN (');
          for I := 0 to Pred(ACondition.Comparison.ValueCount) do
            ASQL.WriteString(':' + sParameterName + '_' + IntToStr(Succ(I)) +
              IfThen(I < Pred(ACondition.Comparison.ValueCount), ', '));
          ASQL.WriteString(')');
        end;
    end;

    for I := 0 to Pred(ACondition.Comparison.ValueCount) do
    begin
      lParameter := TRLParameterDef.Create;
      if ACondition.Comparison.ValueCount = 1 then
        lParameter.Name := sParameterName
      else
        lParameter.Name := sParameterName + '_' + IntToStr(Succ(I));
      lParameter.ValueType := ACondition.ValueType;
      lParameter.Value := ACondition.Comparison.Values[I];
      AParameters.Add(lParameter);
    end;
  end;

  if ACondition.Conditions.Count > 0 then
  begin
    BuildConditionsSQL(ASQL, AParameters, ACondition.Conditions);
    ASQL.WriteString(')');
  end;
end;

procedure TRLDatabase.RetrieveSchemas(const ASchemas: TRLSchemas);
var
  lQuery: TRLQuery;
begin
  lQuery := TRLQuery.FindQuery(Provider);
  try
    BuildSchemasSQL(lQuery.SQL);
    lQuery.Open;
    while not lQuery.Eof do
    begin
      FillSchema(ASchemas.Add, lQuery);
      lQuery.Next;
    end;
  finally
    FreeAndNil(lQuery);
  end;
end;

procedure TRLDatabase.RetrieveRelations(const ASchemas: TRLSchemas);
var
  lQuery: TRLQuery;
  lSchema: TRLSchema;
begin
  lQuery := TRLQuery.FindQuery(Provider);
  try
    BuildRelationsSQL(lQuery.SQL);
    lQuery.Open;
    while not lQuery.Eof do
    begin
      lSchema := ASchemas.Find(lQuery['SCHEMA_NAME'].AsString);
      if Assigned(lSchema) then
        FillRelation(lSchema.Relations.Add, lQuery);
      lQuery.Next;
    end;
  finally
    FreeAndNil(lQuery);
  end;
end;

procedure TRLDatabase.RetrieveFields(const ASchemas: TRLSchemas);
var
  lQuery: TRLQuery;
  lSchema: TRLSchema;
  lRelation: TRLRelation;
begin
  lQuery := TRLQuery.FindQuery(Provider);
  try
    BuildFieldsSQL(lQuery.SQL);
    lQuery.Open;
    while not lQuery.Eof do
    begin
      lSchema := ASchemas.Find(lQuery['SCHEMA_NAME'].AsString);
      if Assigned(lSchema) then
      begin
        lRelation := lSchema.Relations.Find(lQuery['RELATION_NAME'].AsString);
        if Assigned(lRelation) then
          FillField(lRelation.Fields.Add, lQuery);
      end;

      lQuery.Next;
    end;
  finally
    FreeAndNil(lQuery);
  end;
end;

procedure TRLDatabase.RetrieveKeys(const ASchemas: TRLSchemas);
var
  lQuery: TRLQuery;
  lSchema: TRLSchema;
  lRelation: TRLRelation;
  lKey: TRLKey;
begin
  lQuery := TRLQuery.FindQuery(Provider);
  try
    BuildKeysSQL(lQuery.SQL);
    lQuery.Open;
    while not lQuery.Eof do
    begin
      lSchema := ASchemas.Find(lQuery['SCHEMA_NAME'].AsString);
      if Assigned(lSchema) then
      begin
        lRelation := lSchema.Relations.Find(lQuery['RELATION_NAME'].AsString);
        if Assigned(lRelation) then
        begin
          lKey := lRelation.Keys.Find(lQuery['KEY_NAME'].AsString);
          if not Assigned(lKey) then
          begin
            lKey := lRelation.Keys.Add;
            FillKey(lKey, lQuery);
          end;

          FillKeyField(lKey.Fields.Add, lQuery);
        end;
      end;

      lQuery.Next;
    end;
  finally
    FreeAndNil(lQuery);
  end;
end;

procedure TRLDatabase.RetrieveIndexes(const ASchemas: TRLSchemas);
var
  lQuery: TRLQuery;
  lSchema: TRLSchema;
  lRelation: TRLRelation;
  lIndex: TRLIndex;
begin
  lQuery := TRLQuery.FindQuery(Provider);
  try
    BuildIndexesSQL(lQuery.SQL);
    lQuery.Open;
    while not lQuery.Eof do
    begin
      lSchema := ASchemas.Find(lQuery['SCHEMA_NAME'].AsString);
      if Assigned(lSchema) then
      begin
        lRelation := lSchema.Relations.Find(lQuery['RELATION_NAME'].AsString);
        if Assigned(lRelation) then
        begin
          lIndex := lRelation.Indexes.Find(lQuery['INDEX_NAME'].AsString);
          if not Assigned(lIndex) then
          begin
            lIndex := lRelation.Indexes.Add;
            FillIndex(lIndex, lQuery);
          end;

          FillIndexField(lIndex.Fields.Add, lQuery);
        end;
      end;

      lQuery.Next;
    end;
  finally
    FreeAndNil(lQuery);
  end;
end;

procedure TRLDatabase.RetrieveReferences(const ASchemas: TRLSchemas);
var
  lQuery: TRLQuery;
  lSchema: TRLSchema;
  lRelation: TRLRelation;
  lReference: TRLReference;
begin
  lQuery := TRLQuery.FindQuery(Provider);
  try
    BuildReferencesSQL(lQuery.SQL);
    lQuery.Open;
    while not lQuery.Eof do
    begin
      lSchema := ASchemas.Find(lQuery['SCHEMA_NAME'].AsString);
      if Assigned(lSchema) then
      begin
        lRelation := lSchema.Relations.Find(lQuery['RELATION_NAME'].AsString);
        if Assigned(lRelation) then
        begin
          lReference := lRelation.References.Find(
            lQuery['REFERENCE_NAME'].AsString);
          if not Assigned(lReference) then
          begin
            lReference := lRelation.References.Add;
            FillReference(lReference, lQuery);
          end;
            
          FillReferenceField(lReference.Fields.Add, lQuery);
        end;
      end;

      lQuery.Next;
    end;
  finally
    FreeAndNil(lQuery);
  end;
end;

procedure TRLDatabase.FillSchema(const ASchema: TRLSchema;
  const AQuery: TRLQuery);
begin
  ASchema.Name := AQuery['SCHEMA_NAME'].AsString;
end;

procedure TRLDatabase.FillRelation(const ARelation: TRLRelation;
  const AQuery: TRLQuery);
begin
  ARelation.Name := AQuery['RELATION_NAME'].AsString;
  ARelation.RelationType := StringToRelationType(
    AQuery['RELATION_TYPE'].AsString);
end;

procedure TRLDatabase.FillField(const AField: TRLField; const AQuery: TRLQuery);
begin
  AField.Name := AQuery['FIELD_NAME'].AsString;
  AField.Position := AQuery['POSITION'].AsInteger;
  AField.DataType := AQuery['DATA_TYPE'].AsString;
  AField.ValueType := GetValueType(AField);
  AField.Length := AQuery['LENGTH'].AsInteger;
  AField.Precision := AQuery['PRECISION'].AsInteger;
  AField.Scale := AQuery['SCALE'].AsInteger;
  AField.AutoIncrement := StrToBool(AQuery['AUTO_INCREMENT'].AsString);
  AField.Required := StrToBool(AQuery['REQUIRED'].AsString);
end;

procedure TRLDatabase.FillKey(const AKey: TRLKey; const AQuery: TRLQuery);
begin
  AKey.Name := AQuery['KEY_NAME'].AsString;
  AKey.KeyType := StringToKeyType(AQuery['KEY_TYPE'].AsString);
  AKey.Enabled := StrToBool(AQuery['ENABLED'].AsString);
end;

procedure TRLDatabase.FillKeyField(const AField: TRLKeyField;
  const AQuery: TRLQuery);
begin
  AField.Name := AQuery['FIELD_NAME'].AsString;
  AField.Position := AQuery['POSITION'].AsInteger;
end;

procedure TRLDatabase.FillIndex(const AIndex: TRLIndex; const AQuery: TRLQuery);
begin
  AIndex.Name := AQuery['INDEX_NAME'].AsString;
  AIndex.IndexType := StringToIndexType(AQuery['INDEX_TYPE'].AsString);
  AIndex.Enabled := StrToBool(AQuery['ENABLED'].AsString);
end;

procedure TRLDatabase.FillIndexField(const AField: TRLIndexField;
  const AQuery: TRLQuery);
begin
  AField.Name := AQuery['FIELD_NAME'].AsString;
  AField.Position := AQuery['POSITION'].AsInteger;
  AField.Ascending := StrToBool(AQuery['ASCENDING'].AsString);
end;

procedure TRLDatabase.FillReference(const AReference: TRLReference;
  const AQuery: TRLQuery);
begin
  AReference.Name := AQuery['REFERENCE_NAME'].AsString;
  AReference.ParentSchemaName := AQuery['PARENT_SCHEMA_NAME'].AsString;
  AReference.ParentTableName := AQuery['PARENT_RELATION_NAME'].AsString;
  AReference.UpdateRule := StringToReferenceRule(
    AQuery['UPDATE_RULE'].AsString);
  AReference.DeleteRule := StringToReferenceRule(
    AQuery['DELETE_RULE'].AsString);
  AReference.Enabled := StrToBool(AQuery['ENABLED'].AsString);
end;

procedure TRLDatabase.FillReferenceField(const AField: TRLReferenceField;
  const AQuery: TRLQuery);
begin
  AField.Name := AQuery['FIELD_NAME'].AsString;
  AField.Position := AQuery['POSITION'].AsInteger;
  AField.ParentFieldName := AQuery['PARENT_FIELD_NAME'].AsString;
end;

function TRLDatabase.GetDateAndTime: TDateTime;
var
  lQuery: TRLQuery;
begin
  CheckProvider;
  try
    lQuery := TRLQuery.FindQuery(Provider);
    try
      BuildDateAndTimeSQL(lQuery.SQL);
      lQuery.Open;
      Result := lQuery['DATE_TIME'].AsDateTime;
    finally
      FreeAndNil(lQuery);
    end;
  except
    on E: Exception do
      raise ERLCouldNotGetDateAndTime.Create('Could not get date and time.' +
        sLineBreak + E.Message);
  end;
end;

function TRLDatabase.GetDate: TDate;
begin
  Result := Trunc(GetDateAndTime);
end;

function TRLDatabase.GetTime: TTime;
begin
  Result := Frac(GetDateAndTime);
end;

function TRLDatabase.GetSchemas: TRLSchemas;
begin
  CheckProvider;
  Result := CreateSchemas;
  try
    RetrieveSchemas(Result);
    RetrieveRelations(Result);
    RetrieveFields(Result);
    RetrieveKeys(Result);
    RetrieveIndexes(Result);
    RetrieveReferences(Result);
  except
    on E: Exception do
    begin
      FreeAndNil(Result);
      raise ERLCouldNotGetSchemas.Create('Could not get schemas.' + sLineBreak +
        E.Message);
    end;
  end;
end;

function TRLDatabase.GetSequenceValue(const ASequenceName: string): Integer;
var
  lQuery: TRLQuery;
begin
  CheckProvider;
  try
    lQuery := TRLQuery.FindQuery(Provider);
    try
      BuildSequenceValueSQL(lQuery.SQL, ASequenceName);
      lQuery.Open;
      Result := lQuery['VALUE'].AsInteger;
    finally
      FreeAndNil(lQuery);
    end;
  except
    on E: Exception do
      raise ERLCouldNotGetSequenceValue.Create('Could not get sequence value.' +
        sLineBreak + E.Message);
  end;
end;

procedure TRLDatabase.BuildSQL(const AQuery: TRLQuery;
  const ASQL: TRLStringList; const AConditions: TRLConditions);
var
  lParameters: TRLParameterDefArray;
begin
  if Assigned(AQuery) and Assigned(ASQL) then
  begin
    lParameters := TRLParameterDefArray.Create;
    try
      AQuery.Close;
      AQuery.SQL.BeginUpdate;
      try
        AQuery.SQL.Clear;
        AQuery.SQL.Add('SELECT T.*');
        AQuery.SQL.Add('FROM (');
        AQuery.SQL.AddStrings(ASQL);
        AQuery.SQL.Add(') T');
        BuildSQLConditions(AQuery.SQL, AConditions, lParameters);
      finally
        AQuery.SQL.EndUpdate;
      end;

      AQuery.FillParameters(lParameters);
    finally
      FreeAndNil(lParameters);
    end;
  end;
end;

procedure TRLDatabase.BuildSQLConditions(const ASQL: TRLSQL;
  const AConditions: TRLConditions; const AParameters: TRLParameterDefArray);
var
  lSQL: TRLStringStream;
begin
  if Assigned(ASQL) and Assigned(AConditions) and Assigned(AParameters) then
  begin
    ASQL.BeginUpdate;
    try
      lSQL := TRLStringStream.Create('');
      try
        BuildConditionsSQL(lSQL, AParameters, AConditions);
        ASQL.Add('WHERE');
        ASQL.Add(lSQL.DataString);
      finally
        FreeAndNil(lSQL);
      end;
    finally
      ASQL.EndUpdate;
    end;
  end;
end;

class function TRLDatabase.GetDatabase(
  const AProvider: TRLProvider): TRLDatabase;
begin
  Result := TRLDatabase(GetDatabaseCommand('Database', AProvider));
end;

class function TRLDatabase.FindDatabase(
  const AProvider: TRLProvider): TRLDatabase;
begin
  Result := GetDatabase(AProvider);
  if not Assigned(Result) then
    raise ERLDatabaseNotFound.Create('Database not found.');
end;

end.
