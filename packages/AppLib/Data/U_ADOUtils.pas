//=============================================================================
// U_ADOUtils
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOUtils;

interface

uses
  ADODB,
  U_BaseType;

type
  // TRLADOUtils
  TRLADOUtils = class(TRLBaseType)
  public
    class function EditConnectionString(const AParentWindow: THandle;
      var AConnectionString: string): Boolean;
  end;

implementation

uses
  ActiveX,
  ComObj,
  OleDB;

{ TRLADOUtils }

class function TRLADOUtils.EditConnectionString(const AParentWindow: THandle;
  var AConnectionString: string): Boolean;
var
  lData: IDataInitialize;
  lPrompt: IDBPromptInitialize;
  lDataSource: IUnknown;
  ptrConnectionString: PWideChar;
begin
//from ADODB.PromptDataSource
  lData := CreateComObject(CLSID_DataLinks) as IDataInitialize;
  if AConnectionString <> '' then
    lData.GetDataSource(nil, CLSCTX_INPROC_SERVER,
      PWideChar(WideString(AConnectionString)), IUnknown, lDataSource);
  lPrompt := CreateComObject(CLSID_DataLinks) as IDBPromptInitialize;
  Result := Succeeded(lPrompt.PromptDataSource(nil, AParentWindow,
    DBPROMPTOPTIONS_PROPERTYSHEET, 0, nil, nil, IUnknown, lDataSource));
  if Result then
  begin
    ptrConnectionString := nil;
    lData.GetInitializationString(lDataSource, True, ptrConnectionString);
    AConnectionString := ptrConnectionString;
  end;
end;

end.
