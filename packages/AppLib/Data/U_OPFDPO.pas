//=============================================================================
// U_OPFDPO
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFDPO;

interface

uses
  SysUtils,
  DB,
  U_Exception,
  U_StringList,
  U_Collection,
  U_Value,
  U_RTTIAttributes,
  U_Parameters,
  U_Query,
  U_OPFCommand,
  U_OPFAttributes,
  U_OPFClasses,
  U_OPFConditions,
  U_OPFSQLEngine;

type
  // Exceptions
  ERLOPFCouldNotInsertRecord = class(ERLException);
  ERLOPFCouldNotUpdateRecord = class(ERLException);
  ERLOPFCouldNotLoadRecordByKey = class(ERLException);
  ERLOPFCouldNotDeleteRecordByKey = class(ERLException);
  ERLOPFCouldNotLoadRecordByConditions = class(ERLException);
  ERLOPFCouldNotLoadRecordsByConditions = class(ERLException);
  ERLOPFCouldNotDeleteRecordsByConditions = class(ERLException);

  // TRLOPFDPO
  TRLOPFDPO = class(TRLOPFCommand)
  private
    FNames: TRLStringList;
    FSortedNames: TRLStringList;
    FSQLEngine: TRLOPFSQLEngine;
    FFreeNames: Boolean;
    FFreeSortedNames: Boolean;
    function GetNames: TRLStringList;
    procedure SetNames(const AValue: TRLStringList);
    function GetSortedNames: TRLStringList;
    procedure SetSortedNames(const AValue: TRLStringList);
  protected
    procedure ProviderChanged; override;
    procedure DatabaseChanged; override;
    procedure ClassMappingChanged; override;
    function CreateNames: TRLStringList; virtual;
    function CreateSortedNames: TRLStringList; virtual;
    function CreateSQLEngine: TRLOPFSQLEngine; virtual;
    function GetItemClass: TRLCollectionItemClass; virtual; abstract;
    function GetCollectionClass: TRLCollectionClass; virtual; abstract;
    procedure InsertRecord(const AInstance: TRLCollectionItem); virtual;
    function UpdateRecord(const AInstance: TRLCollectionItem): Boolean; virtual;
    procedure LoadRecordByKey(const AInstance: TRLCollectionItem); virtual;
    procedure DeleteRecordByKey(const AInstance: TRLCollectionItem); virtual;
    function LoadRecordByConditions(const AConditions: TRLOPFConditions): TRLCollectionItem; virtual;
    function LoadRecordsByConditions(const AConditions: TRLOPFConditions): TRLCollection; virtual;
    procedure DeleteRecordsByConditions(const AConditions: TRLOPFConditions); virtual;
    procedure FillAttributes(const AInstance: TRLCollectionItem; const AClass: TRLOPFClass; const AQuery: TRLQuery); virtual;
    property SQLEngine: TRLOPFSQLEngine read FSQLEngine;
  public
    constructor Create; override;
    destructor Destroy; override;
    property Names: TRLStringList read GetNames write SetNames;
    property SortedNames: TRLStringList read GetSortedNames write SetSortedNames;
  end;

implementation

uses
  U_RTTIContext;

{ TRLOPFDPO }

constructor TRLOPFDPO.Create;
begin
  inherited;
  FSQLEngine := CreateSQLEngine;
end;

destructor TRLOPFDPO.Destroy;
begin
  FreeAndNil(FSQLEngine);
  if Assigned(FSortedNames) and FFreeSortedNames then
    FreeAndNil(FSortedNames);
  if AssigneD(FNames) and FFreeNames then
    FreeAndNil(FNames);
  inherited;
end;

function TRLOPFDPO.GetNames: TRLStringList;
begin
  if not Assigned(FNames) then
  begin
    FNames := CreateNames;
    FFreeNames := True;
  end;

  Result := FNames;
end;

procedure TRLOPFDPO.SetNames(const AValue: TRLStringList);
begin
  if AValue <> FNames then
  begin
    if Assigned(FNames) and FFreeNames then
    begin
      FreeAndNil(FNames);
      FFreeNames := False;
    end;

    FNames := AValue;
  end;
end;

function TRLOPFDPO.GetSortedNames: TRLStringList;
begin
  if not Assigned(FSortedNames) then
  begin
    FSortedNames := CreateSortedNames;
    FFreeSortedNames := True;
  end;

  Result := FSortedNames;
end;

procedure TRLOPFDPO.SetSortedNames(const AValue: TRLStringList);
begin
  if AValue <> FSortedNames then
  begin
    if Assigned(FSortedNames) and FFreeSortedNames then
    begin
      FreeAndNil(FSortedNames);
      FFreeSortedNames := False;
    end;

    FSortedNames := AValue;
  end;
end;

procedure TRLOPFDPO.ProviderChanged;
begin
  inherited;
  FSQLEngine.Provider := Provider;
end;

procedure TRLOPFDPO.DatabaseChanged;
begin
  inherited;
  FSQLEngine.Database := Database;
end;

procedure TRLOPFDPO.ClassMappingChanged;
begin
  inherited;
  FSQLEngine.ClassMapping := ClassMapping;
end;

function TRLOPFDPO.CreateNames: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLOPFDPO.CreateSortedNames: TRLStringList;
begin
  Result := TRLStringList.Create;
end;

function TRLOPFDPO.CreateSQLEngine: TRLOPFSQLEngine;
begin
  Result := TRLOPFSQLEngine.Create;
end;

procedure TRLOPFDPO.InsertRecord(const AInstance: TRLCollectionItem);
var
  lClass: TRLOPFClass;
  lAutoIncrementAttribute: TRLOPFAttribute;
  lQuery: TRLQuery;
  lParameter: TRLParameter;
  lAttribute: TRLRTTIAttribute;
  lValue: TRLValue;
 begin
  CheckProvider;
  CheckClassMapping;
  if Assigned(AInstance) then
    try
      lClass := ClassMapping.Classes.FindByReference(AInstance.ClassType);
      lAutoIncrementAttribute := lClass.Attributes.GetAutoIncrementAttribute;
      lQuery := FSQLEngine.GetInsertQuery(AInstance);
      if Assigned(lQuery) then
        try
          lQuery.Execute;
          if Assigned(lAutoIncrementAttribute) then
          begin
            lAttribute :=
              TRLRTTIContext.Instance.Inspector.GetObjectAttributes(
                AInstance).Find(lAutoIncrementAttribute.Name);
            if Assigned(lAttribute) and Assigned(lAttribute.Reference) and
              lAttribute.Reference.InheritsFrom(TRLValue) then
              lValue := TRLValue(
                TRLRTTIContext.Instance.Inspector.ReadObject(AInstance,
                  lAttribute))
            else
              lValue := nil;
            if Assigned(lValue) then
            begin
              lParameter := lQuery.FindParameter(lAutoIncrementAttribute.Name);
              if Assigned(lParameter) then
                lValue.AsInteger := lParameter.AsInteger;
            end;
          end;
        finally
          FreeAndNil(lQuery);
        end;
    except
      on E: Exception do
        raise ERLOPFCouldNotInsertRecord.Create('Could not insert record.' +
          sLineBreak + E.Message);
    end;
end;

function TRLOPFDPO.UpdateRecord(const AInstance: TRLCollectionItem): Boolean;
var
  lQuery: TRLQuery;
begin
  Result := False;
  CheckProvider;
  CheckClassMapping;
  if Assigned(AInstance) then
    try
      lQuery := FSQLEngine.GetUpdateQuery(AInstance);
      if Assigned(lQuery) then
        try
          lQuery.Execute;
          Result := True;
        finally
          FreeAndNil(lQuery);
        end;
    except
      on E: Exception do
        raise ERLOPFCouldNotUpdateRecord.Create('Could not update record.' +
          sLineBreak + E.Message);
    end;
end;

procedure TRLOPFDPO.LoadRecordByKey(const AInstance: TRLCollectionItem);
var
  lClass: TRLOPFClass;
  lQuery: TRLQuery;
begin
  CheckProvider;
  CheckClassMapping;
  try
    lClass := ClassMapping.Classes.FindByReference(AInstance.ClassType);
    lQuery := FSQLEngine.GetSelectQueryByKey(AInstance, Names);
    if Assigned(lQuery) then
      try
        lQuery.Open;
        if not lQuery.IsEmpty then
        begin
          AInstance.BeginUpdate;
          try
            FillAttributes(AInstance, lClass, lQuery);
          finally
            AInstance.EndUpdate;
          end;
        end;
      finally
        FreeAndNil(lQuery);
      end;
  except
    on E: Exception do
      raise ERLOPFCouldNotLoadRecordByKey.Create(
        'Could not load record by key.' + sLineBreak + E.Message);
  end;
end;

procedure TRLOPFDPO.DeleteRecordByKey(const AInstance: TRLCollectionItem);
var
  lQuery: TRLQuery;
begin
  CheckProvider;
  CheckClassMapping;
  try
    lQuery := FSQLEngine.GetDeleteQueryByKey(AInstance);
    if Assigned(lQuery) then
      try
        lQuery.Execute;
      finally
        FreeAndNil(lQuery);
      end;
  except
    on E: Exception do
      raise ERLOPFCouldNotDeleteRecordByKey.Create(
        'Could not delete record by key.' + sLineBreak + E.Message);
  end;
end;

function TRLOPFDPO.LoadRecordByConditions(
  const AConditions: TRLOPFConditions): TRLCollectionItem;
var
  lClass: TRLOPFClass;
  lQuery: TRLQuery;
begin
  Result := nil;
  CheckProvider;
  CheckClassMapping;
  try
    lClass := ClassMapping.Classes.FindByReference(GetItemClass);
    lQuery := FSQLEngine.GetSelectQueryByConditions(GetItemClass, Names, nil,
      AConditions);
    if Assigned(lQuery) then
      try
        lQuery.Open;
        if not lQuery.IsEmpty then
        begin
          Result := GetItemClass.CreateItem;
          FillAttributes(Result, lClass, lQuery);
        end;
      finally
        FreeAndNil(lQuery);
      end;
  except
    on E: Exception do
      raise ERLOPFCouldNotLoadRecordByConditions.Create(
        'Could not load record by conditions.' + sLineBreak + E.Message);
  end;
end;

function TRLOPFDPO.LoadRecordsByConditions(
  const AConditions: TRLOPFConditions): TRLCollection;
var
  lClass: TRLOPFClass;
  lQuery: TRLQuery;
  lItem: TRLCollectionItem;
begin
  Result := nil;
  CheckProvider;
  CheckClassMapping;
  try
    lClass := ClassMapping.Classes.FindByReference(GetItemClass);
    lQuery := FSQLEngine.GetSelectQueryByConditions(GetItemClass, Names,
      SortedNames, AConditions);
    if Assigned(lQuery) then
      try
        lQuery.Open;
        lQuery.DisableControls;
        try
          Result := GetCollectionClass.Create;
          while not lQuery.Eof do
          begin
            lItem := TRLCollectionItem(Result.Add);
            lItem.BeginUpdate;
            try
              FillAttributes(lItem, lClass, lQuery);
            finally
              lItem.EndUpdate;
            end;

            lQuery.Next;                        
          end;
        finally
          lQuery.EnableControls;
        end;
      finally
        FreeAndNil(lQuery);
      end;
  except
    on E: Exception do
    begin
      if Assigned(Result) then
        FreeAndNil(Result);
      raise ERLOPFCouldNotLoadRecordsByConditions.Create(
        'Could not load records by conditions.' + sLineBreak + E.Message);
    end;
  end;
end;

procedure TRLOPFDPO.DeleteRecordsByConditions(
  const AConditions: TRLOPFConditions);
var
  lQuery: TRLQuery;
begin
  CheckProvider;
  CheckClassMapping;
  try
    lQuery := FSQLEngine.GetDeleteQueryByConditions(GetItemClass, AConditions);
    if Assigned(lQuery) then
      try
        lQuery.Execute;
      finally
        FreeAndNil(lQuery);
      end;
  except
    on E: Exception do
      raise ERLOPFCouldNotDeleteRecordsByConditions.Create(
        'Could not delete records by conditions.' + sLineBreak + E.Message);
  end;
end;

procedure TRLOPFDPO.FillAttributes(const AInstance: TRLCollectionItem;
  const AClass: TRLOPFClass; const AQuery: TRLQuery);
var
  lAttributes: TRLRTTIAttributes;
  lAttribute: TRLRTTIAttribute;
  lMappingAttribute: TRLOPFAttribute;
  lValue: TRLValue;
  I: Integer;
begin
  lAttributes := TRLRTTIContext.Instance.Inspector.GetAttributes(
    AInstance.ClassType);
  for I := 0 to Pred(AQuery.Fields.Count) do
    if not AQuery.Fields[I].IsNull then
    begin
      lMappingAttribute := AClass.Attributes.FindFieldName(
        AQuery.Fields[I].FieldName);
      if Assigned(lMappingAttribute) then
      begin
        lAttribute := lAttributes.Find(lMappingAttribute.Name);
        if Assigned(lAttribute) and Assigned(lAttribute.Reference) and
          lAttribute.Reference.InheritsFrom(TRLValue) then
        begin
          lValue := TRLValue(
            TRLRTTIContext.Instance.Inspector.ReadObject(AInstance,
              lAttribute));
          if Assigned(lValue) then
            case lValue.ValueType of
              vtString: lValue.InitializeString(AQuery.Fields[I].AsString);
              vtInteger: lValue.InitializeInteger(AQuery.Fields[I].AsInteger);
              vtInt64:
                if AQuery.Fields[I] is TLargeintField then
                  lValue.InitializeInt64(
                    TLargeintField(AQuery.Fields[I]).AsLargeInt)
                else
                  lValue.InitializeInt64(AQuery.Fields[I].AsInteger);
              vtFloat: lValue.InitializeFloat(AQuery.Fields[I].AsFloat);
              vtCurrency: lValue.InitializeCurrency(
                AQuery.Fields[I].AsCurrency);
              vtDate: lValue.InitializeDate(AQuery.Fields[I].AsDateTime);
              vtTime: lValue.InitializeTime(AQuery.Fields[I].AsDateTime);
              vtDateTime: lValue.InitializeDateTime(
                AQuery.Fields[I].AsDateTime);
              vtBoolean: lValue.InitializeBoolean(AQuery.Fields[I].AsBoolean);
              vtBinary:
                begin
                  { TODO -oRafael : Implementar. }
                end;
            end;
        end;
      end;
    end;
end;

end.
