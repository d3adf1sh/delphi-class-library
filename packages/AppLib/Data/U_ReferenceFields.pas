//=============================================================================
// U_ReferenceFields
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ReferenceFields;

interface

uses
  Classes,
  U_Exception,
  U_NamedCollection;

type
  // Exceptions
  ERLReferenceFieldNotFound = class(ERLException);

  // TRLReferenceField
  TRLReferenceField = class(TRLNamedCollectionItem)
  private
    FPosition: Integer;
    FParentFieldName: string;
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Position: Integer read FPosition write FPosition;
    property ParentFieldName: string read FParentFieldName write FParentFieldName;
  end;

  // TRLReferenceFields
  TRLReferenceFields = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLReferenceField;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLReferenceField;
    function Insert(const AIndex: Integer): TRLReferenceField;
    function Find(const AName: string): TRLReferenceField;
    function FindByName(const AName: string): TRLReferenceField;
    property Items[const AIndex: Integer]: TRLReferenceField read GetItem; default;
  end;

implementation

{ TRLReferenceField }

procedure TRLReferenceField.Assign(Source: TPersistent);
var
  lField: TRLReferenceField;
begin
  inherited;
  if Source is TRLReferenceField then
  begin
    lField := TRLReferenceField(Source);
    Position := lField.Position;
    ParentFieldName := lField.ParentFieldName;
  end;
end;

{ TRLReferenceFields }

constructor TRLReferenceFields.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLReferenceFields.GetItem(const AIndex: Integer): TRLReferenceField;
begin
  Result := TRLReferenceField(inherited Items[AIndex]);
end;

function TRLReferenceFields.GetItemClass: TCollectionItemClass;
begin
  Result := TRLReferenceField;
end;

function TRLReferenceFields.Add: TRLReferenceField;
begin
  Result := TRLReferenceField(inherited Add);
end;

function TRLReferenceFields.Insert(const AIndex: Integer): TRLReferenceField;
begin
  Result := TRLReferenceField(inherited Insert(AIndex));
end;

function TRLReferenceFields.Find(const AName: string): TRLReferenceField;
begin
  Result := TRLReferenceField(inherited Find(AName));
end;

function TRLReferenceFields.FindByName(const AName: string): TRLReferenceField;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLReferenceFieldNotFound.CreateFmt('ReferenceField "%s" not found.',
      [AName]);
end;

end.
