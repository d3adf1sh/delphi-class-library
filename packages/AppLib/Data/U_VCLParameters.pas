//=============================================================================
// U_VCLParameters
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLParameters;

{$I RL.INC}

interface

uses
  FMTBCD,
  Classes,
  DB,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_Parameters;

type
  // TRLVCLParameter
  TRLVCLParameter = class(TRLParameter)
  private
    function GetReference: TParam;
    procedure SetReference(const AValue: TParam);
  protected
    procedure KeyChanged; override;
    function GetModifier: TRLParameterModifier; override;
    function GetDataType: TFieldType; override;
    function GetSize: Integer; override;
    function GetPrecision: Integer; override;
    function GetScale: Integer; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsCardinal: Cardinal; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    function GetAsFMTBCD: TBcd; override;
    function GetAsDate: TDate; override;
    function GetAsTime: TTime; override;
    function GetAsDateTime: TDateTime; override;
    function GetAsBoolean: Boolean; override;
    function GetAsVariant: Variant; override;
    procedure SetModifier(const AValue: TRLParameterModifier); override;
    procedure SetDataType(const AValue: TFieldType); override;
    procedure SetSize(const AValue: Integer); override;
    procedure SetPrecision(const AValue: Integer); override;
    procedure SetScale(const AValue: Integer); override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsCardinal(const AValue: Cardinal); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
    procedure SetAsFMTBCD(const AValue: TBcd); override;
    procedure SetAsDate(const AValue: TDate); override;
    procedure SetAsTime(const AValue: TTime); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
    procedure SetAsBoolean(const AValue: Boolean); override;
    procedure SetAsVariant(const AValue: Variant); override;
  public
   	function IsNull: Boolean; override;
    procedure Clear; override;
    property Reference: TParam read GetReference write SetReference;
  end;

  // TRLVCLParameters
  TRLVCLParameters = class(TRLParameters)
  private
    function GetReference: TParams;
    procedure SetReference(const AValue: TParams);
  protected
    function GetItemClass: TCollectionItemClass; override;
    function InsertParameter(const AIndex: Integer): TPersistent; override;
  public
    procedure Refresh; override;
    property Reference: TParams read GetReference write SetReference;
  end;

implementation

{ TRLVCLParameter }

function TRLVCLParameter.GetReference: TParam;
begin
  Result := TParam(inherited Reference);
end;

procedure TRLVCLParameter.SetReference(const AValue: TParam);
begin
  inherited Reference := AValue;
end;

procedure TRLVCLParameter.KeyChanged;
begin
  if Assigned(Reference) then
    Reference.Name := Name;
end;

function TRLVCLParameter.GetModifier: TRLParameterModifier;
begin
  if Assigned(Reference) then
    case Reference.ParamType of
      ptUnknown: Result := pmUnknown;
      ptInput: Result := pmInput;
      ptOutput: Result := pmOutput;
      ptInputOutput: Result := pmInputOutput;
      ptResult: Result := pmResult;
    else
      Result := pmUnknown;
    end
  else
    Result := pmUnknown;
end;

function TRLVCLParameter.GetDataType: TFieldType;
begin
  if Assigned(Reference) then
    Result := Reference.DataType
  else
    Result := ftUnknown;
end;

function TRLVCLParameter.GetSize: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.Size
  else
    Result := 0;
end;

function TRLVCLParameter.GetPrecision: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.Precision
  else
    Result := 0;
end;

function TRLVCLParameter.GetScale: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.NumericScale
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsString: string;
begin
  if not IsNull then
    Result := Reference.AsString
  else
    Result := '';
end;

function TRLVCLParameter.GetAsInteger: Integer;
begin
  if not IsNull then
    Result := Reference.AsInteger
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsCardinal: Cardinal;
begin
  if not IsNull then
    Result := Reference.AsInteger
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsInt64: Int64;
begin
  if not IsNull then
    Result := Reference.AsInteger
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsFloat: Double;
begin
  if not IsNull then
    Result := Reference.AsFloat
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsCurrency: Currency;
begin
  if not IsNull then
    Result := Reference.AsCurrency
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsFMTBCD: TBcd;
begin
  if not IsNull then
    Result := Reference.AsFMTBCD
  else
    Result := IntegerToBcd(0);
end;

function TRLVCLParameter.GetAsDate: TDate;
begin
  if not IsNull then
    Result := Reference.AsDate
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsTime: TTime;
begin
  if not IsNull then
    Result := Reference.AsTime
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsDateTime: TDateTime;
begin
  if not IsNull then
    Result := Reference.AsDateTime
  else
    Result := 0;
end;

function TRLVCLParameter.GetAsBoolean: Boolean;
begin
  if not IsNull then
    Result := Reference.AsBoolean
  else
    Result := False;
end;

function TRLVCLParameter.GetAsVariant: Variant;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

procedure TRLVCLParameter.SetModifier(const AValue: TRLParameterModifier);
begin
  if Assigned(Reference) then
    case AValue of
      pmUnknown: Reference.ParamType := ptUnknown;
      pmInput: Reference.ParamType := ptInput;
      pmOutput: Reference.ParamType := ptOutput;
      pmInputOutput: Reference.ParamType := ptInputOutput;
      pmResult: Reference.ParamType := ptResult;
    end;
end;

procedure TRLVCLParameter.SetDataType(const AValue: TFieldType);
begin
  if Assigned(Reference) then
    Reference.DataType := AValue;
end;

procedure TRLVCLParameter.SetSize(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Size := AValue;
end;

procedure TRLVCLParameter.SetPrecision(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Precision := AValue;
end;

procedure TRLVCLParameter.SetScale(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.NumericScale := AValue;
end;

procedure TRLVCLParameter.SetAsString(const AValue: string);
begin
  if Assigned(Reference) then
    Reference.AsString := AValue;
end;

procedure TRLVCLParameter.SetAsInteger(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.AsInteger := AValue;
end;

procedure TRLVCLParameter.SetAsCardinal(const AValue: Cardinal);
begin
  if Assigned(Reference) then
    Reference.AsInteger := AValue;
end;

procedure TRLVCLParameter.SetAsInt64(const AValue: Int64);
begin
  if Assigned(Reference) then
    Reference.AsInteger := AValue;
end;

procedure TRLVCLParameter.SetAsFloat(const AValue: Double);
begin
  if Assigned(Reference) then
    Reference.AsFloat := AValue;
end;

procedure TRLVCLParameter.SetAsCurrency(const AValue: Currency);
begin
  if Assigned(Reference) then
    Reference.AsCurrency := AValue;
end;

procedure TRLVCLParameter.SetAsFMTBCD(const AValue: TBcd);
begin
  if Assigned(Reference) then
    Reference.AsBCD := VarFMTBcdCreate(AValue);
end;

procedure TRLVCLParameter.SetAsDate(const AValue: TDate);
begin
  if Assigned(Reference) then
    Reference.AsDate := AValue;
end;

procedure TRLVCLParameter.SetAsTime(const AValue: TTime);
begin
  if Assigned(Reference) then
    Reference.AsTime := AValue;
end;

procedure TRLVCLParameter.SetAsDateTime(const AValue: TDateTime);
begin
  if Assigned(Reference) then
    Reference.AsDateTime := AValue;
end;

procedure TRLVCLParameter.SetAsBoolean(const AValue: Boolean);
begin
  if Assigned(Reference) then
    Reference.AsBoolean := AValue;
end;

procedure TRLVCLParameter.SetAsVariant(const AValue: Variant);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

function TRLVCLParameter.IsNull: Boolean;
begin
  if Assigned(Reference) then
    Result := Reference.IsNull
  else
    Result := True;
end;

procedure TRLVCLParameter.Clear;
begin
  if Assigned(Reference) then
    Reference.Clear;
end;

{ TRLVCLParameters }

function TRLVCLParameters.GetReference: TParams;
begin
  Result := TParams(inherited Reference);
end;

procedure TRLVCLParameters.SetReference(const AValue: TParams);
begin
  inherited Reference := AValue;
end;

function TRLVCLParameters.GetItemClass: TCollectionItemClass;
begin
  Result := TRLVCLParameter;
end;

function TRLVCLParameters.InsertParameter(const AIndex: Integer): TPersistent;
begin
  if Assigned(Reference) then
    Result := TParam(Reference.Insert(AIndex))
  else
    Result := nil;
end;

procedure TRLVCLParameters.Refresh;
var
  lParameter: TRLVCLParameter;
  I: Integer;
begin
  if Assigned(Reference) then
  begin
    ClearReferences;
    BeginUpdate;
    try
      Clear;
      for I := 0 to Pred(Reference.Count) do
      begin
        lParameter := TRLVCLParameter(TCollection(Self).Add);
        lParameter.Name := Reference[I].Name;
        lParameter.Reference := Reference[I];
      end;
    finally
      EndUpdate;
    end;
  end;
end;

end.
