//=============================================================================
// U_OPFRelationships
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFRelationships;

{ TODO -oRafael : Regra de atualiza��o/exclus�o. }

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_StringList,
  U_Collection,
  U_NamedCollection,
  U_OPFRelationshipBindings;

type
  // Enumerations
  TRLOPFRelationshipCardinality = (rcOneToOne, rcOneToMany);

  // Exceptions
  ERLOPFRelatedClassNameIsEmpty = class(ERLException);
  ERLOPFBindingListIsEmpty = class(ERLException);
  ERLOPFRelationshipNotFound = class(ERLException);

  // TRLOPFRelationship
  TRLOPFRelationship = class(TRLNamedCollectionItem)
  private
    FRelatedClassName: string;
    FCardinality: TRLOPFRelationshipCardinality;
    FBindings: TRLOPFRelationshipBindings;
  protected
    function CreateBindings: TRLOPFRelationshipBindings; virtual;
    procedure CheckRelatedClassName; virtual;
    procedure CheckBindingCount; virtual;
    procedure CheckBindings; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Validate; override;
    property RelatedClassName: string read FRelatedClassName write FRelatedClassName;
    property Cardinality: TRLOPFRelationshipCardinality read FCardinality write FCardinality;
    property Bindings: TRLOPFRelationshipBindings read FBindings;
  end;

  // TRLOPFRelationshipArray
  TRLOPFRelationshipArray = class(TRLNamedCollectionItemArray)
  private
    function GetItem(const AIndex: Integer): TRLOPFRelationship;
    procedure SetItem(const AIndex: Integer; const AValue: TRLOPFRelationship);
  public
    procedure Add(const AValue: TRLOPFRelationship);
    procedure Insert(const AIndex: Integer; const AValue: TRLOPFRelationship);
    property Items[const AIndex: Integer]: TRLOPFRelationship read GetItem write SetItem; default;
  end;

  // TRLOPFRelationships
  TRLOPFRelationships = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLOPFRelationship;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLOPFRelationship;
    function Insert(const AIndex: Integer): TRLOPFRelationship;
    function Find(const AName: string): TRLOPFRelationship;
    function FindByName(const AName: string): TRLOPFRelationship;
    function ToArray: TRLOPFRelationshipArray; overload;
    function ToArray(const ANames: TRLStringList): TRLOPFRelationshipArray; overload;
    function ToArray(const ACardinality: TRLOPFRelationshipCardinality): TRLOPFRelationshipArray; overload;
    property Items[const AIndex: Integer]: TRLOPFRelationship read GetItem; default;
  end;

// Utils
function RelationshipCardinalityToString(const AValue: TRLOPFRelationshipCardinality): string;
function StringToRelationshipCardinality(const AValue: string): TRLOPFRelationshipCardinality;

implementation

uses
  TypInfo;

// Utils
function RelationshipCardinalityToString(
  const AValue: TRLOPFRelationshipCardinality): string;
begin
  Result := GetEnumName(TypeInfo(TRLOPFRelationshipCardinality), Ord(AValue));
end;

function StringToRelationshipCardinality(
  const AValue: string): TRLOPFRelationshipCardinality;
begin
  Result := TRLOPFRelationshipCardinality(
    GetEnumValue(TypeInfo(TRLOPFRelationshipCardinality), AValue));
end;

{ TRLOPFRelationship }

constructor TRLOPFRelationship.Create(Collection: TCollection);
begin
  inherited;
  FBindings := CreateBindings;
end;

destructor TRLOPFRelationship.Destroy;
begin
  FreeAndNil(FBindings);
  inherited;
end;

function TRLOPFRelationship.CreateBindings: TRLOPFRelationshipBindings;
begin
  Result := TRLOPFRelationshipBindings.Create;
end;

procedure TRLOPFRelationship.CheckRelatedClassName;
begin
  if FRelatedClassName = '' then
    raise ERLOPFRelatedClassNameIsEmpty.Create('RelatedClassName is empty.');
end;

procedure TRLOPFRelationship.CheckBindingCount;
begin
  if FBindings.Count = 0 then
    raise ERLOPFBindingListIsEmpty.Create('Binding list is empty.');
end;

procedure TRLOPFRelationship.CheckBindings;
begin
  FBindings.Validate;
end;

procedure TRLOPFRelationship.Validate;
begin
  inherited;
  CheckRelatedClassName;
  CheckBindingCount;
  CheckBindings;
end;

{ TRLOPFRelationshipArray }

function TRLOPFRelationshipArray.GetItem(
  const AIndex: Integer): TRLOPFRelationship;
begin
  Result := TRLOPFRelationship(inherited Items[AIndex]);
end;

procedure TRLOPFRelationshipArray.SetItem(
  const AIndex: Integer; const AValue: TRLOPFRelationship);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLOPFRelationshipArray.Add(const AValue: TRLOPFRelationship);
begin
  inherited Add(AValue);
end;

procedure TRLOPFRelationshipArray.Insert(const AIndex: Integer;
  const AValue: TRLOPFRelationship);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLOPFRelationships }       

constructor TRLOPFRelationships.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
  CaseSensitive := False;
end;

function TRLOPFRelationships.GetItem(const AIndex: Integer): TRLOPFRelationship;
begin
  Result := TRLOPFRelationship(inherited Items[AIndex]);
end;

function TRLOPFRelationships.GetItemClass: TCollectionItemClass;
begin
  Result := TRLOPFRelationship;
end;

function TRLOPFRelationships.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLOPFRelationshipArray;
end;

function TRLOPFRelationships.Add: TRLOPFRelationship;
begin
  Result := TRLOPFRelationship(inherited Add);
end;

function TRLOPFRelationships.Insert(const AIndex: Integer): TRLOPFRelationship;
begin
  Result := TRLOPFRelationship(inherited Insert(AIndex));
end;

function TRLOPFRelationships.Find(const AName: string): TRLOPFRelationship;
begin
  Result := TRLOPFRelationship(inherited Find(AName));
end;

function TRLOPFRelationships.FindByName(
  const AName: string): TRLOPFRelationship;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLOPFRelationshipNotFound.CreateFmt('Relationship "%s" not found.',
      [AName]);
end;

function TRLOPFRelationships.ToArray: TRLOPFRelationshipArray;
begin
  Result := TRLOPFRelationshipArray(inherited ToArray);
end;

function TRLOPFRelationships.ToArray(
  const ANames: TRLStringList): TRLOPFRelationshipArray;
begin
  Result := TRLOPFRelationshipArray(inherited ToArray(ANames));
end;

function TRLOPFRelationships.ToArray(
  const ACardinality: TRLOPFRelationshipCardinality): TRLOPFRelationshipArray;
var
  I: Integer;
begin
  Result := TRLOPFRelationshipArray.Create;
  for I := 0 to Pred(Count) do
    if Items[I].Cardinality = ACardinality then
      Result.Add(Items[I]);
end;

end.
