//=============================================================================
// U_Keys
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Keys;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_StringList,
  U_Collection,
  U_DBObjects,
  U_KeyFields;

type
  // Enumerations
  TRLKeyType = (ktPrimary, ktUnique);

  // Exceptions
  ERLKeyNotFound = class(ERLException);

  // TRLKey
  TRLKey = class(TRLDBObject)
  private
    FFields: TRLKeyFields;
    FKeyType: TRLKeyType;
    FEnabled: Boolean;
    procedure SetKeyType(const AValue: TRLKeyType);
  protected
    function CreateFields: TRLKeyFields; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Fields: TRLKeyFields read FFields;
    property KeyType: TRLKeyType read FKeyType write SetKeyType;
    property Enabled: Boolean read FEnabled write FEnabled;
  end;

  // TRLKeyArray
  TRLKeyArray = class(TRLDBObjectArray)
  private
    function GetItem(const AIndex: Integer): TRLKey;
    procedure SetItem(const AIndex: Integer; const AValue: TRLKey);
  public
    procedure Add(const AValue: TRLKey);
    procedure Insert(const AIndex: Integer; const AValue: TRLKey);
    property Items[const AIndex: Integer]: TRLKey read GetItem write SetItem; default;
  end;

  // TRLKeys
  TRLKeys = class(TRLDBObjects)
  private
    FPrimaryKey: TRLKey;
    FUniqueKey: TRLKey;
    function GetItem(const AIndex: Integer): TRLKey;
    function GetPrimaryKey: TRLKey;
    function GetUniqueKey: TRLKey;
  protected
    procedure Update(Item: TCollectionItem); override;
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLKey;
    function Insert(const AIndex: Integer): TRLKey;
    function Find(const AName: string): TRLKey;
    function FindByName(const AName: string): TRLKey;
    function ToArray: TRLKeyArray; overload;
    function ToArray(const ANames: TRLStringList): TRLKeyArray; overload;
    function ToArray(const AKeyType: TRLKeyType): TRLKeyArray; overload;
    property Items[const AIndex: Integer]: TRLKey read GetItem; default;
    property PrimaryKey: TRLKey read GetPrimaryKey;
    property UniqueKey: TRLKey read GetUniqueKey;
  end;

// Utils
function KeyTypeToString(const AValue: TRLKeyType): string;
function StringToKeyType(const AValue: string): TRLKeyType;

implementation

uses
  TypInfo;

// Utils
function KeyTypeToString(const AValue: TRLKeyType): string;
begin
  Result := GetEnumName(TypeInfo(TRLKeyType), Ord(AValue));
end;

function StringToKeyType(const AValue: string): TRLKeyType;
begin
  Result := TRLKeyType(GetEnumValue(TypeInfo(TRLKeyType), AValue));
end;

{ TRLKey }

constructor TRLKey.Create(Collection: TCollection);
begin
  inherited;
  FFields := CreateFields;
end;

destructor TRLKey.Destroy;
begin
  FreeAndNil(FFields);
  inherited;
end;

procedure TRLKey.SetKeyType(const AValue: TRLKeyType);
begin
  if AValue <> FKeyType then
  begin
    FKeyType := AValue;
    Changed(False);
  end;
end;

function TRLKey.CreateFields: TRLKeyFields;
begin
  Result := TRLKeyFields.CreateParented(Self);
end;

procedure TRLKey.Assign(Source: TPersistent);
var
  lKey: TRLKey;
begin
  inherited;
  if Source is TRLKey then
  begin
    lKey := TRLKey(Source);
    Fields.Assign(lKey.Fields);
    KeyType := lKey.KeyType;
    Enabled := lKey.Enabled;
  end;
end;

{ TRLKeyArray }

function TRLKeyArray.GetItem(const AIndex: Integer): TRLKey;
begin
  Result := TRLKey(inherited Items[AIndex]);
end;

procedure TRLKeyArray.SetItem(const AIndex: Integer; const AValue: TRLKey);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLKeyArray.Add(const AValue: TRLKey);
begin
  inherited Add(AValue);
end;

procedure TRLKeyArray.Insert(const AIndex: Integer; const AValue: TRLKey);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLKeys }

constructor TRLKeys.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLKeys.GetItem(const AIndex: Integer): TRLKey;
begin
  Result := TRLKey(inherited Items[AIndex]);
end;

function TRLKeys.GetPrimaryKey: TRLKey;
var
  lKeys: TRLKeyArray;
begin
  if not Assigned(FPrimaryKey) then
  begin
    lKeys := ToArray(ktPrimary);
    try
      if lKeys.Count > 0 then
        FPrimaryKey := lKeys[0];
    finally
      FreeAndNil(lKeys);
    end;
  end;

  Result := FPrimaryKey;
end;

function TRLKeys.GetUniqueKey: TRLKey;
var
  lKeys: TRLKeyArray;
begin
  if not Assigned(FUniqueKey) then
  begin
    lKeys := ToArray(ktUnique);
    try
      if lKeys.Count > 0 then
        FUniqueKey := lKeys[0];
    finally
      FreeAndNil(lKeys);
    end;
  end;

  Result := FUniqueKey;
end;

procedure TRLKeys.Update(Item: TCollectionItem);
begin
  inherited;
  FPrimaryKey := nil;
  FUniqueKey := nil;  
end;

function TRLKeys.GetItemClass: TCollectionItemClass;
begin
  Result := TRLKey;
end;

function TRLKeys.Add: TRLKey;
begin
  Result := TRLKey(inherited Add);
end;

function TRLKeys.Insert(const AIndex: Integer): TRLKey;
begin
  Result := TRLKey(inherited Insert(AIndex));
end;

function TRLKeys.Find(const AName: string): TRLKey;
begin
  Result := TRLKey(inherited Find(AName));
end;

function TRLKeys.FindByName(const AName: string): TRLKey;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLKeyNotFound.CreateFmt('Key "%s" not found.', [AName]);
end;

function TRLKeys.ToArray: TRLKeyArray;
begin
  Result := TRLKeyArray(inherited ToArray);
end;

function TRLKeys.ToArray(const ANames: TRLStringList): TRLKeyArray;
begin
  Result := TRLKeyArray(inherited ToArray(ANames));
end;

function TRLKeys.ToArray(const AKeyType: TRLKeyType): TRLKeyArray;
var
  I: Integer;
begin
  Result := TRLKeyArray.Create;
  for I := 0 to Pred(Count) do
    if Items[I].KeyType = AKeyType then
      Result.Add(Items[I]);
end;

end.
