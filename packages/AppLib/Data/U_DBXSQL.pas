//=============================================================================
// U_DBXSQL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXSQL;

interface

uses
  U_VCLSQL;

type
  // TRLDBXSQL
  TRLDBXSQL = class(TRLVCLSQL)
  end;

implementation

end.
