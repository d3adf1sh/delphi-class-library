//=============================================================================
// U_Parameters
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Parameters;

{$I RL.INC}

interface

uses
  SysUtils,
  FMTBCD,
  Classes,
  DB,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_BaseType,
  U_ArrayType,
  U_Exception,
  U_NamedCollection,
  U_Value;

type
  // Enumerations
  TRLParameterModifier = (pmUnknown, pmInput, pmOutput, pmInputOutput,
    pmResult);

  // Exceptions
  ERLParameterNotFound = class(ERLException);

  // TRLParameterDef
  TRLParameterDef = class(TRLBaseType)
  private
    FName: string;
    FFieldName: string;
    FValueType: TRLValueType;
    FValue: TRLValue;
  public
    property Name: string read FName write FName;
    property FieldName: string read FFieldName write FFieldName;
    property ValueType: TRLValueType read FValueType write FValueType;
    property Value: TRLValue read FValue write FValue;
  end;

  // TRLParameterDefArray
  TRLParameterDefArray = class(TRLArrayType)
  private
    function GetItem(const AIndex: Integer): TRLParameterDef;
    procedure SetItem(const AIndex: Integer; const AValue: TRLParameterDef);
  public
    constructor Create; override;
    procedure Add(const AValue: TRLParameterDef);
    procedure Insert(const AIndex: Integer; const AValue: TRLParameterDef);
    property Items[const AIndex: Integer]: TRLParameterDef read GetItem write SetItem; default;
  end;

  // TRLParameter
  TRLParameter = class(TRLNamedCollectionItem)
  private
    FReference: TPersistent;
  protected
    function GetModifier: TRLParameterModifier; virtual; abstract;
    function GetDataType: TFieldType; virtual; abstract;
    function GetSize: Integer; virtual; abstract;
    function GetPrecision: Integer; virtual; abstract;
    function GetScale: Integer; virtual; abstract;
    function GetAsString: string; virtual; abstract;
    function GetAsInteger: Integer; virtual; abstract;
    function GetAsCardinal: Cardinal; virtual; abstract;
    function GetAsInt64: Int64; virtual; abstract;
    function GetAsFloat: Double; virtual; abstract;
    function GetAsCurrency: Currency; virtual; abstract;
    function GetAsFMTBCD: TBcd; virtual; abstract;
    function GetAsDate: TDate; virtual; abstract;
    function GetAsTime: TTime; virtual; abstract;
    function GetAsDateTime: TDateTime; virtual; abstract;
    function GetAsBoolean: Boolean; virtual; abstract;
    function GetAsVariant: Variant; virtual; abstract;
    procedure SetModifier(const AValue: TRLParameterModifier); virtual; abstract;
    procedure SetDataType(const AValue: TFieldType); virtual; abstract;
    procedure SetSize(const AValue: Integer); virtual; abstract;
    procedure SetPrecision(const AValue: Integer); virtual; abstract;
    procedure SetScale(const AValue: Integer); virtual; abstract;
    procedure SetAsString(const AValue: string); virtual; abstract;
    procedure SetAsInteger(const AValue: Integer); virtual; abstract;
    procedure SetAsCardinal(const AValue: Cardinal); virtual; abstract;
    procedure SetAsInt64(const AValue: Int64); virtual; abstract;
    procedure SetAsFloat(const AValue: Double); virtual; abstract;
    procedure SetAsCurrency(const AValue: Currency); virtual; abstract;
    procedure SetAsFMTBCD(const AValue: TBcd); virtual; abstract;
    procedure SetAsDate(const AValue: TDate); virtual; abstract;
    procedure SetAsTime(const AValue: TTime); virtual; abstract;
    procedure SetAsDateTime(const AValue: TDateTime); virtual; abstract;
    procedure SetAsBoolean(const AValue: Boolean); virtual; abstract;
    procedure SetAsVariant(const AValue: Variant); virtual; abstract;
  public
    destructor Destroy; override;
    procedure LoadFromFile(const AFileName: string; const ABlobType: TBlobType); virtual;
    procedure SaveToFile(const AFileName: string); virtual;
    procedure LoadFromStream(const AStream: TStream; const ABlobType: TBlobType); virtual;
    procedure SaveToStream(const AStream: TStream); virtual;
   	function IsNull: Boolean; virtual; abstract;
    procedure Clear; virtual; abstract;
    property Reference: TPersistent read FReference write FReference;
    property Modifier: TRLParameterModifier read GetModifier write SetModifier;
    property DataType: TFieldType read GetDataType write SetDataType;
    property Size: Integer read GetSize write SetSize;
    property Precision: Integer read GetPrecision write SetPrecision;
    property Scale: Integer read GetScale write SetScale;
    property AsString: string read GetAsString write SetAsString;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    property AsCardinal: Cardinal read GetAsCardinal write SetAsCardinal;
    property AsInt64: Int64 read GetAsInt64 write SetAsInt64;
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property AsCurrency: Currency read GetAsCurrency write SetAsCurrency;
    property AsFMTBCD: TBcd read GetAsFMTBCD write SetAsFMTBCD;
    property AsDate: TDate read GetAsDate write SetAsDate;
    property AsTime: TTime read GetAsTime write SetAsTime;
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    property AsVariant: Variant read GetAsVariant write SetAsVariant;
  end;

  // TRLParameters
  TRLParameters = class(TRLNamedCollection)
  private
    FReference: TPersistent;
    function GetItem(const AIndex: Integer): TRLParameter;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function InsertParameter(const AIndex: Integer): TPersistent; virtual; abstract;
    procedure ClearReferences; virtual;
  public
    destructor Destroy; override;
    function Add: TRLParameter;
    function Insert(const AIndex: Integer): TRLParameter;
    function Find(const AName: string): TRLParameter;
    function FindByName(const AName: string): TRLParameter;
    procedure Refresh; virtual; abstract;
    procedure Fill(const AParameters: TRLParameterDefArray); virtual;
    property Items[const AIndex: Integer]: TRLParameter read GetItem; default;
    property Reference: TPersistent read FReference write FReference;
  end;

// Utils
function ValueTypeToFieldType(const AValue: TRLValueType): TFieldType;
function FieldTypeToValueType(const AValue: TFieldType): TRLValueType;

implementation

uses
  Variants,
  U_System;

// Utils
function ValueTypeToFieldType(const AValue: TRLValueType): TFieldType;
begin
  case AValue of
    vtString: Result := ftString;
    vtInteger: Result := ftInteger;
    vtInt64: Result := ftLargeint;
    vtFloat: Result := ftFloat;
    vtCurrency: Result := ftCurrency;
    vtDate: Result := ftDate;
    vtTime: Result := ftTime;
    vtDateTime: Result := ftDateTime;
    vtBoolean: Result := ftBoolean;
    vtBinary: Result := ftBlob;
  else
    Result := ftString;
  end;
end;

function FieldTypeToValueType(const AValue: TFieldType): TRLValueType;
begin
  case AValue of
    ftString: Result := vtString;
    ftInteger: Result := vtInteger;
    ftLargeint: Result := vtInt64;
    ftFloat: Result := vtFloat;
    ftCurrency: Result := vtCurrency;
    ftDate: Result := vtDate;
    ftTime: Result := vtTime;
    ftDateTime: Result := vtDateTime;
    ftBoolean: Result := vtBoolean;
    ftBlob: Result := vtBinary;
  else
    Result := vtString;
  end;
end;

{ TRLParameterDefArray }

constructor TRLParameterDefArray.Create;
begin
  inherited;
  FreeValues := True;
end;

function TRLParameterDefArray.GetItem(const AIndex: Integer): TRLParameterDef;
begin
  Result := TRLParameterDef(inherited Items[AIndex]);
end;

procedure TRLParameterDefArray.SetItem(const AIndex: Integer;
  const AValue: TRLParameterDef);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLParameterDefArray.Add(const AValue: TRLParameterDef);
begin
  inherited Add(AValue);
end;

procedure TRLParameterDefArray.Insert(const AIndex: Integer;
  const AValue: TRLParameterDef);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLParameter }

destructor TRLParameter.Destroy;
begin
  if FReference is TCollectionItem then
    TCollectionItem(FReference).Free;
  inherited;
end;

procedure TRLParameter.LoadFromFile(const AFileName: string;
  const ABlobType: TBlobType);
var
  lStream: TFileStream;
begin
  if FileExists(AFileName) then
  begin
    lStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyWrite);
    try
      LoadFromStream(lStream, ABlobType);
    finally
      FreeAndNil(lStream);
    end;
  end;
end;

procedure TRLParameter.SaveToFile(const AFileName: string);
var
  lStream: TFileStream;
begin
  lStream := TFileStream.Create(AFileName, fmCreate);
  try
    SaveToStream(lStream);
  finally
    FreeAndNil(lStream);
  end;
end;

procedure TRLParameter.LoadFromStream(const AStream: TStream;
  const ABlobType: TBlobType);
begin
  if Assigned(AStream) and (AStream.Size > 0) then
  begin
    DataType := ABlobType;
    AsVariant := U_System.GetStreamData(AStream);
  end
  else
    AsVariant := Null;
end;

procedure TRLParameter.SaveToStream(const AStream: TStream);
begin
  if Assigned(AStream) then
    U_System.SetStreamData(AStream, AsVariant);
end;

{ TRLParameters }

destructor TRLParameters.Destroy;
begin
  ClearReferences;
  inherited;
end;

function TRLParameters.GetItem(const AIndex: Integer): TRLParameter;
begin
  Result := TRLParameter(inherited Items[AIndex]);
end;

function TRLParameters.GetItemClass: TCollectionItemClass;
begin
  Result := TRLParameter;
end;

procedure TRLParameters.ClearReferences;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Reference := nil;
end;

function TRLParameters.Add: TRLParameter;
begin
  Result := Insert(Count);
end;

function TRLParameters.Insert(const AIndex: Integer): TRLParameter;
begin
  Result := TRLParameter(inherited Insert(AIndex));
  Result.Reference := InsertParameter(AIndex);
end;

function TRLParameters.Find(const AName: string): TRLParameter;
begin
  Result := TRLParameter(inherited Find(AName));
end;

function TRLParameters.FindByName(const AName: string): TRLParameter;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLParameterNotFound.CreateFmt('Parameter "%s" not found.', [AName]);
end;

procedure TRLParameters.Fill(const AParameters: TRLParameterDefArray);
var
  lParameter: TRLParameter;
  I: Integer;
begin
  if Assigned(AParameters) then
    for I := 0 to Pred(AParameters.Count) do
    begin
      lParameter := Find(AParameters[I].Name);
      if Assigned(lParameter) then
      begin
        if Assigned(AParameters[I].Value) and
          not AParameters[I].Value.IsNull then
          case AParameters[I].ValueType of
            vtString: lParameter.AsString := AParameters[I].Value.AsString;
            vtInteger: lParameter.AsInteger := AParameters[I].Value.AsInteger;
            vtInt64: lParameter.AsInt64 := AParameters[I].Value.AsInt64;
            vtFloat: lParameter.AsFloat := AParameters[I].Value.AsFloat;
            vtCurrency: lParameter.AsCurrency :=
              AParameters[I].Value.AsCurrency;
            vtDate: lParameter.AsDate := AParameters[I].Value.AsDate;
            vtTime: lParameter.AsTime := AParameters[I].Value.AsTime;
            vtDateTime: lParameter.AsDateTime :=
              AParameters[I].Value.AsDateTime;
            vtBoolean: lParameter.AsBoolean := AParameters[I].Value.AsBoolean;
            vtBinary:
              begin
                { TODO -oRafael : Implementar. }
              end;
          end
        else
          lParameter.DataType := ValueTypeToFieldType(AParameters[I].ValueType);
      end;
    end;
end;

end.
