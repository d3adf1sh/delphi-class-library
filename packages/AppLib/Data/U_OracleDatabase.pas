//=============================================================================
// U_OracleDatabase
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OracleDatabase;

interface

uses
  SysUtils,
  Classes,
  U_Value,
  U_Database,
  U_SQL,
  U_Fields;

type
  // TRLOracleDatabase
  TRLOracleDatabase = class(TRLDatabase)
  protected
    function GetValueType(const AField: TRLField): TRLValueType; override;
    procedure BuildDateAndTimeSQL(const ASQL: TRLSQL); override;
    procedure BuildSchemasSQL(const ASQL: TRLSQL); override;
    procedure BuildRelationsSQL(const ASQL: TRLSQL); override;
    procedure BuildFieldsSQL(const ASQL: TRLSQL); override;
    procedure BuildKeysSQL(const ASQL: TRLSQL); override;
    procedure BuildIndexesSQL(const ASQL: TRLSQL); override;
    procedure BuildReferencesSQL(const ASQL: TRLSQL); override;
  end;

const
  RLOracle = 'Oracle';

implementation

uses
  StrUtils;

{ TRLOracleDatabase }

function TRLOracleDatabase.GetValueType(const AField: TRLField): TRLValueType;
var
  sDataType: string;
begin
  sDataType := UpperCase(AField.DataType);
  if (sDataType = 'CHAR') or
    (sDataType = 'NCHAR') or
    (sDataType = 'VARCHAR2') or
    (sDataType = 'NVARCHAR2') or
    (sDataType = 'ROWID') or
    (sDataType = 'UROWID') then
    Result := vtString
  else if (sDataType = 'NUMBER') and (AField.Precision = 0) then
    Result := vtInteger
  else if ((sDataType = 'NUMBER') and (AField.Precision > 0)) or
    (sDataType = 'FLOAT') or
    (sDataType = 'BINARY_FLOAT') or
    (sDataType = 'BINARY_DOUBLE') then
    Result := vtFloat
  else if sDataType = 'DATE' then
    Result := vtDate
  else if sDataType = 'TIMESTAMP' then
    Result := vtDateTime
  else if (sDataType = 'BLOB') or
    (sDataType = 'CLOB') or
    (sDataType = 'NLOB') or
    (sDataType = 'RAW') or
    (sDataType = 'LONG') or
    (sDataType = 'LONG RAW') then
    Result := vtBinary
  else
    Result := inherited GetValueType(AField);
end;

procedure TRLOracleDatabase.BuildDateAndTimeSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('SELECT SYSDATE');
    ASQL.Add('  FROM DUAL');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOracleDatabase.BuildSchemasSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('SELECT USER SCHEMA_NAME');
    ASQL.Add('  FROM DUAL');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOracleDatabase.BuildRelationsSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('  SELECT USER SCHEMA_NAME');
    ASQL.Add('       , O.OBJECT_NAME RELATION_NAME');
    ASQL.Add('       , CASE O.OBJECT_TYPE');
    ASQL.Add('           WHEN ''TABLE'' THEN ''rtTable''');
    ASQL.Add('           WHEN ''VIEW'' THEN ''rtView''');
    ASQL.Add('         END RELATION_TYPE');
    ASQL.Add('    FROM USER_OBJECTS O');
    ASQL.Add('   WHERE O.OBJECT_TYPE IN (''TABLE'', ''VIEW'')');
    ASQL.Add('     AND NOT O.OBJECT_NAME LIKE ''BIN$%''');
    ASQL.Add('ORDER BY O.OBJECT_NAME');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOracleDatabase.BuildFieldsSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT USER SCHEMA_NAME');
    ASQL.Add('         , O.OBJECT_NAME RELATION_NAME');
    ASQL.Add('         , TC.COLUMN_NAME FIELD_NAME');
    ASQL.Add('         , TC.COLUMN_ID - 1 POSITION');
    ASQL.Add('         , CASE TC.DATA_TYPE');
    ASQL.Add('             WHEN ''TIMESTAMP('' || TO_CHAR(TC.DATA_SCALE) || '')'' THEN ''TIMESTAMP''');
    ASQL.Add('             WHEN ''TIMESTAMP('' || TO_CHAR(TC.DATA_SCALE) || '') WITH TIME ZONE'' THEN ''TIMESTAMP WITH TIME ZONE''');
    ASQL.Add('             WHEN ''TIMESTAMP('' || TO_CHAR(TC.DATA_SCALE) || '') WITH LOCAL TIME ZONE'' THEN ''TIMESTAMP WITH LOCAL TIME ZONE''');
    ASQL.Add('             WHEN ''INTERVAL YEAR('' || TO_CHAR(TC.DATA_PRECISION) || '') TO MONTH'' THEN ''INTERVAL YEAR TO MONTH''');
    ASQL.Add('             WHEN ''INTERVAL DAY('' || TO_CHAR(TC.DATA_PRECISION) || '') TO SECOND('' || TO_CHAR(TC.DATA_SCALE) || '')'' THEN ''INTERVAL DAY TO SECOND''');
    ASQL.Add('             ELSE TC.DATA_TYPE');
    ASQL.Add('           END DATA_TYPE');
    ASQL.Add('         , TC.CHAR_LENGTH LENGTH');
    ASQL.Add('         , COALESCE(TC.DATA_PRECISION, 0) PRECISION');
    ASQL.Add('         , CASE TC.DATA_TYPE');
    ASQL.Add('             WHEN ''FLOAT'' THEN 0');
    ASQL.Add('             ELSE COALESCE(TC.DATA_PRECISION, 0)');
    ASQL.Add('           END PRECISION');
    ASQL.Add('         , COALESCE(TC.DATA_SCALE, 0) SCALE');
    ASQL.Add('         , ''False'' AUTO_INCREMENT');
    ASQL.Add('         , CASE TC.NULLABLE');
    ASQL.Add('             WHEN ''N'' THEN ''True''');
    ASQL.Add('             WHEN ''Y'' THEN ''False''');
    ASQL.Add('           END REQUIRED');
    ASQL.Add('      FROM USER_OBJECTS O');
    ASQL.Add('INNER JOIN USER_TAB_COLUMNS TC ON TC.TABLE_NAME = O.OBJECT_NAME');
    ASQL.Add('     WHERE O.OBJECT_TYPE IN (''TABLE'', ''VIEW'')');
    ASQL.Add('       AND NOT O.OBJECT_NAME LIKE ''BIN$%''');
    ASQL.Add('  ORDER BY O.OBJECT_NAME');
    ASQL.Add('         , TC.COLUMN_ID');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOracleDatabase.BuildKeysSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT C.OWNER SCHEMA_NAME');
    ASQL.Add('         , C.TABLE_NAME RELATION_NAME');
    ASQL.Add('         , C.CONSTRAINT_NAME KEY_NAME');
    ASQL.Add('         , CASE C.CONSTRAINT_TYPE');
    ASQL.Add('             WHEN ''P'' THEN ''ktPrimary''');
    ASQL.Add('             WHEN ''U'' THEN ''ktUnique''');
    ASQL.Add('           END KEY_TYPE');
    ASQL.Add('         , CC.COLUMN_NAME FIELD_NAME');
    ASQL.Add('         , CC.POSITION - 1 POSITION');
    ASQL.Add('         , CASE C.STATUS');
    ASQL.Add('             WHEN ''ENABLED'' THEN ''True''');
    ASQL.Add('             WHEN ''DISABLED'' THEN ''False''');
    ASQL.Add('           END ENABLED');
    ASQL.Add('      FROM USER_CONSTRAINTS C');
    ASQL.Add('INNER JOIN USER_TABLES T ON T.TABLE_NAME = C.TABLE_NAME');
    ASQL.Add('INNER JOIN USER_CONS_COLUMNS CC ON CC.CONSTRAINT_NAME = C.CONSTRAINT_NAME');
    ASQL.Add('     WHERE C.CONSTRAINT_TYPE IN (''P'', ''U'')');
    ASQL.Add('  ORDER BY C.OWNER');
    ASQL.Add('         , C.TABLE_NAME');
    ASQL.Add('         , C.CONSTRAINT_NAME');
    ASQL.Add('         , CC.POSITION');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOracleDatabase.BuildIndexesSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT I.TABLE_OWNER SCHEMA_NAME');
    ASQL.Add('         , I.TABLE_NAME RELATION_NAME');
    ASQL.Add('         , I.INDEX_NAME INDEX_NAME');
    ASQL.Add('         , CASE I.UNIQUENESS');
    ASQL.Add('             WHEN ''NONUNIQUE'' THEN ''itIndex''');
    ASQL.Add('             WHEN ''UNIQUE'' THEN ''itUniqueIndex''');
    ASQL.Add('           END INDEX_TYPE');
    ASQL.Add('         , IC.COLUMN_NAME FIELD_NAME');
    ASQL.Add('         , IC.COLUMN_POSITION - 1 POSITION');
    ASQL.Add('         , CASE IC.DESCEND');
    ASQL.Add('             WHEN ''ASC'' THEN ''True''');
    ASQL.Add('             WHEN ''DESC'' THEN ''False''');
    ASQL.Add('           END ASCENDING');
    ASQL.Add('         , CASE I.STATUS');
    ASQL.Add('             WHEN ''VALID'' THEN ''True''');
    ASQL.Add('             WHEN ''UNUSABLE'' THEN ''False''');
    ASQL.Add('           END ENABLED');
    ASQL.Add('      FROM USER_INDEXES I');
    ASQL.Add('INNER JOIN USER_IND_COLUMNS IC ON IC.INDEX_NAME = I.INDEX_NAME');
    ASQL.Add(' LEFT JOIN USER_CONSTRAINTS C ON C.OWNER = I.TABLE_OWNER AND C.TABLE_NAME = I.TABLE_NAME AND C.CONSTRAINT_NAME = I.INDEX_NAME');
    ASQL.Add('     WHERE C.CONSTRAINT_NAME IS NULL');
    ASQL.Add('  ORDER BY I.TABLE_OWNER');
    ASQL.Add('         , I.TABLE_NAME');
    ASQL.Add('         , I.INDEX_NAME');
    ASQL.Add('         , IC.COLUMN_POSITION');
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOracleDatabase.BuildReferencesSQL(const ASQL: TRLSQL);
begin
  ASQL.BeginUpdate;
  try
    ASQL.Add('    SELECT C.OWNER SCHEMA_NAME');
    ASQL.Add('         , C.TABLE_NAME RELATION_NAME');
    ASQL.Add('         , C.CONSTRAINT_NAME REFERENCE_NAME');
    ASQL.Add('         , CC.COLUMN_NAME FIELD_NAME');
    ASQL.Add('         , CC.POSITION - 1 POSITION');
    ASQL.Add('         , C.R_OWNER PARENT_SCHEMA_NAME');
    ASQL.Add('         , CP.TABLE_NAME PARENT_RELATION_NAME');
    ASQL.Add('         , CCP.COLUMN_NAME PARENT_FIELD_NAME');
    ASQL.Add('         , ''rrNoAction'' UPDATE_RULE');
    ASQL.Add('         , CASE C.DELETE_RULE');
    ASQL.Add('             WHEN ''NO ACTION'' THEN ''rrNoAction''');
    ASQL.Add('             WHEN ''CASCADE'' THEN ''rrCascade''');
    ASQL.Add('             WHEN ''SET NULL'' THEN ''rrSetNull''');
    ASQL.Add('           END DELETE_RULE');
    ASQL.Add('         , CASE C.STATUS');
    ASQL.Add('             WHEN ''ENABLED'' THEN ''True''');
    ASQL.Add('             WHEN ''DISABLED'' THEN ''False''');
    ASQL.Add('           END ENABLED');
    ASQL.Add('      FROM USER_CONSTRAINTS C');
    ASQL.Add('INNER JOIN USER_TABLES T ON T.TABLE_NAME = C.TABLE_NAME');
    ASQL.Add('INNER JOIN USER_CONS_COLUMNS CC ON CC.CONSTRAINT_NAME = C.CONSTRAINT_NAME');
    ASQL.Add('INNER JOIN USER_CONSTRAINTS CP ON CP.CONSTRAINT_NAME = C.R_CONSTRAINT_NAME');
    ASQL.Add('INNER JOIN USER_CONS_COLUMNS CCP ON CCP.CONSTRAINT_NAME = CP.CONSTRAINT_NAME AND CCP.POSITION = CC.POSITION');
    ASQL.Add('     WHERE C.CONSTRAINT_TYPE = ''R''');
    ASQL.Add('  ORDER BY C.OWNER');
    ASQL.Add('         , C.TABLE_NAME');
    ASQL.Add('         , C.CONSTRAINT_NAME');
    ASQL.Add('         , CC.POSITION');
  finally
    ASQL.EndUpdate;
  end;
end;

initialization
  RegisterClass(TRLOracleDatabase);

end.
