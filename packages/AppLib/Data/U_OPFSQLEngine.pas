//=============================================================================
// U_OPFSQLEngine
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFSQLEngine;

interface

uses
  SysUtils,
  Classes,
  DB,
  U_StringList,
  U_Value,
  U_RTTIAttributes,
  U_SQL,
  U_Parameters,
  U_Query,
  U_MSSQLDatabase,
  U_OPFCommand,
  U_OPFAttributes,
  U_OPFClasses,
  U_OPFConditions;

type
  // TRLOPFSQLEngine
  TRLOPFSQLEngine = class(TRLOPFCommand)
  protected
    function GetAttributes(const AInstance: TPersistent; const AClass: TRLOPFClass; const AInsertion: Boolean): TRLParameterDefArray; virtual;
    procedure AddFieldsByMapping(const ASQL: TRLSQL; const AClass: TRLOPFClass); virtual;
    procedure AddFieldsByNames(const ASQL: TRLSQL; const AClass: TRLOPFClass; const ANames: TRLStringList); virtual;
    procedure AddFieldsByAttributes(const ASQL: TRLSQL; AAttributes: TRLOPFAttributeArray); virtual;
    procedure AddWhereByKey(const ASQL: TRLSQL; const AInstance: TPersistent; const AClass: TRLOPFClass; const AParameters: TRLParameterDefArray); virtual;
  public
    function GetInsertQuery(const AInstance: TPersistent): TRLQuery; virtual;
    function GetUpdateQuery(const AInstance: TPersistent): TRLQuery; virtual;
    function GetSelectQueryByKey(const AInstance: TPersistent; const ANames: TRLStringList): TRLQuery; virtual;
    function GetDeleteQueryByKey(const AInstance: TPersistent): TRLQuery; virtual;
    function GetSelectQueryByConditions(const AReference: TClass; const ANames, ASortedNames: TRLStringList; const AConditions: TRLOPFConditions): TRLQuery; virtual;
    function GetDeleteQueryByConditions(const AReference: TClass; const AConditions: TRLOPFConditions): TRLQuery; virtual;
  end;

implementation

uses
  StrUtils,
  U_RTTIContext;

{ TRLOPFSQLEngine }

function TRLOPFSQLEngine.GetAttributes(const AInstance: TPersistent;
  const AClass: TRLOPFClass; const AInsertion: Boolean): TRLParameterDefArray;
var
  lAttributes: TRLRTTIAttributes;
  lAttribute: TRLOPFAttribute;
  lParameter: TRLParameterDef;
  lValue: TRLValue;
  I: Integer;
begin
  Result := TRLParameterDefArray.Create;
  lAttributes := TRLRTTIContext.Instance.Inspector.GetAttributes(
    AInstance.ClassType);
  for I := 0 to Pred(lAttributes.Count) do
    if Assigned(lAttributes[I].Reference) and
      lAttributes[I].Reference.InheritsFrom(TRLValue) then
    begin
      lValue := TRLValue(
        TRLRTTIContext.Instance.Inspector.ReadObject(AInstance,
          lAttributes[I]));
      if Assigned(lValue) and lValue.Modified and
        (not lValue.IsNull or not AInsertion) then
      begin
        lAttribute := AClass.Attributes.Find(lAttributes[I].Name);
        if Assigned(lAttribute) and (not lAttribute.IsKey or AInsertion) and
          not lAttribute.AutoIncrement then
        begin
          lParameter := TRLParameterDef.Create;
          lParameter.Name := lAttribute.Name;
          lParameter.FieldName := lAttribute.FieldName;
          lParameter.ValueType := lAttribute.ValueType;
          lParameter.Value := lValue;
          Result.Add(lParameter);
        end;
      end;
    end;
end;

procedure TRLOPFSQLEngine.AddFieldsByMapping(const ASQL: TRLSQL;
  const AClass: TRLOPFClass);
var
  lAttributes: TRLOPFAttributeArray;
begin
  lAttributes := AClass.Attributes.ToArray;
  try
    AddFieldsByAttributes(ASQL, lAttributes);
  finally
    FreeAndNil(lAttributes);
  end;
end;

procedure TRLOPFSQLEngine.AddFieldsByNames(const ASQL: TRLSQL;
  const AClass: TRLOPFClass; const ANames: TRLStringList);
var
  lAttributes: TRLOPFAttributeArray;
begin
  lAttributes := AClass.Attributes.ToArray(ANames);
  try
    AddFieldsByAttributes(ASQL, lAttributes);
  finally
    FreeAndNil(lAttributes);
  end;
end;

procedure TRLOPFSQLEngine.AddFieldsByAttributes(const ASQL: TRLSQL;
  AAttributes: TRLOPFAttributeArray);
var
  I: Integer;
begin
  ASQL.BeginUpdate;
  try
    for I := 0 to Pred(AAttributes.Count) do
      ASQL.Add(AAttributes[I].FieldName +
        IfThen(I < Pred(AAttributes.Count), ','));
  finally
    ASQL.EndUpdate;
  end;
end;

procedure TRLOPFSQLEngine.AddWhereByKey(const ASQL: TRLSQL;
  const AInstance: TPersistent; const AClass: TRLOPFClass;
  const AParameters: TRLParameterDefArray);
var
  lAttributes: TRLRTTIAttributes;
  lAttribute: TRLRTTIAttribute;
  lKeyAttributes: TRLOPFAttributeArray;
  lParameter: TRLParameterDef;
  lValue: TRLValue;
  I: Integer;
begin
  lAttributes := TRLRTTIContext.Instance.Inspector.GetAttributes(
    AInstance.ClassType);
  ASQL.BeginUpdate;
  try
    lKeyAttributes := AClass.Attributes.GetKeyAttributes;
    try
      for I := 0 to Pred(lKeyAttributes.Count) do
      begin
        ASQL.Add(lKeyAttributes[I].FieldName + ' = :' + lKeyAttributes[I].Name +
          IfThen(I < Pred(lKeyAttributes.Count), ' AND'));
        lAttribute := lAttributes.Find(lKeyAttributes[I].Name);
        if Assigned(lAttribute) and Assigned(lAttribute.Reference) and
          lAttribute.Reference.InheritsFrom(TRLValue) then
          lValue := TRLValue(
            TRLRTTIContext.Instance.Inspector.ReadObject(AInstance, lAttribute))
        else
          lValue := nil;
        lParameter := TRLParameterDef.Create;
        lParameter.Name := lKeyAttributes[I].Name;
        lParameter.FieldName := lKeyAttributes[I].FieldName;
        lParameter.ValueType := lKeyAttributes[I].ValueType;
        lParameter.Value := lValue;
        AParameters.Add(lParameter);
      end;
    finally
      FreeAndNil(lKeyAttributes);
    end;
  finally
    ASQL.EndUpdate;
  end;
end;

function TRLOPFSQLEngine.GetInsertQuery(const AInstance: TPersistent): TRLQuery;
var
  lClass: TRLOPFClass;
  lAutoIncrementAttribute: TRLOPFAttribute;
  lParameters: TRLParameterDefArray;
  lParameter: TRLParameter;
  I: Integer;
begin
  CheckProvider;
  CheckClassMapping;
  if Assigned(AInstance) then
  begin
    lClass := ClassMapping.Classes.FindByReference(AInstance.ClassType);
    lAutoIncrementAttribute := lClass.Attributes.GetAutoIncrementAttribute;
    lParameters := GetAttributes(AInstance, lClass, True);
    try
      Result := TRLQuery.FindQuery(Provider);
      try
        Result.SQL.BeginUpdate;
        try
          Result.SQL.Add('INSERT INTO ' + lClass.FullRelationName + ' (');
          for I := 0 to Pred(lParameters.Count) do
            Result.SQL.Add(lParameters[I].FieldName +
              IfThen(I < Pred(lParameters.Count), ','));
          Result.SQL.Add(')');
          Result.SQL.Add('VALUES (');
          for I := 0 to Pred(lParameters.Count) do
            Result.SQL.Add(':' + lParameters[I].Name +
              IfThen(I < Pred(lParameters.Count), ','));
          Result.SQL.Add(');');
          if Assigned(lAutoIncrementAttribute) and
            (Database is TRLMSSQLDatabase) then
            Result.SQL.Add('SET :' + lAutoIncrementAttribute.Name +
              ' = SCOPE_IDENTITY();');
        finally
          Result.SQL.EndUpdate;
        end;

        if Assigned(lAutoIncrementAttribute) and
          (Database is TRLMSSQLDatabase) then
        begin
          lParameter := Result.ParameterByName(lAutoIncrementAttribute.Name);
          lParameter.DataType := ftInteger;
          lParameter.Modifier := pmOutput;
        end;

        Result.FillParameters(lParameters);
      except
        FreeAndNil(Result);
        raise;
      end;
    finally
      FreeAndNil(lParameters);
    end;
  end
  else
    Result := nil;
end;

function TRLOPFSQLEngine.GetUpdateQuery(const AInstance: TPersistent): TRLQuery;
var
  lClass: TRLOPFClass;
  lParameters: TRLParameterDefArray;
  lKeyParameters: TRLParameterDefArray;
  I: Integer;
begin
  Result := nil;
  CheckProvider;
  CheckClassMapping;
  if Assigned(AInstance) then
  begin
    lClass := ClassMapping.Classes.FindByReference(AInstance.ClassType);
    lParameters := GetAttributes(AInstance, lClass, False);
    try
      if lParameters.Count > 0 then
      begin
        lKeyParameters := TRLParameterDefArray.Create;
        try
          Result := TRLQuery.FindQuery(Provider);
          Result.SQL.BeginUpdate;
          try
            Result.SQL.Add('UPDATE ' + lClass.FullRelationName);
            Result.SQL.Add('SET');
            for I := 0 to Pred(lParameters.Count) do
              Result.SQL.Add(lParameters[I].FieldName + ' = :' +
                lParameters[I].Name + IfThen(I < Pred(lParameters.Count), ','));
            Result.SQL.Add('WHERE');
            AddWhereByKey(Result.SQL, AInstance, lClass, lKeyParameters);
          finally
            Result.SQL.EndUpdate;
          end;

          Result.FillParameters(lParameters);
          Result.FillParameters(lKeyParameters);
        finally
          FreeAndNil(lKeyParameters);
        end;
      end;
    finally
      FreeAndNil(lParameters);
    end;
  end;
end;

function TRLOPFSQLEngine.GetSelectQueryByKey(const AInstance: TPersistent;
  const ANames: TRLStringList): TRLQuery;
var
  lClass: TRLOPFClass;
  lKeyParameters: TRLParameterDefArray;
begin
  CheckProvider;
  CheckClassMapping;
  if Assigned(AInstance) then
  begin
    lClass := ClassMapping.Classes.FindByReference(AInstance.ClassType);
    lKeyParameters := TRLParameterDefArray.Create;
    try
      Result := TRLQuery.FindQuery(Provider);
      Result.SQL.BeginUpdate;
      try
        Result.SQL.Add('SELECT');
        if Assigned(ANames) and (ANames.Count > 0) then
          AddFieldsByNames(Result.SQL, lClass, ANames)
        else
          AddFieldsByMapping(Result.SQL, lClass);
        Result.SQL.Add('FROM ' + lClass.FullRelationName);
        Result.SQL.Add('WHERE');
        AddWhereByKey(Result.SQL, AInstance, lClass, lKeyParameters);
      finally
        Result.SQL.EndUpdate;
      end;

      Result.FillParameters(lKeyParameters);
    finally
      FreeAndNil(lKeyParameters);
    end;
  end
  else
    Result := nil;
end;

function TRLOPFSQLEngine.GetDeleteQueryByKey(
  const AInstance: TPersistent): TRLQuery;
var
  lClass: TRLOPFClass;
  lKeyParameters: TRLParameterDefArray;
begin
  CheckProvider;
  CheckClassMapping;
  if Assigned(AInstance) then
  begin
    lClass := ClassMapping.Classes.FindByReference(AInstance.ClassType);
    lKeyParameters := TRLParameterDefArray.Create;
    try
      Result := TRLQuery.FindQuery(Provider);
      Result.SQL.BeginUpdate;
      try
        Result.SQL.Add('DELETE ' + lClass.FullRelationName);
        Result.SQL.Add('WHERE');
        AddWhereByKey(Result.SQL, AInstance, lClass, lKeyParameters);
      finally
        Result.SQL.EndUpdate;
      end;

      Result.FillParameters(lKeyParameters);
    finally
      FreeAndNil(lKeyParameters);
    end;
  end
  else
    Result := nil;
end;

function TRLOPFSQLEngine.GetSelectQueryByConditions(const AReference: TClass;
  const ANames, ASortedNames: TRLStringList;
  const AConditions: TRLOPFConditions): TRLQuery;
var
  lClass: TRLOPFClass;
  lParameters: TRLParameterDefArray;
begin
  CheckProvider;
  CheckClassMapping;
  if Assigned(AReference) then
  begin
    lClass := ClassMapping.Classes.FindByReference(AReference);
    if Assigned(AConditions) then
    begin
      if not Assigned(AConditions.Attributes) then
        AConditions.Attributes := lClass.Attributes; 
      AConditions.Validate;
      lParameters := TRLParameterDefArray.Create;
    end
    else
      lParameters := nil;
    try
      Result := TRLQuery.FindQuery(Provider);
      Result.SQL.BeginUpdate;
      try
        Result.SQL.Add('SELECT');
        if Assigned(ANames) and (ANames.Count > 0) then
          AddFieldsByNames(Result.SQL, lClass, ANames)
        else
          AddFieldsByMapping(Result.SQL, lClass);
        Result.SQL.Add('FROM ' + lClass.FullRelationName);
        if Assigned(lParameters) then
          Database.BuildSQLConditions(Result.SQL, AConditions, lParameters);
        if Assigned(ASortedNames) and (ASortedNames.Count > 0) then
        begin
          Result.SQL.Add('ORDER BY');
          AddFieldsByNames(Result.SQL, lClass, ASortedNames);
        end;
      finally
        Result.SQL.EndUpdate;
      end;

      if Assigned(lParameters) then
        Result.FillParameters(lParameters);
    finally
      if Assigned(lParameters) then
        FreeAndNil(lParameters);
    end;
  end
  else
    Result := nil;
end;

function TRLOPFSQLEngine.GetDeleteQueryByConditions(const AReference: TClass;
  const AConditions: TRLOPFConditions): TRLQuery;
var
  lClass: TRLOPFClass;
  lParameters: TRLParameterDefArray;
begin
  CheckProvider;
  CheckClassMapping;
  if Assigned(AReference) then
  begin
    lClass := ClassMapping.Classes.FindByReference(AReference);
    if Assigned(AConditions) then
    begin
      if not Assigned(AConditions.Attributes) then
        AConditions.Attributes := lClass.Attributes; 
      AConditions.Validate;
      lParameters := TRLParameterDefArray.Create;
    end
    else
      lParameters := nil;
    try
      Result := TRLQuery.FindQuery(Provider);
      Result.SQL.BeginUpdate;
      try
        Result.SQL.Add('DELETE ' + lClass.FullRelationName);
        if Assigned(lParameters) then
          Database.BuildSQLConditions(Result.SQL, AConditions, lParameters);
      finally
        Result.SQL.EndUpdate;
      end;

      if Assigned(lParameters) then
        Result.FillParameters(lParameters);
    finally
      if Assigned(lParameters) then
        FreeAndNil(lParameters);
    end;
  end
  else
    Result := nil;
end;

end.
