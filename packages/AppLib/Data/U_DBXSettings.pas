//=============================================================================
// U_DBXSettings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXSettings;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_Settings;

const
  CConnectionParameters = 'ConnectionParameters';
  
type
  // Exceptions
  ERLDriverNameIsEmpty = class(ERLException);
  ERLDriverFunctionIsEmpty = class(ERLException);
  ERLLibraryNameIsEmpty = class(ERLException);
  ERLVendorLibraryIsEmpty = class(ERLException);
  ERLConnectionParametersIsEmpty = class(ERLException);

  // TRLDBXSettings
  TRLDBXSettings = class(TRLSettings)
  private
    FDriverName: string;
    FDriverFunction: string;
    FLibraryName: string;
    FVendorLibrary: string;
    procedure SetDriverName(const AValue: string);
    function GetConnectionParameters: string;
    procedure SetConnectionParameters(const AValue: string);
  protected
    function IsSerializableAttribute(const AName: string): Boolean; override;
    procedure DriverNameChanged; virtual;
    procedure CheckDriverName; virtual;
    procedure CheckDriverFunction; virtual;
    procedure CheckLibraryName; virtual;
    procedure CheckVendorLibrary; virtual;
    procedure CheckConnectionParameters; virtual;
  public
    procedure Assign(Source: TPersistent); override;
    procedure Validate; override;
  published
    property DriverName: string read FDriverName write SetDriverName;
    property DriverFunction: string read FDriverFunction write FDriverFunction;
    property LibraryName: string read FLibraryName write FLibraryName;
    property VendorLibrary: string read FVendorLibrary write FVendorLibrary;
    property ConnectionParameters: string read GetConnectionParameters write SetConnectionParameters;
  end;

implementation

{ TRLDBXSettings }

procedure TRLDBXSettings.SetDriverName(const AValue: string);
begin
  if AValue <> FDriverName then
  begin
    FDriverName := AValue;
    DriverNameChanged;
  end;
end;

function TRLDBXSettings.GetConnectionParameters: string;
begin
  Result := ToString;
end;

procedure TRLDBXSettings.SetConnectionParameters(const AValue: string);
begin
  FromString(AValue);
end;

function TRLDBXSettings.IsSerializableAttribute(const AName: string): Boolean;
begin
  Result := inherited IsSerializableAttribute(AName) and
    not SameText(CConnectionParameters, AName);
end;

procedure TRLDBXSettings.DriverNameChanged;
begin
end;

procedure TRLDBXSettings.CheckDriverName;
begin
  if FDriverName = '' then
    raise ERLDriverNameIsEmpty.Create('DriverName is empty.');
end;

procedure TRLDBXSettings.CheckDriverFunction;
begin
  if FDriverFunction = '' then
    raise ERLDriverFunctionIsEmpty.Create('DriverFunction is empty.');
end;

procedure TRLDBXSettings.CheckLibraryName;
begin
  if FLibraryName = '' then
    raise ERLLibraryNameIsEmpty.Create('LibraryName is empty.');
end;

procedure TRLDBXSettings.CheckVendorLibrary;
begin
  if FVendorLibrary = '' then
    raise ERLVendorLibraryIsEmpty.Create('VendorLibrary is empty.');
end;

procedure TRLDBXSettings.CheckConnectionParameters;
begin
  if ConnectionParameters = '' then
    raise ERLConnectionParametersIsEmpty.Create(
      'ConnectionParameters is empty.');
end;

procedure TRLDBXSettings.Assign(Source: TPersistent);
var
  lSettings: TRLDBXSettings;
begin
  inherited;
  if Source is TRLDBXSettings then
  begin
    lSettings := TRLDBXSettings(Source);
    DriverName := lSettings.DriverName;
    DriverFunction := lSettings.DriverFunction;
    LibraryName := lSettings.LibraryName;
    VendorLibrary := lSettings.VendorLibrary;
  end;
end;

procedure TRLDBXSettings.Validate;
begin
  inherited;
  CheckDriverName;
  CheckDriverFunction;
  CheckLibraryName;
  CheckVendorLibrary;
  CheckConnectionParameters;
end;

initialization
  RegisterClass(TRLDBXSettings);

end.
