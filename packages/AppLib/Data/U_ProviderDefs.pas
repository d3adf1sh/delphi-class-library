//=============================================================================
// U_ProviderDefs
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ProviderDefs;

{ TODO -oRafael : Renomear FindReferenceProvider para FindProviderReference. }

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_NamedCollection,
  U_Drivers,
  U_Provider;

type
  // Exceptions
  ERLProviderReferenceAlreadyExists = class(ERLException);
  ERLProviderReferenceNotFound = class(ERLException);
  ERLProviderDefNotFound = class(ERLException);

  // TRLProviderDef
  TRLProviderDef = class(TRLNamedCollectionItem)
  private
    FDescription: string;
    FReference: TRLProviderClass;
    FDrivers: TRLDrivers;
    procedure SetReference(const AValue: TRLProviderClass);
  protected
    procedure AlternativeKeyAlreadyExists(const AValue: string); override;
    function CreateDrivers: TRLDrivers; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property Description: string read FDescription write FDescription;
    property Reference: TRLProviderClass read FReference write SetReference;
    property Drivers: TRLDrivers read FDrivers;
  end;

  // TRLProviderDefs
  TRLProviderDefs = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLProviderDef;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLProviderDef;
    function Insert(const AIndex: Integer): TRLProviderDef;
    function Find(const AName: string): TRLProviderDef;
    function FindByName(const AName: string): TRLProviderDef;
    function FindReference(const AReference: TRLProviderClass): TRLProviderDef;
    function FindByReference(const AReference: TRLProviderClass): TRLProviderDef;
    property Items[const AIndex: Integer]: TRLProviderDef read GetItem; default;
  end;

  // TRLProviderManager
  TRLProviderManager = class(TRLBaseType)
  private
    FProviders: TRLProviderDefs;
  protected
    function CreateProviders: TRLProviderDefs; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    function GetProvider(const AName: string): TRLProviderDef; virtual;
    function GetReferenceProvider(const AReference: TRLProviderClass): TRLProviderDef; virtual;
    function FindProvider(const AName: string): TRLProviderDef; virtual;
    function FindReferenceProvider(const AReference: TRLProviderClass): TRLProviderDef; virtual;
    function RegisterProvider(const AName, ADescription: string; const AReference: TRLProviderClass): TRLProviderDef; virtual;
    procedure UnregisterProvider(const AName: string); virtual;
    function GetDriver(const AProviderName, AName: string): TRLDriver; virtual;
    function RegisterDriver(const AProviderName, AName, ADatabaseName: string): TRLDriver; overload; virtual;
    function RegisterDriver(const AType: TRLDriverClass; const AProviderName, AName, ADatabaseName: string): TRLDriver; overload; virtual;
    procedure UnregisterDriver(const AProviderName, AName: string); virtual;
    procedure RegisterDriverSetting(const AProviderName, ADriverName, AName: string); overload; virtual;
    procedure RegisterDriverSetting(const AProviderName, ADriverName, AName, ADefaultValue: string); overload; virtual;
    procedure UnregisterDriverSetting(const AProviderName, ADriverName, AName: string); virtual;
    function IsEmpty: Boolean; virtual;
    procedure Clear; virtual;
    property Providers: TRLProviderDefs read FProviders;
    class function Instance: TRLProviderManager;
    class procedure DeleteInstance;
  end;
  
implementation

var
  RLInstance: TRLProviderManager;

{ TRLProviderDef }

constructor TRLProviderDef.Create(Collection: TCollection);
begin
  inherited;
  FDrivers := CreateDrivers;
end;

destructor TRLProviderDef.Destroy;
begin
  if Assigned(FReference) then
    UnregisterClass(FReference);
  FreeAndNil(FDrivers);
  inherited;
end;

procedure TRLProviderDef.SetReference(const AValue: TRLProviderClass);
begin
  if Assigned(AValue) then
  begin
    SetAlternativeKey(AValue.ClassName);
    RegisterClass(AValue);
  end
  else
    SetAlternativeKey('');
  if Assigned(FReference) then
    UnregisterClass(FReference);
  FReference := AValue;
end;

procedure TRLProviderDef.AlternativeKeyAlreadyExists(const AValue: string);
begin
  raise ERLProviderReferenceAlreadyExists.Create('Reference already exists.');
end;

function TRLProviderDef.CreateDrivers: TRLDrivers;
begin
  Result := TRLDrivers.Create;
end;

{ TRLProviderDefs }

constructor TRLProviderDefs.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLProviderDefs.GetItem(const AIndex: Integer): TRLProviderDef;
begin
  Result := TRLProviderDef(inherited Items[AIndex]);
end;

function TRLProviderDefs.GetItemClass: TCollectionItemClass;
begin
  Result := TRLProviderDef;
end;

function TRLProviderDefs.Add: TRLProviderDef;
begin
  Result := TRLProviderDef(inherited Add);
end;

function TRLProviderDefs.Insert(const AIndex: Integer): TRLProviderDef;
begin
  Result := TRLProviderDef(inherited Insert(AIndex));
end;

function TRLProviderDefs.Find(const AName: string): TRLProviderDef;
begin
  Result := TRLProviderDef(inherited Find(AName));
end;

function TRLProviderDefs.FindByName(const AName: string): TRLProviderDef;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLProviderDefNotFound.CreateFmt('ProviderDef "%s" not found.',
      [AName]);
end;

function TRLProviderDefs.FindReference(
  const AReference: TRLProviderClass): TRLProviderDef;
begin
  if Assigned(AReference) then
    Result := TRLProviderDef(
      FindAlternativeHash(AReference.ClassName))
  else
    Result := nil;
end;

function TRLProviderDefs.FindByReference(
  const AReference: TRLProviderClass): TRLProviderDef;
begin
  Result := FindReference(AReference);
  if not Assigned(Result) then
    raise ERLProviderReferenceNotFound.Create('Reference not found.');
end;

{ TRLProviderManager }

constructor TRLProviderManager.Create;
begin
  inherited;
  FProviders := CreateProviders;
end;

destructor TRLProviderManager.Destroy;
begin
  FreeAndNil(FProviders);
  inherited;
end;

function TRLProviderManager.CreateProviders: TRLProviderDefs;
begin
  Result := TRLProviderDefs.Create;
end;

function TRLProviderManager.GetProvider(const AName: string): TRLProviderDef;
begin
  Result := FProviders.Find(AName);
end;

function TRLProviderManager.GetReferenceProvider(
  const AReference: TRLProviderClass): TRLProviderDef;
begin
  Result := FProviders.FindReference(AReference);
end;

function TRLProviderManager.FindProvider(const AName: string): TRLProviderDef;
begin
  Result := FProviders.FindByName(AName);
end;

function TRLProviderManager.FindReferenceProvider(
  const AReference: TRLProviderClass): TRLProviderDef;
begin
  Result := FProviders.FindByReference(AReference);
end;

function TRLProviderManager.RegisterProvider(const AName, ADescription: string;
  const AReference: TRLProviderClass): TRLProviderDef;
begin
  Result := GetProvider(AName);
  if not Assigned(Result) then
  begin
    Result := FProviders.Add;
    Result.Name := AName;
  end;

  Result.Description := ADescription;
  Result.Reference := AReference;
end;

procedure TRLProviderManager.UnregisterProvider(const AName: string);
begin
  FProviders.Remove(AName);
end;

function TRLProviderManager.GetDriver(const AProviderName,
  AName: string): TRLDriver;
var
  lProvider: TRLProviderDef;
begin
  lProvider := GetProvider(AProviderName);
  if Assigned(lProvider) then
    Result := lProvider.Drivers.Find(AName)
  else
    Result := nil;
end;

function TRLProviderManager.RegisterDriver(const AProviderName, AName,
  ADatabaseName: string): TRLDriver;
begin
  Result := RegisterDriver(TRLDriver, AProviderName, AName, ADatabaseName);
end;

function TRLProviderManager.RegisterDriver(const AType: TRLDriverClass;
  const AProviderName, AName, ADatabaseName: string): TRLDriver;
var
  lProvider: TRLProviderDef;
begin
  Result := nil;
  if Assigned(AType) then
  begin
    lProvider := GetProvider(AProviderName);
    if Assigned(lProvider) then
    begin
      Result := lProvider.Drivers.Find(AName);
      if not Assigned(Result) then
      begin
        Result := TRLDriver(lProvider.Drivers.AddType(AType));
        Result.Name := AName;
      end;

      Result.DatabaseName := ADatabaseName;
    end;
  end;
end;

procedure TRLProviderManager.UnregisterDriver(const AProviderName,
  AName: string);
var
  lProvider: TRLProviderDef;
begin
  lProvider := GetProvider(AProviderName);
  if Assigned(lProvider) then
    lProvider.Drivers.Remove(AName);
end;

procedure TRLProviderManager.RegisterDriverSetting(const AProviderName,
  ADriverName, AName: string);
begin
  RegisterDriverSetting(AProviderName, ADriverName, AName, '');
end;

procedure TRLProviderManager.RegisterDriverSetting(const AProviderName,
  ADriverName, AName, ADefaultValue: string);
var
  lDriver: TRLDriver;
begin
  lDriver := GetDriver(AProviderName, ADriverName);
  if Assigned(lDriver) then
    lDriver.Settings.Add(AName, ADefaultValue);
end;

procedure TRLProviderManager.UnregisterDriverSetting(const AProviderName,
  ADriverName, AName: string);
var
  lDriver: TRLDriver;
begin
  lDriver := GetDriver(AProviderName, ADriverName);
  if Assigned(lDriver) then
    lDriver.Settings.Remove(AName);
end;

function TRLProviderManager.IsEmpty: Boolean;
begin
  Result := FProviders.Count = 0;
end;

procedure TRLProviderManager.Clear;
begin
  FProviders.Clear;
end;

class function TRLProviderManager.Instance: TRLProviderManager;
begin
  if not Assigned(RLInstance) then
    RLInstance := Self.Create;
  Result := RLInstance;
end;

class procedure TRLProviderManager.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLProviderManager.DeleteInstance;

end.
