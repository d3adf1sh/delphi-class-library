//=============================================================================
// U_IdentifierMapping
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_IdentifierMapping;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_KeyedCollection;

type
  // Enumerations
  TRLIdentifierMappingKind = (imkReplace, imkAddToBegin, imkAddToEnd);

  // Exceptions
  ERLIdentifierTextAlreadyExists = class(ERLException);
  ERLIdentifierTextNotFound = class(ERLException);

  // TRLIdentifier
  TRLIdentifier = class(TRLKeyedCollectionItem)
  private
    FMappingKind: TRLIdentifierMappingKind;
    FMappingText: string;
  protected
    function GetKey: string; override;
    procedure SetKey(const AValue: string); override;
    procedure KeyAlreadyExists(const AValue: String); override;
  published
    property Text: string read GetKey write SetKey;
    property MappingKind: TRLIdentifierMappingKind read FMappingKind write FMappingKind;
    property MappingText: string read FMappingText write FMappingText;
  end;

  // TRLIdentifiers
  TRLIdentifiers = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLIdentifier;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLIdentifier;
    function Insert(const AIndex: Integer): TRLIdentifier;
    function Find(const AText: string): TRLIdentifier;
    function FindByText(const AText: string): TRLIdentifier;
    function Contains(const AText: string): Boolean; reintroduce; virtual;
    function Remove(const AText: string): Boolean; reintroduce; virtual;
    property Items[const AIndex: Integer]: TRLIdentifier read GetItem; default;
  end;

  // TRLIdentifierMapping
  TRLIdentifierMapping = class(TRLBaseType)
  private
    FIdentifiers: TRLIdentifiers;
  protected
    function CreateIdentifiers: TRLIdentifiers; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    function GetIdentifier(const AText: string): TRLIdentifier; virtual;
    function FindIdentifier(const AText: string): TRLIdentifier; virtual;
    function RegisterIdentifier(const AText: string; const AMappingKind: TRLIdentifierMappingKind; const AMappingText: string): TRLIdentifier; virtual;
    procedure UnregisterIdentifier(const AText: string); virtual;
    function IsEmpty: Boolean; virtual;
    procedure Clear; virtual;
    property Identifiers: TRLIdentifiers read FIdentifiers;
    class function Instance: TRLIdentifierMapping;
    class procedure DeleteInstance;
  end;

implementation

var
  RLInstance: TRLIdentifierMapping;

{ TRLIdentifier }

function TRLIdentifier.GetKey: string;
begin
  Result := inherited GetKey;
end;

procedure TRLIdentifier.SetKey(const AValue: string);
begin
  inherited;
end;

procedure TRLIdentifier.KeyAlreadyExists(const AValue: String);
begin
  raise ERLIdentifierTextAlreadyExists.CreateFmt('Text "%s" already exists.',
    [AValue]);
end;

{ TRLIdentifiers }

constructor TRLIdentifiers.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 64;
  CaseSensitive := False; 
end;

function TRLIdentifiers.GetItem(const AIndex: Integer): TRLIdentifier;
begin
  Result := TRLIdentifier(inherited Items[AIndex]);
end;

function TRLIdentifiers.GetItemClass: TCollectionItemClass;
begin
  Result := TRLIdentifier;
end;

function TRLIdentifiers.Add: TRLIdentifier;
begin
  Result := TRLIdentifier(inherited Add);
end;

function TRLIdentifiers.Insert(const AIndex: Integer): TRLIdentifier;
begin
  Result := TRLIdentifier(inherited Insert(AIndex));
end;

function TRLIdentifiers.Find(const AText: string): TRLIdentifier;
begin
  Result := TRLIdentifier(inherited Find(AText));
end;

function TRLIdentifiers.FindByText(const AText: string): TRLIdentifier;
begin
  Result := Find(AText);
  if not Assigned(Result) then
    raise ERLIdentifierTextNotFound.CreateFmt('Text "%s" not found.', [AText]);
end;

function TRLIdentifiers.Contains(const AText: string): Boolean;
begin
  Result := inherited Contains(AText);
end;

function TRLIdentifiers.Remove(const AText: string): Boolean;
begin
  Result := inherited Remove(AText);
end;

{ TRLIdentifierMapping }

constructor TRLIdentifierMapping.Create;
begin
  inherited;
  FIdentifiers := CreateIdentifiers;
end;

destructor TRLIdentifierMapping.Destroy;
begin
  FreeAndNil(FIdentifiers);
  inherited;
end;

function TRLIdentifierMapping.CreateIdentifiers: TRLIdentifiers;
begin
  Result := TRLIdentifiers.Create;
end;

function TRLIdentifierMapping.GetIdentifier(const AText: string): TRLIdentifier;
begin
  Result := FIdentifiers.Find(AText);
end;

function TRLIdentifierMapping.FindIdentifier(
  const AText: string): TRLIdentifier;
begin
  Result := FIdentifiers.FindByText(AText);
end;

function TRLIdentifierMapping.RegisterIdentifier(const AText: string;
  const AMappingKind: TRLIdentifierMappingKind;
  const AMappingText: string): TRLIdentifier;
begin
  Result := GetIdentifier(AText);
  if not Assigned(Result) then
  begin
    Result := FIdentifiers.Add;
    Result.Text := AText;
  end;

  Result.MappingKind := AMappingKind;
  Result.MappingText := AMappingText;
end;

procedure TRLIdentifierMapping.UnregisterIdentifier(const AText: string);
begin
  FIdentifiers.Remove(AText);
end;

function TRLIdentifierMapping.IsEmpty: Boolean;
begin
  Result := FIdentifiers.Count = 0;
end;

procedure TRLIdentifierMapping.Clear;
begin
  FIdentifiers.Clear;
end;

class function TRLIdentifierMapping.Instance: TRLIdentifierMapping;
begin
  if not Assigned(RLInstance) then
    RLInstance := Self.Create;
  Result := RLInstance;
end;

class procedure TRLIdentifierMapping.DeleteInstance;
begin
  if Assigned(RLInstance) then
    FreeAndNil(RLInstance);
end;

initialization
  ;

finalization
  TRLIdentifierMapping.DeleteInstance;

end.
