//=============================================================================
// U_SDACParameters
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACParameters;

{$I RL.INC}

interface

uses
  FMTBCD,
  Classes,
  DB,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  MSAccess,
  U_Parameters;

type
  // TRLSDACParameter
  TRLSDACParameter = class(TRLParameter)
  private
    function GetReference: TMSParam;
    procedure SetReference(const AValue: TMSParam);
  protected
    procedure KeyChanged; override;
    function GetModifier: TRLParameterModifier; override;
    function GetDataType: TFieldType; override;
    function GetSize: Integer; override;
    function GetPrecision: Integer; override;
    function GetScale: Integer; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsCardinal: Cardinal; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    function GetAsFMTBCD: TBcd; override;
    function GetAsDate: TDate; override;
    function GetAsTime: TTime; override;
    function GetAsDateTime: TDateTime; override;
    function GetAsBoolean: Boolean; override;
    function GetAsVariant: Variant; override;
    procedure SetModifier(const AValue: TRLParameterModifier); override;
    procedure SetDataType(const AValue: TFieldType); override;
    procedure SetSize(const AValue: Integer); override;
    procedure SetPrecision(const AValue: Integer); override;
    procedure SetScale(const AValue: Integer); override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsCardinal(const AValue: Cardinal); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
    procedure SetAsFMTBCD(const AValue: TBcd); override;
    procedure SetAsDate(const AValue: TDate); override;
    procedure SetAsTime(const AValue: TTime); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
    procedure SetAsBoolean(const AValue: Boolean); override;
    procedure SetAsVariant(const AValue: Variant); override;
  public
   	function IsNull: Boolean; override;
    procedure Clear; override;
    property Reference: TMSParam read GetReference write SetReference;
  end;

  // TRLSDACParameters
  TRLSDACParameters = class(TRLParameters)
  private
    function GetReference: TMSParams;
    procedure SetReference(const AValue: TMSParams);
  protected
    function GetItemClass: TCollectionItemClass; override;
    function InsertParameter(const AIndex: Integer): TPersistent; override;
  public
    procedure Refresh; override;
    property Reference: TMSParams read GetReference write SetReference;
  end;

implementation

uses DBAccess;

{ TRLSDACParameter }

function TRLSDACParameter.GetReference: TMSParam;
begin
  Result := TMSParam(inherited Reference);
end;

procedure TRLSDACParameter.SetReference(const AValue: TMSParam);
begin
  inherited Reference := AValue;
end;

procedure TRLSDACParameter.KeyChanged;
begin
  if Assigned(Reference) then
    Reference.Name := Name;
end;

function TRLSDACParameter.GetModifier: TRLParameterModifier;
begin
  if Assigned(Reference) then
    case Reference.ParamType of
      ptUnknown: Result := pmUnknown;
      ptInput: Result := pmInput;
      ptOutput: Result := pmOutput;
      ptInputOutput: Result := pmInputOutput;
      ptResult: Result := pmResult;
    else
      Result := pmUnknown;
    end
  else
    Result := pmUnknown;
end;

function TRLSDACParameter.GetDataType: TFieldType;
begin
  if Assigned(Reference) then
    Result := Reference.DataType
  else
    Result := ftUnknown;
end;

function TRLSDACParameter.GetSize: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.Size
  else
    Result := 0;
end;

function TRLSDACParameter.GetPrecision: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.Precision
  else
    Result := 0;
end;

function TRLSDACParameter.GetScale: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.NumericScale
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsString: string;
begin
  if not IsNull then
    Result := Reference.AsString
  else
    Result := '';
end;

function TRLSDACParameter.GetAsInteger: Integer;
begin
  if not IsNull then
    Result := Reference.AsInteger
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsCardinal: Cardinal;
begin
  if not IsNull then
    Result := Reference.AsInteger
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsInt64: Int64;
begin
  if not IsNull then
    Result := Reference.AsLargeInt
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsFloat: Double;
begin
  if not IsNull then
    Result := Reference.AsFloat
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsCurrency: Currency;
begin
  if not IsNull then
    Result := Reference.AsCurrency
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsFMTBCD: TBcd;
begin
  if not IsNull then
    Result := Reference.AsFMTBCD
  else
    Result := IntegerToBcd(0);
end;

function TRLSDACParameter.GetAsDate: TDate;
begin
  if not IsNull then
    Result := Reference.AsDate
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsTime: TTime;
begin
  if not IsNull then
    Result := Reference.AsTime
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsDateTime: TDateTime;
begin
  if not IsNull then
    Result := Reference.AsDateTime
  else
    Result := 0;
end;

function TRLSDACParameter.GetAsBoolean: Boolean;
begin
  if not IsNull then
    Result := Reference.AsBoolean
  else
    Result := False;
end;

function TRLSDACParameter.GetAsVariant: Variant;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

procedure TRLSDACParameter.SetModifier(const AValue: TRLParameterModifier);
begin
  if Assigned(Reference) then
    case AValue of
      pmUnknown: Reference.ParamType := ptUnknown;
      pmInput: Reference.ParamType := ptInput;
      pmOutput: Reference.ParamType := ptOutput;
      pmInputOutput: Reference.ParamType := ptInputOutput;
      pmResult: Reference.ParamType := ptResult;
    end;
end;

procedure TRLSDACParameter.SetDataType(const AValue: TFieldType);
begin
  if Assigned(Reference) then
    Reference.DataType := AValue;
end;

procedure TRLSDACParameter.SetSize(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Size := AValue;
end;

procedure TRLSDACParameter.SetPrecision(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Precision := AValue;
end;

procedure TRLSDACParameter.SetScale(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.NumericScale := AValue;
end;

procedure TRLSDACParameter.SetAsString(const AValue: string);
begin
  if Assigned(Reference) then
    Reference.AsString := AValue;
end;

procedure TRLSDACParameter.SetAsInteger(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.AsInteger := AValue;
end;

procedure TRLSDACParameter.SetAsCardinal(const AValue: Cardinal);
begin
  if Assigned(Reference) then
    Reference.AsInteger := AValue;
end;

procedure TRLSDACParameter.SetAsInt64(const AValue: Int64);
begin
  if Assigned(Reference) then
    Reference.AsLargeInt := AValue;
end;

procedure TRLSDACParameter.SetAsFloat(const AValue: Double);
begin
  if Assigned(Reference) then
    Reference.AsFloat := AValue;
end;

procedure TRLSDACParameter.SetAsCurrency(const AValue: Currency);
begin
  if Assigned(Reference) then
    Reference.AsCurrency := AValue; //ftBCD
end;

procedure TRLSDACParameter.SetAsFMTBCD(const AValue: TBcd);
begin
  if Assigned(Reference) then
    Reference.AsFMTBCD := AValue;
end;

procedure TRLSDACParameter.SetAsDate(const AValue: TDate);
begin
  if Assigned(Reference) then
    Reference.AsDate := AValue;
end;

procedure TRLSDACParameter.SetAsTime(const AValue: TTime);
begin
  if Assigned(Reference) then
    Reference.AsTime := AValue;
end;

procedure TRLSDACParameter.SetAsDateTime(const AValue: TDateTime);
begin
  if Assigned(Reference) then
    Reference.AsDateTime := AValue;
end;

procedure TRLSDACParameter.SetAsBoolean(const AValue: Boolean);
begin
  if Assigned(Reference) then
    Reference.AsBoolean := AValue;
end;

procedure TRLSDACParameter.SetAsVariant(const AValue: Variant);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

function TRLSDACParameter.IsNull: Boolean;
begin
  if Assigned(Reference) then
    Result := Reference.IsNull
  else
    Result := True;
end;

procedure TRLSDACParameter.Clear;
begin
  if Assigned(Reference) then
    Reference.Clear;
end;

{ TRLSDACParameters }

function TRLSDACParameters.GetReference: TMSParams;
begin
  Result := TMSParams(inherited Reference);
end;

procedure TRLSDACParameters.SetReference(const AValue: TMSParams);
begin
  inherited Reference := AValue;
end;

function TRLSDACParameters.GetItemClass: TCollectionItemClass;
begin
  Result := TRLSDACParameter;
end;

function TRLSDACParameters.InsertParameter(const AIndex: Integer): TPersistent;
begin
  if Assigned(Reference) then
    Result := TMSParam(Reference.Insert(AIndex))
  else
    Result := nil;
end;

procedure TRLSDACParameters.Refresh;
var
  lParameter: TRLSDACParameter;
  I: Integer;
begin
  if Assigned(Reference) then
  begin
    ClearReferences;
    BeginUpdate;
    try
      Clear;
      for I := 0 to Pred(Reference.Count) do
      begin
        lParameter := TRLSDACParameter(TCollection(Self).Add);
        lParameter.Name := Reference[I].Name;
        lParameter.Reference := Reference[I];
      end;
    finally
      EndUpdate;
    end;
  end;
end;

end.
