//=============================================================================
// U_ADOQuery
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOQuery;

interface

uses
  SysUtils,
  Classes,
  DB,
  ADODB,
  U_SQL,
  U_Parameters,
  U_Query,
  U_ADOProvider,
  U_ADOSQL,
  U_ADOParameters;

type
  // TRLADOQuery
  TRLADOQuery = class(TRLQuery)
  private
    FQuery: TADOQuery;
  protected
    procedure ProviderChanged; override;
    function CreateSQL: TRLSQL; override;
    function CreateParameters: TRLParameters; override;
    function GetDataSet: TDataSet; override;
    property Query: TADOQuery read FQuery;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
  end;

implementation

{ TRLADOQuery }

constructor TRLADOQuery.Create;
begin
  inherited;
  FQuery := TADOQuery.Create(nil);
  SQL.Reference := FQuery.SQL;
  Parameters.Reference := FQuery.Parameters;
  ProviderChanged;
end;

destructor TRLADOQuery.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FQuery);
end;

procedure TRLADOQuery.ProviderChanged;
begin
  if Assigned(FQuery) then
  begin
    if Provider is TRLADOProvider then
      FQuery.Connection := TRLADOProvider(Provider).Connection
    else
      FQuery.Connection := nil;
  end;
end;

function TRLADOQuery.CreateSQL: TRLSQL;
begin
  Result := TRLADOSQL.Create;
end;

function TRLADOQuery.CreateParameters: TRLParameters;
begin
  Result := TRLADOParameters.CreateParented(Self);
end;

function TRLADOQuery.GetDataSet: TDataSet;
begin
  Result := FQuery;
end;

procedure TRLADOQuery.Execute;
begin
  CheckProvider;
  FQuery.ExecSQL;
end;

initialization
  RegisterClass(TRLADOQuery);

end.
