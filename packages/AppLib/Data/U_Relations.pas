//=============================================================================
// U_Relations
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Relations;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_StringList,
  U_Collection,
  U_DBObjects,
  U_Fields,
  U_Keys,
  U_Indexes,
  U_References;

type
  // Enumerations
  TRLRelationType = (rtTable, rtView);

  // Exceptions
  ERLRelationNotFound = class(ERLException);

  // TRLRelation
  TRLRelation = class(TRLDBObject)
  private
    FFields: TRLFields;
    FKeys: TRLKeys;
    FIndexes: TRLIndexes;
    FReferences: TRLReferences;
    FRelationType: TRLRelationType;
  protected
    function CreateFields: TRLFields; virtual;
    function CreateKeys: TRLKeys; virtual;
    function CreateIndexes: TRLIndexes; virtual;
    function CreateReferences: TRLReferences; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Fields: TRLFields read FFields;
    property Keys: TRLKeys read FKeys;
    property Indexes: TRLIndexes read FIndexes;
    property References: TRLReferences read FReferences;
    property RelationType: TRLRelationType read FRelationType write FRelationType;
  end;

  // TRLRelationArray
  TRLRelationArray = class(TRLDBObjectArray)
  private
    function GetItem(const AIndex: Integer): TRLRelation;
    procedure SetItem(const AIndex: Integer; const AValue: TRLRelation);
  public
    procedure Add(const AValue: TRLRelation);
    procedure Insert(const AIndex: Integer; const AValue: TRLRelation);
    property Items[const AIndex: Integer]: TRLRelation read GetItem write SetItem; default;
  end;

  // TRLRelations
  TRLRelations = class(TRLDBObjects)
  private
    function GetItem(const AIndex: Integer): TRLRelation;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLRelation;
    function Insert(const AIndex: Integer): TRLRelation;
    function Find(const AName: string): TRLRelation;
    function FindByName(const AName: string): TRLRelation;
    function ToArray: TRLRelationArray; overload;
    function ToArray(const ANames: TRLStringList): TRLRelationArray; overload;
    function ToArray(const ARelationType: TRLRelationType): TRLRelationArray; overload;
    property Items[const AIndex: Integer]: TRLRelation read GetItem; default;
  end;

// Utils
function RelationTypeToString(const AValue: TRLRelationType): string;
function StringToRelationType(const AValue: string): TRLRelationType;

implementation

uses
  TypInfo;
  
// Utils
function RelationTypeToString(const AValue: TRLRelationType): string;
begin
  Result := GetEnumName(TypeInfo(TRLRelationType), Ord(AValue));
end;

function StringToRelationType(const AValue: string): TRLRelationType;
begin
  Result := TRLRelationType(GetEnumValue(TypeInfo(TRLRelationType), AValue));
end;

{ TRLRelation }

constructor TRLRelation.Create(Collection: TCollection);
begin
  inherited;
  FFields := CreateFields;
  FKeys := CreateKeys;
  FIndexes := CreateIndexes;
  FReferences := CreateReferences;
end;

destructor TRLRelation.Destroy;
begin
  FreeAndNil(FReferences);
  FreeAndNil(FIndexes);
  FreeAndNil(FKeys);
  FreeAndNil(FFields);
  inherited;
end;

function TRLRelation.CreateFields: TRLFields;
begin
  Result := TRLFields.CreateParented(Self);;
end;

function TRLRelation.CreateKeys: TRLKeys;
begin
  Result := TRLKeys.CreateParented(Self);
end;

function TRLRelation.CreateIndexes: TRLIndexes;
begin
  Result := TRLIndexes.CreateParented(Self);
end;

function TRLRelation.CreateReferences: TRLReferences;
begin
  Result := TRLReferences.CreateParented(Self);
end;

procedure TRLRelation.Assign(Source: TPersistent);
var
  lRelation: TRLRelation;
begin
  inherited;
  if Source is TRLRelation then
  begin
    lRelation := TRLRelation(Source);
    Fields.Assign(lRelation.Fields);
    Keys.Assign(lRelation.Keys);
    Indexes.Assign(lRelation.Indexes);
    References.Assign(lRelation.References);
    RelationType := lRelation.RelationType;
  end;
end;

{ TRLRelationArray }

function TRLRelationArray.GetItem(const AIndex: Integer): TRLRelation;
begin
  Result := TRLRelation(inherited Items[AIndex]);
end;

procedure TRLRelationArray.SetItem(const AIndex: Integer;
  const AValue: TRLRelation);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLRelationArray.Add(const AValue: TRLRelation);
begin
  inherited Add(AValue);
end;

procedure TRLRelationArray.Insert(const AIndex: Integer;
  const AValue: TRLRelation);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLRelations }

constructor TRLRelations.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 4096;
end;

function TRLRelations.GetItem(const AIndex: Integer): TRLRelation;
begin
  Result := TRLRelation(inherited Items[AIndex]);
end;

function TRLRelations.GetItemClass: TCollectionItemClass;
begin
  Result := TRLRelation;
end;

function TRLRelations.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLRelationArray;
end;

function TRLRelations.Add: TRLRelation;
begin
  Result := TRLRelation(inherited Add);
end;

function TRLRelations.Insert(const AIndex: Integer): TRLRelation;
begin
  Result := TRLRelation(inherited Insert(AIndex));
end;

function TRLRelations.Find(const AName: string): TRLRelation;
begin
  Result := TRLRelation(inherited Find(AName));
end;

function TRLRelations.FindByName(const AName: string): TRLRelation;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLRelationNotFound.CreateFmt('Relation "%s" not found.', [AName]);
end;

function TRLRelations.ToArray: TRLRelationArray;
begin
  Result := TRLRelationArray(inherited ToArray);
end;

function TRLRelations.ToArray(const ANames: TRLStringList): TRLRelationArray;
begin
  Result := TRLRelationArray(inherited ToArray(ANames));
end;

function TRLRelations.ToArray(
  const ARelationType: TRLRelationType): TRLRelationArray;
var
  I: Integer;
begin
  Result := TRLRelationArray.Create;
  for I := 0 to Pred(Count) do
    if Items[I].RelationType = ARelationType then
      Result.Add(Items[I]);
end;

end.
