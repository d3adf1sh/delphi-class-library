//=============================================================================
// U_Conditions
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Conditions;

interface

uses
  SysUtils,
  Classes,
  U_Collection,
  U_Value,
  U_Comparison;

type
  // Forward declarations
  TRLConditions = class;

  // TRLCondition
  TRLCondition = class(TRLCollectionItem)
  private
    FIsNot: Boolean;
    FIsOr: Boolean;
    FComparison: TRLComparison;
    FConditions: TRLConditions;
  protected
    function CreateComparison: TRLComparison; virtual;
    function CreateConditions: TRLConditions; virtual;
    procedure CheckComparison; virtual;
    procedure CheckConditions; virtual;
    function GetFieldName: string; virtual; abstract;
    function GetValueType: TRLValueType; virtual; abstract;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Validate; override;
    property FieldName: string read GetFieldName;
    property ValueType: TRLValueType read GetValueType;
  published
    property IsNot: Boolean read FIsNot write FIsNot;
    property IsOr: Boolean read FIsOr write FIsOr;
    property Comparison: TRLComparison read FComparison;
    property Conditions: TRLConditions read FConditions;
  end;

  // TRLConditions 
  TRLConditions = class(TRLCollection)
  private
    function GetItem(const AIndex: Integer): TRLCondition;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TRLCondition;
    function Insert(const AIndex: Integer): TRLCondition;
    property Items[const AIndex: Integer]: TRLCondition read GetItem; default;
  end;

implementation

{ TRLCondition }

constructor TRLCondition.Create(Collection: TCollection);
begin
  inherited;
  FComparison := CreateComparison;
  FConditions := CreateConditions;
end;

destructor TRLCondition.Destroy;
begin
  FreeAndNil(FConditions);
  FreeAndNil(FComparison);
  inherited;
end;

function TRLCondition.CreateComparison: TRLComparison;
begin
  Result := TRLComparison.Create;
end;

function TRLCondition.CreateConditions: TRLConditions;
begin
  Result := TRLConditions.CreateParented(Self);
end;

procedure TRLCondition.CheckComparison;
begin
  FComparison.Validate;
end;

procedure TRLCondition.CheckConditions;
begin
  FConditions.Validate;
end;

procedure TRLCondition.Assign(Source: TPersistent);
var
  lCondition: TRLCondition;
begin
  if Source is TRLCondition then
  begin
    lCondition := TRLCondition(Source);
    IsNot := lCondition.IsNot;
    IsOr := lCondition.IsOr;
    Comparison.Assign(lCondition.Comparison);
    Conditions.Assign(lCondition.Conditions);
  end;
end;

procedure TRLCondition.Validate;
begin
  inherited;
  CheckComparison;
  CheckConditions;
end;

{ TRLConditions }

function TRLConditions.GetItem(const AIndex: Integer): TRLCondition;
begin
  Result := TRLCondition(inherited Items[AIndex]);
end;

function TRLConditions.GetItemClass: TCollectionItemClass;
begin
  Result := TRLCondition;
end;

function TRLConditions.Add: TRLCondition;
begin
  Result := TRLCondition(inherited Add);
end;

function TRLConditions.Insert(const AIndex: Integer): TRLCondition;
begin
  Result := TRLCondition(inherited Insert(AIndex));
end;

end.
