//=============================================================================
// U_OPFClassMapping
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFClassMapping;

interface

uses
  SysUtils,
  Classes,
  U_BaseType,
  U_Exception,
  U_Value,
  U_Validator,
  U_OPFAttributes,
  U_OPFRelationships,
  U_OPFRelationshipBindings,
  U_OPFClasses;

type
  // Exceptions
  ERLOPFClassMappingIsNull = class(ERLException); //External
  ERLOPFClassMappinRLopulatorNotFound = class(ERLException);

  // TRLOPFClassMapping
  TRLOPFClassMapping = class(TRLBaseType, IRLValidator)
  private
    FClasses: TRLOPFClasses;
  protected
    function CreateClasses: TRLOPFClasses; virtual;
    procedure CheckClasses; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    function GetClass(const AReference: TClass): TRLOPFClass; virtual;
    function GetObjectClass(const AInstance: TObject): TRLOPFClass; virtual;
    function RegisterClass(const AReference: TClass; const ASchemaName, ARelationName: string): TRLOPFClass; virtual;
    procedure UnregisterClass(const AReference: TClass); virtual;
    function GetAttribute(const AClass: TClass; const AName: string): TRLOPFAttribute; virtual;
    function RegisterAttribute(const AClass: TClass; const AName, AFieldName: string; const AValueType: TRLValueType): TRLOPFAttribute; overload; virtual;
    function RegisterAttribute(const AClass: TClass; const AName, AFieldName: string; const AValueType: TRLValueType; const AAutoIncrement: Boolean): TRLOPFAttribute; overload; virtual;
    function RegisterAttribute(const AClass: TClass; const AName, AFieldName: string; const AValueType: TRLValueType; const AAutoIncrement, AKey: Boolean): TRLOPFAttribute; overload; virtual;
    procedure UnregisterAttribute(const AClass: TClass; const AName: string); virtual;
    function GetRelationship(const AClass: TClass; const AName: string): TRLOPFRelationship; virtual;
    function RegisterRelationship(const AClass: TClass; const AName, ARelatedClassName: string; const ACardinality: TRLOPFRelationshipCardinality): TRLOPFRelationship; virtual;
    procedure UnregisterRelationship(const AClass: TClass; const AName: string); virtual;
    function GetRelationshipBinding(const AClass: TClass; const ARelationshipName, AAttributeName: string): TRLOPFRelationshipBinding; virtual;
    function RegisterRelationshipBinding(const AClass: TClass; const ARelationshipName, AAttributeName, ARelatedAttributeName: string): TRLOPFRelationshipBinding; virtual;
    procedure UnregisterRelationshipBinding(const AClass: TClass; const ARelationshipName, AAttributeName: string); virtual;
    function IsEmpty: Boolean; virtual;
    procedure Clear; virtual;
    procedure Validate; virtual;
    property Classes: TRLOPFClasses read FClasses;
  end;

  // TRLOPFClassMappingPopulator
  TRLOPFClassMappingPopulator = class(TRLBaseType)
  public
    procedure Populate(const AClassMapping: TRLOPFClassMapping); virtual; abstract;
    class function GetClassMappinRLopulator(const AName: string): TRLOPFClassMappingPopulator;
    class function FindClassMappinRLopulator(const AName: string): TRLOPFClassMappingPopulator;
  end;

implementation

{ TRLOPFClassMapping }

constructor TRLOPFClassMapping.Create;
begin
  inherited;
  FClasses := CreateClasses;
end;

destructor TRLOPFClassMapping.Destroy;
begin
  FreeAndNil(FClasses);
  inherited;
end;

function TRLOPFClassMapping.CreateClasses: TRLOPFClasses;
begin
  Result := TRLOPFClasses.Create;
end;

procedure TRLOPFClassMapping.CheckClasses;
begin
  FClasses.Validate;
end;

function TRLOPFClassMapping.GetClass(const AReference: TClass): TRLOPFClass;
begin
  Result := FClasses.Find(AReference);
end;

function TRLOPFClassMapping.GetObjectClass(
  const AInstance: TObject): TRLOPFClass;
begin
  if Assigned(AInstance) then
    Result := GetClass(AInstance.ClassType)
  else
    Result := nil;
end;

function TRLOPFClassMapping.RegisterClass(const AReference: TClass;
  const ASchemaName, ARelationName: string): TRLOPFClass;
begin
  Result := GetClass(AReference);
  if not Assigned(Result) then
  begin
    Result := FClasses.Add;
    Result.Reference := AReference;
  end;

  Result.SchemaName := ASchemaName;
  Result.RelationName := ARelationName;
end;

procedure TRLOPFClassMapping.UnregisterClass(const AReference: TClass);
begin
  FClasses.Remove(AReference);
end;

function TRLOPFClassMapping.GetAttribute(const AClass: TClass;
  const AName: string): TRLOPFAttribute;
var
  lClass: TRLOPFClass;
begin
  lClass := GetClass(AClass);
  if Assigned(lClass) then
    Result := lClass.Attributes.Find(AName) 
  else
    Result := nil;
end;

function TRLOPFClassMapping.RegisterAttribute(const AClass: TClass; const AName,
  AFieldName: string; const AValueType: TRLValueType): TRLOPFAttribute;
begin
  Result := RegisterAttribute(AClass, AName, AFieldName, AValueType, False);
end;

function TRLOPFClassMapping.RegisterAttribute(const AClass: TClass; const AName,
  AFieldName: string; const AValueType: TRLValueType;
  const AAutoIncrement: Boolean): TRLOPFAttribute;
begin
  Result := RegisterAttribute(AClass, AName, AFieldName, AValueType,
    AAutoIncrement, False);
end;

function TRLOPFClassMapping.RegisterAttribute(const AClass: TClass; const AName,
  AFieldName: string; const AValueType: TRLValueType; const AAutoIncrement,
  AKey: Boolean): TRLOPFAttribute;
var
  lClass: TRLOPFClass;
begin
  lClass := GetClass(AClass);
  if Assigned(lClass) then
  begin
    Result := lClass.Attributes.Find(AName);
    if not Assigned(Result) then
    begin
      Result := lClass.Attributes.Add;
      Result.Name := AName;
    end;

    Result.FieldName := AFieldName;
    Result.ValueType := AValueType;
    Result.AutoIncrement := AAutoIncrement;
    Result.IsKey := AKey;
  end
  else
    Result := nil;
end;

procedure TRLOPFClassMapping.UnregisterAttribute(const AClass: TClass;
  const AName: string);
var
  lClass: TRLOPFClass;
begin
  lClass := GetClass(AClass);
  if Assigned(lClass) then
    lClass.Attributes.Remove(AName);
end;

function TRLOPFClassMapping.GetRelationship(const AClass: TClass;
  const AName: string): TRLOPFRelationship;
var
  lClass: TRLOPFClass;
begin
  lClass := GetClass(AClass);
  if Assigned(lClass) then
    Result := lClass.Relationships.Find(AName)
  else
    Result := nil;
end;

function TRLOPFClassMapping.RegisterRelationship(const AClass: TClass;
  const AName, ARelatedClassName: string;
  const ACardinality: TRLOPFRelationshipCardinality): TRLOPFRelationship;
var
  lClass: TRLOPFClass;
begin
  lClass := GetClass(AClass);
  if Assigned(lClass) then
  begin
    Result := lClass.Relationships.Find(AName);
    if not Assigned(Result) then
    begin
      Result := lClass.Relationships.Add;
      Result.Name := AName;
    end;

    Result.RelatedClassName := ARelatedClassName;
    Result.Cardinality := ACardinality;
  end
  else
    Result := nil;
end;

procedure TRLOPFClassMapping.UnregisterRelationship(const AClass: TClass;
  const AName: string);
var
  lClass: TRLOPFClass;
begin
  lClass := GetClass(AClass);
  if Assigned(lClass) then
    lClass.Relationships.Remove(AName);
end;

function TRLOPFClassMapping.GetRelationshipBinding(const AClass: TClass;
  const ARelationshipName, AAttributeName: string): TRLOPFRelationshipBinding;
var
  lRelationship: TRLOPFRelationship;
begin
  lRelationship := GetRelationship(AClass, ARelationshipName);
  if Assigned(lRelationship) then
    Result := lRelationship.Bindings.Find(AAttributeName)
  else
    Result := nil;
end;

function TRLOPFClassMapping.RegisterRelationshipBinding(const AClass: TClass;
  const ARelationshipName, AAttributeName,
  ARelatedAttributeName: string): TRLOPFRelationshipBinding;
var
  lRelationship: TRLOPFRelationship;
begin
  lRelationship := GetRelationship(AClass, ARelationshipName);
  if Assigned(lRelationship) then
  begin
    Result := lRelationship.Bindings.Find(AAttributeName);
    if not Assigned(Result) then
    begin
      Result := lRelationship.Bindings.Add;
      Result.AttributeName := AAttributeName;
    end;

    Result.RelatedAttributeName := ARelatedAttributeName;
  end
  else
    Result := nil;
end;

procedure TRLOPFClassMapping.UnregisterRelationshipBinding(const AClass: TClass;
  const ARelationshipName, AAttributeName: string);
var
  lRelationship: TRLOPFRelationship;
begin
  lRelationship := GetRelationship(AClass, ARelationshipName);
  if Assigned(lRelationship) then
    lRelationship.Bindings.Remove(AAttributeName);
end;

function TRLOPFClassMapping.IsEmpty: Boolean;
begin
  Result := FClasses.Count = 0;
end;

procedure TRLOPFClassMapping.Clear;
begin
  FClasses.Clear;
end;

procedure TRLOPFClassMapping.Validate;
begin
  CheckClasses;
end;

{ TRLOPFClassMappingPopulator }

class function TRLOPFClassMappingPopulator.GetClassMappinRLopulator(
  const AName: string): TRLOPFClassMappingPopulator;
var
  lClass: TRLBaseTypeClass;
begin
  lClass := TRLBaseTypeClass(
    GetClass(Format('TRL%sClassMappingPopulator', [AName])));
  if Assigned(lClass) then
    Result := TRLOPFClassMappingPopulator(lClass.Create)
  else
    Result := nil;
end;

class function TRLOPFClassMappingPopulator.FindClassMappinRLopulator(
  const AName: string): TRLOPFClassMappingPopulator;
begin
  Result := GetClassMappinRLopulator(AName);
  if not Assigned(Result) then
    raise ERLOPFClassMappinRLopulatorNotFound.CreateFmt(
      'sClassMappingPopulator "%s" not found.', [AName]);
end;

end.
