//=============================================================================
// U_Indexes
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Indexes;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_StringList,
  U_Collection,
  U_DBObjects,
  U_IndexFields;

type
  // Enumerations
  TRLIndexType = (itIndex, itUniqueIndex);

  // Exceptions
  ERLIndexNotFound = class(ERLException);

  // TRLIndex
  TRLIndex = class(TRLDBObject)
  private
    FFields: TRLIndexFields;
    FIndexType: TRLIndexType;
    FEnabled: Boolean;
  protected
    function CreateFields: TRLIndexFields; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Fields: TRLIndexFields read FFields;
    property IndexType: TRLIndexType read FIndexType write FIndexType;
    property Enabled: Boolean read FEnabled write FEnabled;
  end;

  // TRLIndexArray
  TRLIndexArray = class(TRLDBObjectArray)
  private
    function GetItem(const AIndex: Integer): TRLIndex;
    procedure SetItem(const AIndex: Integer; const AValue: TRLIndex);
  public
    procedure Add(const AValue: TRLIndex);
    procedure Insert(const AIndex: Integer; const AValue: TRLIndex);
    property Items[const AIndex: Integer]: TRLIndex read GetItem write SetItem; default;
  end;

  // TRLIndexes
  TRLIndexes = class(TRLDBObjects)
  private
    function GetItem(const AIndex: Integer): TRLIndex;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLIndex;
    function Insert(const AIndex: Integer): TRLIndex;
    function Find(const AName: string): TRLIndex;
    function FindByName(const AName: string): TRLIndex;
    function ToArray: TRLIndexArray; overload;
    function ToArray(const ANames: TRLStringList): TRLIndexArray; overload;
    function ToArray(const AIndexType: TRLIndexType): TRLIndexArray; overload;
    property Items[const AIndex: Integer]: TRLIndex read GetItem; default;
  end;

// Utils
function IndexTypeToString(const AValue: TRLIndexType): string;
function StringToIndexType(const AValue: string): TRLIndexType;

implementation

uses
  TypInfo;
  
// Utils
function IndexTypeToString(const AValue: TRLIndexType): string;
begin
  Result := GetEnumName(TypeInfo(TRLIndexType), Ord(AValue));
end;

function StringToIndexType(const AValue: string): TRLIndexType;
begin
  Result := TRLIndexType(GetEnumValue(TypeInfo(TRLIndexType), AValue));
end;

{ TRLIndex }

constructor TRLIndex.Create(Collection: TCollection);
begin
  inherited;
  FFields := CreateFields;
end;

destructor TRLIndex.Destroy;
begin
  FreeAndNil(FFields);
  inherited;
end;

function TRLIndex.CreateFields: TRLIndexFields;
begin
  Result := TRLIndexFields.CreateParented(Self);
end;

procedure TRLIndex.Assign(Source: TPersistent);
var
  lIndex: TRLIndex;
begin
  inherited;
  if Source is TRLIndex then
  begin
    lIndex := TRLIndex(Source);
    Fields.Assign(lIndex.Fields);
    IndexType := lIndex.IndexType;
    Enabled := lIndex.Enabled;
  end;
end;

{ TRLIndexArray }

function TRLIndexArray.GetItem(const AIndex: Integer): TRLIndex;
begin
  Result := TRLIndex(inherited Items[AIndex]);
end;

procedure TRLIndexArray.SetItem(const AIndex: Integer; const AValue: TRLIndex);
begin
  inherited Items[AIndex] := AValue;
end;

procedure TRLIndexArray.Add(const AValue: TRLIndex);
begin
  inherited Add(AValue);
end;

procedure TRLIndexArray.Insert(const AIndex: Integer; const AValue: TRLIndex);
begin
  inherited Insert(AIndex, AValue);
end;

{ TRLIndexes }

constructor TRLIndexes.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLIndexes.GetItem(const AIndex: Integer): TRLIndex;
begin
  Result := TRLIndex(inherited Items[AIndex]);
end;

function TRLIndexes.GetItemClass: TCollectionItemClass;
begin
  Result := TRLIndex;
end;

function TRLIndexes.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLIndexArray;
end;

function TRLIndexes.Add: TRLIndex;
begin
  Result := TRLIndex(inherited Add);
end;

function TRLIndexes.Insert(const AIndex: Integer): TRLIndex;
begin
  Result := TRLIndex(inherited Insert(AIndex));
end;

function TRLIndexes.Find(const AName: string): TRLIndex;
begin
  Result := TRLIndex(inherited Find(AName));
end;

function TRLIndexes.FindByName(const AName: string): TRLIndex;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLIndexNotFound.CreateFmt('Index "%s" not found.', [AName]);
end;

function TRLIndexes.ToArray: TRLIndexArray;
begin
  Result := TRLIndexArray(inherited ToArray);
end;

function TRLIndexes.ToArray(const ANames: TRLStringList): TRLIndexArray;
begin
  Result := TRLIndexArray(inherited ToArray(ANames));
end;

function TRLIndexes.ToArray(const AIndexType: TRLIndexType): TRLIndexArray;
var
  I: Integer;
begin
  Result := TRLIndexArray.Create;
  for I := 0 to Pred(Count) do
    if Items[I].IndexType = AIndexType then
      Result.Add(Items[I]);
end;

end.
