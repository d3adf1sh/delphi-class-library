//=============================================================================
// U_ADOSQL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOSQL;

interface

uses
  Classes,
  ADODB,
  U_SQL;

type
  // TRLADOSQL
  TRLADOSQL = class(TRLSQL)
  protected
    procedure UpdateSQL; override;
  end;

implementation

{ TRLADOSQL }

procedure TRLADOSQL.UpdateSQL;
begin
  if Assigned(Reference) then
  begin
    TStrings(Reference).Assign(Self);
    if Assigned(Parameters.Reference) then
      TParameters(Parameters.Reference).ParseSQL(Text, True);
  end;

  inherited;
end;

end.
