//=============================================================================
// U_SDACSQL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACSQL;

interface

uses
  Classes,
  MSAccess,
  U_SQL;

type
  // TRLSDACSQL
  TRLSDACSQL = class(TRLSQL)
  protected
    procedure UpdateSQL; override;
  end;

implementation

{ TRLSDACSQL }

procedure TRLSDACSQL.UpdateSQL;
begin
  if Assigned(Reference) then
  begin
    TStrings(Reference).Assign(Self);
    if Assigned(Parameters.Reference) then
      TMSParams(Parameters.Reference).ParseSQL(Text, True);
  end;

  inherited;
end;

end.
