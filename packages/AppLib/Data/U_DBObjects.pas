//=============================================================================
// U_DBObjects
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBObjects;

interface

uses
  SysUtils,
  Classes,
  U_StringList,
  U_Collection,
  U_NamedCollection,
  U_IdentifierMapping;

type
  // TRLDBObject
  TRLDBObject = class(TRLNamedCollectionItem)
  private
    FQuotedName: string;
    FIdentifier: string;
    FTargetIdentifier: string;
    function GetFullName: string;
    function GetQuotedName: string;
    function GetQuotedFullName: string;
    function GetIdentifier: string;
    function GetTargetIdentifier: string;
  protected
    procedure KeyChanged; override;
    function MakeFullName(const AQuote: Boolean): string; virtual;
    function MakeIdentifier: string; virtual;
    function ApplyIdentifierMapping(var AText: string): Boolean; virtual;
    function ReadTargetIdentifier: string; virtual;
  public
    property FullName: string read GetFullName;
    property QuotedName: string read GetQuotedName;
    property QuotedFullName: string read GetQuotedFullName;
    property Identifier: string read GetIdentifier;
    property TargetIdentifier: string read GetTargetIdentifier;
  end;

  // TRLDBObjectArray
  TRLDBObjectArray = class(TRLNamedCollectionItemArray)
  private
    function GetItem(const AIndex: Integer): TRLDBObject;
    procedure SetItem(const AIndex: Integer; const AValue: TRLDBObject);
  public
    property Items[const AIndex: Integer]: TRLDBObject read GetItem write SetItem; default;
  end;
                  
  // TRLDBObjects
  TRLDBObjects = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLDBObject;
  protected
    function GetItemClass: TCollectionItemClass; override;
    function GetItemArrayClass: TRLCollectionItemArrayClass; override;
  public
    function ToArray: TRLDBObjectArray; overload;
    function ToArray(const ANames: TRLStringList): TRLDBObjectArray; overload;
    property Items[const AIndex: Integer]: TRLDBObject read GetItem; default;
  end;

implementation

uses
  U_System;

{ TRLDBObject }
            
function TRLDBObject.GetFullName: string;
begin
  Result := MakeFullName(False);
end;

function TRLDBObject.GetQuotedName: string;
begin
  if FQuotedName = '' then
    FQuotedName := Format('"%s"', [Name]);
  Result := FQuotedName;
end;

function TRLDBObject.GetQuotedFullName: string;
begin
  Result := MakeFullName(True);
end;

function TRLDBObject.GetIdentifier: string;
begin
  if FIdentifier = '' then
    FIdentifier := MakeIdentifier;
  Result := FIdentifier;
end;

function TRLDBObject.GetTargetIdentifier: string;
begin
  if FTargetIdentifier = '' then
    FTargetIdentifier := ReadTargetIdentifier;
  Result := FTargetIdentifier;
end;

procedure TRLDBObject.KeyChanged;
begin
  inherited;
  FQuotedName := '';
  FIdentifier := '';
  FTargetIdentifier := '';
end;

function TRLDBObject.MakeFullName(const AQuote: Boolean): string;
var
  lCollection: TRLDBObjects;
  lItem: TRLDBObject;
  sName: string;
  sFullName: string;
begin
  Result := '';
  if AQuote then
    sName := QuotedName
  else
    sName := Name;
  if Collection is TRLDBObjects then
  begin
    lCollection := TRLDBObjects(Collection);
    if lCollection.Parent is TRLDBObject then
    begin
      lItem := TRLDBObject(lCollection.Parent);
      if AQuote then
        sFullName := lItem.QuotedFullName
      else
        sFullName := lItem.FullName;
    end
    else
      sFullName := '';
  end
  else
    sFullName := '';
  if sFullName <> '' then
    Result := Format('%s.%s', [sFullName, sName])
  else
    Result := sName;
end;

function TRLDBObject.MakeIdentifier: string;
var
  I: Integer;
  bUpNext: Boolean;
  bUpFirst: Boolean;
begin
  Result := Name;
  if Result <> '' then
  begin
    I := 1;
    bUpNext := False;
    bUpFirst := False;
    while I <= Length(Result) do
    begin
      if Result[I] = '-' then
      begin
        Delete(Result, I, 1);
        bUpNext := True;
        bUpFirst := True;
      end
      else
        if not U_System.CharInSet(Result[I], ['A'..'z', '0'..'9', '_']) then
          Delete(Result, I, 1)
        else
        begin
          if bUpNext then
            Result[I] := U_System.UpChar(Result[I]);
          bUpNext := False;
          Inc(I);
        end;
    end;

    if Result <> '' then
    begin
      if bUpFirst then
        Result[1] := U_System.UpChar(Result[1]);
      ApplyIdentifierMapping(Result);
    end;
  end;
end;

function TRLDBObject.ApplyIdentifierMapping(var AText: string): Boolean;
var
  lIdentifier: TRLIdentifier;
begin
  lIdentifier := TRLIdentifierMapping.Instance.GetIdentifier(AText);
  Result := Assigned(lIdentifier);
  if Result then
    case lIdentifier.MappingKind of
      imkReplace: AText := lIdentifier.MappingText;
      imkAddToBegin: AText := lIdentifier.MappingText + AText;
      imkAddToEnd: AText := AText + lIdentifier.MappingText;
    end;
end;

function TRLDBObject.ReadTargetIdentifier: string;
var
  I: Integer;
begin
  Result := Identifier;
  I := Length(Result);
  while (Result[I] <> '_') and (I > 0) do
    Dec(I);
  if I > 0 then
    Delete(Result, 1, I);
end;

{ TRLDBObjectArray }

function TRLDBObjectArray.GetItem(const AIndex: Integer): TRLDBObject;
begin
  Result := TRLDBObject(inherited Items[AIndex]);
end;

procedure TRLDBObjectArray.SetItem(const AIndex: Integer;
  const AValue: TRLDBObject);
begin
  inherited Items[AIndex] := AValue;
end;

{ TRLDBObjects }

function TRLDBObjects.GetItem(const AIndex: Integer): TRLDBObject;
begin
  Result := TRLDBObject(inherited Items[AIndex]);
end;

function TRLDBObjects.GetItemClass: TCollectionItemClass;
begin
  Result := TRLDBObject;
end;

function TRLDBObjects.GetItemArrayClass: TRLCollectionItemArrayClass;
begin
  Result := TRLDBObjectArray;
end;

function TRLDBObjects.ToArray: TRLDBObjectArray;
begin
  Result := TRLDBObjectArray(inherited ToArray);
end;

function TRLDBObjects.ToArray(const ANames: TRLStringList): TRLDBObjectArray;
begin
  Result := TRLDBObjectArray(inherited ToArray(ANames));
end;

end.

