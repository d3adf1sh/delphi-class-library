//=============================================================================
// U_ADOSettings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOSettings;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_Settings;                 

const
  CConnectionString = 'ConnectionString';
  
type
  // Exceptions
  ERLConnectionStringIsEmpty = class(ERLException);

  // TRLADOSettings
  TRLADOSettings = class(TRLSettings)
  private
    function GetConnectionString: string;
    procedure SetConnectionString(const AValue: string);
  protected
    function IsSerializableAttribute(const AName: string): Boolean; override;
    procedure CheckConnectionString; virtual;
  public
    procedure Validate; override;
  published
    property ConnectionString: string read GetConnectionString write SetConnectionString;
  end;

implementation

{ TRLADOSettings }

function TRLADOSettings.GetConnectionString: string;
begin
  Result := ToString;
end;

procedure TRLADOSettings.SetConnectionString(const AValue: string);
begin
  FromString(AValue);
end;

function TRLADOSettings.IsSerializableAttribute(const AName: string): Boolean;
begin
  Result := inherited IsSerializableAttribute(AName) and
    not SameText(CConnectionString, AName);
end;

procedure TRLADOSettings.CheckConnectionString;
begin
  if ConnectionString = '' then
    raise ERLConnectionStringIsEmpty.Create('ConnectionString is empty.');
end;

procedure TRLADOSettings.Validate;
begin
  inherited;
  CheckConnectionString;
end;

initialization
  RegisterClass(TRLADOSettings);

end.
