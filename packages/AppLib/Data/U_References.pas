//=============================================================================
// U_References
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_References;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_DBObjects,
  U_ReferenceFields;

type
  // Enumerations
  TRLReferenceRule = (rrNoAction, rrCascade, rrSetDefault, rrSetNull);

  // Exceptions
  ERLReferenceNotFound = class(ERLException);

  // TRLReference
  TRLReference = class(TRLDBObject)
  private
    FFields: TRLReferenceFields;
    FParentSchemaName: string;
    FParentTableName: string;
    FUpdateRule: TRLReferenceRule;
    FDeleteRule: TRLReferenceRule;
    FEnabled: Boolean;
  protected
    function CreateFields: TRLReferenceFields; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Fields: TRLReferenceFields read FFields;
    property ParentSchemaName: string read FParentSchemaName write FParentSchemaName;
    property ParentTableName: string read FParentTableName write FParentTableName;
    property UpdateRule: TRLReferenceRule read FUpdateRule write FUpdateRule;
    property DeleteRule: TRLReferenceRule read FDeleteRule write FDeleteRule;
    property Enabled: Boolean read FEnabled write FEnabled;
  end;

  // TRLReferences
  TRLReferences = class(TRLDBObjects)
  private
    function GetItem(const AIndex: Integer): TRLReference;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLReference;
    function Insert(const AIndex: Integer): TRLReference;
    function Find(const AName: string): TRLReference;
    function FindByName(const AName: string): TRLReference;
    property Items[const AIndex: Integer]: TRLReference read GetItem; default;
  end;

// Utils
function ReferenceRuleToString(const AValue: TRLReferenceRule): string;
function StringToReferenceRule(const AValue: string): TRLReferenceRule;

implementation

uses
  TypInfo;
  
// Utils
function ReferenceRuleToString(const AValue: TRLReferenceRule): string;
begin
  Result := GetEnumName(TypeInfo(TRLReferenceRule), Ord(AValue));
end;

function StringToReferenceRule(const AValue: string): TRLReferenceRule;
begin
  Result := TRLReferenceRule(GetEnumValue(TypeInfo(TRLReferenceRule), AValue));
end;

{ TRLReference }

constructor TRLReference.Create(Collection: TCollection);
begin
  inherited;
  FFields := CreateFields;
end;

destructor TRLReference.Destroy;
begin
  FreeAndNil(FFields);
  inherited;
end;

function TRLReference.CreateFields: TRLReferenceFields;
begin
  Result := TRLReferenceFields.CreateParented(Self);
end;

procedure TRLReference.Assign(Source: TPersistent);
var
  lReference: TRLReference;
begin
  inherited;
  if Source is TRLReference then
  begin
    lReference := TRLReference(Source);
    Fields.Assign(lReference.Fields);
    ParentSchemaName := lReference.ParentSchemaName;
    ParentTableName := lReference.ParentTableName;
    UpdateRule := lReference.UpdateRule;
    DeleteRule := lReference.DeleteRule;
    Enabled := lReference.Enabled;
  end;
end;

{ TRLReferences }

constructor TRLReferences.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 16;
end;

function TRLReferences.GetItem(const AIndex: Integer): TRLReference;
begin
  Result := TRLReference(inherited Items[AIndex]);
end;

function TRLReferences.GetItemClass: TCollectionItemClass;
begin
  Result := TRLReference;
end;

function TRLReferences.Add: TRLReference;
begin
  Result := TRLReference(inherited Add);
end;

function TRLReferences.Insert(const AIndex: Integer): TRLReference;
begin
  Result := TRLReference(inherited Insert(AIndex));
end;

function TRLReferences.Find(const AName: string): TRLReference;
begin
  Result := TRLReference(inherited Find(AName));
end;

function TRLReferences.FindByName(const AName: string): TRLReference;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLReferenceNotFound.CreateFmt('Reference "%s" not found.', [AName]);
end;

end.
