//=============================================================================
// U_SQL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SQL;

interface

uses
  Classes,
  U_StringList,
  U_Parameters;

type
  // TRLSQL
  TRLSQL = class(TRLStringList)
  private
    FReference: TPersistent;
    FParameters: TRLParameters;
    FUpdating: Boolean;
  protected
    procedure SetUpdateState(Updating: Boolean); override;
    procedure SetTextStr(const AValue: string); override;
    procedure Changed; override;
    procedure UpdateSQL; virtual;
    property Updating: Boolean read FUpdating;
  public
    property Reference: TPersistent read FReference write FReference;
    property Parameters: TRLParameters read FParameters write FParameters;
  end;

implementation

{ TRLSQL }

procedure TRLSQL.SetUpdateState(Updating: Boolean);
begin
  inherited;
  FUpdating := Updating;
  if not FUpdating then
    UpdateSQL;
end;

procedure TRLSQL.SetTextStr(const AValue: string);
begin
  inherited;
  UpdateSQL;
end;

procedure TRLSQL.Changed;
begin
  inherited;
  if not FUpdating then
    UpdateSQL;
end;

procedure TRLSQL.UpdateSQL;
begin
  if Assigned(FParameters) then
    FParameters.Refresh;
end;

end.
