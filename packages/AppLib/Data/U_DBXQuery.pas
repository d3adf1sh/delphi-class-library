//=============================================================================
// U_DBXQuery
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXQuery;

{ TODO -oRafael : RecordCount. }

interface

uses
  SysUtils,
  Classes,
  DB,
  SqlExpr,
  U_SQL,
  U_Parameters,
  U_Query,
  U_DBXProvider,
  U_DBXSQL,
  U_DBXParameters;

type
  // TRLDBXQuery
  TRLDBXQuery = class(TRLQuery)
  private
    FQuery: TSQLQuery;
  protected
    procedure ProviderChanged; override;
    function CreateSQL: TRLSQL; override;
    function CreateParameters: TRLParameters; override;
    function GetDataSet: TDataSet; override;
    property Query: TSQLQuery read FQuery;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Execute; override;
  end;

implementation

{ TRLDBXQuery }

constructor TRLDBXQuery.Create;
begin
  inherited;
  FQuery := TSQLQuery.Create(nil);
  SQL.Reference := FQuery.SQL;
  Parameters.Reference := FQuery.Params;
  ProviderChanged;
end;

destructor TRLDBXQuery.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FQuery);
end;

procedure TRLDBXQuery.ProviderChanged;
begin
  if Assigned(FQuery) then
  begin
    if Provider is TRLDBXProvider then
      FQuery.SQLConnection := TRLDBXProvider(Provider).Connection
    else
      FQuery.SQLConnection := nil;
  end;
end;

function TRLDBXQuery.CreateSQL: TRLSQL;
begin
  Result := TRLDBXSQL.Create;
end;

function TRLDBXQuery.CreateParameters: TRLParameters;
begin
  Result := TRLDBXParameters.CreateParented(Self);
end;

function TRLDBXQuery.GetDataSet: TDataSet;
begin
  Result := FQuery;
end;

procedure TRLDBXQuery.Execute;
begin
  CheckProvider;
  FQuery.ExecSQL;
end;

initialization
  RegisterClass(TRLDBXQuery);

end.
