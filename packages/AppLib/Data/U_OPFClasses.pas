//=============================================================================
// U_OPFClasses
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_OPFClasses;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_KeyedCollection,
  U_OPFAttributes,
  U_OPFRelationships;

type
  // Exceptions
  ERLOPFReferenceIsNull = class(ERLException);
  ERLOPFReferenceAlreadyExists = class(ERLException);
  ERLOPFReferenceNotFound = class(ERLException);
  ERLOPFRelationNameIsEmpty = class(ERLException);
  ERLOPFAttributeListIsEmpty = class(ERLException);

  // TRLOPFClass
  TRLOPFClass = class(TRLKeyedCollectionItem)
  private
    FReference: TClass;
    FSchemaName: string;
    FRelationName: string;
    FFullRelationName: string;
    FAttributes: TRLOPFAttributes;
    FRelationships: TRLOPFRelationships;
    procedure SetReference(const AValue: TClass);
    procedure SetSchemaName(const AValue: string);
    procedure SetRelationName(const AValue: string);
    function GetFullRelationMame: string;
  protected
    procedure KeyChanged; override;
    procedure KeyAlreadyExists(const AValue: string); override;
    function CreateAttributes: TRLOPFAttributes; virtual;
    function CreateRelationships: TRLOPFRelationships; virtual;
    procedure SchemaNameChanged; virtual;
    procedure RelationNameChanged; virtual;
    procedure ResetFullRelationName; virtual;
    procedure CheckReference; virtual;
    procedure CheckRelationName; virtual;
    procedure CheckAttributeCount; virtual;
    procedure CheckAttributes; virtual;
    procedure CheckRelationships; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Validate; override;
    property Reference: TClass read FReference write SetReference;
    property SchemaName: string read FSchemaName write SetSchemaName;
    property RelationName: string read FRelationName write SetRelationName;
    property FullRelationName: string read GetFullRelationMame;
    property Attributes: TRLOPFAttributes read FAttributes;
    property Relationships: TRLOPFRelationships read FRelationships;
  end;

  // TRLOPFClasses
  TRLOPFClasses = class(TRLKeyedCollection)
  private
    function GetItem(const AIndex: Integer): TRLOPFClass;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLOPFClass;
    function Insert(const AIndex: Integer): TRLOPFClass;
    function Find(const AReference: TClass): TRLOPFClass;
    function FindByReference(const AReference: TClass): TRLOPFClass;
    function Contains(const AReference: TClass): Boolean; reintroduce; virtual;
    function Remove(const AReference: TClass): Boolean; reintroduce; virtual;
    property Items[const AIndex: Integer]: TRLOPFClass read GetItem; default;
  end;

implementation

{ TRLOPFClass }

constructor TRLOPFClass.Create(Collection: TCollection);
begin
  inherited;
  FAttributes := CreateAttributes;
  FRelationships := CreateRelationships;
end;

destructor TRLOPFClass.Destroy;
begin
  FreeAndNil(FRelationships);
  FreeAndNil(FAttributes);
  inherited;
end;

procedure TRLOPFClass.SetReference(const AValue: TClass);
begin
  if Assigned(AValue) then
    SetKey(AValue.ClassName)
  else
    SetKey('');
  FReference := AValue;
end;

procedure TRLOPFClass.SetSchemaName(const AValue: string);
begin
  if FSchemaName <> AValue then
  begin
    FSchemaName := AValue;
    ResetFullRelationName;
    SchemaNameChanged;
  end;
end;

procedure TRLOPFClass.SetRelationName(const AValue: string);
begin
  if FRelationName <> AValue then
  begin
    FRelationName := AValue;
    ResetFullRelationName;
    RelationNameChanged;
  end;
end;

function TRLOPFClass.GetFullRelationMame: string;
begin
  if FFullRelationName = '' then
  begin
    if FSchemaName <> '' then
      FFullRelationName := Format('%s.%s', [FSchemaName, FRelationName])
    else
      FFullRelationName := FRelationName;
  end;

  Result := FFullRelationName;
end;

procedure TRLOPFClass.KeyChanged;
begin
  inherited;
  FReference := nil;
end;

procedure TRLOPFClass.KeyAlreadyExists(const AValue: string);
begin
  raise ERLOPFReferenceAlreadyExists.Create('Reference already exists.');
end;

function TRLOPFClass.CreateAttributes: TRLOPFAttributes;
begin
  Result := TRLOPFAttributes.CreateParented(Self);
end;

function TRLOPFClass.CreateRelationships: TRLOPFRelationships;
begin
  Result := TRLOPFRelationships.CreateParented(Self);
end;

procedure TRLOPFClass.SchemaNameChanged;
begin
end;

procedure TRLOPFClass.RelationNameChanged;
begin
end;

procedure TRLOPFClass.ResetFullRelationName;
begin
  FFullRelationName := '';
end;

procedure TRLOPFClass.CheckReference;
begin
  if not Assigned(Reference) then
    raise ERLOPFReferenceIsNull.Create('Reference is null.');
end;

procedure TRLOPFClass.CheckRelationName;
begin
  if FRelationName = '' then
    raise ERLOPFRelationNameIsEmpty.Create('RelationName is empty.');
end;

procedure TRLOPFClass.CheckAttributeCount;
begin
  if FAttributes.Count = 0 then
    raise ERLOPFAttributeListIsEmpty.Create('Attribute list is empty.');
end;

procedure TRLOPFClass.CheckAttributes;
begin
  FAttributes.Validate;
end;

procedure TRLOPFClass.CheckRelationships;
begin
  FRelationships.Validate;
end;

procedure TRLOPFClass.Validate;
begin
  inherited;
  CheckReference;
  CheckRelationName;
  CheckAttributeCount;
  CheckAttributes;
  CheckRelationships;
end;

{ TRLOPFClasses }

constructor TRLOPFClasses.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 1024;
end;

function TRLOPFClasses.GetItem(const AIndex: Integer): TRLOPFClass;
begin
  Result := TRLOPFClass(inherited Items[AIndex]);
end;

function TRLOPFClasses.GetItemClass: TCollectionItemClass;
begin
  Result := TRLOPFClass;
end;

function TRLOPFClasses.Add: TRLOPFClass;
begin
  Result := TRLOPFClass(inherited Add);
end;

function TRLOPFClasses.Insert(const AIndex: Integer): TRLOPFClass;
begin
  Result := TRLOPFClass(inherited Insert(AIndex));
end;

function TRLOPFClasses.Find(const AReference: TClass): TRLOPFClass;
begin
  if Assigned(AReference) then
    Result := TRLOPFClass(inherited Find(AReference.ClassName))
  else
    Result := TRLOPFClass(inherited Find(''));
end;

function TRLOPFClasses.FindByReference(const AReference: TClass): TRLOPFClass;
begin
  Result := Find(AReference);
  if not Assigned(Result) then
    raise ERLOPFReferenceNotFound.Create('Reference not found.');
end;

function TRLOPFClasses.Contains(const AReference: TClass): Boolean;
begin
  if Assigned(AReference) then
    Result := inherited Contains(AReference.ClassName)
  else
    Result := inherited Contains('');
end;

function TRLOPFClasses.Remove(const AReference: TClass): Boolean;
begin
  if Assigned(AReference) then
    Result := inherited Remove(AReference.ClassName)
  else
    Result := inherited Remove('');
end;

end.
