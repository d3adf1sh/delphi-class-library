//=============================================================================
// U_ADOParameters
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOParameters;

{$I RL.INC}

interface

uses
  FMTBCD,
  Classes,
  DB,
  ADODB,
{$IFDEF D10DOWN}
  Controls,
{$ENDIF}
  U_Parameters;

type
  // TRLADOParameter
  TRLADOParameter = class(TRLParameter)
  private
    function GetReference: TParameter;
    procedure SetReference(const AValue: TParameter);
  protected
    procedure KeyChanged; override;
    function GetModifier: TRLParameterModifier; override;
    function GetDataType: TFieldType; override;
    function GetSize: Integer; override;
    function GetPrecision: Integer; override;
    function GetScale: Integer; override;
    function GetAsString: string; override;
    function GetAsInteger: Integer; override;
    function GetAsCardinal: Cardinal; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Double; override;
    function GetAsCurrency: Currency; override;
    function GetAsFMTBCD: TBcd; override;
    function GetAsDate: TDate; override;
    function GetAsTime: TTime; override;
    function GetAsDateTime: TDateTime; override;
    function GetAsBoolean: Boolean; override;
    function GetAsVariant: Variant; override;
    procedure SetModifier(const AValue: TRLParameterModifier); override;
    procedure SetDataType(const AValue: TFieldType); override;
    procedure SetSize(const AValue: Integer); override;
    procedure SetPrecision(const AValue: Integer); override;
    procedure SetScale(const AValue: Integer); override;
    procedure SetAsString(const AValue: string); override;
    procedure SetAsInteger(const AValue: Integer); override;
    procedure SetAsCardinal(const AValue: Cardinal); override;
    procedure SetAsInt64(const AValue: Int64); override;
    procedure SetAsFloat(const AValue: Double); override;
    procedure SetAsCurrency(const AValue: Currency); override;
    procedure SetAsFMTBCD(const AValue: TBcd); override;
    procedure SetAsDate(const AValue: TDate); override;
    procedure SetAsTime(const AValue: TTime); override;
    procedure SetAsDateTime(const AValue: TDateTime); override;
    procedure SetAsBoolean(const AValue: Boolean); override;
    procedure SetAsVariant(const AValue: Variant); override;
  public
   	function IsNull: Boolean; override;
    procedure Clear; override;
    property Reference: TParameter read GetReference write SetReference;
  end;

  // TRLADOParameters
  TRLADOParameters = class(TRLParameters)
  private
    function GetReference: TParameters;
    procedure SetReference(const AValue: TParameters);
  protected
    function GetItemClass: TCollectionItemClass; override;
    function InsertParameter(const AIndex: Integer): TPersistent; override;
  public
    procedure Refresh; override;
    property Reference: TParameters read GetReference write SetReference;
  end;

implementation

uses
  Variants;

{ TRLADOParameter }

function TRLADOParameter.GetReference: TParameter;
begin
  Result := TParameter(inherited Reference);
end;

procedure TRLADOParameter.SetReference(const AValue: TParameter);
begin
  inherited Reference := AValue;
end;

procedure TRLADOParameter.KeyChanged;
begin
  if Assigned(Reference) then
    Reference.Name := Name;
end;

function TRLADOParameter.GetModifier: TRLParameterModifier;
begin
  if Assigned(Reference) then
    case Reference.Direction of
      pdUnknown: Result := pmUnknown;
      pdInput: Result := pmInput;
      pdOutput: Result := pmOutput;
      pdInputOutput: Result := pmInputOutput;
      pdReturnValue: Result := pmResult;
    else
      Result := pmUnknown;
    end
  else
    Result := pmUnknown;
end;

function TRLADOParameter.GetDataType: TFieldType;
begin
  if Assigned(Reference) then
    Result := Reference.DataType
  else
    Result := ftUnknown;
end;

function TRLADOParameter.GetSize: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.Size
  else
    Result := 0;
end;

function TRLADOParameter.GetPrecision: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.Precision
  else
    Result := 0;
end;

function TRLADOParameter.GetScale: Integer;
begin
  if Assigned(Reference) then
    Result := Reference.NumericScale
  else
    Result := 0;
end;

function TRLADOParameter.GetAsString: string;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := '';
end;

function TRLADOParameter.GetAsInteger: Integer;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsCardinal: Cardinal;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsInt64: Int64;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsFloat: Double;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsCurrency: Currency;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsFMTBCD: TBcd;
begin
  if not IsNull then
    Result := VarToBcd(Reference.Value)
  else
    Result := IntegerToBcd(0);
end;

function TRLADOParameter.GetAsDate: TDate;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsTime: TTime;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsDateTime: TDateTime;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

function TRLADOParameter.GetAsBoolean: Boolean;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := False;
end;

function TRLADOParameter.GetAsVariant: Variant;
begin
  if not IsNull then
    Result := Reference.Value
  else
    Result := 0;
end;

procedure TRLADOParameter.SetModifier(const AValue: TRLParameterModifier);
begin
  if Assigned(Reference) then
    case AValue of
      pmUnknown: Reference.Direction := pdUnknown; //Exception no Delphi XE
      pmInput: Reference.Direction := pdInput;
      pmOutput: Reference.Direction := pdOutput;
      pmInputOutput: Reference.Direction := pdInputOutput;
      pmResult: Reference.Direction := pdReturnValue;
    end;
end;

procedure TRLADOParameter.SetDataType(const AValue: TFieldType);
begin
  if Assigned(Reference) then
    Reference.DataType := AValue; //Exception se ftUnknow
end;

procedure TRLADOParameter.SetSize(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Size := AValue;
end;

procedure TRLADOParameter.SetPrecision(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Precision := AValue;
end;

procedure TRLADOParameter.SetScale(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.NumericScale := AValue;
end;

procedure TRLADOParameter.SetAsString(const AValue: string);
begin
  if Assigned(Reference) then
    Reference.Value := AValue; //ftWideString
end;

procedure TRLADOParameter.SetAsInteger(const AValue: Integer);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

procedure TRLADOParameter.SetAsCardinal(const AValue: Cardinal);
begin
  if Assigned(Reference) then
    Reference.Value := AValue; //ftLargeint
end;

procedure TRLADOParameter.SetAsInt64(const AValue: Int64);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

procedure TRLADOParameter.SetAsFloat(const AValue: Double);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

procedure TRLADOParameter.SetAsCurrency(const AValue: Currency);
begin
  if Assigned(Reference) then
    Reference.Value := AValue; //ftBCD
end;

procedure TRLADOParameter.SetAsFMTBCD(const AValue: TBcd);
begin
  if Assigned(Reference) then
    Reference.Value := VarFMTBcdCreate(AValue); //ftBCD
end;

procedure TRLADOParameter.SetAsDate(const AValue: TDate);
begin
  if Assigned(Reference) then
    Reference.Value := AValue; //ftFloat
end;

procedure TRLADOParameter.SetAsTime(const AValue: TTime);
begin
  if Assigned(Reference) then
    Reference.Value := AValue; //ftFloat
end;

procedure TRLADOParameter.SetAsDateTime(const AValue: TDateTime);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

procedure TRLADOParameter.SetAsBoolean(const AValue: Boolean);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

procedure TRLADOParameter.SetAsVariant(const AValue: Variant);
begin
  if Assigned(Reference) then
    Reference.Value := AValue;
end;

function TRLADOParameter.IsNull: Boolean;
begin
  if Assigned(Reference) then
    Result := VarIsNull(Reference.Value)
  else
    Result := True;
end;

procedure TRLADOParameter.Clear;
begin
  if Assigned(Reference) then
    Reference.Value := Null;
end;

{ TRLADOParameters }

function TRLADOParameters.GetReference: TParameters;
begin
  Result := TParameters(inherited Reference);
end;

procedure TRLADOParameters.SetReference(const AValue: TParameters);
begin
  inherited Reference := AValue;
end;

function TRLADOParameters.GetItemClass: TCollectionItemClass;
begin
  Result := TRLADOParameter;
end;

function TRLADOParameters.InsertParameter(const AIndex: Integer): TPersistent;
begin
  if Assigned(Reference) then
    Result := TParameter(Reference.Insert(AIndex))
  else
    Result := nil;
end;

procedure TRLADOParameters.Refresh;
var
  lParameter: TRLADOParameter;
  I: Integer;
begin
  if Assigned(Reference) then
  begin
    ClearReferences;
    BeginUpdate;
    try
      Clear;
      for I := 0 to Pred(Reference.Count) do
      begin
        lParameter := TRLADOParameter(TCollection(Self).Add);
        lParameter.Name := Reference[I].Name;
        lParameter.Reference := Reference[I];
      end;
    finally
      EndUpdate;
    end;
  end;
end;

end.
