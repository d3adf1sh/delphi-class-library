//=============================================================================
// U_Command
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Command;

interface

uses
  Classes,
  U_BaseType,
  U_Drivers,
  U_Provider,
  U_ProviderDefs;

type
  // References
  TRLCommandClass = class of TRLCommand;

  // TRLCommand
  TRLCommand = class(TRLBaseType)
  private
    FProvider: TRLProvider;
    procedure SetProvider(const AValue: TRLProvider);
  protected
    procedure ProviderChanged; virtual;
    procedure CheckProvider; virtual;
  public
    property Provider: TRLProvider read FProvider write SetProvider;
    class function GetCommand(const AName: string; const AProvider: TRLProvider): TRLCommand;
    class function GetProviderCommand(const AName: string; const AProvider: TRLProvider): TRLCommand;
    class function GetDatabaseCommand(const AName: string; const AProvider: TRLProvider): TRLCommand;
  end;

implementation

{ TRLCommand }

procedure TRLCommand.SetProvider(const AValue: TRLProvider);
begin
  if AValue <> FProvider then
  begin
    FProvider := AValue;
    ProviderChanged;
  end;
end;

procedure TRLCommand.ProviderChanged;
begin
end;

procedure TRLCommand.CheckProvider;
begin
  if not Assigned(FProvider) then
    raise ERLProviderIsNull.Create('Provider is null.');
  if not FProvider.IsOpen then
    raise ERLProviderIsNotActive.Create('Provider is not active.');
end;

class function TRLCommand.GetCommand(const AName: string;
  const AProvider: TRLProvider): TRLCommand;
var
  lClass: TRLCommandClass;
begin
  lClass := TRLCommandClass(GetClass('TRL' + AName));
  if Assigned(lClass) then
  begin
    Result := lClass.Create;
    Result.Provider := AProvider;
  end
  else
    Result := nil;
end;

class function TRLCommand.GetProviderCommand(const AName: string;
  const AProvider: TRLProvider): TRLCommand;
var
  lProvider: TRLProviderDef;
begin
  Result := nil;
  if Assigned(AProvider) then
  begin
    lProvider := TRLProviderManager.Instance.GetReferenceProvider(
      TRLProviderClass(AProvider.ClassType));
    if Assigned(lProvider) then
      Result := GetCommand(lProvider.Name + AName, AProvider);
  end;
end;

class function TRLCommand.GetDatabaseCommand(const AName: string;
  const AProvider: TRLProvider): TRLCommand;
var
  lProvider: TRLProviderDef;
  lDriver: TRLDriver;
begin
  Result := nil;
  if Assigned(AProvider) then
  begin
    lProvider := TRLProviderManager.Instance.GetReferenceProvider(
      TRLProviderClass(AProvider.ClassType));
    if Assigned(lProvider) then
    begin
      lDriver := lProvider.Drivers.Find(AProvider.DriverName);
      if Assigned(lDriver) then
        Result := GetCommand(lDriver.DatabaseName + AName, AProvider);
    end;
  end;
end;

end.
