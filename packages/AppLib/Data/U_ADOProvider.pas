//=============================================================================
// U_ADOProvider
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOProvider;

interface

uses
  SysUtils,
  ADODB,
  U_Settings,
  U_Provider,
  U_ADOSettings;

type
  // TRLADOProvider
  TRLADOProvider = class(TRLProvider)
  private
    FConnection: TADOConnection;
    function GetSettings: TRLADOSettings;
  protected
    function CreateSettings: TRLSettings; override;
    function GetDriverName: string; override;
    function CreateConnection: TADOConnection; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Open; override;
    procedure Close; override;
    function IsOpen: Boolean; override;
    procedure StartTransaction; override;
    function InTransaction: Boolean; override;
    procedure CommitTransaction; override;
    procedure RollbackTransaction; override;
    property Settings: TRLADOSettings read GetSettings;
    property Connection: TADOConnection read FConnection;
  end;

const
  RLADO = 'ADO';  

implementation

{ TRLADOProvider }

constructor TRLADOProvider.Create;
begin
  inherited;
  FConnection := CreateConnection;
end;

destructor TRLADOProvider.Destroy;
begin
  inherited; //herda para fechar primeiro
  FreeAndNil(FConnection);
end;

function TRLADOProvider.GetSettings: TRLADOSettings;
begin
  Result := TRLADOSettings(inherited Settings);
end;

function TRLADOProvider.CreateSettings: TRLSettings;
begin
  Result := TRLADOSettings.Create;
end;

function TRLADOProvider.GetDriverName: string;
begin
  Result := Settings['Provider'];
end;

function TRLADOProvider.CreateConnection: TADOConnection;
begin
  Result := TADOConnection.Create(nil);
  Result.LoginPrompt := False;
end;

procedure TRLADOProvider.Open;
begin
  if not IsOpen then
  begin
    FConnection.ConnectionString := Settings.ConnectionString;
    FConnection.Open;
  end;
end;

procedure TRLADOProvider.Close;
begin
  if IsOpen then
  begin
    RollbackTransaction;
    FConnection.Close;
  end;
end;

function TRLADOProvider.IsOpen: Boolean;
begin
  Result := FConnection.Connected;
end;

procedure TRLADOProvider.StartTransaction;
begin
  CheckActive;
  if not InTransaction then
    FConnection.BeginTrans;
end;

function TRLADOProvider.InTransaction: Boolean;
begin
  Result := FConnection.InTransaction;
end;

procedure TRLADOProvider.CommitTransaction;
begin
  if InTransaction then
    FConnection.CommitTrans;
end;

procedure TRLADOProvider.RollbackTransaction;
begin
  if InTransaction then
    FConnection.RollbackTrans;
end;

end.
