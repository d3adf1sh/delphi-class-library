//=============================================================================
// U_Schemas
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Schemas;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_DBObjects,
  U_Relations;

type
  // Exceptions
  ERLSchemaNotFound = class(ERLException);

  // TRLSchema
  TRLSchema = class(TRLDBObject)
  private
    FRelations: TRLRelations;
  protected
    function CreateRelations: TRLRelations; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Relations: TRLRelations read FRelations;
  end;

  // TRLSchemas
  TRLSchemas = class(TRLDBObjects)
  private
    function GetItem(const AIndex: Integer): TRLSchema;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor CreateParented(const AParent: TObject); override;
    function Add: TRLSchema;
    function Insert(const AIndex: Integer): TRLSchema;
    function Find(const AName: string): TRLSchema;
    function FindByName(const AName: string): TRLSchema;
    property Items[const AIndex: Integer]: TRLSchema read GetItem; default;
  end;

implementation

{ TRLSchema }

constructor TRLSchema.Create(Collection: TCollection);
begin
  inherited;
  FRelations := CreateRelations;
end;

destructor TRLSchema.Destroy;
begin
  FreeAndNil(FRelations);
  inherited;
end;

function TRLSchema.CreateRelations: TRLRelations;
begin
  Result := TRLRelations.CreateParented(Self);
end;

procedure TRLSchema.Assign(Source: TPersistent);
begin
  inherited;
  if Source is TRLSchema then
    Relations.Assign(TRLSchema(Source).Relations);
end;

{ TRLSchemas }

constructor TRLSchemas.CreateParented(const AParent: TObject);
begin
  inherited;
  HashSize := 32;
end;

function TRLSchemas.GetItem(const AIndex: Integer): TRLSchema;
begin
  Result := TRLSchema(inherited Items[AIndex]);
end;

function TRLSchemas.GetItemClass: TCollectionItemClass;
begin
  Result := TRLSchema;
end;

function TRLSchemas.Add: TRLSchema;
begin
  Result := TRLSchema(inherited Add);
end;

function TRLSchemas.Insert(const AIndex: Integer): TRLSchema;
begin
  Result := TRLSchema(inherited Insert(AIndex));
end;

function TRLSchemas.Find(const AName: string): TRLSchema;
begin
  Result := TRLSchema(inherited Find(AName));
end;

function TRLSchemas.FindByName(const AName: string): TRLSchema;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLSchemaNotFound.CreateFmt('Schema "%s" not found.', [AName]);
end;

end.
