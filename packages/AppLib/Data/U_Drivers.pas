//=============================================================================
// U_Drivers
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Drivers;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_NamedCollection,
  U_Dictionary;

type
  // References
  TRLDriverClass = class of TRLDriver;

  // Eceptions
  ERLDriverNotFound = class(ERLException);

  // TRLDriver
  TRLDriver = class(TRLNamedCollectionItem)
  private
    FDatabaseName: string;
    FSettings: TRLDictionary;
    function GetSettings: TRLDictionary;
  protected
    function CreateSettings: TRLDictionary; virtual;
  public
    destructor Destroy; override;
    property DatabaseName: string read FDatabaseName write FDatabaseName;
    property Settings: TRLDictionary read GetSettings;
  end;

  // TRLDrivers
  TRLDrivers = class(TRLNamedCollection)
  private
    function GetItem(const AIndex: Integer): TRLDriver;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    constructor Create; override;
    function Add: TRLDriver;
    function Insert(const AIndex: Integer): TRLDriver;
    function Find(const AName: string): TRLDriver;
    function FindByName(const AName: string): TRLDriver;
    property Items[const AIndex: Integer]: TRLDriver read GetItem; default;
  end;

implementation

{ TRLDriver }

destructor TRLDriver.Destroy;
begin
  if Assigned(FSettings) then
    FreeAndNil(FSettings);
  inherited;
end;

function TRLDriver.GetSettings: TRLDictionary;
begin
  if not Assigned(FSettings) then
    FSettings := CreateSettings;
  Result := FSettings;
end;

function TRLDriver.CreateSettings: TRLDictionary;
begin
  Result := TRLDictionary.Create;
  Result.HashSize := 16;
end;

{ TRLDrivers }

constructor TRLDrivers.Create;
begin
  inherited;
  HashSize := 16;
end;

function TRLDrivers.GetItem(const AIndex: Integer): TRLDriver;
begin
  Result := TRLDriver(inherited Items[AIndex]);
end;

function TRLDrivers.GetItemClass: TCollectionItemClass;
begin
  Result := TRLDriver;
end;

function TRLDrivers.Add: TRLDriver;
begin
  Result := TRLDriver(inherited Add);
end;

function TRLDrivers.Insert(const AIndex: Integer): TRLDriver;
begin
  Result := TRLDriver(inherited Insert(AIndex));
end;

function TRLDrivers.Find(const AName: string): TRLDriver;
begin
  Result := TRLDriver(inherited Find(AName));
end;

function TRLDrivers.FindByName(const AName: string): TRLDriver;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLDriverNotFound.CreateFmt('Driver "%s" not found.', [AName]);
end;

end.
