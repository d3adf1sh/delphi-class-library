//=============================================================================
// U_VCLSQL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLSQL;

interface

uses
  Classes,
  DB,
  U_SQL;

type
  // TRLVCLSQL
  TRLVCLSQL = class(TRLSQL)
  protected
    procedure UpdateSQL; override;
  end;

implementation

{ TRLVCLSQL }

procedure TRLVCLSQL.UpdateSQL;
begin
  if Assigned(Reference) then
  begin
    TStrings(Reference).Assign(Self);
    if Assigned(Parameters.Reference) then
      TParams(Parameters.Reference).ParseSQL(Text, True);
  end;

  inherited;
end;

end.
