//=============================================================================
// U_SDACSettings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACSettings;

interface

uses
  SysUtils,
  Classes,
  U_Exception,
  U_Settings;

const
  CServer = 'Server';
  CDatabase = 'Database';
  CUsername = 'Username';
  CPassword = 'Password';

type
  // Exceptions
  ERLServerIsEmpty = class(ERLException);
  ERLDatabaseIsEmpty = class(ERLException);
  ERLUsernameIsEmpty = class(ERLException);
  ERLPasswordIsEmpty = class(ERLException);

  // TRLSDACSettings
  TRLSDACSettings = class(TRLSettings)
  private
    function GetServer: string;
    procedure SetServer(const AValue: string);
    function GetDatabase: string;
    procedure SetDatabase(const AValue: string);
    function GetUsername: string;
    procedure SetUsername(const AValue: string);
    function GetPassword: string;
    procedure SetPassword(const AValue: string);
  protected
    function IsSerializableAttribute(const AName: string): Boolean; override;
    procedure CheckServer; virtual;
    procedure CheckDatabase; virtual;
    procedure CheckUsername; virtual;
    procedure CheckPassword; virtual;
  public
    procedure Validate; override;
  published
    property Server: string read GetServer write SetServer;
    property Database: string read GetDatabase write SetDatabase;
    property Username: string read GetUsername write SetUsername;
    property Password: string read GetPassword write SetPassword;
  end;

implementation

{ TRLSDACSettings }

function TRLSDACSettings.GetServer: string;
begin
  Result := Values[CServer];
end;

procedure TRLSDACSettings.SetServer(const AValue: string);
begin
  Values[CServer] := AValue;
end;

function TRLSDACSettings.GetDatabase: string;
begin
  Result := Values[CDatabase];
end;

procedure TRLSDACSettings.SetDatabase(const AValue: string);
begin
  Values[CDatabase] := AValue;
end;

function TRLSDACSettings.GetUsername: string;
begin
  Result := Values[CUsername];
end;

procedure TRLSDACSettings.SetUsername(const AValue: string);
begin
  Values[CUsername] := AValue;
end;

function TRLSDACSettings.GetPassword: string;
begin
  Result := Values[CPassword];
end;

procedure TRLSDACSettings.SetPassword(const AValue: string);
begin
  Values[CPassword] := AValue;
end;

function TRLSDACSettings.IsSerializableAttribute(const AName: string): Boolean;
begin
  Result := inherited IsSerializableAttribute(AName) and
    not SameText(CServer, AName) and
    not SameText(CDatabase, AName) and
    not SameText(CUsername, AName) and
    not SameText(CPassword, AName);
end;

procedure TRLSDACSettings.CheckServer;
begin
  if Server = '' then
    raise ERLServerIsEmpty.Create('Server is empty.');
end;

procedure TRLSDACSettings.CheckDatabase;
begin
  if Database = '' then
    raise ERLDatabaseIsEmpty.Create('Database is empty.');
end;

procedure TRLSDACSettings.CheckUsername;
begin
  if Username = '' then
    raise ERLUsernameIsEmpty.Create('Username is empty.');
end;

procedure TRLSDACSettings.CheckPassword;
begin
  if Password = '' then
    raise ERLPasswordIsEmpty.Create('Password is empty.');
end;

procedure TRLSDACSettings.Validate;
begin
  inherited;
  CheckServer;
  CheckDatabase;
  CheckUsername;
  CheckPassword;
end;

initialization
  RegisterClass(TRLSDACSettings);

end.
