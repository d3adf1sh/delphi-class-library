//=============================================================================
// U_VCLHacks
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLHacks;

interface

uses
  Classes,
  Controls,
  StdCtrls;

type
  // TRLControl
  TRLControl = class(TControl)
  public
    class function GetCaption(const AInstance: TControl): string;
    class procedure SetCaption(const AInstance: TControl; const AValue: string);
    class function GetText(const AInstance: TControl): string;
    class procedure SetText(const AInstance: TControl; const AValue: string);
    class function GetOnClick(const AInstance: TControl): TNotifyEvent;
    class procedure SetOnClick(const AInstance: TControl; const AValue: TNotifyEvent);
    { TODO -oRafael : Complementar. }
  end;

  // TRLCustomMemo
  TRLCustomMemo = class(TCustomMemo)
  public
    class function GetWantReturns(const AInstance: TControl): Boolean;
    class procedure SetWantReturns(const AInstance: TControl; const AValue: Boolean);
    { TODO -oRafael : Complementar. }
  end;

implementation

{ TRLControl }

class function TRLControl.GetCaption(const AInstance: TControl): string;
begin
  if AInstance is TControl then
    Result := TRLControl(AInstance).Caption
  else
    Result := '';
end;

class procedure TRLControl.SetCaption(const AInstance: TControl;
  const AValue: string);
begin
  if AInstance is TControl then
    TRLControl(AInstance).Caption := AValue;
end;

class function TRLControl.GetText(const AInstance: TControl): string;
begin
  if AInstance is TControl then
    Result := TRLControl(AInstance).Text
  else
    Result := '';
end;

class procedure TRLControl.SetText(const AInstance: TControl;
  const AValue: string);
begin
  if AInstance is TControl then
    TRLControl(AInstance).Text := AValue;
end;

class function TRLControl.GetOnClick(const AInstance: TControl): TNotifyEvent;
begin
  if AInstance is TControl then
    Result := TRLControl(AInstance).OnClick
  else
    Result := nil;
end;

class procedure TRLControl.SetOnClick(const AInstance: TControl;
  const AValue: TNotifyEvent);
begin
  if AInstance is TControl then
    TRLControl(AInstance).OnClick := AValue;
end;

{ TRLCustomMemo }

class function TRLCustomMemo.GetWantReturns(const AInstance: TControl): Boolean;
begin
  if AInstance is TCustomMemo then
    Result := TRLCustomMemo(AInstance).WantReturns
  else
    Result := False;
end;

class procedure TRLCustomMemo.SetWantReturns(const AInstance: TControl;
  const AValue: Boolean);
begin
  if AInstance is TCustomMemo then
    TRLCustomMemo(AInstance).WantReturns := AValue;
end;

end.
