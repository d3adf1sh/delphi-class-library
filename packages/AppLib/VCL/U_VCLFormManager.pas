//=============================================================================
// U_VCLFormManager
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLFormManager;

interface

uses
  Classes,
  Controls,
  StdCtrls,
  ComCtrls,
  U_FormManager,
  U_VCLHacks;

type
  // TRLVCLFormManager
  TRLVCLFormManager = class(TRLFormManager)
  public
    function CanSkipControl(const AControl: TControl; const AShift: TShiftState): Boolean; override;
    class function Instance: TRLVCLFormManager;
  end;

implementation

{ TRLVCLFormManager }

function TRLVCLFormManager.CanSkipControl(const AControl: TControl;
  const AShift: TShiftState): Boolean;
begin
  if AControl is TCustomMemo then
    Result := not TRLCustomMemo.GetWantReturns(AControl) or (ssCtrl in AShift)
  else if (AControl is TCustomListBox) or
    (AControl is TCustomListView) or
    (AControl is TCustomTreeView) or
    (AControl is TButton) then
    Result := ssCtrl in AShift
  else
    Result := (AControl is TCustomEdit) or
      (AControl is TCustomComboBox) or
      (AControl is TCustomCheckBox) or
      (AControl is TCustomTabControl) or
      (AControl is TRadioButton) or
      (AControl is TDateTimePicker) or
      (AControl is TMonthCalendar) or
      (AControl is TTrackBar);
end;

class function TRLVCLFormManager.Instance: TRLVCLFormManager;
begin
  Result := TRLVCLFormManager(inherited Instance);
end;

initialization
  TRLFormManager.SetInstanceType(TRLVCLFormManager);

finalization
  if TRLFormManager.GetInstanceType = TRLVCLFormManager then
    TRLFormManager.DeleteInstance;

end.
