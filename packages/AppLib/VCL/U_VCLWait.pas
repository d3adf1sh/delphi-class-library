//=============================================================================
// U_VCLWait
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_VCLWait;

interface

uses
  Windows,
  SysUtils,
  Controls,
  Forms,
  StdCtrls,
  ExtCtrls,
  U_Dialog,
  U_Wait,
  U_FormType,
  U_VCLHacks;

const
  CHorizontalMargin = 10;
  CVerticalMargin = 10;
  CHorizontalSpacing = 10;
  CVerticalSpacing = 10;
  CIconHeight = 32;
  CIconWidth = 32;
  CButtonHeight = 23;
  CButtonWidth = 60;
  CButtonCaptionSpacing = 10;

type
  // TRLVCLWaitForm
  TRLVCLWaitForm = class(TRLFormType)
  private
    FIcon: TImage;
    FMessage: TControl;
    FCancelButton: TButton;
    FCanceled: Boolean;
    FFreeing: Boolean;
    procedure CancelButtonClick(Sender: TObject);
  protected
    procedure DoShow; override;
    procedure Deactivate; override;
    property Freeing: Boolean read FFreeing;
  public
    property Icon: TImage read FIcon write FIcon;
    property Message: TControl read FMessage write FMessage;
    property CancelButton: TButton read FCancelButton write FCancelButton;
    property Canceled: Boolean read FCanceled;
  end;

  // TRLVCLWait
  TRLVCLWait = class(TRLWait)
  private
    FForm: TRLVCLWaitForm;
  protected
    procedure ShowMessage; override;
    procedure HideMessage; override;
    function MessageCanceled: Boolean; override;
    function CreateIcon: TImage; virtual;
    function CreateMessage: TControl; virtual;
    function CreateButton: TButton; virtual;
  public
    class function Instance: TRLVCLWait;
  end;

implementation

uses
  Math;

{ TRLVCLWaitForm }

procedure TRLVCLWaitForm.CancelButtonClick(Sender: TObject);
begin
  FCanceled := True;
  FCancelButton.Enabled := False;
end;

procedure TRLVCLWaitForm.DoShow;
begin
  inherited;
  if Assigned(FCancelButton) then
    FCancelButton.OnClick := CancelButtonClick;
  FCanceled := False;
end;

procedure TRLVCLWaitForm.Deactivate;
begin
  if not FFreeing then
    Show;
end;

{ TRLVCLWait }

procedure TRLVCLWait.ShowMessage;
var
  lTextRect: TRect;
  nButtonWidth: Integer;
  nTextHeight: Integer;
  nTextWidth: Integer;
  I: Integer;
begin
  if not Assigned(FForm) then
  begin
    FForm := TRLVCLWaitForm.CreateNew(nil);
    if Messages.HasCancel then
    begin
      SetRect(lTextRect, 0, 0, 0, 0);
      DrawText(FForm.Canvas.Handle, PChar(RLDialogButtonCaption[dbCancel]), -1,
        lTextRect,
          DT_CALCRECT or DT_LEFT or DT_SINGLELINE or
            FForm.DrawTextBiDiModeFlagsReadingOnly);
      nButtonWidth := lTextRect.Right - lTextRect.Left + CButtonCaptionSpacing;
      if nButtonWidth < CButtonWidth then
        nButtonWidth := CButtonWidth;
    end
    else
      nButtonWidth := 0;
    nTextHeight := 0;
    nTextWidth := 0;
    for I := 0 to Pred(Messages.Count) do
    begin
      SetRect(lTextRect, 0, 0, Screen.Width div 2, 0);
      DrawText(FForm.Canvas.Handle, PChar(Messages[I].Text),
        Succ(Length(Messages[I].Text)), lTextRect,
          DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK or
            FForm.DrawTextBiDiModeFlagsReadingOnly);
      if lTextRect.Right > nTextWidth then
        nTextWidth := lTextRect.Right;
      if lTextRect.Bottom > nTextHeight then
        nTextHeight := lTextRect.Bottom;
    end;

    if nTextHeight < CIconHeight then
      nTextHeight := CIconHeight;
    SetRect(lTextRect, 0, 0, nTextWidth, nTextHeight);
    FForm.BorderStyle := bsToolWindow;
    FForm.BorderIcons := [];
    FForm.AllowClose := False;
    FForm.Position := poScreenCenter;
    FForm.ClientHeight := lTextRect.Bottom +
      IfThen(nButtonWidth > 0, CVerticalSpacing + CButtonHeight) +
      (CVerticalMargin * 2);
    FForm.ClientWidth :=
      Max(CIconWidth + CHorizontalSpacing + lTextRect.Right,
        nButtonWidth) +
      (CHorizontalMargin * 2);
    FForm.Icon := CreateIcon;
    FForm.Icon.Parent := FForm;
    FForm.Icon.Picture.Icon.Handle := LoadIcon(MainInstance,
      PChar(RLWaitKindIcon[Messages.Kind]));
    FForm.Icon.Top := CVerticalMargin;
    FForm.Icon.Left := CHorizontalMargin;
    FForm.Icon.Height := CIconHeight;
    FForm.Icon.Width := CIconWidth;
    FForm.Message := CreateMessage;
    FForm.Message.Parent := FForm;
    FForm.Message.Top := CVerticalMargin;
    FForm.Message.Left := CHorizontalMargin + CIconWidth + CHorizontalSpacing;
    FForm.Message.Height := lTextRect.Bottom;
    FForm.Message.Width := lTextRect.Right;
    if nButtonWidth > 0 then
    begin
      FForm.CancelButton := CreateButton;
      FForm.CancelButton.Parent := FForm;
      FForm.CancelButton.Caption := RLDialogButtonCaption[dbCancel];
      FForm.CancelButton.Top := FForm.Message.Top + lTextRect.Bottom +
        CVerticalSpacing;
      FForm.CancelButton.Left := FForm.ClientWidth - nButtonWidth -
        CHorizontalMargin;
      FForm.CancelButton.Height := CButtonHeight;
      FForm.CancelButton.Width := nButtonWidth;
      FForm.CancelButton.Default := True;
    end;

    FForm.Show;
  end;

  TRLControl.SetCaption(FForm.Message, Message.Text);
  if Assigned(FForm.CancelButton) then
    FForm.CancelButton.Enabled := Message.AllowCancel;
  FForm.Refresh;
end;

procedure TRLVCLWait.HideMessage;
begin
  FForm.FFreeing := True; //for Delphi 7
  FreeAndNil(FForm);
end;

function TRLVCLWait.MessageCanceled: Boolean;
begin
  Result := FForm.Canceled;
end;

function TRLVCLWait.CreateIcon: TImage;
begin
  Result := TImage.Create(nil);
end;

function TRLVCLWait.CreateMessage: TControl;
var
  lMessage: TLabel;
begin
  lMessage := TLabel.Create(nil);
  lMessage.AutoSize := False;
  lMessage.Layout := tlCenter;
  lMessage.WordWrap := True;
  lMessage.Transparent := True;
  Result := lMessage;
end;

function TRLVCLWait.CreateButton: TButton;
begin
  Result := TButton.Create(nil);
end;

class function TRLVCLWait.Instance: TRLVCLWait;
begin
  Result := TRLVCLWait(inherited Instance);
end;

initialization
  TRLWait.SetInstanceType(TRLVCLWait);

finalization
  if TRLWait.GetInstanceType = TRLVCLWait then
    TRLWait.DeleteInstance;

end.
