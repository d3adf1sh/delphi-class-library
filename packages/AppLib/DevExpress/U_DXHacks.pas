//=============================================================================
// U_DXHacks
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DXHacks;

interface

uses
  Controls,
  cxEdit;

type
  // TRLcxCustomEdit
  TRLcxCustomEdit = class(TcxCustomEdit)
  public
    class function GetEditProperties(const AInstance: TControl): TcxCustomEditProperties;
    { TODO -oRafael : Complementar. }
  end;

implementation

{ TRLcxCustomEdit }

class function TRLcxCustomEdit.GetEditProperties(
  const AInstance: TControl): TcxCustomEditProperties;
begin
  Result := TRLcxCustomEdit(AInstance).Properties;
end;

end.
