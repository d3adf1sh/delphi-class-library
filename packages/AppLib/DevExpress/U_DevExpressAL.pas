//=============================================================================
// U_DevExpressAL
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DevExpressAL;

interface

uses
  U_DXFormManager,
  U_DXDialog,
  U_DXWait,
  U_DXSkinController;

implementation

end.
