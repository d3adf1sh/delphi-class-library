//=============================================================================
// U_DXWait
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DXWait;

interface

uses
  Controls,
  StdCtrls,
  cxButtons,
  cxEdit,
  cxLabel,
  U_Wait,
  U_VCLWait;

type
  // TRLDXWait
  TRLDXWait = class(TRLVCLWait)
  protected
    function CreateMessage: TControl; override;
    function CreateButton: TButton; override;
  public
    class function Instance: TRLDXWait;
  end;

implementation

{ TRLDXWait }

function TRLDXWait.CreateMessage: TControl;
var
  lMessage: TcxLabel;
begin
  lMessage := TcxLabel.Create(nil);
  lMessage.AutoSize := False;
  lMessage.Properties.Alignment.Vert := taVCenter;
  lMessage.Properties.WordWrap := True;
  lMessage.Transparent := True;
  Result := lMessage;
end;

function TRLDXWait.CreateButton: TButton;
begin
  Result := inherited CreateButton; //TcxButton.Create(nil);
end;

class function TRLDXWait.Instance: TRLDXWait;
begin
  Result := TRLDXWait(inherited Instance);
end;

initialization
  TRLWait.SetInstanceType(TRLDXWait);

finalization
  if TRLWait.GetInstanceType = TRLDXWait then
    TRLWait.DeleteInstance;

end.
