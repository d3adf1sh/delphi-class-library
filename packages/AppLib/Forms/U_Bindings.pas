//=============================================================================
// U_Bindings
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_Bindings;

interface

uses
  SysUtils,
  Classes,
  Controls,
  U_ArrayType,
  U_Exception,
  U_HashTable,
  U_Collection,
  U_KeyedCollection,
  U_NamedCollection,
  U_Dictionary,
  U_BindingAttributes;

type
  // Exceptions
  ERLBindingControlIsNull = class(ERLException);
  ERLBindingInstanceIsNull = class(ERLException);
  ERLBindingAttributeListIsEmpty = class(ERLException);
  ERLBindingNotFound = class(ERLException);

  // TRLBinding
  TRLBinding = class(TRLNamedCollectionItem)
  private
    FInstance: TPersistent;
    FControl: TControl;
    procedure SetInstance(const AValue: TPersistent);
    procedure SetControl(const AValue: TControl);
  protected
    procedure InstanceChanged; virtual;
    procedure ControlChanged; virtual;
    procedure CheckInstance; virtual;
    procedure CheckControl; virtual;
    function GetInstanceType: TClass; virtual;
    function GetEnabled: Boolean; virtual;
    procedure SetEnabled(const AValue: Boolean); virtual;
    function GetReadOnly: Boolean; virtual;
    procedure SetReadOnly(const AValue: Boolean); virtual;
    property InstanceType: TClass read GetInstanceType;
  public
    constructor Create(Collection: TCollection); override;
    procedure Initialize; virtual;
    function CanFocus: Boolean; virtual;
    procedure SetFocus; virtual;
    procedure Read; virtual;
    procedure Write; virtual;
    property Instance: TPersistent read FInstance write SetInstance;
    property Control: TControl read FControl write SetControl;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly;
  end;

  // TRLAttributeBinding
  TRLAttributeBinding = class(TRLBinding)
  private
    FAttribute: TRLBindingAttribute;
  protected
    procedure KeyChanged; override;
    procedure InstanceChanged; override;
    function CreateAttribute: TRLBindingAttribute; virtual;
    function ReadValue: Variant; virtual;
    function ReadChar: string; virtual;
    function ReadString: string; virtual;
    function ReadInteger: Integer; virtual;
    function ReadInt64: Int64; virtual;
    function ReadFloat: Double; virtual;
    function ReadCurrency: Currency; virtual;
    function ReadDate: TDate; virtual;
    function ReadTime: TTime; virtual;
    function ReadDateTime: TDateTime; virtual;
    function ReadBoolean: Boolean; virtual;
    function ReadVariant: Variant; virtual;
    procedure WriteValue(const AValue: Variant); virtual;
    procedure WriteChar(const AValue: Char); virtual;
    procedure WriteString(const AValue: string); virtual;
    procedure WriteInteger(const AValue: Integer); virtual;
    procedure WriteInt64(const AValue: Int64); virtual;
    procedure WriteFloat(const AValue: Double); virtual;
    procedure WriteCurrency(const AValue: Currency); virtual;
    procedure WriteDate(const AValue: TDate); virtual;
    procedure WriteTime(const AValue: TTime); virtual;
    procedure WriteDateTime(const AValue: TDateTime); virtual;
    procedure WriteBoolean(const AValue: Boolean); virtual;
    procedure WriteVariant(const AValue: Variant); virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property Attribute: TRLBindingAttribute read FAttribute;
  end;

  // TRLListBinding
  TRLListBinding = class(TRLBinding)
  private
    FHashTable: TRLHashTable;
    function GetHashSize: Integer;
    procedure SetHashSize(const AValue: Integer);
  protected
    function CreateHashTable: TRLHashTable; virtual;
    function AddList(const AInstance: TPersistent): TObject; virtual; abstract;
    procedure SelectList(const AInstance: TPersistent; const AItem: TObject); virtual; abstract;
    procedure RefreshList(const AInstance: TPersistent; const AItem: TObject); virtual; abstract;
    procedure CheckList(const AInstance: TPersistent; const AItem: TObject); virtual; abstract;
    procedure UncheckList(const AInstance: TPersistent; const AItem: TObject); virtual; abstract;
    procedure RemoveList(const AInstance: TPersistent; const AItem: TObject); virtual; abstract;
    procedure BeginListUpdate; virtual; abstract;
    procedure EndListUpdate; virtual; abstract;
    procedure ClearList; virtual; abstract;
    property HashTable: TRLHashTable read FHashTable;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Add(const AInstance: TPersistent; const ASelect: Boolean); virtual;
    function GetSelected: TPersistent; virtual;
    function GetSelection: TRLArrayType; virtual;
    function GetChecked: TRLArrayType; virtual;
    function GetUnchecked: TRLArrayType; virtual;
    procedure Select(const AInstance: TPersistent); virtual;
    procedure Refresh(const AInstance: TPersistent); virtual;
    procedure Check(const AInstance: TPersistent); virtual;
    procedure Uncheck(const AInstance: TPersistent); virtual;
    procedure Remove(const AInstance: TPersistent); virtual;
    procedure RefreshArray(const AArray: TRLArrayType); virtual;
    procedure CheckArray(const AArray: TRLArrayType); virtual;
    procedure UncheckArray(const AArray: TRLArrayType); virtual;
    procedure RemoveArray(const AArray: TRLArrayType); virtual;
    procedure RefreshSelection; virtual;
    procedure CheckSelection; virtual;
    procedure UncheckSelection; virtual;
    procedure RemoveSelection; virtual;
    procedure RefreshAll; virtual;
    procedure CheckAll; virtual;
    procedure UncheckAll; virtual;
    procedure RemoveAll; virtual;
    property HashSize: Integer read GetHashSize write SetHashSize;
  end;

  // TRLAttributesBinding
  TRLAttributesBinding = class(TRLListBinding)
  private
    FAttributes: TRLBindingAttributes;
  protected
    procedure InstanceChanged; override;
    function CreateAttributes: TRLBindingAttributes; virtual;
    procedure CheckAttributes; virtual;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Read; override;
    procedure Write; override;
    property Attributes: TRLBindingAttributes read FAttributes;
  end;

  // TRLStringsBinding
  TRLStringsBinding = class(TRLAttributeBinding)
  private
    FItems: TRLDictionary;
  protected
    function CreateItems: TRLDictionary; virtual;
    function GetStrings: TStrings; virtual; abstract;
    function GetItemIndex: Integer; virtual; abstract;
    procedure SetItemIndex(const AValue: Integer); virtual; abstract;
    procedure Populate; virtual;
    function GetValue: string; virtual;
    procedure SetValue(const AValue: string); virtual;
    property Strings: TStrings read GetStrings;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property Items: TRLDictionary read FItems;
  end;

  // TRLCollectionBinding
  TRLCollectionBinding = class(TRLAttributesBinding)
  private
    function GetInstance: TRLCollection;
    procedure SetInstance(const AValue: TRLCollection);
  protected
    function GetInstanceType: TClass; override;
    function AddList(const AInstance: TPersistent): TObject; override;
    procedure SelectList(const AInstance: TPersistent; const AItem: TObject); override;
    procedure RefreshList(const AInstance: TPersistent; const AItem: TObject); override;
    procedure CheckList(const AInstance: TPersistent; const AItem: TObject); override;
    procedure UncheckList(const AInstance: TPersistent; const AItem: TObject); override;
    procedure RemoveList(const AInstance: TPersistent; const AItem: TObject); override;
//specialized
    function AddItem(const AInstance: TRLCollectionItem): TObject; virtual; abstract;
    procedure SelectItem(const AInstance: TRLCollectionItem; const AItem: TObject); virtual; abstract;
    procedure RefreshItem(const AInstance: TRLCollectionItem; const AItem: TObject); virtual; abstract;
    procedure CheckItem(const AInstance: TRLCollectionItem; const AItem: TObject); virtual; abstract;
    procedure UncheckItem(const AInstance: TRLCollectionItem; const AItem: TObject); virtual; abstract;
    procedure RemoveItem(const AInstance: TRLCollectionItem; const AItem: TObject); virtual;
  public
    procedure Initialize; override;
    procedure Read; override;
    function GetSelected: TPersistent; override;
    function GetSelection: TRLArrayType; override;
    function GetChecked: TRLArrayType; override;
    function GetUnchecked: TRLArrayType; override;
    function GetSelectedItem: TRLCollectionItem; virtual;
    function GetSelectionItems: TRLCollectionItemArray; virtual;
    function GetCheckedItems: TRLCollectionItemArray; virtual;
    function GetUncheckedItems: TRLCollectionItemArray; virtual;
    procedure RefreshAll; override;
    procedure CheckAll; override;
    procedure UncheckAll; override;
    procedure RemoveAll; override;
    property Instance: TRLCollection read GetInstance write SetInstance;
  end;
  
  // TRLBindings
  TRLBindings = class(TRLNamedCollection)
  private
    FDefaultInstance: TPersistent;
    function GetItem(const AIndex: Integer): TRLBinding;
  protected
    function GetItemClass: TCollectionItemClass; override;
  public
    function Add: TRLBinding;
    function AddType(const AType: TCollectionItemClass): TRLBinding;
    function Insert(const AIndex: Integer): TRLBinding;
    function InsertType(const AType: TCollectionItemClass; const AIndex: Integer): TRLBinding;
    function Find(const AName: string): TRLBinding;
    function FindByName(const AName: string): TRLBinding;
    procedure Initialize(const AName: string); virtual;
    function CanFocus(const AName: string): Boolean; virtual;
    procedure SetFocus(const AName: string); virtual;
    procedure SetEnabled(const AName: string; const AValue: Boolean); virtual;
    procedure SetReadOnly(const AName: string; const AValue: Boolean); virtual;
    procedure Read(const AName: string); virtual;
    procedure Write(const AName: string); virtual;
    procedure InitializeAll; virtual;
    procedure SetEnabledAll(const AValue: Boolean); virtual;
    procedure SetReadOnlyAll(const AValue: Boolean); virtual;
    procedure ReadAll; virtual;
    procedure WriteAll; virtual;
    property DefaultInstance: TPersistent read FDefaultInstance write FDefaultInstance;
    property Items[const AIndex: Integer]: TRLBinding read GetItem; default;
  end;

implementation

uses
  U_RTTIContext;

{ TRLBinding }

constructor TRLBinding.Create(Collection: TCollection);
begin
  inherited;
  SetInstance(TRLBindings(Collection).DefaultInstance);
end;

procedure TRLBinding.SetInstance(const AValue: TPersistent);
begin
  if AValue <> FInstance then
  begin
    FInstance := AValue;
    InstanceChanged;
  end;
end;

procedure TRLBinding.SetControl(const AValue: TControl);
begin
  if AValue <> FControl then
  begin
    FControl := AValue;
    ControlChanged;
  end;
end;

procedure TRLBinding.InstanceChanged;
begin
end;

procedure TRLBinding.ControlChanged;
begin
end;

procedure TRLBinding.CheckInstance;
begin
  if not Assigned(FInstance) then
    raise ERLBindingInstanceIsNull.Create('Binding instance is null.');
end;

procedure TRLBinding.CheckControl;
begin
  if not Assigned(FControl) then
    raise ERLBindingControlIsNull.Create('Binding control is null.');
end;

function TRLBinding.GetInstanceType: TClass;
begin
  if Assigned(FInstance) then
    Result := FInstance.ClassType
  else
    Result := nil;
end;

function TRLBinding.GetEnabled: Boolean;
begin
  CheckControl;
  Result := Control.Enabled;
end;

procedure TRLBinding.SetEnabled(const AValue: Boolean);
begin
  CheckControl;
  Control.Enabled := AValue;
end;

function TRLBinding.GetReadOnly: Boolean;
begin
  CheckControl;
  Result := False;
end;

procedure TRLBinding.SetReadOnly(const AValue: Boolean);
begin
  CheckControl;
end;

procedure TRLBinding.Initialize;
begin
  CheckControl;
end;

function TRLBinding.CanFocus: Boolean;
begin
  CheckControl;
  Result := False;
end;

procedure TRLBinding.SetFocus;
begin
  CheckControl;
end;

procedure TRLBinding.Read;
begin
  CheckInstance;
  CheckControl;
end;

procedure TRLBinding.Write;
begin
  CheckInstance;
  CheckControl;
end;

{ TRLAttributeBinding }

constructor TRLAttributeBinding.Create(Collection: TCollection);
begin
  FAttribute := CreateAttribute;
  inherited; //herda para setar a inst�ncia padr�o
end;

destructor TRLAttributeBinding.Destroy;
begin
  FreeAndNil(FAttribute);
  inherited;
end;

procedure TRLAttributeBinding.KeyChanged;
begin
  inherited;
  FAttribute.Name := Name;
end;

procedure TRLAttributeBinding.InstanceChanged;
begin
  inherited;
  FAttribute.InstanceType := InstanceType;
end;

function TRLAttributeBinding.CreateAttribute: TRLBindingAttribute;
begin
  Result := TRLBindingAttribute.Create(nil);
end;

function TRLAttributeBinding.ReadValue: Variant;
begin
  Result := FAttribute.ReadValue(Instance);
end;

function TRLAttributeBinding.ReadChar: string;
begin
  Result := FAttribute.ReadChar(Instance);
end;

function TRLAttributeBinding.ReadString: string;
begin
  Result := FAttribute.ReadString(Instance);
end;

function TRLAttributeBinding.ReadInteger: Integer;
begin
  Result := FAttribute.ReadInteger(Instance);
end;

function TRLAttributeBinding.ReadInt64: Int64;
begin
  Result := FAttribute.ReadInt64(Instance);
end;

function TRLAttributeBinding.ReadFloat: Double;
begin
  Result := FAttribute.ReadFloat(Instance);
end;

function TRLAttributeBinding.ReadCurrency: Currency;
begin
  Result := FAttribute.ReadCurrency(Instance);
end;

function TRLAttributeBinding.ReadDate: TDate;
begin
  Result := FAttribute.ReadDate(Instance);
end;

function TRLAttributeBinding.ReadTime: TTime;
begin
  Result := FAttribute.ReadTime(Instance);
end;

function TRLAttributeBinding.ReadDateTime: TDateTime;
begin
  Result := FAttribute.ReadDateTime(Instance);
end;

function TRLAttributeBinding.ReadBoolean: Boolean;
begin
  Result := FAttribute.ReadBoolean(Instance);
end;

function TRLAttributeBinding.ReadVariant: Variant;
begin
  Result := FAttribute.ReadVariant(Instance);
end;

procedure TRLAttributeBinding.WriteValue(const AValue: Variant);
begin
  FAttribute.WriteValue(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteChar(const AValue: Char);
begin
  FAttribute.WriteChar(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteString(const AValue: string);
begin
  FAttribute.WriteString(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteInteger(const AValue: Integer);
begin
  FAttribute.WriteInteger(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteInt64(const AValue: Int64);
begin
  FAttribute.WriteInt64(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteFloat(const AValue: Double);
begin
  FAttribute.WriteFloat(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteCurrency(const AValue: Currency);
begin
  FAttribute.WriteCurrency(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteDate(const AValue: TDate);
begin
  FAttribute.WriteDate(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteTime(const AValue: TTime);
begin
  FAttribute.WriteTime(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteDateTime(const AValue: TDateTime);
begin
  FAttribute.WriteTime(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteBoolean(const AValue: Boolean);
begin
  FAttribute.WriteBoolean(Instance, AValue);
end;

procedure TRLAttributeBinding.WriteVariant(const AValue: Variant);
begin
  FAttribute.WriteVariant(Instance, AValue);
end;

{ TRLListBinding }

constructor TRLListBinding.Create(Collection: TCollection);
begin
  inherited;
  FHashTable := CreateHashTable;
end;

destructor TRLListBinding.Destroy;
begin
  FreeAndNil(FHashTable);
  inherited;
end;

function TRLListBinding.GetHashSize: Integer;
begin
  Result := FHashTable.Size;
end;

procedure TRLListBinding.SetHashSize(const AValue: Integer);
begin
  FHashTable.Size := AValue;
end;

function TRLListBinding.CreateHashTable: TRLHashTable;
begin
  Result := TRLHashTable.Create;
end;

procedure TRLListBinding.Add(const AInstance: TPersistent;
  const ASelect: Boolean);
var
  lItem: TObject;
begin
  CheckControl;
  if Assigned(AInstance) then
  begin
    lItem := AddList(AInstance);
    FHashTable.Add(AInstance, lItem);
    if ASelect then
      Select(AInstance);
  end;
end;

function TRLListBinding.GetSelected: TPersistent;
begin
  CheckControl;
  Result := nil;
end;

function TRLListBinding.GetSelection: TRLArrayType;
begin
  CheckControl;
  Result := nil;
end;

function TRLListBinding.GetChecked: TRLArrayType;
begin
  CheckControl;
  Result := nil;
end;

function TRLListBinding.GetUnchecked: TRLArrayType;
begin
  CheckControl;
  Result := nil;
end;

procedure TRLListBinding.Select(const AInstance: TPersistent);
var
  lItem: TObject;
begin
  CheckControl;
  if FHashTable.Find(AInstance, lItem) then
    SelectList(AInstance, lItem);
end;

procedure TRLListBinding.Refresh(const AInstance: TPersistent);
var
  lItem: TObject;
begin
  CheckControl;
  if FHashTable.Find(AInstance, lItem) then
    RefreshList(AInstance, lItem);
end;

procedure TRLListBinding.Check(const AInstance: TPersistent);
var
  lItem: TObject;
begin
  CheckControl;
  if FHashTable.Find(AInstance, lItem) then
    CheckList(AInstance, lItem);
end;

procedure TRLListBinding.Uncheck(const AInstance: TPersistent);
var
  lItem: TObject;
begin
  CheckControl;
  if FHashTable.Find(AInstance, lItem) then
    UncheckList(AInstance, lItem);
end;

procedure TRLListBinding.Remove(const AInstance: TPersistent);
var
  lItem: TObject;
begin
  CheckInstance;
  CheckControl;
  if FHashTable.Find(AInstance, lItem) then
  begin
    RemoveList(AInstance, lItem);
    FHashTable.Remove(AInstance);
  end;
end;

procedure TRLListBinding.RefreshArray(const AArray: TRLArrayType);
var
  I: Integer;
begin
  if Assigned(AArray) then
  begin
    BeginListUpdate;
    try
      for I := 0 to Pred(AArray.Count) do
        Refresh(AArray[I]);
    finally
      EndListUpdate;
    end;
  end;
end;

procedure TRLListBinding.CheckArray(const AArray: TRLArrayType);
var
  I: Integer;
begin
  if Assigned(AArray) then
  begin
    BeginListUpdate;
    try
      for I := 0 to Pred(AArray.Count) do
        Check(AArray[I]);
    finally
      EndListUpdate;
    end;
  end;
end;

procedure TRLListBinding.UncheckArray(const AArray: TRLArrayType);
var
  I: Integer;
begin
  if Assigned(AArray) then
  begin
    BeginListUpdate;
    try
      for I := 0 to Pred(AArray.Count) do
        Uncheck(AArray[I]);
    finally
      EndListUpdate;
    end;
  end;
end;

procedure TRLListBinding.RemoveArray(const AArray: TRLArrayType);
var
  I: Integer;
begin
  if Assigned(AArray) then
  begin
    BeginListUpdate;
    try
      for I := 0 to Pred(AArray.Count) do
        Remove(AArray[I]);
    finally
      EndListUpdate;
    end;
  end;
end;

procedure TRLListBinding.RefreshSelection;
var
  lSelection: TRLArrayType;
begin
  lSelection := GetSelection;
  try
    RefreshArray(lSelection);
  finally
    FreeAndNil(lSelection);
  end;
end;

procedure TRLListBinding.CheckSelection;
var
  lSelection: TRLArrayType;
begin
  lSelection := GetSelection;
  try
    CheckArray(lSelection);
  finally
    FreeAndNil(lSelection);
  end;
end;

procedure TRLListBinding.UncheckSelection;
var
  lSelection: TRLArrayType;
begin
  lSelection := GetSelection;
  try
    UncheckArray(lSelection);
  finally
    FreeAndNil(lSelection);
  end;
end;

procedure TRLListBinding.RemoveSelection;
var
  lSelection: TRLArrayType;
begin
  lSelection := GetSelection;
  try
    RemoveArray(lSelection);
  finally
    FreeAndNil(lSelection);
  end;
end;

procedure TRLListBinding.RefreshAll;
begin
  CheckInstance;
end;

procedure TRLListBinding.CheckAll;
begin
  CheckInstance;
end;

procedure TRLListBinding.UncheckAll;
begin
  CheckInstance;
end;

procedure TRLListBinding.RemoveAll;
begin
  CheckInstance;
end;

{ TRLAttributesBinding }

constructor TRLAttributesBinding.Create(Collection: TCollection);
begin
  FAttributes := CreateAttributes;
  inherited; //herda para setar a inst�ncia padr�o
end;

destructor TRLAttributesBinding.Destroy;
begin
  FreeAndNil(FAttributes);
  inherited;
end;

procedure TRLAttributesBinding.InstanceChanged;
begin
  inherited;
  FAttributes.InstanceType := InstanceType;
end;

function TRLAttributesBinding.CreateAttributes: TRLBindingAttributes;
begin
  Result := TRLBindingAttributes.Create;
end;

procedure TRLAttributesBinding.CheckAttributes;
begin
  if FAttributes.Count = 0 then
    raise ERLBindingAttributeListIsEmpty.Create(
      'Binding attribute list is empty.');
end;

procedure TRLAttributesBinding.Read;
begin
  inherited;
  CheckAttributes;
end;

procedure TRLAttributesBinding.Write;
begin
  inherited;
  CheckAttributes;
end;

{ TRLStringsBinding }

constructor TRLStringsBinding.Create(Collection: TCollection);
begin
  inherited;
  FItems := CreateItems;
end;

destructor TRLStringsBinding.Destroy;
begin
  FreeAndNil(FItems);
  inherited;
end;

function TRLStringsBinding.CreateItems: TRLDictionary;
begin
  Result := TRLDictionary.Create;
  Result.HashSize := 32;
end;

procedure TRLStringsBinding.Populate;
var
  I: Integer;
begin
  if Items.Count > 0 then
  begin
    Strings.BeginUpdate;
    try
      Strings.Clear;
      for I := 0 to Pred(FItems.Count) do
        if FItems.ValueFromIndex[I] <> '' then
          Strings.Add(FItems.ValueFromIndex[I])
        else
          Strings.Add(FItems.Keys[I]);
    finally
      Strings.EndUpdate;
    end;
  end;
end;

function TRLStringsBinding.GetValue: string;
begin
  Result := '';
  if ItemIndex <> -1 then
  begin
    if FItems.Count > 0 then
      Result := FItems.Keys[ItemIndex]
    else
      Result := Strings[ItemIndex];
  end;
end;

procedure TRLStringsBinding.SetValue(const AValue: string);
begin
  if FItems.Count > 0 then
    ItemIndex := FItems.IndexOfKey(AValue)
  else
    ItemIndex := Strings.IndexOf(AValue);
end;

{ TRLCollectionBinding }

function TRLCollectionBinding.GetInstance: TRLCollection;
begin
  Result := TRLCollection(inherited Instance);
end;

procedure TRLCollectionBinding.SetInstance(const AValue: TRLCollection);
begin
  inherited Instance := AValue;
end;

function TRLCollectionBinding.GetInstanceType: TClass;
begin
  if Assigned(Instance) then
    Result := Instance.ItemClass
  else
    Result := nil;
end;

function TRLCollectionBinding.AddList(const AInstance: TPersistent): TObject;
begin
  Result := AddItem(TRLCollectionItem(AInstance));
end;

procedure TRLCollectionBinding.SelectList(const AInstance: TPersistent;
  const AItem: TObject);
begin
  SelectItem(TRLCollectionItem(AInstance), AItem);
end;

procedure TRLCollectionBinding.RefreshList(const AInstance: TPersistent;
  const AItem: TObject);
begin
  RefreshItem(TRLCollectionItem(AInstance), AItem);
end;

procedure TRLCollectionBinding.CheckList(const AInstance: TPersistent;
  const AItem: TObject);
begin
  CheckItem(TRLCollectionItem(AInstance), AItem);
end;

procedure TRLCollectionBinding.UncheckList(const AInstance: TPersistent;
  const AItem: TObject);
begin
  UncheckItem(TRLCollectionItem(AInstance), AItem);
end;

procedure TRLCollectionBinding.RemoveList(const AInstance: TPersistent;
  const AItem: TObject);
begin
  RemoveItem(TRLCollectionItem(AInstance), AItem);
end;

procedure TRLCollectionBinding.Initialize;
begin
  inherited;
  if FInstance is TRLKeyedCollection then
    HashTable.Size := TRLKeyedCollection(FInstance).HashSize;
end;

procedure TRLCollectionBinding.Read;
var
  I: Integer;
begin
  inherited;
  HashTable.Clear;
  BeginListUpdate;
  try
    ClearList;
    for I := 0 to Pred(Instance.Count) do
      Add(Instance.Items[I], False);
  finally
    EndListUpdate;
  end;
end;

function TRLCollectionBinding.GetSelected: TPersistent;
begin
  Result := GetSelectedItem;
end;

function TRLCollectionBinding.GetSelection: TRLArrayType;
begin
  Result := GetSelectionItems;
end;

function TRLCollectionBinding.GetChecked: TRLArrayType;
begin
  Result := GetCheckedItems;
end;

function TRLCollectionBinding.GetUnchecked: TRLArrayType;
begin
  Result := GetUncheckedItems;
end;

function TRLCollectionBinding.GetSelectedItem: TRLCollectionItem;
begin
  CheckControl;
  Result := nil;
end;

function TRLCollectionBinding.GetSelectionItems: TRLCollectionItemArray;
begin
  CheckControl;
  Result := nil;
end;

function TRLCollectionBinding.GetCheckedItems: TRLCollectionItemArray;
begin
  CheckControl;
  Result := nil;
end;

function TRLCollectionBinding.GetUncheckedItems: TRLCollectionItemArray;
begin
  CheckControl;
  Result := nil;
end;

procedure TRLCollectionBinding.RefreshAll;
var
  I: Integer;
begin
  inherited;
  BeginListUpdate;
  try
    for I := 0 to Pred(Instance.Count) do
      Refresh(Instance.Items[I]);
  finally
    EndListUpdate;
  end;
end;

procedure TRLCollectionBinding.CheckAll;
var
  I: Integer;
begin
  inherited;
  BeginListUpdate;
  try
    for I := 0 to Pred(Instance.Count) do
      Check(Instance.Items[I]);
  finally
    EndListUpdate;
  end;
end;

procedure TRLCollectionBinding.UncheckAll;
var
  I: Integer;
begin
  inherited;
  BeginListUpdate;
  try
    for I := 0 to Pred(Instance.Count) do
      Uncheck(Instance.Items[I]);
  finally
    EndListUpdate;
  end;
end;

procedure TRLCollectionBinding.RemoveAll;
begin
  inherited;
  BeginListUpdate;
  try
    while Instance.Count > 0 do
      Remove(Instance.Items[0]);
  finally
    EndListUpdate;
  end;
end;

procedure TRLCollectionBinding.RemoveItem(const AInstance: TRLCollectionItem;
  const AItem: TObject);
begin
  AInstance.Free;
end;

{ TRLBindings }

function TRLBindings.GetItem(const AIndex: Integer): TRLBinding;
begin
  Result := TRLBinding(inherited Items[AIndex]);
end;

function TRLBindings.GetItemClass: TCollectionItemClass;
begin
  Result := TRLBinding;
end;

function TRLBindings.Add: TRLBinding;
begin
  Result := TRLBinding(inherited Add);
end;

function TRLBindings.AddType(const AType: TCollectionItemClass): TRLBinding;
begin
  Result := TRLBinding(inherited AddType(AType));
end;

function TRLBindings.Insert(const AIndex: Integer): TRLBinding;
begin
  Result := TRLBinding(inherited Insert(AIndex));
end;

function TRLBindings.InsertType(const AType: TCollectionItemClass;
  const AIndex: Integer): TRLBinding;
begin
  Result := TRLBinding(inherited InsertType(AType, AIndex));
end;

function TRLBindings.Find(const AName: string): TRLBinding;
begin
  Result := TRLBinding(inherited Find(AName));
end;

function TRLBindings.FindByName(const AName: string): TRLBinding;
begin
  Result := Find(AName);
  if not Assigned(Result) then
    raise ERLBindingNotFound.CreateFmt('Binding "%s" not found.', [AName]);
end;

procedure TRLBindings.Initialize(const AName: string);
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  if Assigned(lBinding) then
    lBinding.Initialize;
end;

function TRLBindings.CanFocus(const AName: string): Boolean;
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  Result := Assigned(lBinding) and lBinding.CanFocus;
end;

procedure TRLBindings.SetFocus(const AName: string);
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  if Assigned(lBinding) then
    lBinding.SetFocus;
end;

procedure TRLBindings.SetEnabled(const AName: string;
  const AValue: Boolean);
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  if Assigned(lBinding) then
    lBinding.SetEnabled(AValue);
end;

procedure TRLBindings.SetReadOnly(const AName: string;
  const AValue: Boolean);
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  if Assigned(lBinding) then
    lBinding.SetReadOnly(AValue);
end;

procedure TRLBindings.Read(const AName: string);
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  if Assigned(lBinding) then
    lBinding.Read;
end;

procedure TRLBindings.Write(const AName: string);
var
  lBinding: TRLBinding;
begin
  lBinding := Find(AName);
  if Assigned(lBinding) then
    lBinding.Write;
end;

procedure TRLBindings.InitializeAll;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Initialize;
end;

procedure TRLBindings.SetEnabledAll(const AValue: Boolean);
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].SetEnabled(AValue);
end;

procedure TRLBindings.SetReadOnlyAll(const AValue: Boolean);
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].SetReadOnly(AValue);
end;

procedure TRLBindings.ReadAll;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Read;
end;

procedure TRLBindings.WriteAll;
var
  I: Integer;
begin
  for I := 0 to Pred(Count) do
    Items[I].Write;
end;

end.
