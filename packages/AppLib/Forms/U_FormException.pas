//=============================================================================
// U_FormException
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_FormException;

interface

uses
  U_Exception;

type
  // Enumerations
  TRLFormExceptionKind = (fekError, fekWarning);

  // ERLFormException
  ERLFormException = class(ERLException)
  private
    FKind: TRLFormExceptionKind;
    FDetails: string;
  public
    constructor CreateFormException(const AMessage: string); overload; virtual;
    constructor CreateFormException(const AMessage, ADetails: string); overload; virtual;
    constructor CreateFormException(const AMessage, ADetails: string; const AKind: TRLFormExceptionKind); overload; virtual;
    property Kind: TRLFormExceptionKind read FKind write FKind;
    property Details: string read FDetails write FDetails;
  end;

implementation

{ ERLFormException }

constructor ERLFormException.CreateFormException(const AMessage: string);
begin
  CreateFormException(AMessage, '');
end;

constructor ERLFormException.CreateFormException(const AMessage,
  ADetails: string);
begin
  CreateFormException(AMessage, ADetails, fekError);
end;

constructor ERLFormException.CreateFormException(const AMessage,
  ADetails: string; const AKind: TRLFormExceptionKind);
begin
  inherited Create(AMessage);
  FKind := AKind;
  FDetails := ADetails;
end;

end.
