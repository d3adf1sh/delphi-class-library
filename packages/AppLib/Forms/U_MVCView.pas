//=============================================================================
// U_MVCView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_MVCView;

{ TODO -oRafael : Testar ThreadStarted e ThreadCompleted. }

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  U_Exception,
  U_FormException,
  U_BindingException,
  U_Bindings,
  U_Dialog,
  U_FormType,
  U_MVCAbstract;

type
  // Exceptions
  ERLMVCCannotDisplayModal = class(ERLException);

  // TRLMVCView
  TRLMVCView = class(TRLFormType, IRLMVCView)
  private
    FController: TObject;
    FControllerIntf: IRLMVCController;
    FFreeController: Boolean;
    function GetController: TObject;
    procedure SetController(const AValue: TObject);
    function GetFreeController: Boolean;
    function GetTitle: string;
    procedure SetTitle(const AValue: string);
    function GetHandle: THandle;
    function GetControllerIntf: IRLMVCController;
  protected
    procedure ControllerChanged; virtual;
    procedure CheckController; virtual;
    procedure ThreadStarted; virtual;
    procedure ThreadCompleted(const AException: Exception); virtual;
    procedure ShowException(const AException: Exception); virtual;
    property ControllerIntf: IRLMVCController read GetControllerIntf;
  public
    destructor Destroy; override;
    procedure AfterConstruction; override;
    procedure Initialize; virtual;
    procedure Display; virtual;
    function CanDisplayModal: Boolean; virtual;
    function DisplayModal: Boolean; virtual;
    property Controller: TObject read GetController write SetController;
    property FreeController: Boolean read GetFreeController;
    property Title: string read GetTitle write SetTitle;
    property Handle: THandle read GetHandle;
  end;

implementation

uses
  U_MVCController;

{ TRLMVCView }

destructor TRLMVCView.Destroy;
begin
  if Assigned(FController) and FreeController then
    FreeAndNil(FController);
  inherited;
end;

function TRLMVCView.GetController: TObject;
begin
  Result := FController;
end;

procedure TRLMVCView.SetController(const AValue: TObject);
begin
  if AValue <> FController then
  begin
    FController := AValue;
    FControllerIntf := nil;
    ControllerChanged;
  end;
end;

function TRLMVCView.GetFreeController: Boolean;
begin
  Result := FFreeController;
end;

function TRLMVCView.GetTitle: string;
begin
  Result := Caption;
end;

procedure TRLMVCView.SetTitle(const AValue: string);
begin
  Caption := AValue;
end;

function TRLMVCView.GetHandle: THandle;
begin
  Result := inherited Handle;
end;

function TRLMVCView.GetControllerIntf: IRLMVCController;
begin
  if not Assigned(FControllerIntf) then
  begin
    CheckController;
    Controller.GetInterface(IRLMVCController, FControllerIntf);
  end;

  Result := FControllerIntf;
end;

procedure TRLMVCView.ControllerChanged;
begin
end;

procedure TRLMVCView.CheckController;
begin
  if not Assigned(Controller) then
    raise ERLMVCControllerIsNull.Create('Controller is null.');
end;

procedure TRLMVCView.ThreadStarted;
begin
end;

procedure TRLMVCView.ThreadCompleted(const AException: Exception);
begin
  ShowException(AException);
end;

procedure TRLMVCView.ShowException(const AException: Exception);
var
  lFormException: ERLFormException;
  lBindingException: ERLBindingException;
  lBinding: TRLBinding;
  lKind: TRLDialogKind;
begin
  if Assigned(AException) then
  begin
    if AException is ERLFormException then
    begin
      lFormException := ERLFormException(AException);
      if lFormException.Kind = fekWarning then
        lKind := dkWarning
      else
        lKind := dkError;
      TRLDialog.Instance.Show(lFormException.Message, lKind, [dbOk], dbOk,
        lFormException.Details);
      if lFormException is ERLBindingException and Assigned(ControllerIntf) then
      begin
        lBindingException := ERLBindingException(lFormException);
        lBinding := ControllerIntf.GetBindings.Find(
          lBindingException.BindingName);
        if lBinding.CanFocus then
          lBinding.SetFocus;
      end;
    end
    else
      TRLDialog.Instance.Show(AException.Message, dkError, [dbOk], dbOk);
  end;
end;

procedure TRLMVCView.AfterConstruction;
begin
  inherited;
  AllowSkipControl := True;
end;

procedure TRLMVCView.Initialize;
begin
end;

procedure TRLMVCView.Display;
begin
  if CanDisplayModal then
    DisplayModal
  else
  begin
    FFreeController := True;
    WindowState := wsNormal;
    Visible := True;
    BringToFront;
  end;
end;

function TRLMVCView.CanDisplayModal: Boolean;
begin
  Result := PreviousFormStyle = fsNormal;
end;

function TRLMVCView.DisplayModal: Boolean;
begin
  if not CanDisplayModal then
    raise ERLMVCCannotDisplayModal.Create('Cannot display modal.');
  FFreeController := False;
  Result := ShowModal = mrOk;
end;

end.
