//=============================================================================
// U_FormType
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_FormType;

interface

uses
  Windows,
  Messages,
  Classes,
  Controls,
  Forms,
  U_FormManager;

type
  // References
  TRLFormTypeClass = class of TRLFormType;

  // TRLFormType
  TRLFormType = class(TForm)
  private
    FPreviousFormStyle: TFormStyle;
    FAllowEscape: Boolean;
    FAllowSkipControl: Boolean;
    FDestroyed: Boolean;
    function GetAllowClose: Boolean;
    procedure SetAllowClose(const AValue: Boolean);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoClose(var Action: TCloseAction); override;
    procedure PeekMessage; virtual;
    function CanSkipControl(const AControl: TControl; const AShift: TShiftState): Boolean; virtual;
    property Destroyed: Boolean read FDestroyed;
  public
    destructor Destroy; override;
    procedure AfterConstruction; override;
    function CloseQuery: Boolean; override;
    property PreviousFormStyle: TFormStyle read FPreviousFormStyle;
    property AllowClose: Boolean read GetAllowClose write SetAllowClose;
    property AllowEscape: Boolean read FAllowEscape write FAllowEscape;
    property AllowSkipControl: Boolean read FAllowSkipControl write FAllowSkipControl;
    { TODO -oRafael : Complementar. }
  end;

implementation

uses
  Math;

{ TRLFormType }

destructor TRLFormType.Destroy;
begin
  FDestroyed := True;
  inherited;
end;

function TRLFormType.GetAllowClose: Boolean;
begin
  Result := not GetMenuState(GetSystemMenu(Handle, False), SC_CLOSE,
    MF_BYCOMMAND) and MF_GRAYED <> 0;
end;

procedure TRLFormType.SetAllowClose(const AValue: Boolean);
begin
  EnableMenuItem(GetSystemMenu(Handle, False), SC_CLOSE,
    IfThen(AValue, MF_ENABLED, MF_GRAYED));
end;

procedure TRLFormType.CreateParams(var Params: TCreateParams);
begin
  inherited;
  FPreviousFormStyle := FormStyle;
  if (TRLFormManager.Instance.EnvironmentStyle = esSDI) and
    (FPreviousFormStyle = fsMDIChild) then
  begin
    FormStyle := fsNormal;
    Visible := False;
  end;
end;

procedure TRLFormType.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_ESCAPE:
      if FAllowEscape and not Assigned(Parent) then
        ModalResult := mrCancel;
    VK_RETURN:
      if FAllowSkipControl and CanSkipControl(ActiveControl, Shift) then
      begin
        Key := 0;
        SelectNext(ActiveControl, True, True);
      end;
  end;

  if Key = 0 then
    PeekMessage;
end;

procedure TRLFormType.DoClose(var Action: TCloseAction);
begin
  inherited;
  if FPreviousFormStyle in [fsMDIChild, fsStayOnTop] then
    Action := caFree;
end;

procedure TRLFormType.PeekMessage;
var
  lMsg: TMsg;
begin
  Windows.PeekMessage(lMsg, 0, WM_CHAR, WM_CHAR, PM_REMOVE);
end;

function TRLFormType.CanSkipControl(const AControl: TControl;
  const AShift: TShiftState): Boolean;
begin
  Result := TRLFormManager.Instance.CanSkipControl(AControl, AShift);
end;

procedure TRLFormType.AfterConstruction;
var
  lMetrics: TNonClientMetrics;
begin
  inherited;
  KeyPreview := True;
  if TRLFormManager.Instance.UseSystemFont then
  begin
    lMetrics.cbSize := SizeOf(lMetrics);
    if SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, @lMetrics, 0) then
      Font.Handle := CreateFontIndirect(lMetrics.lfMessageFont);
  end;

  if (TRLFormManager.Instance.EnvironmentStyle = esSDI) and
    (FPreviousFormStyle = fsMDIChild) and
    TRLFormManager.Instance.ZoomWindows and
    Assigned(Application.MainForm) then
  begin
    Top := Application.MainForm.Top + Application.MainForm.Height;
    Left := Application.MainForm.Left;
    Height := Screen.WorkAreaHeight - Top;
    Width := Screen.WorkAreaWidth - Left;
  end;
end;

function TRLFormType.CloseQuery: Boolean;
begin
  Result := inherited CloseQuery and AllowClose;
end;

end.
