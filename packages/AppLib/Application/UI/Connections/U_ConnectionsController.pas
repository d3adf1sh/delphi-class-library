//=============================================================================
// U_ConnectionsController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ConnectionsController;

interface                  

uses
  SysUtils,
  Classes,
  U_ProviderDefs,
  U_Connections,
  U_FormException,
  U_BindingException,
  U_BindingAttributes,
  U_Bindings,
  U_MVCController,
  U_ConnectionsIntf,
  U_ConnectionController,
  U_SettingsController;

const
  CConnections = 'Connections';
  
type
  // TRLConnectionsController
  TRLConnectionsController = class(TRLMVCController)
  private
    FConnections: TRLConnections;
    FConnectionsBinding: TRLCollectionBinding;
    function GetConnectionsView: IRLConnectionsView;
    procedure ReadProviderName(Sender: TObject; Instance: TPersistent; var AValue: Variant);
  protected
    function CreateConnections: TRLConnections; virtual;
    procedure CheckConnection; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; override;
    function New: Boolean; virtual;
    function Edit: Boolean; virtual;
    function Configure: Boolean; virtual;
    procedure Test; virtual;
    procedure Remove; virtual;
    property ConnectionsView: IRLConnectionsView read GetConnectionsView;
    property Connections: TRLConnections read FConnections;
  end;

implementation

uses
  Variants;

{ TRLConnectionsController }

constructor TRLConnectionsController.Create;
begin
  inherited;
  FConnections := CreateConnections;
  Title := 'Edit connections';
end;

destructor TRLConnectionsController.Destroy;
begin
  FreeAndNil(FConnections);
  inherited;
end;

function TRLConnectionsController.GetConnectionsView: IRLConnectionsView;
begin
  View.GetInterface(IRLConnectionsView, Result);
end;

procedure TRLConnectionsController.ReadProviderName(Sender: TObject;
  Instance: TPersistent; var AValue: Variant);
var
  lProvider: TRLProviderDef;
begin
  lProvider := TRLProviderManager.Instance.GetProvider(VarToStr(AValue));
  if Assigned(lProvider) then
    AValue := lProvider.Description;
end;

function TRLConnectionsController.CreateConnections: TRLConnections;
begin
  Result := TRLConnections.Create;
end;

procedure TRLConnectionsController.CheckConnection;
begin
  if FConnectionsBinding.GetSelected = nil then
    raise ERLBindingException.CreateBindingException('Select a connection.',
      CConnections);
end;

procedure TRLConnectionsController.Initialize;
var
  lProviderName: TRLBindingAttribute;
begin
  inherited;
  Bindings.DefaultInstance := FConnections;
  FConnectionsBinding := ConnectionsView.GetConnectionsBinding;
  FConnectionsBinding.Name := CConnections;
  FConnectionsBinding.Attributes.Add.Name := 'Name';
  FConnectionsBinding.Attributes.Add.Name := 'Description';
  lProviderName := FConnectionsBinding.Attributes.Add;
  lProviderName.Name := 'ProviderName';
  lProviderName.OnReadValue := ReadProviderName;
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

function TRLConnectionsController.New: Boolean;
var
  lController: TRLConnectionController;
  lConnection: TRLConnection;
begin
  lController := TRLConnectionController.Create;
  try
    lController.Title := 'New connection';
    lController.Connections := FConnections;
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
    begin
      lConnection := FConnections.Add;
      lConnection.Assign(lController.Connection);
      FConnectionsBinding.Add(lConnection, True);
    end;
  finally
    FreeAndNil(lController);
  end;
end;

function TRLConnectionsController.Edit: Boolean;
var
  lController: TRLConnectionController;
begin
  CheckConnection;
  lController := TRLConnectionController.Create;
  try
    lController.Title := 'Edit connection';
    lController.Connection.Assign(FConnectionsBinding.GetSelected);
    lController.Connections := FConnections;
    lController.ConnectionID := FConnectionsBinding.GetSelectedItem.ID;
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
    begin
      FConnectionsBinding.GetSelected.Assign(lController.Connection);
      FConnectionsBinding.RefreshSelection;
    end;
  finally
    FreeAndNil(lController);
  end;
end;

function TRLConnectionsController.Configure: Boolean;
var
  lConnection: TRLConnection;
  lController: TRLSettingsController;
begin
  CheckConnection;
  lConnection := TRLConnection(FConnectionsBinding.GetSelected);
  lController := TRLSettingsController.FindSettingsController(
    lConnection.ProviderName);
  try
    lController.Settings.Assign(lConnection.Settings);
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
      lConnection.Settings.Assign(lController.Settings);
  finally
    FreeAndNil(lController);
  end;
end;

procedure TRLConnectionsController.Test;
var
  sError: string;
begin
  CheckConnection;
  if not TRLConnection(FConnectionsBinding.GetSelected).Test(sError) then
    raise ERLFormException.CreateFormException('Connection error.', sError,
      fekError);
end;

procedure TRLConnectionsController.Remove;
begin
  CheckConnection;
  FConnectionsBinding.RemoveSelection;
end;

initialization
  RegisterClass(TRLConnectionsController);

end.
