//=============================================================================
// U_ConnectionsView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ConnectionsView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxLookAndFeels,
  cxTL,
  U_Bindings,
  U_Dialog,
  U_MVCDynamicView,
  U_DXBindings,
  U_ConnectionsIntf,
  U_ConnectionsController;

type
  // TRLConnectionsView
  TRLConnectionsView = class(TRLMVCDynamicView, IRLConnectionsView)
  private
    tlConnections: TcxTreeList;
    tlcConnectionsName: TcxTreeListColumn;
    tlcConnectionsDescription: TcxTreeListColumn;
    tlcConnectionsProviderName: TcxTreeListColumn;
    btnNew: TButton;
    btnEdit: TButton;
    btnConfigure: TButton;
    btnTest: TButton;
    btnRemove: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    function GetController: TRLConnectionsController;
    procedure SetController(const AValue: TRLConnectionsController);
    function GetConnectionsBinding: TRLCollectionBinding;
    procedure tlConnectionsFocusedNodeChanged(Sender: TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure tlConnectionsDblClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnConfigureClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLConnectionsController read GetController write SetController;
  end;

implementation

{ TRLConnectionsView }

function TRLConnectionsView.GetController: TRLConnectionsController;
begin
  Result := TRLConnectionsController(inherited Controller);
end;

procedure TRLConnectionsView.SetController(
  const AValue: TRLConnectionsController);
begin
  inherited Controller := AValue;
end;

function TRLConnectionsView.GetConnectionsBinding: TRLCollectionBinding;
begin
  Result := TRLcxTreeListBinding.CreateBinding(Controller.Bindings);
  Result.Control := tlConnections;
end;

procedure TRLConnectionsView.tlConnectionsFocusedNodeChanged(
  Sender: TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
  btnEdit.Enabled := Assigned(tlConnections.FocusedNode);
  btnConfigure.Enabled := btnEdit.Enabled;
  btnTest.Enabled := btnEdit.Enabled;
  btnRemove.Enabled := btnEdit.Enabled;
end;

procedure TRLConnectionsView.tlConnectionsDblClick(Sender: TObject);
begin
  if btnEdit.Enabled then
    btnEditClick(nil);
end;

procedure TRLConnectionsView.btnNewClick(Sender: TObject);
begin
  try
    Controller.New;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionsView.btnEditClick(Sender: TObject);
begin
  try
    Controller.Edit;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionsView.btnConfigureClick(Sender: TObject);
begin
  try
    Controller.Configure;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionsView.btnTestClick(Sender: TObject);
begin
  try
    btnTest.Enabled := False;
    Screen.Cursor := crHourGlass;
    try
      Controller.Test;
    finally
      Screen.Cursor := crDefault;
      btnTest.Enabled := True;
    end;

    TRLDialog.Instance.Show('Successfully connected.');
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionsView.btnRemoveClick(Sender: TObject);
begin
  try
    Controller.Remove;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionsView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionsView.CreateControls;
begin
  inherited;
  tlConnections := TcxTreeList.Create(Self);
  tlConnections.Parent := Self;
  tlConnections.Top := 5;
  tlConnections.Left := 5;
  tlConnections.Height := 220;
  tlConnections.Width := 550;
  tlConnections.LookAndFeel.Kind := lfFlat;
  tlConnections.OptionsView.FocusRect := False;
  tlConnections.OptionsView.ShowRoot := False;
  tlConnections.OptionsSelection.InvertSelect := False;
  tlConnections.OptionsBehavior.ImmediateEditor := False;
  tlConnections.OptionsCustomizing.ColumnVertSizing := False;
  tlConnections.OnDblClick := tlConnectionsDblClick;
  tlConnections.OnFocusedNodeChanged := tlConnectionsFocusedNodeChanged;
  tlcConnectionsName := tlConnections.CreateColumn;
  tlcConnectionsName.Caption.Text := 'Name';
  tlcConnectionsName.Width := 100;
  tlcConnectionsDescription := tlConnections.CreateColumn;
  tlcConnectionsDescription.Caption.Text := 'Description';
  tlcConnectionsDescription.Width := 200;
  tlcConnectionsProviderName := tlConnections.CreateColumn;
  tlcConnectionsProviderName.Caption.Text := 'Provider';
  tlcConnectionsProviderName.Width := 100;
  btnNew := TButton.Create(Self);
  btnNew.Parent := Self;
  btnNew.Caption := '&New...';
  btnNew.Top := 230;
  btnNew.Left := 5;
  btnNew.Height := 23;
  btnNew.Width := 65;
  btnNew.OnClick := btnNewClick;
  btnEdit := TButton.Create(Self);
  btnEdit.Parent := Self;
  btnEdit.Caption := '&Edit...';
  btnEdit.Top := 230;
  btnEdit.Left := 75;
  btnEdit.Height := 23;
  btnEdit.Width := 65;
  btnEdit.Enabled := False;
  btnEdit.OnClick := btnEditClick;
  btnConfigure := TButton.Create(Self);
  btnConfigure.Parent := Self;
  btnConfigure.Caption := 'Con&figure...';
  btnConfigure.Top := 230;
  btnConfigure.Left := 145;
  btnConfigure.Height := 23;
  btnConfigure.Width := 75;
  btnConfigure.Enabled := False;
  btnConfigure.OnClick := btnConfigureClick;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test';
  btnTest.Top := 230;
  btnTest.Left := 225;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.Enabled := False;
  btnTest.OnClick := btnTestClick;
  btnRemove := TButton.Create(Self);
  btnRemove.Parent := Self;
  btnRemove.Caption := '&Remove';
  btnRemove.Top := 230;
  btnRemove.Left := 295;
  btnRemove.Height := 23;
  btnRemove.Width := 65;
  btnRemove.Enabled := False;
  btnRemove.OnClick := btnRemoveClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 230;
  btnOk.Left := 420;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 230;
  btnCancel.Left := 490;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLConnectionsView.AfterConstruction;
begin
  inherited;
  Name := 'RLConnectionsView';
  ClientHeight := 250;
  ClientWidth := 550;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
end;

initialization
  RegisterClass(TRLConnectionsView);

end.
