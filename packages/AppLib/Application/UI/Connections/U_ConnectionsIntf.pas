//=============================================================================
// U_ConnectionsIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ConnectionsIntf;

interface

uses
  U_Bindings;

type
  // IRLConnectionsView
  IRLConnectionsView = interface
    ['{63AC6DE2-284D-4385-8FAF-BF06E83E0297}']
    function GetConnectionsBinding: TRLCollectionBinding;
  end;

implementation

end.
