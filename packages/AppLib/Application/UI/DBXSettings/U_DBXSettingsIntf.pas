//=============================================================================
// U_DBXSettingsIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXSettingsIntf;

interface

uses
  U_Bindings;

type
  // IRLDBXSettingsView
  IRLDBXSettingsView = interface
    ['{311E340C-41DA-4488-8BAE-C368F6605AE2}']
    function GetDriverNameBinding: TRLStringsBinding;
    function GetDriverFunctionBinding: TRLAttributeBinding;
    function GetLibraryNameBinding: TRLAttributeBinding;
    function GetVendorLibraryBinding: TRLAttributeBinding;
    function GetConnectionParametersBinding: TRLAttributeBinding;
  end;

implementation

end.
