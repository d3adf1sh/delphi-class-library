//=============================================================================
// U_DBXSettingsController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXSettingsController;

interface

uses
  SysUtils,
  Classes,
  U_Settings,
  U_ProviderDefs,
  U_DBXDriver,
  U_DBXSettings,
  U_DBXProvider,
  U_BindingException,
  U_Bindings,
  U_DBXSettingsIntf,
  U_SettingsController;

const
  CDriverName = 'DriverName';
  CDriverFunction = 'DriverFunction';
  CLibraryName = 'LibraryName';
  CVendorLibrary = 'VendorLibrary';
  CConnectionParameters = 'ConnectionParameters';

type
  // Forward declarations
  TRLDBXSettingsController = class;

  // TRLDBXSettings
  TRLDBXSettings = class(U_DBXSettings.TRLDBXSettings)
  private
    FController: TRLDBXSettingsController;
  protected
    procedure DriverNameChanged; override;
  public
    property Controller: TRLDBXSettingsController read FController write FController;
  end;

  // TRLDBXSettingsController
  TRLDBXSettingsController = class(TRLSettingsController)
  private
  private
    FDriverName: TRLStringsBinding;
    FDriverFunction: TRLAttributeBinding;
    FLibraryName: TRLAttributeBinding;
    FVendorLibrary: TRLAttributeBinding;
    FConnectionParameters: TRLAttributeBinding;
    function GetDBXSettingsView: IRLDBXSettingsView;
    function GetSettings: TRLDBXSettings;
  protected
    function CreateSettings: TRLSettings; override;
    function GetProviderName: string; override;
    procedure Validate; override;
    procedure ValidateDriverName; virtual;
  public
    constructor Create; override;
    procedure Initialize; override;
    procedure Restore; virtual;
    property DBXSettingsView: IRLDBXSettingsView read GetDBXSettingsView;
    property Settings: TRLDBXSettings read GetSettings;
  end;

implementation

{ TRLDBXSettings }

procedure TRLDBXSettings.DriverNameChanged;
begin
  inherited;
  if Assigned(FController) then
    FController.Restore;
end;

{ TRLDBXSettingsController }

constructor TRLDBXSettingsController.Create;
begin
  inherited;
  Title := 'Configure connection';
end;

function TRLDBXSettingsController.GetDBXSettingsView: IRLDBXSettingsView;
begin
  View.GetInterface(IRLDBXSettingsView, Result);
end;

function TRLDBXSettingsController.GetSettings: TRLDBXSettings;
begin
  Result := TRLDBXSettings(inherited Settings);
end;

function TRLDBXSettingsController.CreateSettings: TRLSettings;
begin
  Result := TRLDBXSettings.Create;
  TRLDBXSettings(Result).Controller := Self;
end;

function TRLDBXSettingsController.GetProviderName: string;
begin
  Result := RLDBX;
end;

procedure TRLDBXSettingsController.Validate;
begin
  ValidateDriverName;
  try
    inherited;
  except
    on E: ERLDriverFunctionIsEmpty do
      raise ERLBindingException.CreateBindingException(
        'Driver function is empty.', CDriverFunction);
    on E: ERLLibraryNameIsEmpty do
      raise ERLBindingException.CreateBindingException('Library name is empty.',
        CLibraryName);
    on E: ERLVendorLibraryIsEmpty do
      raise ERLBindingException.CreateBindingException(
        'Vendor library is empty.', CVendorLibrary);
    on E: ERLConnectionParametersIsEmpty do
      raise ERLBindingException.CreateBindingException(
       'Connection parameters is empty.', CConnectionParameters);
    on E: Exception do
      raise;
  end;
end;

procedure TRLDBXSettingsController.ValidateDriverName;
begin
  if Settings.DriverName = '' then
    raise ERLBindingException.CreateBindingException('Select a driver.',
      CDriverName);
end;

procedure TRLDBXSettingsController.Initialize;
var
  lProvider: TRLProviderDef;
  I: Integer;
begin
  inherited;
  Bindings.DefaultInstance := Settings;
  FDriverName := DBXSettingsView.GetDriverNameBinding;
  FDriverName.Name := CDriverName;
  lProvider := TRLProviderManager.Instance.Providers.Find(ProviderName);
  if Assigned(lProvider) then
    for I := 0 to Pred(lProvider.Drivers.Count) do
      FDriverName.Items.Add(lProvider.Drivers[I].Name,
        lProvider.Drivers[I].Name);
  FDriverFunction := DBXSettingsView.GetDriverFunctionBinding;
  FDriverFunction.Name := CDriverFunction;
  FLibraryName := DBXSettingsView.GetLibraryNameBinding;
  FLibraryName.Name := CLibraryName;
  FVendorLibrary := DBXSettingsView.GetVendorLibraryBinding;
  FVendorLibrary.Name := CVendorLibrary;
  FConnectionParameters := DBXSettingsView.GetConnectionParametersBinding;
  FConnectionParameters.Name := CConnectionParameters;
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

procedure TRLDBXSettingsController.Restore;
var
  lProvider: TRLProviderDef;
  lDriver: TRLDBXDriver;
begin
  if Assigned(FDriverName) then
  begin
    ValidateDriverName;
    lProvider := TRLProviderManager.Instance.Providers.Find(ProviderName);
    if Assigned(lProvider) then
    begin
      lDriver := TRLDBXDriver(lProvider.Drivers.Find(Settings.DriverName));
      if Assigned(lDriver) then
      begin
        Settings.DriverFunction := lDriver.DriverFunction;
        Settings.LibraryName := lDriver.LibraryName;
        Settings.VendorLibrary := lDriver.VendorLibrary;
        Settings.Assign(lDriver.Settings);
        FDriverFunction.Read;
        FLibraryName.Read;
        FVendorLibrary.Read;
        FConnectionParameters.Read;
      end;
    end;
  end;
end;

initialization
  RegisterClass(TRLDBXSettingsController);

end.
