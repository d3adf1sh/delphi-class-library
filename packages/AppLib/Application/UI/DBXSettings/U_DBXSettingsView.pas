//=============================================================================
// U_DBXSettingsView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXSettingsView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxDropDownEdit,
  cxEdit,
  cxLabel,
  cxMemo,
  cxTextEdit,
  U_Bindings,
  U_Dialog,
  U_MVCDynamicView,
  U_DXBindings,
  U_DBXSettingsIntf,
  U_DBXSettingsController;

type
  // TRLDBXSettingsView
  TRLDBXSettingsView = class(TRLMVCDynamicView, IRLDBXSettingsView)
  private
    cbDriverName: TcxComboBox;
    lblDriverName: TcxLabel;
    edtDriverFunction: TcxTextEdit;
    lblDriverFunction: TcxLabel;
    edtLibraryName: TcxTextEdit;
    lblLibraryName: TcxLabel;
    edtVendorLibrary: TcxTextEdit;
    lblVendorLibrary: TcxLabel;
    mConnectionParameters: TcxMemo;
    lblConnectionParameters: TcxLabel;
    btnRestore: TButton;
    btnTest: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    FDriverName: TRLStringsBinding;
    function GetController: TRLDBXSettingsController;
    procedure SetController(const AValue: TRLDBXSettingsController);
    function GetDriverNameBinding: TRLStringsBinding;
    function GetDriverFunctionBinding: TRLAttributeBinding;
    function GetLibraryNameBinding: TRLAttributeBinding;
    function GetVendorLibraryBinding: TRLAttributeBinding;
    function GetConnectionParametersBinding: TRLAttributeBinding;
    procedure cbDriverNamePropertiesChange(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLDBXSettingsController read GetController write SetController;
  end;

implementation

{ TRLDBXSettingsView }

function TRLDBXSettingsView.GetController: TRLDBXSettingsController;
begin
  Result := TRLDBXSettingsController(inherited Controller);
end;

procedure TRLDBXSettingsView.SetController(
  const AValue: TRLDBXSettingsController);
begin
  inherited Controller := AValue;
end;

function TRLDBXSettingsView.GetDriverNameBinding: TRLStringsBinding;
begin
  FDriverName := TRLcxComboBoxBinding.CreateBinding(Controller.Bindings);
  FDriverName.Control := cbDriverName;
  Result := FDriverName;
end;

function TRLDBXSettingsView.GetDriverFunctionBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtDriverFunction;
end;

function TRLDBXSettingsView.GetLibraryNameBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtLibraryName;
end;

function TRLDBXSettingsView.GetVendorLibraryBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtVendorLibrary;
end;

function TRLDBXSettingsView.GetConnectionParametersBinding: TRLAttributeBinding;
begin
  Result := TRLcxMemoBinding.CreateBinding(Controller.Bindings);
  Result.Control := mConnectionParameters;
end;

procedure TRLDBXSettingsView.cbDriverNamePropertiesChange(Sender: TObject);
begin
  FDriverName.Write;
end;

procedure TRLDBXSettingsView.btnRestoreClick(Sender: TObject);
begin
  try
    Controller.Restore;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLDBXSettingsView.btnTestClick(Sender: TObject);
begin
  try
    btnTest.Enabled := False;
    Screen.Cursor := crHourGlass;
    try
      Controller.Test;
    finally
      Screen.Cursor := crDefault;
      btnTest.Enabled := True;
    end;

    TRLDialog.Instance.Show('Successfully connected.');
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLDBXSettingsView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLDBXSettingsView.CreateControls;
begin
  inherited;
  cbDriverName := TcxComboBox.Create(Self);
  cbDriverName.Parent := Self;
  cbDriverName.Top := 10;
  cbDriverName.Left := 145;
  cbDriverName.Width := 125;
  cbDriverName.Style.BorderStyle := ebsFlat;
  cbDriverName.Properties.DropDownListStyle := lsFixedList;
  cbDriverName.Properties.ImmediatePost := True;
  cbDriverName.Properties.OnChange := cbDriverNamePropertiesChange;
  lblDriverName := TcxLabel.Create(Self);
  lblDriverName.Parent := Self;
  lblDriverName.Caption := '&Driver:';
  lblDriverName.FocusControl := cbDriverName;
  lblDriverName.Top := 15;
  lblDriverName.Left := 10;
  lblDriverName.Transparent := True;
  lblDriverName.Style.TransparentBorder := False;
  edtDriverFunction := TcxTextEdit.Create(Self);
  edtDriverFunction.Parent := Self;
  edtDriverFunction.Top := 40;
  edtDriverFunction.Left := 145;
  edtDriverFunction.Width := 125;
  edtDriverFunction.Style.BorderStyle := ebsFlat;
  lblDriverFunction := TcxLabel.Create(Self);
  lblDriverFunction.Parent := Self;
  lblDriverFunction.Caption := 'Driver &function:';
  lblDriverFunction.FocusControl := edtDriverFunction;
  lblDriverFunction.Top := 45;
  lblDriverFunction.Left := 10;
  lblDriverFunction.Transparent := True;
  lblDriverFunction.Style.TransparentBorder := False;
  edtLibraryName := TcxTextEdit.Create(Self);
  edtLibraryName.Parent := Self;
  edtLibraryName.Top := 70;
  edtLibraryName.Left := 145;
  edtLibraryName.Width := 125;
  edtLibraryName.Style.BorderStyle := ebsFlat;
  lblLibraryName := TcxLabel.Create(Self);
  lblLibraryName.Parent := Self;
  lblLibraryName.Caption := '&Library name:';
  lblLibraryName.FocusControl := edtLibraryName;
  lblLibraryName.Top := 75;
  lblLibraryName.Left := 10;
  lblLibraryName.Transparent := True;
  lblLibraryName.Style.TransparentBorder := False;
  edtVendorLibrary := TcxTextEdit.Create(Self);
  edtVendorLibrary.Parent := Self;
  edtVendorLibrary.Top := 100;
  edtVendorLibrary.Left := 145;
  edtVendorLibrary.Width := 125;
  edtVendorLibrary.Style.BorderStyle := ebsFlat;
  lblVendorLibrary := TcxLabel.Create(Self);
  lblVendorLibrary.Parent := Self;
  lblVendorLibrary.Caption := '&Vendor library:';
  lblVendorLibrary.FocusControl := edtVendorLibrary;
  lblVendorLibrary.Top := 105;
  lblVendorLibrary.Left := 10;
  lblVendorLibrary.Transparent := True;
  lblVendorLibrary.Style.TransparentBorder := False;
  mConnectionParameters := TcxMemo.Create(Self);
  mConnectionParameters.Parent := Self;
  mConnectionParameters.Top := 130;
  mConnectionParameters.Left := 145;
  mConnectionParameters.Height := 100;
  mConnectionParameters.Width := 300;
  mConnectionParameters.Style.BorderStyle := ebsFlat;
  mConnectionParameters.Properties.ScrollBars := ssVertical;
  mConnectionParameters.Properties.WantTabs := True;
  lblConnectionParameters := TcxLabel.Create(Self);
  lblConnectionParameters.Parent := Self;
  lblConnectionParameters.Caption := '&Connection &parameters:';
  lblConnectionParameters.FocusControl := mConnectionParameters;
  lblConnectionParameters.Top := 135;
  lblConnectionParameters.Left := 10;
  lblConnectionParameters.Transparent := True;
  lblConnectionParameters.Style.TransparentBorder := False;
  btnRestore := TButton.Create(Self);
  btnRestore.Parent := Self;
  btnRestore.Caption := '&Restore';
  btnRestore.Top := 240;
  btnRestore.Left := 5;
  btnRestore.Height := 23;
  btnRestore.Width := 65;
  btnRestore.OnClick := btnRestoreClick;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test';
  btnTest.Top := 240;
  btnTest.Left := 75;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.OnClick := btnTestClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 240;
  btnOk.Left := 320;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 240;
  btnCancel.Left := 390;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLDBXSettingsView.AfterConstruction;
begin
  inherited;
  Name := 'RLDBXSettingsView';
  ClientHeight := 260;
  ClientWidth := 450;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  ActiveControl := cbDriverName;
  AllowEscape := True;
end;

initialization
  RegisterClass(TRLDBXSettingsView);

end.
