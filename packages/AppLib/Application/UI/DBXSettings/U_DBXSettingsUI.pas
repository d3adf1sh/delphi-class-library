//=============================================================================
// U_DBXSettingsUI
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DBXSettingsUI;

interface

uses
  U_DBXSettingsController,
  U_DBXSettingsView;

implementation

end.
