 //=============================================================================
// U_EmailAccountsController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountsController;

interface

uses
  SysUtils,
  Classes,
  U_EmailAccounts,
  U_BindingException,
  U_Bindings,
  U_MVCController,
  U_EmailAccountsIntf,
  U_EmailAccountController,
  U_TestEmailAccountController;

const
  CEmailAccounts = 'EmailAccounts';
  
type
  // TRLEmailAccountsController
  TRLEmailAccountsController = class(TRLMVCController)
  private
    FEmailAccounts: TRLEmailAccounts;
    FEmailAccountsBinding: TRLCollectionBinding;
    function GetEmailAccountsView: IRLEmailAccountsView;
  protected
    function CreateEmailAccounts: TRLEmailAccounts; virtual;
    procedure CheckEmailAccount; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; override;
    function New: Boolean; virtual;
    function Edit: Boolean; virtual;
    function Test: Boolean; virtual;
    procedure Remove; virtual;
    property EmailAccountsView: IRLEmailAccountsView read GetEmailAccountsView;
    property EmailAccounts: TRLEmailAccounts read FEmailAccounts;
  end;

implementation

{ TRLEmailAccountsController }

constructor TRLEmailAccountsController.Create;
begin
  inherited;
  FEmailAccounts := CreateEmailAccounts;
  Title := 'Edit email accounts';
end;

destructor TRLEmailAccountsController.Destroy;
begin
  FreeAndNil(FEmailAccounts);
  inherited;
end;

function TRLEmailAccountsController.GetEmailAccountsView: IRLEmailAccountsView;
begin
  View.GetInterface(IRLEmailAccountsView, Result);
end;

function TRLEmailAccountsController.CreateEmailAccounts: TRLEmailAccounts;
begin
  Result := TRLEmailAccounts.Create;
end;

procedure TRLEmailAccountsController.CheckEmailAccount;
begin
  if FEmailAccountsBinding.GetSelected = nil then
    raise ERLBindingException.CreateBindingException('Select an email account.',
      CEmailAccounts);
end;

procedure TRLEmailAccountsController.Initialize;
begin
  inherited;
  Bindings.DefaultInstance := FEmailAccounts;
  FEmailAccountsBinding := EmailAccountsView.GetEmailAccountsBinding;
  FEmailAccountsBinding.Name := CEmailAccounts;
  FEmailAccountsBinding.Attributes.Add.Name := 'Name';
  FEmailAccountsBinding.Attributes.Add.Name := 'Description';
  FEmailAccountsBinding.Attributes.Add.Name := 'Email';
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

function TRLEmailAccountsController.New: Boolean;
var
  lController: TRLEmailAccountController;
  lEmailAccount: TRLEmailAccount;
begin
  lController := TRLEmailAccountController.Create;
  try
    lController.Title := 'New email account';
    lController.EmailAccounts := FEmailAccounts;
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
    begin
      lEmailAccount := FEmailAccounts.Add;
      lEmailAccount.Assign(lController.EmailAccount);
      FEmailAccountsBinding.Add(lEmailAccount, True);
    end;
  finally
    FreeAndNil(lController);
  end;
end;

function TRLEmailAccountsController.Edit: Boolean;
var
  lController: TRLEmailAccountController;
begin
  CheckEmailAccount;
  lController := TRLEmailAccountController.Create;
  try
    lController.Title := 'Edit email account';
    lController.EmailAccount.Assign(FEmailAccountsBinding.GetSelected);
    lController.EmailAccounts := FEmailAccounts;
    lController.EmailAccountID := FEmailAccountsBinding.GetSelectedItem.ID;
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
    begin
      FEmailAccountsBinding.GetSelected.Assign(lController.EmailAccount);
      FEmailAccountsBinding.RefreshSelection;
    end;
  finally
    FreeAndNil(lController);
  end;
end;

function TRLEmailAccountsController.Test: Boolean;
var
  lController: TRLTestEmailAccountController;
begin
  CheckEmailAccount;
  lController := TRLTestEmailAccountController.Create;
  try
    lController.EmailAccount := TRLEmailAccount(
      FEmailAccountsBinding.GetSelected);
    lController.Initialize;
    Result := lController.DisplayModal;
  finally
    FreeAndNil(lController);
  end;
end;

procedure TRLEmailAccountsController.Remove;
begin
  CheckEmailAccount;
  FEmailAccountsBinding.RemoveSelection;
end;

initialization
  RegisterClass(TRLEmailAccountsController);

end.
