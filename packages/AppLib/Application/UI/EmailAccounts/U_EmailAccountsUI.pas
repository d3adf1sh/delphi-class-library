//=============================================================================
// U_EmailAccountsUI
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountsUI;

interface

uses
  U_EmailAccountsController,
  U_EmailAccountsView;

implementation

end.
