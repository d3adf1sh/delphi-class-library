//=============================================================================
// U_EmailAccountsIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountsIntf;

interface

uses
  U_Bindings;

type
  // IRLEmailAccountsView
  IRLEmailAccountsView = interface
    ['{C90E9FAA-7859-457C-BEBA-04DB430E7A8F}']
    function GetEmailAccountsBinding: TRLCollectionBinding;
  end;

implementation

end.
