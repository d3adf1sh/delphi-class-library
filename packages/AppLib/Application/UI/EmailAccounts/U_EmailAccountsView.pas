//=============================================================================
// U_EmailAccountsView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountsView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxLookAndFeels,
  cxTL,
  U_Bindings,
  U_MVCDynamicView,
  U_DXBindings,
  U_EmailAccountsIntf,
  U_EmailAccountsController;

type
  // TRLEmailAccountsView
  TRLEmailAccountsView = class(TRLMVCDynamicView, IRLEmailAccountsView)
  private
    tlEmailAccounts: TcxTreeList;
    tlcEmailAccountsName: TcxTreeListColumn;
    tlcEmailAccountsDescription: TcxTreeListColumn;
    tlcEmailAccountsEmail: TcxTreeListColumn;
    btnNew: TButton;
    btnEdit: TButton;
    btnTest: TButton;
    btnRemove: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    function GetController: TRLEmailAccountsController;
    procedure SetController(const AValue: TRLEmailAccountsController);
    function GetEmailAccountsBinding: TRLCollectionBinding;
    procedure tlEmailAccountsFocusedNodeChanged(Sender: TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
    procedure tlEmailAccountsDblClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLEmailAccountsController read GetController write SetController;
  end;

implementation

{ TRLEmailAccountsView }

function TRLEmailAccountsView.GetController: TRLEmailAccountsController;
begin
  Result := TRLEmailAccountsController(inherited Controller);
end;

procedure TRLEmailAccountsView.SetController(
  const AValue: TRLEmailAccountsController);
begin
  inherited Controller := AValue;
end;

function TRLEmailAccountsView.GetEmailAccountsBinding: TRLCollectionBinding;
begin
  Result := TRLcxTreeListBinding.CreateBinding(Controller.Bindings);
  Result.Control := tlEmailAccounts;
end;

procedure TRLEmailAccountsView.tlEmailAccountsFocusedNodeChanged(
  Sender: TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
  btnEdit.Enabled := Assigned(tlEmailAccounts.FocusedNode);
  btnTest.Enabled := btnEdit.Enabled;
  btnRemove.Enabled := btnEdit.Enabled;
end;

procedure TRLEmailAccountsView.tlEmailAccountsDblClick(Sender: TObject);
begin
  if btnEdit.Enabled then
    btnEditClick(nil);
end;

procedure TRLEmailAccountsView.btnNewClick(Sender: TObject);
begin
  try
    Controller.New;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountsView.btnEditClick(Sender: TObject);
begin
  try
    Controller.Edit;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountsView.btnTestClick(Sender: TObject);
begin
  try
    Controller.Test;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountsView.btnRemoveClick(Sender: TObject);
begin
  try
    Controller.Remove;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountsView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountsView.CreateControls;
begin
  inherited;
  tlEmailAccounts := TcxTreeList.Create(Self);
  tlEmailAccounts.Parent := Self;
  tlEmailAccounts.Top := 5;
  tlEmailAccounts.Left := 5;
  tlEmailAccounts.Height := 220;
  tlEmailAccounts.Width := 500;
  tlEmailAccounts.LookAndFeel.Kind := lfFlat;
  tlEmailAccounts.OptionsView.FocusRect := False;
  tlEmailAccounts.OptionsView.ShowRoot := False;
  tlEmailAccounts.OptionsSelection.InvertSelect := False;
  tlEmailAccounts.OptionsBehavior.ImmediateEditor := False;
  tlEmailAccounts.OptionsCustomizing.ColumnVertSizing := False;
  tlEmailAccounts.OnDblClick := tlEmailAccountsDblClick;
  tlEmailAccounts.OnFocusedNodeChanged := tlEmailAccountsFocusedNodeChanged;
  tlcEmailAccountsName := tlEmailAccounts.CreateColumn;
  tlcEmailAccountsName.Caption.Text := 'Name';
  tlcEmailAccountsName.Width := 100;
  tlcEmailAccountsDescription := tlEmailAccounts.CreateColumn;
  tlcEmailAccountsDescription.Caption.Text := 'Description';
  tlcEmailAccountsDescription.Width := 200;
  tlcEmailAccountsEmail := tlEmailAccounts.CreateColumn;
  tlcEmailAccountsEmail.Caption.Text := 'Email';
  tlcEmailAccountsEmail.Width := 150;
  btnNew := TButton.Create(Self);
  btnNew.Parent := Self;
  btnNew.Caption := '&New...';
  btnNew.Top := 230;
  btnNew.Left := 5;
  btnNew.Height := 23;
  btnNew.Width := 65;
  btnNew.OnClick := btnNewClick;
  btnEdit := TButton.Create(Self);
  btnEdit.Parent := Self;
  btnEdit.Caption := '&Edit...';
  btnEdit.Top := 230;
  btnEdit.Left := 75;
  btnEdit.Height := 23;
  btnEdit.Width := 65;
  btnEdit.Enabled := False;
  btnEdit.OnClick := btnEditClick;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test...';
  btnTest.Top := 230;
  btnTest.Left := 145;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.Enabled := False;
  btnTest.OnClick := btnTestClick;
  btnRemove := TButton.Create(Self);
  btnRemove.Parent := Self;
  btnRemove.Caption := '&Remove';
  btnRemove.Top := 230;
  btnRemove.Left := 215;
  btnRemove.Height := 23;
  btnRemove.Width := 65;
  btnRemove.Enabled := False;
  btnRemove.OnClick := btnRemoveClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 230;
  btnOk.Left := 370;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 230;
  btnCancel.Left := 440;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLEmailAccountsView.AfterConstruction;
begin
  inherited;
  Name := 'RLEmailAccountsView';
  ClientHeight := 250;
  ClientWidth := 500;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
end;

initialization
  RegisterClass(TRLEmailAccountsView);

end.
