//=============================================================================
// U_SDACSettingsView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACSettingsView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxEdit,
  cxLabel,
  cxTextEdit,
  U_Bindings,
  U_Dialog,
  U_MVCDynamicView,
  U_DXBindings,
  U_SDACSettingsIntf,
  U_SDACSettingsController;

type
  // TRLSDACSettingsView
  TRLSDACSettingsView = class(TRLMVCDynamicView, IRLSDACSettingsView)
  private
    edtServer: TcxTextEdit;
    lblServer: TcxLabel;
    edtDatabase: TcxTextEdit;
    lblDatabase: TcxLabel;
    edtUsername: TcxTextEdit;
    lblUsername: TcxLabel;
    edtPassword: TcxTextEdit;
    lblPassword: TcxLabel;
    btnTest: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    function GetController: TRLSDACSettingsController;
    procedure SetController(const AValue: TRLSDACSettingsController);
    function GetServerBinding: TRLAttributeBinding;
    function GetDatabaseBinding: TRLAttributeBinding;
    function GetUsernameBinding: TRLAttributeBinding;
    function GetPasswordBinding: TRLAttributeBinding;
    procedure btnTestClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLSDACSettingsController read GetController write SetController;
  end;

implementation

{ TRLSDACSettingsView }

function TRLSDACSettingsView.GetController: TRLSDACSettingsController;
begin
  Result := TRLSDACSettingsController(inherited Controller);
end;

procedure TRLSDACSettingsView.SetController(
  const AValue: TRLSDACSettingsController);
begin
  inherited Controller := AValue;
end;

function TRLSDACSettingsView.GetServerBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtServer;
end;

function TRLSDACSettingsView.GetDatabaseBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtDatabase;
end;

function TRLSDACSettingsView.GetUsernameBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtUsername;
end;

function TRLSDACSettingsView.GetPasswordBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtPassword;
end;

procedure TRLSDACSettingsView.btnTestClick(Sender: TObject);
begin
  try
    btnTest.Enabled := False;
    Screen.Cursor := crHourGlass;
    try
      Controller.Test;
    finally
      Screen.Cursor := crDefault;
      btnTest.Enabled := True;
    end;

    TRLDialog.Instance.Show('Successfully connected.');
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLSDACSettingsView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLSDACSettingsView.CreateControls;
begin
  inherited;
  edtServer := TcxTextEdit.Create(Self);
  edtServer.Parent := Self;
  edtServer.Top := 10;
  edtServer.Left := 75;
  edtServer.Width := 150;
  edtServer.Style.BorderStyle := ebsFlat;
  lblServer := TcxLabel.Create(Self);
  lblServer.Parent := Self;
  lblServer.Caption := '&Server:';
  lblServer.FocusControl := edtServer;
  lblServer.Top := 15;
  lblServer.Left := 10;
  lblServer.Transparent := True;
  lblServer.Style.TransparentBorder := False;
  edtDatabase := TcxTextEdit.Create(Self);
  edtDatabase.Parent := Self;
  edtDatabase.Top := 40;
  edtDatabase.Left := 75;
  edtDatabase.Width := 150;
  edtDatabase.Style.BorderStyle := ebsFlat;
  lblDatabase := TcxLabel.Create(Self);
  lblDatabase.Parent := Self;
  lblDatabase.Caption := '&Database:';
  lblDatabase.FocusControl := edtDatabase;
  lblDatabase.Top := 45;
  lblDatabase.Left := 10;
  lblDatabase.Transparent := True;
  lblDatabase.Style.TransparentBorder := False;
  edtUsername := TcxTextEdit.Create(Self);
  edtUsername.Parent := Self;
  edtUsername.Top := 70;
  edtUsername.Left := 75;
  edtUsername.Width := 150;
  edtUsername.Style.BorderStyle := ebsFlat;
  lblUsername := TcxLabel.Create(Self);
  lblUsername.Parent := Self;
  lblUsername.Caption := '&Username:';
  lblUsername.FocusControl := edtUsername;
  lblUsername.Top := 75;
  lblUsername.Left := 10;
  lblUsername.Transparent := True;
  lblUsername.Style.TransparentBorder := False;
  edtPassword := TcxTextEdit.Create(Self);
  edtPassword.Parent := Self;
  edtPassword.Top := 100;
  edtPassword.Left := 75;
  edtPassword.Width := 150;
  edtPassword.Style.BorderStyle := ebsFlat;
  edtPassword.Properties.EchoMode := eemPassword;
  lblPassword := TcxLabel.Create(Self);
  lblPassword.Parent := Self;
  lblPassword.Caption := '&Password:';
  lblPassword.FocusControl := edtPassword;
  lblPassword.Top := 105;
  lblPassword.Left := 10;
  lblPassword.Transparent := True;
  lblPassword.Style.TransparentBorder := False;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test';
  btnTest.Top := 135;
  btnTest.Left := 5;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.OnClick := btnTestClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 135;
  btnOk.Left := 120;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 135;
  btnCancel.Left := 190;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLSDACSettingsView.AfterConstruction;
begin
  inherited;
  Name := 'RLSDACSettingsView';
  ClientHeight := 155;
  ClientWidth := 250;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  ActiveControl := edtServer;
  AllowEscape := True;
end;

initialization
  RegisterClass(TRLSDACSettingsView);

end.
