//=============================================================================
// U_SDACSettingsUI
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACSettingsUI;

interface

uses
  U_SDACSettingsController,
  U_SDACSettingsView;

implementation

end.
