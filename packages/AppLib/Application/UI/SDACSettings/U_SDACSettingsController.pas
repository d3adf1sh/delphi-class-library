//=============================================================================
// U_SDACSettingsController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACSettingsController;

interface

uses
  SysUtils,
  Classes,
  U_Settings,
  U_SDACSettings,
  U_SDACProvider,
  U_BindingException,
  U_SDACSettingsIntf,
  U_SettingsController;

const
  CServer = 'Server';
  CDatabase = 'Database';
  CUsername = 'Username';
  CPassword = 'Password';
  
type
  // TRLSDACSettingsController
  TRLSDACSettingsController = class(TRLSettingsController)
  private
    function GetSDACSettingsView: IRLSDACSettingsView;
    function GetSettings: TRLSDACSettings;
  protected
    function CreateSettings: TRLSettings; override;
    function GetProviderName: string; override;
    procedure Validate; override;
  public
    constructor Create; override;
    procedure Initialize; override;
    property SDACSettingsView: IRLSDACSettingsView read GetSDACSettingsView;
    property Settings: TRLSDACSettings read GetSettings;
  end;

implementation

{ TRLSDACSettingsController }

constructor TRLSDACSettingsController.Create;
begin
  inherited;
  Title := 'Configure connection';
end;

function TRLSDACSettingsController.GetSDACSettingsView: IRLSDACSettingsView;
begin
  View.GetInterface(IRLSDACSettingsView, Result);
end;

function TRLSDACSettingsController.GetSettings: TRLSDACSettings;
begin
  Result := TRLSDACSettings(inherited Settings);
end;

function TRLSDACSettingsController.CreateSettings: TRLSettings;
begin
  Result := TRLSDACSettings.Create;
end;

function TRLSDACSettingsController.GetProviderName: string;
begin
  Result := RLSDAC;
end;

procedure TRLSDACSettingsController.Validate;
begin
  try
    inherited;
  except
    on E: ERLServerIsEmpty do
      raise ERLBindingException.CreateBindingException('Server is empty.',
        CServer);
    on E: ERLDatabaseIsEmpty do
      raise ERLBindingException.CreateBindingException('Database is empty.',
        CDatabase);
    on E: ERLUsernameIsEmpty do
      raise ERLBindingException.CreateBindingException('Username is empty.',
        CUsername);
    on E: ERLPasswordIsEmpty do
      raise ERLBindingException.CreateBindingException('Password is empty.',
        CPassword);
    on E: Exception do
      raise;
  end;
end;

procedure TRLSDACSettingsController.Initialize;
begin
  inherited;
  Bindings.DefaultInstance := Settings;
  SDACSettingsView.GetServerBinding.Name := CServer;
  SDACSettingsView.GetDatabaseBinding.Name := CDatabase;
  SDACSettingsView.GetUsernameBinding.Name := CUsername;
  SDACSettingsView.GetPasswordBinding.Name := CPassword;
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

initialization
  RegisterClass(TRLSDACSettingsController);

end.
