//=============================================================================
// U_SDACSettingsIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SDACSettingsIntf;

interface

uses
  U_Bindings;

type
  // IRLSDACSettingsView
  IRLSDACSettingsView = interface
    ['{2C3E4393-019A-4DC0-AE29-7E9A74C914C0}']
    function GetServerBinding: TRLAttributeBinding;
    function GetDatabaseBinding: TRLAttributeBinding;
    function GetUsernameBinding: TRLAttributeBinding;
    function GetPasswordBinding: TRLAttributeBinding;
  end;

implementation

end.
