//=============================================================================
// U_EmailAccountController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountController;

interface

uses
  SysUtils,
  Classes,
  U_EmailAccounts,
  U_BindingException,
  U_Bindings,
  U_MVCController,
  U_EmailAccountIntf,
  U_TestEmailAccountController;

const
  CName = 'Name';
  CDescription = 'Description';
  CEmail = 'Email';
  CHost = 'Host';
  CPort = 'Port';
  CUserName = 'UserName';
  CPassword = 'Password';
  CTLS = 'TLS';
  
type
  // TRLEmailAccountController
  TRLEmailAccountController = class(TRLMVCController)
  private
    FEmailAccount: TRLEmailAccount;
    FEmailAccounts: TRLEmailAccounts;
    FEmailAccountID: Integer;
    function GetEmailAccountView: IRLEmailAccountView;
  protected
    procedure Validate; override;
    procedure ValidateName; virtual;
    function CreateEmailAccount: TRLEmailAccount; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; override;
    function Test: Boolean; virtual;
    property EmailAccountView: IRLEmailAccountView read GetEmailAccountView;
    property EmailAccount: TRLEmailAccount read FEmailAccount;
    property EmailAccounts: TRLEmailAccounts read FEmailAccounts write FEmailAccounts;
    property EmailAccountID: Integer read FEmailAccountID write FEmailAccountID;
  end;

implementation

{ TRLEmailAccountController }

constructor TRLEmailAccountController.Create;
begin
  inherited;
  FEmailAccount := CreateEmailAccount;
  FEmailAccountID := -1;
end;

destructor TRLEmailAccountController.Destroy;
begin
  FreeAndNil(FEmailAccount);
  inherited;
end;

function TRLEmailAccountController.GetEmailAccountView: IRLEmailAccountView;
begin
  View.GetInterface(IRLEmailAccountView, Result);
end;

procedure TRLEmailAccountController.Validate;
begin
  inherited;
  ValidateName;
  try
    FEmailAccount.Validate;
  except
    on E: ERLEmailIsEmpty do
      raise ERLBindingException.CreateBindingException('Email is empty.',
        CEmail);
    on E: ERLHostIsEmpty do
      raise ERLBindingException.CreateBindingException('Host is empty.', CHost);
    on E: ERLInvalidPort do
      raise ERLBindingException.CreateBindingException('Invalid port.', CPort);
    on E: Exception do
      raise;
  end;
end;

procedure TRLEmailAccountController.ValidateName;
var
  lEmailAccount: TRLEmailAccount;
begin
  if FEmailAccount.Name = '' then
    raise ERLBindingException.CreateBindingException('Name is empty.', CName);
  if Assigned(FEmailAccounts) then
  begin
    lEmailAccount := FEmailAccounts.Find(FEmailAccount.Name);
    if Assigned(lEmailAccount) and (lEmailAccount.ID <> FEmailAccountID) then
      raise ERLBindingException.CreateBindingException('Name already exists.',
        CName);
  end;
end;

function TRLEmailAccountController.CreateEmailAccount: TRLEmailAccount;
begin
  Result := TRLEmailAccount.Create(nil);
end;

procedure TRLEmailAccountController.Initialize;
var
  lTLS: TRLStringsBinding;
begin
  inherited;
  Bindings.DefaultInstance := FEmailAccount;
  EmailAccountView.GetNameBinding.Name := CName;
  EmailAccountView.GetDescriptionBinding.Name := CDescription;
  EmailAccountView.GetEmailBinding.Name := CEmail;
  EmailAccountView.GetHostBinding.Name := CHost;
  EmailAccountView.GetPortBinding.Name := CPort;
  EmailAccountView.GetUserNameBinding.Name := CUserName;
  EmailAccountView.GetPasswordBinding.Name := CPassword;
  lTLS := EmailAccountView.GetTLSBinding;
  lTLS.Name := CTLS;
  PopulateTLSDictionary(lTLS.Items);
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

function TRLEmailAccountController.Test: Boolean;
var
  lController: TRLTestEmailAccountController;
begin
  Bindings.WriteAll;
  Validate;
  lController := TRLTestEmailAccountController.Create;
  try
    lController.EmailAccount := FEmailAccount;
    lController.Initialize;
    Result := lController.DisplayModal;
  finally
    FreeAndNil(lController);
  end;
end;

initialization
  RegisterClass(TRLEmailAccountController);

end.
