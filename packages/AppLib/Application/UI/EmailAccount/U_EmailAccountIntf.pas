//=============================================================================
// U_EmailAccountIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountIntf;

interface

uses
  U_Bindings;

type
  // IRLEmailAccountView
  IRLEmailAccountView = interface
    ['{478331A5-5D69-4B5F-98AC-C9EB5A1C20E1}']
    function GetNameBinding: TRLAttributeBinding;
    function GetDescriptionBinding: TRLAttributeBinding;
    function GetEmailBinding: TRLAttributeBinding;
    function GetHostBinding: TRLAttributeBinding;
    function GetPortBinding: TRLAttributeBinding;
    function GetUserNameBinding: TRLAttributeBinding;
    function GetPasswordBinding: TRLAttributeBinding;
    function GetTLSBinding: TRLStringsBinding;
  end;

implementation

end.
