//=============================================================================
// U_EmailAccountView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_EmailAccountView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxCurrencyEdit,
  cxDropDownEdit,
  cxEdit,
  cxLabel,
  cxTextEdit,
  U_Bindings,
  U_MVCDynamicView,
  U_DXBindings,
  U_EmailAccountIntf,
  U_EmailAccountController;

type
  // TRLEmailAccountView
  TRLEmailAccountView = class(TRLMVCDynamicView, IRLEmailAccountView)
  private
    edtName: TcxTextEdit;
    lblName: TcxLabel;
    edtDescription: TcxTextEdit;
    lblDescription: TcxLabel;
    edtEmail: TcxTextEdit;
    lblEmail: TcxLabel;
    edtHost: TcxTextEdit;
    lblHost: TcxLabel;
    edtPort: TcxCurrencyEdit;
    lblPort: TcxLabel;
    edtUserName: TcxTextEdit;
    lblUserName: TcxLabel;
    edtPassword: TcxTextEdit;
    lblPassword: TcxLabel;
    cbTLS: TcxComboBox;
    lblTLS: TcxLabel;
    btnTest: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    function GetController: TRLEmailAccountController;
    procedure SetController(const AValue: TRLEmailAccountController);
    function GetNameBinding: TRLAttributeBinding;
    function GetDescriptionBinding: TRLAttributeBinding;
    function GetEmailBinding: TRLAttributeBinding;
    function GetHostBinding: TRLAttributeBinding;
    function GetPortBinding: TRLAttributeBinding;
    function GetUserNameBinding: TRLAttributeBinding;
    function GetPasswordBinding: TRLAttributeBinding;
    function GetTLSBinding: TRLStringsBinding;
    procedure edtPortPropertiesValidate(Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure btnTestClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLEmailAccountController read GetController write SetController;
  end;

implementation

{ TRLEmailAccountView }

function TRLEmailAccountView.GetController: TRLEmailAccountController;
begin
  Result := TRLEmailAccountController(inherited Controller);
end;

procedure TRLEmailAccountView.SetController(
  const AValue: TRLEmailAccountController);
begin
  inherited Controller := AValue;
end;

function TRLEmailAccountView.GetNameBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtName;
end;

function TRLEmailAccountView.GetDescriptionBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtDescription;
end;

function TRLEmailAccountView.GetEmailBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtEmail;
end;

function TRLEmailAccountView.GetHostBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtHost;
end;

function TRLEmailAccountView.GetPortBinding: TRLAttributeBinding;
begin
  Result := TRLcxCurrencyEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtPort;
end;

function TRLEmailAccountView.GetUserNameBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtUserName;
end;

function TRLEmailAccountView.GetPasswordBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtPassword;
end;

function TRLEmailAccountView.GetTLSBinding: TRLStringsBinding;
begin
  Result := TRLcxComboBoxBinding.CreateBinding(Controller.Bindings);
  Result.Control := cbTLS;
end;

procedure TRLEmailAccountView.edtPortPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  if Error or (DisplayValue < 0) then
    DisplayValue := '0';
  Error := False;
end;

procedure TRLEmailAccountView.btnTestClick(Sender: TObject);
begin
  try
    Controller.Test;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLEmailAccountView.CreateControls;
begin
  inherited;
  edtName := TcxTextEdit.Create(Self);
  edtName.Parent := Self;
  edtName.Top := 10;
  edtName.Left := 80;
  edtName.Width := 250;
  edtName.Style.BorderStyle := ebsFlat;
  lblName := TcxLabel.Create(Self);
  lblName.Parent := Self;
  lblName.Caption := '&Name:';
  lblName.FocusControl := edtName;
  lblName.Top := 15;
  lblName.Left := 10;
  lblName.Transparent := True;
  lblName.Style.TransparentBorder := False;
  edtDescription := TcxTextEdit.Create(Self);
  edtDescription.Parent := Self;
  edtDescription.Top := 40;
  edtDescription.Left := 80;
  edtDescription.Width := 250;
  edtDescription.Style.BorderStyle := ebsFlat;
  lblDescription := TcxLabel.Create(Self);
  lblDescription.Parent := Self;
  lblDescription.Caption := '&Description:';
  lblDescription.FocusControl := edtDescription;
  lblDescription.Top := 45;
  lblDescription.Left := 10;
  lblDescription.Transparent := True;
  lblDescription.Style.TransparentBorder := False;
  edtEmail := TcxTextEdit.Create(Self);
  edtEmail.Parent := Self;
  edtEmail.Top := 70;
  edtEmail.Left := 80;
  edtEmail.Width := 250;
  edtEmail.Style.BorderStyle := ebsFlat;
  lblEmail := TcxLabel.Create(Self);
  lblEmail.Parent := Self;
  lblEmail.Caption := '&Email:';
  lblEmail.FocusControl := edtEmail;
  lblEmail.Top := 75;
  lblEmail.Left := 10;
  lblEmail.Transparent := True;
  lblEmail.Style.TransparentBorder := False;
  edtHost := TcxTextEdit.Create(Self);
  edtHost.Parent := Self;
  edtHost.Top := 100;
  edtHost.Left := 80;
  edtHost.Width := 250;
  edtHost.Style.BorderStyle := ebsFlat;
  lblHost := TcxLabel.Create(Self);
  lblHost.Parent := Self;
  lblHost.Caption := '&Host:';
  lblHost.FocusControl := edtHost;
  lblHost.Top := 105;
  lblHost.Left := 10;
  lblHost.Transparent := True;
  lblHost.Style.TransparentBorder := False;
  edtPort := TcxCurrencyEdit.Create(Self);
  edtPort.Parent := Self;
  edtPort.Top := 130;
  edtPort.Left := 80;
  edtPort.Width := 40;
  edtPort.Style.BorderStyle := ebsFlat;
  edtPort.Properties.MaxLength := 5;
  edtPort.Properties.Nullable := False;
  edtPort.Properties.DisplayFormat := '0';
  edtPort.Properties.OnValidate := edtPortPropertiesValidate;
  lblPort := TcxLabel.Create(Self);
  lblPort.Parent := Self;
  lblPort.Caption := '&Port:';
  lblPort.FocusControl := edtPort;
  lblPort.Top := 135;
  lblPort.Left := 10;
  lblPort.Transparent := True;
  lblPort.Style.TransparentBorder := False;
  edtUserName := TcxTextEdit.Create(Self);
  edtUserName.Parent := Self;
  edtUserName.Top := 160;
  edtUserName.Left := 80;
  edtUserName.Width := 250;
  edtUserName.Style.BorderStyle := ebsFlat;
  lblUserName := TcxLabel.Create(Self);
  lblUserName.Parent := Self;
  lblUserName.Caption := '&User name:';
  lblUserName.FocusControl := edtUserName;
  lblUserName.Top := 165;
  lblUserName.Left := 10;
  lblUserName.Transparent := True;
  lblUserName.Style.TransparentBorder := False;
  edtPassword := TcxTextEdit.Create(Self);
  edtPassword.Parent := Self;
  edtPassword.Top := 190;
  edtPassword.Left := 80;
  edtPassword.Width := 250;
  edtPassword.Style.BorderStyle := ebsFlat;
  edtPassword.Properties.EchoMode := eemPassword;
  lblPassword := TcxLabel.Create(Self);
  lblPassword.Parent := Self;
  lblPassword.Caption := 'P&assword:';
  lblPassword.FocusControl := edtPassword;
  lblPassword.Top := 195;
  lblPassword.Left := 10;
  lblPassword.Transparent := True;
  lblPassword.Style.TransparentBorder := False;
  cbTLS := TcxComboBox.Create(Self);
  cbTLS.Parent := Self;
  cbTLS.Top := 220;
  cbTLS.Left := 80;
  cbTLS.Width := 100;
  cbTLS.Style.BorderStyle := ebsFlat;
  cbTLS.Properties.DropDownListStyle := lsFixedList;
  lblTLS := TcxLabel.Create(Self);
  lblTLS.Parent := Self;
  lblTLS.Caption := 'T&LS:';
  lblTLS.FocusControl := cbTLS;
  lblTLS.Top := 225;
  lblTLS.Left := 10;
  lblTLS.Transparent := True;
  lblTLS.Style.TransparentBorder := False;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test...';
  btnTest.Top := 255;
  btnTest.Left := 5;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.OnClick := btnTestClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 255;
  btnOk.Left := 220;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 255;
  btnCancel.Left := 290;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLEmailAccountView.AfterConstruction;
begin
  inherited;
  Name := 'RLEmailAccountView';
  ClientHeight := 275;
  ClientWidth := 350;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  ActiveControl := edtName;
  AllowEscape := True;
end;

initialization
  RegisterClass(TRLEmailAccountView);

end.
