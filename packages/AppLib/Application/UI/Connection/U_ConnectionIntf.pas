//=============================================================================
// U_ConnectionIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ConnectionIntf;

interface

uses
  U_Bindings;

type
  // IRLConnectionView
  IRLConnectionView = interface
    ['{92ABF4FD-4950-4202-AA6C-DC9AA78BA421}']
    function GetNameBinding: TRLAttributeBinding;
    function GetDescriptionBinding: TRLAttributeBinding;
    function GetProviderNameBinding: TRLStringsBinding;
  end;

implementation

end.
