//=============================================================================
// U_ConnectionController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ConnectionController;

interface

uses
  SysUtils,
  Classes,
  U_ProviderDefs,
  U_Connections,
  U_BindingException,
  U_Bindings,
  U_MVCController,
  U_ConnectionIntf,
  U_SettingsController;

const
  CName = 'Name';
  CDescription = 'Description';
  CProviderName = 'ProviderName';
  
type
  // TRLConnectionController
  TRLConnectionController = class(TRLMVCController)
  private
    FConnection: TRLConnection;
    FConnections: TRLConnections;
    FConnectionID: Integer;
    function GetConnectionView: IRLConnectionView;
  protected
    procedure Validate; override;
    procedure ValidateName; virtual;
    procedure ValidateProviderName; virtual;
    function CreateConnection: TRLConnection; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Initialize; override;
    function Configure: Boolean; virtual;
    property ConnectionView: IRLConnectionView read GetConnectionView;
    property Connection: TRLConnection read FConnection;
    property Connections: TRLConnections read FConnections write FConnections;
    property ConnectionID: Integer read FConnectionID write FConnectionID;
  end;

implementation

{ TRLConnectionController }

constructor TRLConnectionController.Create;
begin
  inherited;
  FConnection := CreateConnection;
  FConnectionID := -1;
end;

destructor TRLConnectionController.Destroy;
begin
  FreeAndNil(FConnection);
  inherited;
end;

function TRLConnectionController.GetConnectionView: IRLConnectionView;
begin
  View.GetInterface(IRLConnectionView, Result);
end;

procedure TRLConnectionController.Validate;
begin
  inherited;
  ValidateName;
  ValidateProviderName;
end;

procedure TRLConnectionController.ValidateName;
var
  lConnection: TRLConnection;
begin
  if FConnection.Name = '' then
    raise ERLBindingException.CreateBindingException('Name is empty.', CName);
  if Assigned(FConnections) then
  begin
    lConnection := FConnections.Find(FConnection.Name);
    if Assigned(lConnection) and (lConnection.ID <> FConnectionID) then
      raise ERLBindingException.CreateBindingException('Name already exists.',
        CName);
  end;
end;

procedure TRLConnectionController.ValidateProviderName;
begin
  if FConnection.ProviderName = '' then
    raise ERLBindingException.CreateBindingException('Select a provider.',
      CProviderName);
end;

function TRLConnectionController.CreateConnection: TRLConnection;
begin
  Result := TRLConnection.Create(nil);
end;

procedure TRLConnectionController.Initialize;
var
  I: Integer;
  lProviderName: TRLStringsBinding;
begin
  inherited;
  Bindings.DefaultInstance := FConnection;
  ConnectionView.GetNameBinding.Name := CName;
  ConnectionView.GetDescriptionBinding.Name := CDescription;
  lProviderName := ConnectionView.GetProviderNameBinding;
  lProviderName.Name := CProviderName;
  for I := 0 to Pred(TRLProviderManager.Instance.Providers.Count) do
    lProviderName.Items.Add(TRLProviderManager.Instance.Providers[I].Name,
      TRLProviderManager.Instance.Providers[I].Description);
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

function TRLConnectionController.Configure: Boolean;
var
  lController: TRLSettingsController;
begin
  Bindings.Write(CProviderName);
  ValidateProviderName;
  lController := TRLSettingsController.FindSettingsController(
    FConnection.ProviderName);
  try
    lController.Settings.Assign(FConnection.Settings);
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
      FConnection.Settings.Assign(lController.Settings);
  finally
    FreeAndNil(lController);
  end;
end;

initialization
  RegisterClass(TRLConnectionController);

end.
