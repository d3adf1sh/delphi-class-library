//=============================================================================
// U_ConnectionView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ConnectionView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxDropDownEdit,
  cxEdit,
  cxLabel,
  cxTextEdit,
  U_Bindings,
  U_MVCDynamicView,
  U_DXBindings,
  U_ConnectionIntf,
  U_ConnectionController;

type
  // TRLConnectionView
  TRLConnectionView = class(TRLMVCDynamicView, IRLConnectionView)
  private
    edtName: TcxTextEdit;
    lblName: TcxLabel;
    edtDescription: TcxTextEdit;
    lblDescription: TcxLabel;
    cbProviderName: TcxComboBox;
    lblProviderName: TcxLabel;
    btnConfigure: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    function GetController: TRLConnectionController;
    procedure SetController(const AValue: TRLConnectionController);
    function GetNameBinding: TRLAttributeBinding;
    function GetDescriptionBinding: TRLAttributeBinding;
    function GetProviderNameBinding: TRLStringsBinding;
    procedure btnConfigureClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLConnectionController read GetController write SetController;
  end;

implementation

{ TRLConnectionView }

function TRLConnectionView.GetController: TRLConnectionController;
begin
  Result := TRLConnectionController(inherited Controller);
end;

procedure TRLConnectionView.SetController(
  const AValue: TRLConnectionController);
begin
  inherited Controller := AValue;
end;

function TRLConnectionView.GetNameBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtName;
end;

function TRLConnectionView.GetDescriptionBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtDescription;
end;

function TRLConnectionView.GetProviderNameBinding: TRLStringsBinding;
begin
  Result := TRLcxComboBoxBinding.CreateBinding(Controller.Bindings);
  Result.Control := cbProviderName;
end;

procedure TRLConnectionView.btnConfigureClick(Sender: TObject);
begin
  try
    Controller.Configure;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLConnectionView.CreateControls;
begin
  inherited;
  edtName := TcxTextEdit.Create(Self);
  edtName.Parent := Self;
  edtName.Top := 10;
  edtName.Left := 80;
  edtName.Width := 250;
  edtName.Style.BorderStyle := ebsFlat;
  lblName := TcxLabel.Create(Self);
  lblName.Parent := Self;
  lblName.Caption := '&Name:';
  lblName.FocusControl := edtName;
  lblName.Top := 15;
  lblName.Left := 10;
  lblName.Transparent := True;
  lblName.Style.TransparentBorder := False;
  edtDescription := TcxTextEdit.Create(Self);
  edtDescription.Parent := Self;
  edtDescription.Top := 40;
  edtDescription.Left := 80;
  edtDescription.Width := 250;
  edtDescription.Style.BorderStyle := ebsFlat;
  lblDescription := TcxLabel.Create(Self);
  lblDescription.Parent := Self;
  lblDescription.Caption := '&Description:';
  lblDescription.FocusControl := edtDescription;
  lblDescription.Top := 45;
  lblDescription.Left := 10;
  lblDescription.Transparent := True;
  lblDescription.Style.TransparentBorder := False;
  cbProviderName := TcxComboBox.Create(Self);
  cbProviderName.Parent := Self;
  cbProviderName.Top := 70;
  cbProviderName.Left := 80;
  cbProviderName.Width := 125;
  cbProviderName.Style.BorderStyle := ebsFlat;
  cbProviderName.Properties.DropDownListStyle := lsFixedList;
  lblProviderName := TcxLabel.Create(Self);
  lblProviderName.Parent := Self;
  lblProviderName.Caption := '&Provider:';
  lblProviderName.FocusControl := cbProviderName;
  lblProviderName.Top := 75;
  lblProviderName.Left := 10;
  lblProviderName.Transparent := True;
  lblProviderName.Style.TransparentBorder := False;
  btnConfigure := TButton.Create(Self);
  btnConfigure.Parent := Self;
  btnConfigure.Caption := 'Con&figure...';
  btnConfigure.Top := 105;
  btnConfigure.Left := 5;
  btnConfigure.Height := 23;
  btnConfigure.Width := 75;
  btnConfigure.OnClick := btnConfigureClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 105;
  btnOk.Left := 220;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 105;
  btnCancel.Left := 290;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLConnectionView.AfterConstruction;
begin
  inherited;
  Name := 'RLConnectionView';
  ClientHeight := 125;
  ClientWidth := 350;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  ActiveControl := edtName;
  AllowEscape := True;
end;

initialization
  RegisterClass(TRLConnectionView);

end.
