//=============================================================================
// U_TestEmailAccountController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TestEmailAccountController;

interface

uses
  SysUtils,
  Classes,
  U_EmailAccounts,
  U_EmailSender,
  U_FormException,
  U_BindingException,
  U_MVCController,
  U_TestEmailAccountIntf;

const
  CEmail = 'Email';
  
type
  // TRLTestEmailAccountController
  TRLTestEmailAccountController = class(TRLMVCController)
  private
    FEmail: string;
    FEmailAccount: TRLEmailAccount;
    function GetTestEmailAccountView: IRLTestEmailAccountView;
  protected
    function PostData: Boolean; override;
    procedure CheckEmailAccount; virtual;
  public
    constructor Create; override;
    procedure Initialize; override;
    property TestEmailAccountView: IRLTestEmailAccountView read GetTestEmailAccountView;
    property EmailAccount: TRLEmailAccount read FEmailAccount write FEmailAccount;
  published
    property Email: string read FEmail write FEmail;
  end;

implementation

{ TRLTestEmailAccountController }

constructor TRLTestEmailAccountController.Create;
begin
  inherited;
  Title := 'Test email account';
end;

function TRLTestEmailAccountController.GetTestEmailAccountView: IRLTestEmailAccountView;
begin
  View.GetInterface(IRLTestEmailAccountView, Result);
end;

function TRLTestEmailAccountController.PostData: Boolean;
var
  lEmail: TRLEmailSender;
begin
  Result := inherited PostData;
  if FEmail = '' then
    raise ERLBindingException.CreateBindingException('Email is empty.', CEmail);
  try
    lEmail := TRLEmailSender.FindEmailSender('ID'); //Indy
    try
      lEmail.Account.Assign(FEmailAccount);
      lEmail.Addresses.Add(FEmail);
      lEmail.Subject := 'Testing email account';
      lEmail.Body.Add('Test.');
      lEmail.Send;
    finally
      FreeAndNil(lEmail);
    end;
  except
    on E: Exception do
      raise ERLFormException.CreateFormException('Error sending email.',
        E.Message, fekError);
  end;
end;

procedure TRLTestEmailAccountController.CheckEmailAccount;
begin
  if not Assigned(FEmailAccount) then
    raise ERLEmailAccountIsNull.Create('EmailAccount is null.');
end;

procedure TRLTestEmailAccountController.Initialize;
begin
  inherited;
  CheckEmailAccount;
  FEmail := FEmailAccount.Email;
  Bindings.DefaultInstance := Self;
  TestEmailAccountView.GetEmailBinding.Name := CEmail;
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

initialization
  RegisterClass(TRLTestEmailAccountController);

end.
