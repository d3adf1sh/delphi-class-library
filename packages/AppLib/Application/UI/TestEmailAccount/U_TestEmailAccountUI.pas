//=============================================================================
// U_TestEmailAccountUI
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TestEmailAccountUI;

interface

uses
  U_TestEmailAccountController,
  U_TestEmailAccountView;

implementation

end.
