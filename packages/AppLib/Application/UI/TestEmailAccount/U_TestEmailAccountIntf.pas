//=============================================================================
// U_TestEmailAccountIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TestEmailAccountIntf;

interface

uses
  U_Bindings;

type
  // IRLTestEmailAccountView
  IRLTestEmailAccountView = interface
    ['{781905F5-2C3D-4009-BA47-AADE82E07916}']
    function GetEmailBinding: TRLAttributeBinding;
  end;

implementation

end.
