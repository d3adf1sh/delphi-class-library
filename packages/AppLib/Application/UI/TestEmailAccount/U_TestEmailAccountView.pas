//=============================================================================
// U_TestEmailAccountView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_TestEmailAccountView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxEdit,
  cxLabel,
  cxTextEdit,
  U_Bindings,
  U_Dialog,
  U_MVCDynamicView,
  U_DXBindings,
  U_TestEmailAccountIntf,
  U_TestEmailAccountController;

type
  // TRLTestEmailAccountView
  TRLTestEmailAccountView = class(TRLMVCDynamicView, IRLTestEmailAccountView)
  private
    edtEmail: TcxTextEdit;
    lblEmail: TcxLabel;
    btnTest: TButton;
    btnCancel: TButton;
    function GetController: TRLTestEmailAccountController;
    procedure SetController(const AValue: TRLTestEmailAccountController);
    function GetEmailBinding: TRLAttributeBinding;
    procedure btnTestClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLTestEmailAccountController read GetController write SetController;
  end;

implementation

{ TRLTestEmailAccountView }

function TRLTestEmailAccountView.GetController: TRLTestEmailAccountController;
begin
  Result := TRLTestEmailAccountController(inherited Controller);
end;

procedure TRLTestEmailAccountView.SetController(
  const AValue: TRLTestEmailAccountController);
begin
  inherited Controller := AValue;
end;

function TRLTestEmailAccountView.GetEmailBinding: TRLAttributeBinding;
begin
  Result := TRLcxTextEditBinding.CreateBinding(Controller.Bindings);
  Result.Control := edtEmail;
end;

procedure TRLTestEmailAccountView.btnTestClick(Sender: TObject);
var
  bPosted: Boolean;
begin
  try
    btnTest.Enabled := False;
    Screen.Cursor := crHourGlass;
    try
      bPosted := Controller.Post;
    finally
      Screen.Cursor := crDefault;
      btnTest.Enabled := True;
    end;

    if bPosted then
    begin
      TRLDialog.Instance.Show('Email sent successfully.');
      ModalResult := mrOk;
    end;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLTestEmailAccountView.CreateControls;
begin
  inherited;
  edtEmail := TcxTextEdit.Create(Self);
  edtEmail.Parent := Self;
  edtEmail.Top := 10;
  edtEmail.Left := 50;
  edtEmail.Width := 290;
  edtEmail.Style.BorderStyle := ebsFlat;
  lblEmail := TcxLabel.Create(Self);
  lblEmail.Parent := Self;
  lblEmail.Caption := '&Email:';
  lblEmail.FocusControl := edtEmail;
  lblEmail.Top := 15;
  lblEmail.Left := 10;
  lblEmail.Transparent := True;
  lblEmail.Style.TransparentBorder := False;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test';
  btnTest.Top := 40;
  btnTest.Left := 220;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.OnClick := btnTestClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 40;
  btnCancel.Left := 290;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLTestEmailAccountView.AfterConstruction;
begin
  inherited;
  Name := 'RLTestEmailAccountView';
  ClientHeight := 60;
  ClientWidth := 350;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  ActiveControl := edtEmail;
  AllowEscape := True;
end;

initialization
  RegisterClass(TRLTestEmailAccountView);

end.
