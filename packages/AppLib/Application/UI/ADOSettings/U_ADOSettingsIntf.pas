//=============================================================================
// U_ADOSettingsIntf
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOSettingsIntf;

interface

uses
  U_Bindings;

type
  // IRLADOSettingsView
  IRLADOSettingsView = interface
    ['{311E340C-41DA-4488-8BAE-C368F6605AE2}']
    function GetConnectionStringBinding: TRLAttributeBinding;
  end;

implementation

end.
