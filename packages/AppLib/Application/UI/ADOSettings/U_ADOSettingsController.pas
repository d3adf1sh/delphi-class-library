//=============================================================================
// U_ADOSettingsController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOSettingsController;

interface

uses
  SysUtils,
  Classes,
  U_Settings,
  U_ADOSettings,
  U_ADOProvider,
  U_BindingException,
  U_SettingsController,
  U_ADOSettingsIntf;

const
  CConnectionString = 'ConnectionString';

type
  // TRLADOSettingsController
  TRLADOSettingsController = class(TRLSettingsController)
  private
    function GetADOSettingsView: IRLADOSettingsView;
    function GetSettings: TRLADOSettings;
  protected
    function CreateSettings: TRLSettings; override;
    function GetProviderName: string; override;
    procedure Validate; override;
  public
    constructor Create; override;
    procedure Initialize; override;
    function Edit: Boolean; virtual;
    property ADOSettingsView: IRLADOSettingsView read GetADOSettingsView;
    property Settings: TRLADOSettings read GetSettings;
  end;

implementation

uses
  U_ADOUtils;

{ TRLADOSettingsController }

constructor TRLADOSettingsController.Create;
begin
  inherited;
  Title := 'Configure connection';
end;

function TRLADOSettingsController.GetADOSettingsView: IRLADOSettingsView;
begin
  View.GetInterface(IRLADOSettingsView, Result);
end;

function TRLADOSettingsController.GetSettings: TRLADOSettings;
begin
  Result := TRLADOSettings(inherited Settings);
end;

function TRLADOSettingsController.CreateSettings: TRLSettings;
begin
  Result := TRLADOSettings.Create;
end;

function TRLADOSettingsController.GetProviderName: string;
begin
  Result := RLADO;
end;

procedure TRLADOSettingsController.Validate;
begin
  try
    inherited;
  except
    on E: ERLConnectionStringIsEmpty do
      raise ERLBindingException.CreateBindingException(
        'Connection string is empty.', CConnectionString);
    on E: Exception do
      raise;
  end;
end;

procedure TRLADOSettingsController.Initialize;
begin
  inherited;
  Bindings.DefaultInstance := Settings;
  ADOSettingsView.GetConnectionStringBinding.Name := CConnectionString;
  Bindings.InitializeAll;
  Bindings.ReadAll;
end;

function TRLADOSettingsController.Edit: Boolean;
var
  sConnectionString: string;
begin
  Bindings.Write(CConnectionString);
  sConnectionString := Settings.ConnectionString;
  Result := TRLADOUtils.EditConnectionString(ViewIntf.GetHandle,
    sConnectionString);
  if Result then
  begin
    Settings.ConnectionString := sConnectionString;
    Bindings.Read(CConnectionString);
  end;
end;

initialization
  RegisterClass(TRLADOSettingsController);

end.
