//=============================================================================
// U_ADOSettingsUI
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOSettingsUI;

interface

uses
  U_ADOSettingsController,
  U_ADOSettingsView;

implementation

end.
