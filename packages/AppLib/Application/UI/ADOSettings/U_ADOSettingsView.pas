//=============================================================================
// U_ADOSettingsView
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_ADOSettingsView;

interface

uses
  SysUtils,
  Classes,
  Controls,
  Forms,
  StdCtrls,
  cxEdit,
  cxLabel,
  cxMemo,
  U_Bindings,
  U_Dialog,
  U_MVCDynamicView,
  U_DXBindings,
  U_ADOSettingsIntf,
  U_ADOSettingsController;

type
  // TRLADOSettingsView
  TRLADOSettingsView = class(TRLMVCDynamicView, IRLADOSettingsView)
  private
    mConnectionString: TcxMemo;
    lblConnectionString: TcxLabel;
    btnEdit: TButton;
    btnTest: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    function GetController: TRLADOSettingsController;
    procedure SetController(const AValue: TRLADOSettingsController);
    function GetConnectionStringBinding: TRLAttributeBinding;
    procedure btnEditClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateControls; override;
  public
    procedure AfterConstruction; override;
    property Controller: TRLADOSettingsController read GetController write SetController;
  end;

implementation

{ TRLADOSettingsView }

function TRLADOSettingsView.GetController: TRLADOSettingsController;
begin
  Result := TRLADOSettingsController(inherited Controller);
end;

procedure TRLADOSettingsView.SetController(
  const AValue: TRLADOSettingsController);
begin
  inherited Controller := AValue;
end;

function TRLADOSettingsView.GetConnectionStringBinding: TRLAttributeBinding;
begin
  Result := TRLcxMemoBinding.CreateBinding(Controller.Bindings);
  Result.Control := mConnectionString;
end;

procedure TRLADOSettingsView.btnEditClick(Sender: TObject);
begin
  try
    Controller.Edit;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLADOSettingsView.btnTestClick(Sender: TObject);
begin
  try
    btnTest.Enabled := False;
    Screen.Cursor := crHourGlass;
    try
      Controller.Test;
    finally
      Screen.Cursor := crDefault;
      btnTest.Enabled := True;
    end;

    TRLDialog.Instance.Show('Successfully connected.');
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLADOSettingsView.btnOkClick(Sender: TObject);
begin
  try
    if Controller.Post then
      ModalResult := mrOk;
  except
    on E: Exception do
      ShowException(E);
  end;
end;

procedure TRLADOSettingsView.CreateControls;
begin
  inherited;
  mConnectionString := TcxMemo.Create(Self);
  mConnectionString.Parent := Self;
  mConnectionString.Top := 10;
  mConnectionString.Left := 115;
  mConnectionString.Height := 100;
  mConnectionString.Width := 280;
  mConnectionString.Style.BorderStyle := ebsFlat;
  mConnectionString.Properties.ScrollBars := ssVertical;
  lblConnectionString := TcxLabel.Create(Self);
  lblConnectionString.Parent := Self;
  lblConnectionString.Caption := 'Co&nnection string:';
  lblConnectionString.FocusControl := mConnectionString;
  lblConnectionString.Top := 15;
  lblConnectionString.Left := 10;
  lblConnectionString.Transparent := True;
  lblConnectionString.Style.TransparentBorder := False;
  btnEdit := TButton.Create(Self);
  btnEdit.Parent := Self;
  btnEdit.Caption := '&Edit...';
  btnEdit.Top := 120;
  btnEdit.Left := 5;
  btnEdit.Height := 23;
  btnEdit.Width := 65;
  btnEdit.OnClick := btnEditClick;
  btnTest := TButton.Create(Self);
  btnTest.Parent := Self;
  btnTest.Caption := '&Test';
  btnTest.Top := 120;
  btnTest.Left := 75;
  btnTest.Height := 23;
  btnTest.Width := 65;
  btnTest.OnClick := btnTestClick;
  btnOk := TButton.Create(Self);
  btnOk.Parent := Self;
  btnOk.Caption := '&Ok';
  btnOk.Top := 120;
  btnOk.Left := 270;
  btnOk.Height := 23;
  btnOk.Width := 65;
  btnOk.OnClick := btnOkClick;
  btnCancel := TButton.Create(Self);
  btnCancel.Parent := Self;
  btnCancel.Caption := '&Cancel';
  btnCancel.Top := 120;
  btnCancel.Left := 340;
  btnCancel.Height := 23;
  btnCancel.Width := 65;
  btnCancel.ModalResult := mrCancel;
end;

procedure TRLADOSettingsView.AfterConstruction;
begin
  inherited;
  Caption := 'RLADOSettingsView';
  ClientHeight := 140;
  ClientWidth := 400;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  ActiveControl := mConnectionString;
  AllowEscape := True;
end;

initialization
  RegisterClass(TRLADOSettingsView);

end.
