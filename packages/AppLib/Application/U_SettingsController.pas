//=============================================================================
// U_SettingsController
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_SettingsController;

interface

uses
  SysUtils,
  U_Settings,
  U_Connections,
  U_FormException,
  U_MVCController;

type
  // TRLSettingsController
  TRLSettingsController = class(TRLMVCController)
  private
    FSettings: TRLSettings;
  protected
    procedure Validate; override;
    function CreateSettings: TRLSettings; virtual; abstract;
    function GetProviderName: string; virtual; abstract;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Test; virtual;
    property Settings: TRLSettings read FSettings;
    property ProviderName: string read GetProviderName;
    class function FindSettingsController(const AName: string): TRLSettingsController;
  end;

implementation

{ TRLSettingsController }

constructor TRLSettingsController.Create;
begin
  inherited;
  FSettings := CreateSettings;
end;

destructor TRLSettingsController.Destroy;
begin
  FreeAndNil(FSettings);
  inherited;
end;

procedure TRLSettingsController.Validate;
begin
  inherited;
  FSettings.Validate;
end;

procedure TRLSettingsController.Test;
var
  lConnection: TRLConnection;
  sError: string;
begin
  Post;
  lConnection := TRLConnection.Create(nil);
  try
    lConnection.ProviderName := ProviderName;
    lConnection.Settings.Assign(FSettings);
    if not lConnection.Test(sError) then
      raise ERLFormException.CreateFormException('Connection error.', sError,
        fekError);
  finally
    FreeAndNil(lConnection);
  end;
end;

class function TRLSettingsController.FindSettingsController(
  const AName: string): TRLSettingsController;
begin
  Result := TRLSettingsController(inherited FindController(AName + 'Settings'));
end;

end.
