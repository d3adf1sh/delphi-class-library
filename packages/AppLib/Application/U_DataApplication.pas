//=============================================================================
// U_DataApplication
// Application class library
// Copyright(C) 2015 Rafael Luiz
//=============================================================================

unit U_DataApplication;

interface

uses
  SysUtils,
  U_Application,
  U_Connections,
  U_EmailAccounts,
  U_ConnectionsController,
  U_EmailAccountsController;

const
  CEditModulesParameter = 'EditModules';
  CEditConnectionsParameter = 'EditConnections';
  CEditEmailAccountsParameter = 'EditEmailAccounts';
  CSilentParameter = 'Silent';
  
type
  // TRLDataApplication
  TRLDataApplication = class(TRLApplication)
  private
    FConnectionsFileName: string;
    FEmailAccountsFileName: string;
    FAutoOpenConnections: Boolean;
  protected
    procedure ReadConnections; virtual;
    procedure SaveConnections; virtual;
    procedure ReadEmailAccounts; virtual;
    procedure SaveEmailAccounts; virtual;
  public
    constructor Create; override;
    function Initialize: Boolean; override;
    function EditModules: Boolean; virtual;
    function EditConnections: Boolean; virtual;
    function EditEmailAccounts: Boolean; virtual;
    property ConnectionsFileName: string read FConnectionsFileName write FConnectionsFileName;
    property EmailAccountsFileName: string read FEmailAccountsFileName write FEmailAccountsFileName;
    property AutoOpenConnections: Boolean read FAutoOpenConnections write FAutoOpenConnections;
    class function Instance: TRLDataApplication;
  end;

implementation

uses
  U_RTTIContext;

{ TRLDataApplication }

constructor TRLDataApplication.Create;
begin
  inherited;
  FConnectionsFileName := ChangeFileExt(FileName, '.Connections.xml');
  FEmailAccountsFileName := ChangeFileExt(FileName, '.EmailAccounts.xml');
end;

procedure TRLDataApplication.ReadConnections;
begin
  if FileExists(FConnectionsFileName) then
    TRLRTTIContext.Instance.FromFile(TRLConnections.Instance,
      FConnectionsFileName, sfXML);
end;

procedure TRLDataApplication.SaveConnections;
begin
  TRLRTTIContext.Instance.ToFile(TRLConnections.Instance,
    FConnectionsFileName, sfReadableXML);
end;

procedure TRLDataApplication.ReadEmailAccounts;
begin
  if FileExists(FEmailAccountsFileName) then
    TRLRTTIContext.Instance.FromFile(TRLEmailAccounts.Instance,
      FEmailAccountsFileName, sfXML);
end;

procedure TRLDataApplication.SaveEmailAccounts;
begin
  TRLRTTIContext.Instance.ToFile(TRLEmailAccounts.Instance,
    FEmailAccountsFileName, sfReadableXML);
end;

function TRLDataApplication.Initialize: Boolean;
begin
  Result := inherited Initialize;
  if Result then
  begin
    ReadConnections;
    ReadEmailAccounts;
    Result := (not ContainsParameter(CEditModulesParameter) or EditModules) and
      (not ContainsParameter(CEditConnectionsParameter) or EditConnections) and
      (not ContainsParameter(CEditEmailAccountsParameter) or EditEmailAccounts);
    if Result and FAutoOpenConnections then
      TRLConnections.Instance.OpenAll;
  end;
end;

function TRLDataApplication.EditModules: Boolean;
begin
  Result := True;
  { TODO -oRafael : Implementar. }
end;

function TRLDataApplication.EditConnections: Boolean;
var
  lController: TRLConnectionsController;
begin
  lController := TRLConnectionsController.Create;
  try
    lController.Connections.Assign(TRLConnections.Instance);
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
    begin
      TRLConnections.Instance.Assign(lController.Connections);
      SaveConnections;
    end;
  finally
    FreeAndNil(lController);
  end;
end;

function TRLDataApplication.EditEmailAccounts: Boolean;
var
  lController: TRLEmailAccountsController;
begin
  lController := TRLEmailAccountsController.Create;
  try
    lController.EmailAccounts.Assign(TRLEmailAccounts.Instance);
    lController.Initialize;
    Result := lController.DisplayModal;
    if Result then
    begin
      TRLEmailAccounts.Instance.Assign(lController.EmailAccounts);
      SaveEmailAccounts;
    end;
  finally
    FreeAndNil(lController);
  end;
end;

class function TRLDataApplication.Instance: TRLDataApplication;
begin
  Result := TRLDataApplication(inherited Instance);
end;

end.
